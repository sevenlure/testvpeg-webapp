const { injectBabelPlugin } = require('react-app-rewired');
// const rewireProvidePlugin = require('react-app-rewire-provide-plugin');
const rewireLodashPlugin = require('react-app-rewire-lodash-plugin');
const rewireEslint = require('react-app-rewire-eslint');
const rewireCssModules = require('react-app-rewire-css-modules');


function overrideEslintOptions(options) {
  // do stuff with the eslint options...
  return options;
}

module.exports = function override(config, env) {
  // do stuff with the webpack config...
  config = injectBabelPlugin('transform-decorators-legacy',config)
  // config = injectBabelPlugin( ['decorators', { decoratorsBeforeExport: false }],config)
  config = rewireEslint(config, env, overrideEslintOptions);
  config = rewireLodashPlugin(config, env, {
    collections: true,
    paths: true,
    flattening: true,
    shorthands: true
  });
  config = rewireCssModules(config, env);
  return config;
};
