import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Layout from './layout';
import { BrowserRouter as Router } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './@redux';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      splashFinished: true//false
    };
  }

  componentDidMount() {
    // show the splash for 2 seconds
    setTimeout(() => {
      this.setState({
        splashFinished: true
      });
    }, 100);
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          {bootstrapped => {
            if (bootstrapped && this.state.splashFinished) {
              return (
                <Router>
                  <Layout />
                </Router>
              );
            } else {
              return <SplashLoader />;
            }
          }}
        </PersistGate>
      </Provider>
    );
  }
}

class SplashLoader extends Component {
  render() {
    return <div style={{ height: '100vh' }}>Loadding</div>;
  }
}

export default App;
