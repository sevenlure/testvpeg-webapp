const slug = {
  BASE: '/coso',
  LOGIN: '/login',
  DATA: '/data/:stationType',
  DATA_WITH_PARAMS: '/data/',
  QA: '/qa',
  CO_SO: {
    LIST: '/coso',
    CREATE: '/coso/create',
    EDIT: '/coso/:_id',
    EDIT_WITH_ID: '/coso/',
    VIEW: '/coso/view/:_id',
    VIEW_WITH_ID: '/coso/view/'
  },
  FACILITY_EIA: {
    LIST: '/facilityeia',
    CREATE: '/facilityeia/create',
    EDIT: '/facilityeia/:_id',
    EDIT_WITH_ID: '/facilityeia/',
    VIEW: '/facilityeia/view/:_id',
    VIEW_WITH_ID: '/facilityeia/view/'
  },
  INSPECTTION: {
    LIST: '/inspection',
    CREATE: '/inspection/create',
    EDIT: '/inspection/:_id',
    EDIT_WITH_ID: '/inspection/',
    VIEW: '/inspection/view/:_id',
    VIEW_WITH_ID: '/inspection/view/'
  },

  FACILITY_PERMIT: {
    LIST: '/facilitypermit',
    CREATE: '/facilitypermit/create',
    EDIT: '/facilitypermit/:_id',
    EDIT_WITH_ID: '/facilitypermit/',
    VIEW: '/facilitypermit/view/:_id',
    VIEW_WITH_ID: '/facilitypermit/view/'
  },
  FACILITY_EMISSTION: {
    LIST: '/facilityemission',
    CREATE: '/facilityemission/create',
    EDIT: '/facilityemission/:_id',
    EDIT_WITH_ID: '/facilityemission/',
    VIEW: '/facilityemission/view/:_id',
    VIEW_WITH_ID: '/facilityemission/view/'
  },
  MAP: {
    BASE: '/map'
  },
  MONITORING_DATA: {
    BASE: '/monitoring',
    MAP: '/monitoring/map'
  },
  ENVI_PROTECT_FEE: {
    LIST: '/enviProtectFee',
    CREATE: '/enviProtectFee/create',
    EDIT: '/enviProtectFee/:_id',
    EDIT_WITH_ID: '/enviProtectFee/',
    VIEW: '/enviProtectFee/view/:_id',
    VIEW_WITH_ID: '/enviProtectFee/view/'
  },
  ENVI_MONITORING_REPORT: {
    LIST: '/enviMonitoringReport',
    CREATE: '/enviMonitoringReport/create',
    EDIT: '/enviMonitoringReport/:_id',
    EDIT_WITH_ID: '/enviMonitoringReport/',
    VIEW: '/enviMonitoringReport/view/:_id',
    VIEW_WITH_ID: '/enviMonitoringReport/view/'
  },
  HAZARDOUS_WASTE_REPORT: {
    LIST: '/hazardousWasteReport',
    CREATE: '/hazardousWasteReport/create',
    EDIT: '/hazardousWasteReport/:_id',
    EDIT_WITH_ID: '/hazardousWasteReport/',
    VIEW: '/hazardousWasteReport/view/:_id',
    VIEW_WITH_ID: '/hazardousWasteReport/view/'
  },
  MANAGEMENT: {
    LIST: '/management'
  },
  ADMIN: {
    BASE: '/admin',
    LIST: '/admin'
  },
  REPORT: {
    BASE: '/report'
  },
  USER: {
    LIST: '/user',
    CREATE: '/user/create',
    EDIT: '/user/:_id',
    EDIT_WITH_ID: '/user/',
    VIEW: '/user/view/:_id',
    VIEW_WITH_ID: '/user/view/'
  }
};

export default slug;

export const menus = [
  {
    href: slug.CO_SO.LIST,
    title: 'Tên cơ sở',
    permissionRole: 'facility.view'
  },
  {
    href: slug.INSPECTTION.LIST,
    title: 'Thanh tra',
    permissionRole: 'facilityInspection.view'
  },
  {
    href: slug.FACILITY_PERMIT.LIST,
    title: 'Giấy phép',
    permissionRole: 'facilityPermit.view'
  },
  {
    href: slug.FACILITY_EMISSTION.LIST,
    title: 'Quản lý Ô nhiễm',
    permissionRole: 'facilityEmission.view'
  },
  {
    href: slug.MAP.BASE, //slug.CO_SO.LIST,
    title: 'Bản đồ',
    permissionRole: 'map.view'
  },
  {
    href: slug.MONITORING_DATA.BASE, //slug.CO_SO.LIST,
    title: 'Số liệu quan trắc tự động',
    permissionRole: 'monitoringData.view'
  },
  {
    href: slug.ENVI_PROTECT_FEE.LIST,
    title: 'Phí bảo vệ môi trường',
    permissionRole: 'facilityEnviProtectFee.view'
  },
  {
    href: slug.ENVI_MONITORING_REPORT.LIST,
    title: 'BC Quan trắc môi trường',
    permissionRole: 'facilityEnviMonitoringReport.view'
  },
  {
    href: slug.HAZARDOUS_WASTE_REPORT.LIST,
    title: 'BC Chất thải nguy hại',
    permissionRole: 'facilityHazarWasteReport.view'
  },
  {
    href: slug.REPORT.BASE,
    title: 'Báo cáo',
    permissionRole: 'report.view'
  },
  {
    href: slug.MANAGEMENT.LIST,
    title: 'Quản lý',
    permissionRole: 'manage.full'
  },
  {
    href: slug.ADMIN.LIST,
    title: 'Quản trị',
    permissionRole: 'admin.full'
  }
];
