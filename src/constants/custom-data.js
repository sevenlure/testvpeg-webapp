export const QuaterOfChargeData = [
  { value: 'Quý I', label: 'Quý  I' },
  { value: 'Quý II', label: 'Quý  II' },
  { value: 'Quý III', label: 'Quý  III' },
  { value: 'Quý IV', label: 'Quý  IV' }
];

export const SubjectOfChargeData = [
  { value: 'Cố định', label: 'Cố định' },
  { value: 'Biến đổi', label: 'Biến đổi' }
];

export const WastewaterContentData = ['COD', 'TSS', 'Hg', 'Pb', 'As', 'Cd'];

export const FrequencyOfMonitoringData = [
  { value: '3 tháng', label: '3 tháng' },
  { value: '6 tháng', label: '6 tháng' },
  { value: '1 năm', label: '1 năm' }
];

export const getDataForCustomSelect = value => {
  if (!value) return null;
  return { value, label: value };
};

export const TypeMonitoringStationData = [
  { value: 'NuocThai', label: 'Quan trắc nước thải tự động, liên tục' },
  { value: 'NuocMat', label: 'Quan trắc nước mặt tự động, liên tục' },
  { value: 'NuocNgam', label: 'Quan trắc nước dưới đất tự động, liên tục' },
  { value: 'KhiThai', label: 'Quan trắc khí thải tự động, liên tục' }
];

export const AuthorityBVMTData = [
  { value: 'Cấp Tỉnh', label: 'Cấp Tỉnh' },
  { value: 'Cấp Huyện', label: 'Cấp Huyện' }
];

export const PaymentStatusData = [
  { value: 'Đã nộp', label: 'Đã nộp' },
  { value: 'Chưa nộp', label: 'Chưa nộp' }
];

export const SecurityLevelData = [{ value: 'Bạn đọc', label: 'Bạn đọc' }];

export const VungCapNuocData = [
  {
    name: 'Sài Gòn',
    children: [
      {
        name: 'Thị Tính',
        children: [
          {
            name: 'Bến Ván',
            children: [
              {
                name: 'Cầu Định'
              }
            ]
          },
          {
            name: 'Đồng Sổ',
            children: []
          },
          {
            name: 'Bà Lăng',
            children: []
          },
          {
            name: 'Bến Củi',
            children: [{ name: 'Ông Khương' }]
          },
          {
            name: 'suối Hố Muồng',
            children: []
          },
          {
            name: 'suối Thắng Nù',
            children: []
          },
          {
            name: 'suối Đá Yêu',
            children: [
              {
                name: 'suối Bà Tứ',
                children: []
              },
              {
                name: 'suối Ông Chài',
                children: []
              }
            ]
          },
          {
            name: 'suối Căm Xe',
            children: [
              {
                name: 'suối Boi',
                children: []
              },
              {
                name: 'suối Trà Bí',
                children: []
              },
              {
                name: 'suối Bà Và',
                children: []
              }
            ]
          }
        ]
      },
      {
        name: 'suối Xuy Nô'
      },
      {
        name: 'suối Dứa'
      },
      {
        name: 'suối Đá'
      },
      {
        name: 'Rạch Trầu',
        children: [
          {
            name: 'suối Giữa',
            children: [
              {
                name: 'suối Ông Thiền'
              }
            ]
          }
        ]
      },
      {
        name: 'rạch Sơn'
      },
      {
        name: 'suối Giữa'
      },
      {
        name: 'suối Cát'
      },
      {
        name: 'rạch Búng',
        children: [
          {
            name: 'rạch Chòm Sao',
            children: []
          }
        ]
      },
      {
        name: 'suối Đờn',
        children: [
          {
            name: 'rạch Chòm Sao',
            children: []
          }
        ]
      },
      {
        name: 'sông Cầu Đập'
      },
      {
        name: 'rạch Bà Lụa',
        children: [
          {
            name: 'suối Cát',
            children: []
          }
        ]
      },
      {
        name: 'rạch Miễu',
        children: [
          {
            name: 'rạch Ông Bố',
            children: []
          }
        ]
      },
      {
        name: 'rạch Vĩnh Bình',
        children: [
          {
            name: 'rạch Cầu Đất',
            children: [{ name: 'kênh Ba Bò' }]
          },
          {
            name: 'rạch Thầy Năm',
            children: [
              {
                name: 'kênh Bình Hòa'
              },
              {
                name: 'kênh D'
              }
            ]
          }
        ]
      },
      {
        name: 'rạch Ông Đành'
      }
    ]
  },
  {
    name: 'Đồng Nai',
    children: [
      {
        name: 'Bình Thắng',
        children: []
      },
      {
        name: 'suối Nhum',
        children: []
      },
      {
        name: 'suối Xuân Trường',
        children: []
      },
      {
        name: 'suối Lồ Ô',
        children: []
      },
      {
        name: 'rạch Cái Cầu',
        children: []
      },
      {
        name: 'suối Gầu',
        children: []
      },
      {
        name: 'suối ông Đông',
        children: []
      },
      {
        name: 'suối Bưng Cù',
        children: []
      },
      {
        name: 'suối Hòn đá',
        children: []
      },
      {
        name: 'suối Cái',
        children: [
          {
            name: 'suối con',
            children: []
          },
          {
            name: 'suối ch… ',
            children: []
          },
          {
            name: 'suối Ông Đông',
            children: []
          },
          {
            name: 'Bà Phái',
            children: [
              {
                name: 'suối Bà Pho',
                children: []
              }
            ]
          },
          {
            name: 'Dung Giá',
            children: []
          },
          {
            name: 'suối Tre',
            children: []
          },
          {
            name: 'suối Nhum',
            children: []
          }
        ]
      },
      {
        name: 'suối Cầu',
        children: []
      },
      {
        name: 'suối Rạch Gốc',
        children: []
      },
      {
        name: 'suối Vùng Gấm',
        children: [
          {
            name: 'suối Sâu',
            children: []
          }
        ]
      },
      {
        name: 'rạch con Nai',
        children: []
      },
      {
        name: 'suối Sịp',
        children: []
      },
      {
        name: 'suối Tân Lợi',
        children: []
      }
    ]
  },
  {
    name: 'Sông Bé',
    children: [
      {
        name: 'suối Lòng Cù',
        children: [
          {
            name: 'suối Bà Đủ',
            children: []
          },
          {
            name: 'suối Cầu',
            children: []
          }
        ]
      },
      {
        name: 'suối Sâu',
        children: []
      },
      {
        name: 'suối Giai',
        children: [
          {
            name: 'suối Nước Vàng',
            children: []
          },
          {
            name: 'suối Đá',
            children: []
          },
          {
            name: 'suối Xà Mách',
            children: []
          },
          {
            name: 'suối Bố Mua',
            children: []
          }
        ]
      },
      {
        name: 'suối Mà Đà',
        children: [
          {
            name: 'suối Gieng Chôn',
            children: []
          },
          {
            name: 'suối Sa Lác',
            children: [
              {
                name: 'suối Ra',
                children: []
              }
            ]
          }
        ]
      },
      {
        name: 'suối Ngàn',
        children: []
      },
      {
        name: 'suối Cái',
        children: []
      },
      {
        name: 'suối Nước Trong',
        children: []
      },
      {
        name: 'suối Lồ Ô',
        children: []
      },
      {
        name: 'suối Tiêu',
        children: []
      },
      {
        name: 'suối Ông Dầu',
        children: []
      },
      {
        name: 'suối Thôn',
        children: [
          {
            name: 'suối Nhỏ',
            children: []
          }
        ]
      },
      {
        name: 'suối Nhỏ',
        children: []
      },
      {
        name: 'suối Lũng',
        children: []
      },
      {
        name: 'suối Sác Lác',
        children: []
      },
      {
        name: 'suối N.Tai',
        children: []
      },
      {
        name: 'suối Sam BRing',
        children: []
      },
      {
        name: 'suối Ba Yem',
        children: []
      },
      {
        name: 'suối Sam',
        children: []
      },
      {
        name: 'suối Xà Mách',
        children: [
          {
            name: 'suối Nước Trong',
            children: []
          }
        ]
      },
      {
        name: 'suối Nước Trong',
        children: [
          {
            name: 'suối Can',
            children: [
              {
                name: 'suối Ba',
                children: []
              }
            ]
          },
          {
            name: 'suối Đen Đen',
            children: [
              {
                name: 'suối Nen',
                children: []
              }
            ]
          },
          {
            name: 'suối Tà Nhum',
            children: [
              {
                name: 'suối Thiên',
                children: []
              }
            ]
          }
        ]
      }
    ]
  }
];

export const QuyMoData = [
  { value: 'Cấp Tỉnh', label: 'Cấp Tỉnh' },
  { value: 'Cấp Huyện', label: 'Cấp Huyện' }
];

export const LUU_LUONG = {
  TREN_20M3: '>=20m³/ngày',
  DUOI_20M3: '<20m³/ngày'
};
export const LuuLuongNuocThaiData = [
  { value: LUU_LUONG.TREN_20M3, label: LUU_LUONG.TREN_20M3 },
  { value: LUU_LUONG.DUOI_20M3, label: LUU_LUONG.DUOI_20M3 }
];

export const ThongBaoLanData = [
  { value: 'Lần 1', label: 'Lần 1' },
  { value: 'Lần 2', label: 'Lần 2' },
  { value: 'Lần 3', label: 'Lần 3' },
  { value: 'Lần 4', label: 'Lần 4' },
  { value: 'Lần 5', label: 'Lần 5' },
  { value: 'Lần 6', label: 'Lần 6' },
  { value: 'Lần 7', label: 'Lần 7' },
  { value: 'Lần 8', label: 'Lần 8' },
  { value: 'Lần 9', label: 'Lần 9' },
  { value: 'Lần 10', label: 'Lần 10' }
];

export const LoaiThongBaoData = [
  { value: 'Nộp phí', label: 'Nộp phí' },
  { value: 'Kê khai lại', label: 'Kê khai lại' }
];

export const DOI_TUONG = {
  DANH_GIA_TAC_DONG_MOI_TRUONG: 'Đánh giá tác động môi trường',
  KE_HOACH_BAO_VE_MOI_TRUONG: 'Kế hoạch bảo vệ môi trường'
};
export const DoiTuongData = [
  {
    value: DOI_TUONG.DANH_GIA_TAC_DONG_MOI_TRUONG,
    label: DOI_TUONG.DANH_GIA_TAC_DONG_MOI_TRUONG
  },
  {
    value: DOI_TUONG.KE_HOACH_BAO_VE_MOI_TRUONG,
    label: DOI_TUONG.KE_HOACH_BAO_VE_MOI_TRUONG
  }
];

export const BunThaiData = [
  { value: '1 sao', label: '1 sao' },
  { value: '2 sao', label: '2 sao' }
];

export const KetQuaQuanTracData = [
  { value: 'Đạt', label: 'Đạt' },
  { value: 'Vuợt', label: 'Vuợt' }
];

export const HinhThucNopData = [
  { value: 'Nộp hàng năm', label: 'Nộp hàng năm' },
  { value: 'Nộp báo cáo lần đầu', label: 'Nộp báo cáo lần đầu' }
];
