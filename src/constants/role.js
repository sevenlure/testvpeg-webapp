export const permissionRole = {
  facility: {
    view: true,
    write: true,
    delete: true
  },
  facilityeia: {
    view: true,
    write: true,
    delete: true
  },
  facilityInspection: {
    view: true,
    write: true,
    delete: true
  },
  facilityPermit: {
    view: true,
    write: true,
    delete: true
  },
  facilityEmission: {
    view: true,
    write: true,
    delete: true
  },
  map: {
    view: true
  },
  monitoringData: {
    view: true
  },
  facilityEnviProtectFee: {
    view: true,
    write: true,
    delete: true
  },
  facilityEnviMonitoringReport: {
    view: true,
    write: true,
    delete: true
  },
  facilityHazarWasteReport: {
    view: true,
    write: true,
    delete: true
  },
  report: {
    view: true
  },
  manage: {
    full: true
  },
  admin: {
    full: true
  }
};

export const moduleName = {
  facility: "Cơ sở",
  facilityeia: "Hồ sơ môi truờng",
  facilityInspection: "Thanh tra",
  facilityPermit: "Giấy phép",
  facilityEmission: "Quản lý ô nhiễm",
  map: "Bản đồ",
  monitoringData: "Số liệu quan trắc tự động",
  facilityEnviProtectFee: "Phí bảo vệ môi truờng",
  facilityEnviMonitoringReport: "Báo cáo quan trắc môi truờng",
  facilityHazarWasteReport: "Báo cáo chất thải nguy hại",
  report: "Báo cáo thống kê",
  manage: "Quản lý",
  admin: "Quản trị"
}

export const permissionRoleName = {
  'facility.view': 'Xem',
  'facility.write': 'Thêm mới/Sửa',
  'facility.delete': 'Xoá',

  'facilityeia.view': 'Xem',
  'facilityeia.write': 'Thêm mới/Sửa',
  'facilityeia.delete': 'Xoá',

  'facilityInspection.view': 'Xem',
  'facilityInspection.write': 'Thêm mới/Sửa',
  'facilityInspection.delete': 'Xoá',

  'facilityPermit.view': 'Xem',
  'facilityPermit.write': 'Thêm mới/Sửa',
  'facilityPermit.delete': 'Xoá',

  'facilityEmission.view': 'Xem',
  'facilityEmission.write': 'Thêm mới/Sửa',
  'facilityEmission.delete': 'Xoá',

  'map.view': 'Xem',

  'monitoringData.view': 'Xem',

  'facilityEnviProtectFee.view': 'Xem',
  'facilityEnviProtectFee.write': 'Thêm mới/Sửa',
  'facilityEnviProtectFee.delete': 'Xoá',

  'facilityEnviMonitoringReport.view': 'Xem',
  'facilityEnviMonitoringReport.write': 'Thêm mới/Sửa',
  'facilityEnviMonitoringReport.delete': 'Xoá',

  'facilityHazarWasteReport.view': 'Xem',
  'facilityHazarWasteReport.write': 'Thêm mới/Sửa',
  'facilityHazarWasteReport.delete': 'Xoá',

  'report.view': 'Xem',
  
  'manage.full': 'Xem/Thêm mới/Sửa/Xoá',
  
  'admin.full': 'Xem/Thêm mới/Sửa/Xoá'
}


export const roleArr = [{
  Name: 'Bạn đọc',
  Key: 'GUEST'
},{
  Name: 'Quận nguời dùng',
  Key: 'DISTRICT_USER',
},{
  Name: 'Tỉnh EPA/EMC nguời dùng',
  Key: 'PROVINCE_USER'
},{
  Name: 'Quản trị hệ thống',
  Key: 'ADMIN'
}]