export const errorType = {
  SequelizeConnectionError: 'Không kết nối được đến Cơ sở dữ liệu quan trắc',
  SequelizeConnectionRefusedError:
    'Không kết nối được đến Cơ sở dữ liệu quan trắc'
};
