import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
// import configureStore from './@redux';
// import {store, persistor} from './@redux';
// import browserHistory from 'history/createBrowserHistory';
// import { AppContainer } from 'react-hot-loader';
import '@blueprintjs/icons/lib/css/blueprint-icons.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import '@blueprintjs/table/lib/css/table.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
// import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'rc-message/assets/index.css';
import 'rc-input-number/assets/index.css';
import 'rc-pagination/assets/index.css';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './App.css';
import { COLOR } from 'config';

global.toArrayBuffer = buf => {
  var ab = new ArrayBuffer(buf.length);
  var view = new Uint8Array(ab);
  for (var i = 0; i < buf.length; ++i) {
    view[i] = buf[i];
  }
  return ab;
};

global.getFile = (blob, fileName) => {
  let url = window.URL.createObjectURL(blob);
  var a = document.createElement('a');
  // document.body.appendChild(a);
  // a.style = 'display: none';
  a.href = url;
  a.download = fileName;
  a.innerText = `Click Here If Your Download Doesn't Start Automatically`;
  a.style.color = COLOR.SUCCESS;
  var downloadElement = document.getElementById('downloadlink');
  downloadElement.innerHTML = `<div>Done.
  <br />
  ${a.outerHTML}
  </div>`;
  a.click();

  // window.URL.revokeObjectURL(url);
  // a.remove();
};

global.exportToExcel = (htmls, fileName) => {
  // var htmls = "";
  var uri = 'data:application/vnd.ms-excel;base64,';
  var template =
    '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
  var base64 = function(s) {
    return window.btoa(unescape(encodeURIComponent(s)));
  };

  var format = function(s, c) {
    return s.replace(/{(\w+)}/g, function(m, p) {
      return c[p];
    });
  };

  // htmls = "YOUR HTML AS TABLE"

  var ctx = {
    worksheet: 'Worksheet',
    table: htmls
  };

  var link = document.createElement('a');
  let name = fileName || 'data';
  link.download = name + '.xls';
  link.href = uri + base64(format(template, ctx));
  link.click();
};

// const getStoreDefaultacorn = () => {
//   if (typeof window !== 'undefined')
//     return window.__REDUX_STORE__ ? window.__REDUX_STORE__ : {};
//   return {};
// };

// const {store, persistor} = configureStore(
//   // {},
//   // {
//   //   routerHistory: browserHistory()
//   // }
// );

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
