import moment from 'moment';
import { DATE_FORMAT } from 'config';

export const convertDateFromSever = value => {
  if (value) return moment(value).toDate();
  return null;
};

export const convertDateFromSeverToString = value => {
  if (value) return moment(value).format(DATE_FORMAT);
  return '';
};

export const getQueryString = url => {
  let res = {};
  let searchParams = new URLSearchParams(url);
  for (var pair of searchParams.entries()) {
    res[pair[0]] = pair[1];
  }
  return res;
};
