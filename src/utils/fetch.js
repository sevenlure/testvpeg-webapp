import axios from 'axios';
import { store } from '@redux';
import { UN_LOGGED } from '@redux/actions/authAction';
import Message from 'rc-message';

// const TOKEN =
//   'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Yjc1MDQ2MzEzYjhmMzM3ZGQ0NzQ5MDQiLCJpYXQiOjE1MzcxNzQ5OTF9.JF9Pd2bBej2FtM0njKe_M6rG1Iu9tC-1hOVrhC8g3Ns';

const getHeaders = () => {
  let reduxState = store.getState();
  let token = _.get(reduxState, 'auth.token');
  var headers = {
    Accept: 'application/json',
    Authorization: token
  };
  return headers;
};

const handleResError = dataRes => {
  if (dataRes.error && dataRes.typeError === 'AUTH') {
    Message.error({ content: dataRes.message });
    store.dispatch({
      type: UN_LOGGED
    });
  }
};

const handleResErrorDebounce = _.debounce(
  dataRes => handleResError(dataRes),
  300
);

export function postFetch(url, data, props) {
  let attributes = Object.assign(
    {
      cache: true,
      headers: getHeaders()
    },
    props
  );

  return new Promise((resolve, reject) => {
    axios
      .post(url, data, attributes)
      .then(res => {
        // if (res.status === 200) {
        handleResErrorDebounce(res.data);
        resolve(res.data);
        // } else {
        //   reject({ error: true })
        // }
      })
      .catch(e => reject(e));
  });
}

export function putFetch(url, data, props) {
  let attributes = Object.assign(
    {
      cache: true,
      headers: getHeaders()
    },
    props
  );

  return new Promise((resolve, reject) => {
    axios
      .put(url, data, attributes)
      .then(res => {
        if (res.status === 200) {
          handleResErrorDebounce(res.data);
          resolve(res.data);
        } else {
          reject({ error: true });
        }
      })
      .catch(e => reject(e));
  });
}

export function getFetch(url, params, props) {
  let attributes = Object.assign(
    {
      headers: getHeaders()
    },
    { params }
  );
  return new Promise((resolve, reject) => {
    axios
      .get(url, attributes)
      .then(res => {
        // if (res.status === 200) {
        handleResErrorDebounce(res.data);

        resolve(res.data);
        // } else {
        //   reject({ error: true })
        // }
      })
      .catch(e => {
        reject(e);
      });
  });
}

export function deleteFetch(url, props) {
  let attributes = Object.assign(
    {
      headers: getHeaders(),
      data:props
    },
  );
  return new Promise((resolve, reject) => {
    axios
      .delete(
        url,
        attributes
      )
      .then(res => {
        if (res.status === 200) {
          handleResErrorDebounce(res.data);
          resolve(res.data);
        } else {
          reject({ error: true });
        }
      })
      .catch(e => reject(e));
  });
}
