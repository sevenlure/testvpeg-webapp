import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import { InputGroup, TextArea } from '@blueprintjs/core';
import Clearfix from 'components/elements/clearfix';
import { createForm } from 'rc-form';
import { Row, Col } from 'reactstrap';
// import * as _ from 'lodash';
import SelectTypeAttachment from 'components/elements/select-type-attachment-dynamic';
import { SELECT_ITEM_DEFAULT } from 'config';

import Upload from 'components/elements/upload-file';

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;

class AttachmentFormContainer extends React.Component {
  static propTypes = {
    getAttachmentTypeList: PropTypes.func.isRequired
  };
  destroy = () => {
    this.setState({
      destroyed: true
    });
  };
  setInitialValue(keyPath, defaultValue) {
    let value = _.result(this.props.initialValue, keyPath, defaultValue);
    if (!value) value = defaultValue;
    return value;
  }

  onSuccessUpload = res => {
    if (res) {
      this.props.form.setFieldsValue({
        file: res
      });
    }
  };

  setSelInitialValue(keyPath, defaultValue, nameLabel) {
    let selItem = _.result(this.props.initialValue, keyPath);
    if (!selItem) return defaultValue;
    return {
      value: selItem._id,
      label: nameLabel ? selItem[nameLabel] : selItem.Name
    };
  }

  render() {
    const { getFieldError, getFieldProps } = this.props.form;
    return (
      <div style={{ width: '100%' }}>
        <Row>
          <Col xs={3}>
            <LabelContainer>Loại hồ sơ</LabelContainer>
          </Col>
          <Col xs={5}>
            <InputGroup
              placeholder="Loại hồ sơ"
              {...getFieldProps('Title', {
                initialValue: this.setInitialValue('Title', ''),
                rules: [
                  {
                    required: true,
                    message: 'Trường này không được bỏ trống'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('Title') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={3}>
            <LabelContainer>Loại</LabelContainer>
          </Col>
          <Col xs={5}>
            <SelectTypeAttachment
              hideLabel
              getAttachmentTypeList={this.props.getAttachmentTypeList}
              fieldForm={{
                form: this.props.form,
                name: 'attachmentType',
                option: {
                  initialValue: this.setSelInitialValue(
                    'attachmentType',
                    SELECT_ITEM_DEFAULT
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={16} />
        <Row>
          <Col xs={3}>
            <LabelContainer>File</LabelContainer>
          </Col>
          <Col xs={5}>
            <Upload
              {...getFieldProps('file', {
                rules: [
                  {
                    required: true,
                    message: 'Cần đính kèm file'
                  }
                ]
              })}
              onSuccess={this.onSuccessUpload}
            />
            <ErrorContainer>
              {(getFieldError('file') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={3}>
            <LabelContainer>Chú thích</LabelContainer>
          </Col>
          <Col xs={5}>
            <TextArea
              fill
              placeholder="Chú thích"
              {...getFieldProps('Description', {
                initialValue: this.setInitialValue('Description', '')
              })}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default createForm()(AttachmentFormContainer);
