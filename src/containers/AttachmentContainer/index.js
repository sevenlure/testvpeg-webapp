import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import React from 'react';
import { setMenuSelected } from '@redux/actions/menuAction';
import styled from 'styled-components';
import { Icon, Intent, Colors } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import Clearfix from 'components/elements/clearfix';
import AttachmentListContainer from './AttachmentListContainer';
import AttachmentFormContainer from './AttachmentFormContainer';
// import * as _ from 'lodash';
import Message from 'rc-message';
import RuleDiv from 'components/ruleDiv';

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

class AttachmentContainer extends React.Component {
  static propTypes = {
    getListFunc: PropTypes.func.isRequired,
    deleteOne: PropTypes.func.isRequired,
    createOne: PropTypes.func.isRequired,
    getAttachmentTypeList: PropTypes.func.isRequired,
    moduleName: PropTypes.string,
    facility: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      isEdit: false
    };
  }

  showEditForm = () => {
    this.setState({
      isEdit: true
    });
  };

  hideEditForm = () => {
    this.setState({
      isEdit: false
    });
  };

  renderHeaderContainer() {
    return (
      <div>
        <h3
          style={{
            color: Colors.GREEN2
          }}
          className="bp3-heading"
        >
          File đính kèm
        </h3>
        <RuleDiv initialValue={this.props.facility}>
        <HeaderContainer>
          <div style={{ display: 'inline-flex' }}>
            {!this.state.isEdit && (
              <Icon
                style={{ cursor: 'pointer' }}
                icon={IconNames.ADD}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.SUCCESS}
                onClick={this.showEditForm}
              />
            )}
            {this.state.isEdit && (
              <div style={{ display: 'inline-flex' }}>
                <Icon
                  style={{ cursor: 'pointer' }}
                  icon={IconNames.SAVED}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.PRIMARY}
                  onClick={this.handelSubmit}
                />
                <Clearfix width={8} />
                <Icon
                  style={{ cursor: 'pointer' }}
                  icon={IconNames.DISABLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                  onClick={this.hideEditForm}
                />
              </div>
            )}
          </div>
        </HeaderContainer>
        </RuleDiv>
      </div>
    );
  }

  getFormValue = () => {
    let formValues;
    if (this.AttactmentFormComp && this.AttactmentFormComp.getForm()) {
      const form = this.AttactmentFormComp.getForm();
      form.validateFields((error, value) => {
        console.log(value);
        if (error) return;
        value.attachmentType = _.get(value.attachmentType, 'value');
        if (value.file) {
          const { file } = value.file;
          value.FileNameDisplay = file.originalname;
          value.FileNameServer = file.filename;
          value.NumBytes = file.size;
        }
        formValues = value;
      });
    }
    return formValues;
  };

  handelSubmit = async () => {
    const values = await this.getFormValue();
    if (!values) return;

    const res = await this.props.createOne(_.pickBy(values, _.identity));
    if (res.success) {
      Message.success({ content: 'Create Success!!!' });
      this.hideEditForm();
      if (this.AttactmentListComp && this.AttactmentListComp.fetData)
        this.AttactmentListComp.fetData();
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div>
        {this.renderHeaderContainer()}
        {this.state.isEdit && (
          <AttachmentFormContainer
            {...this.props}
            ref={AttactmentFormComp => {
              this.AttactmentFormComp = AttactmentFormComp;
            }}
          />
        )}
        <AttachmentListContainer
          {...this.props}
          onRef={AttactmentListComp => {
            this.AttactmentListComp = AttactmentListComp;
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AttachmentContainer)
);
