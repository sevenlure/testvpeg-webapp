
import React from 'react';
import PropTypes from 'prop-types';
import { BaseListContainer } from 'containers/BaseContainer/BaseListContainer';
import moment from 'moment';
import { DATE_FORMAT } from 'config';
import styled from 'styled-components';
import { Icon, Intent, Colors } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import Clearfix from 'components/elements/clearfix';
import { HOST_ATTACHMENT_PUBLIC } from 'config';
import Confirm from 'components/elements/confirm-element';
import Message from 'rc-message'
import {connect} from 'react-redux'
import RuleDiv from 'components/ruleDiv';

const SpinnerContainer = styled.div`
  // margin: 100px auto;
  //width: 50px;
  height: 40px;
  text-align: center;
  font-size: 10px;
`;

@connect(state => ({
  permissions: state.permissions,
  token: state.auth.token
}))
export default class AttachmentContainer extends BaseListContainer {
  static propTypes = {
    getListFunc: PropTypes.func.isRequired,
    deleteOne: PropTypes.func.isRequired,
    facility: PropTypes.object.isRequired,
  };

  constructor(props) {
    const funcApi = {
      getList: props.getListFunc
    };

    let columns = [
      {
        dataField: '_id',
        text: '',
        headerStyle: {
          width: 10,
          minWidth: 10,
        },
        formatter: (cell, row) => {
          return (
            <RuleDiv initialValue={this.props.facility}>
              <Icon
              style={{ cursor: 'pointer' }}
              icon={IconNames.DISABLE}
              iconSize={Icon.SIZE_LARGE}
              intent={Intent.DANGER}
              onClick={() => {
                this.handelDelete(cell);
              }}
            />
            </RuleDiv>
          );
        }
      },
      {
        dataField: 'attachment.Title',
        text: 'Loại hồ sơ'
      },
      {
        dataField: 'attachment.attachmentType.Name',
        text: 'Loại'
      },
      {
        dataField: 'attachment',
        text: 'File',
        formatter: (cell, row) => {
          return (
            <a
              style={{
                display: 'inline-flex',
                color: Colors.GREEN2,
                textDecoration: 'underline'
              }}
              href={HOST_ATTACHMENT_PUBLIC + cell.FileNameServer +`?token=${this.props.token}`}
            >
              {cell.FileNameDisplay} <Clearfix width={8} />{' '}
              {`(${cell.NumBytes} KB)`}
            </a>
          );
        }
      },
      {
        dataField: 'attachment.Description',
        text: 'Chú thích',
      },
      {
        dataField: 'attachment.AddedOn',
        text: 'Thêm vào',
        formatter: (cell, row) => {
          return moment(cell).format(DATE_FORMAT);
        }
      },
      {
        dataField: 'attachment.AddedBy',
        text: 'Thêm bởi'
      },
      {
        dataField: 'attachment.UpdatedOn',
        text: 'Cập nhật ngày',
        formatter: (cell, row) => {
          return moment(cell).format(DATE_FORMAT);
        }
      },
      {
        dataField: 'attachment.UpdatedBy',
        text: 'Cập nhật bởi'
      }
    ];
    if(!props.moduleName || !props.permissions.includes(props.moduleName+'.write')){
      columns.shift()
    }
    super(props, funcApi, columns);
    this.state = {
      ...this.state
    };
    this.myRef = React.createRef();
  }

  componentDidMount() {
    this.props.onRef(this);
 }

  handelDelete = _id => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      this.ConfirmElm.showConfirm({
        content: 'Bạn có muốn xoá ',
        cb: async value => {
          if (value) {
            const res = await this.props.deleteOne(_id);
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.fetData();
            }
            if (res.error) Message.error({ content: res.message });
          }
        }
      });
    }
  };

  renderHeaderContainer() {
    return (
      <div>
        <Confirm ref={ConfirmElm => (this.ConfirmElm = ConfirmElm)} />
      </div>
    );
  }

  renderSearchContainer() {
    return <div />;
  }

  renderSpinnerNodata() {
    return (
      <SpinnerContainer className="spinner">
        <h2>No Attachment Data</h2>
      </SpinnerContainer>
    );
  }
}

// class AttachmentComp extends React.Component {
//   render(){
//     return  <AttachmentContainer {...this.props} />
//   }
// }

// export default connect(state => ({
//   permissions: state.permissions
// }))(AttachmentComp)

// // export default AttachmentComp


// const mapStateToProps = state => ({
//   permissions: state.permissions
// });

// const mapDispatchToProps = {
// };

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(AttachmentComp);
