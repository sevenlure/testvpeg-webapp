import React from 'react';
import { errorType } from 'constants/error';
import Message from 'rc-message';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { Row, Col } from 'reactstrap';
import { TypeMonitoringStationData } from 'constants/custom-data';
import Select from 'react-select';
import BootstrapTable from 'react-bootstrap-table-next';
import { getSoLieuMoiNhat } from 'api/monitoringApi';
import { COLOR } from 'config';
import Gmap from 'components/mapElements/map';

const FilterContainer = styled.ul`
  width: 200px;
  padding: 0;
  margin: 0;
  // border: 1px solid #dee2e6;
  // border-top: 1px;
  // border-left: 1px;
  // border-bottom: 1px;
`;

const columns = [
  {
    dataField: 'name',
    text: 'Tên trạm'
    // headerStyle: {
    //   display: 'none'
    // }
  }
];

export default class MapPanel extends React.Component {

  state = {
    data: [],
    LoaiDiem: TypeMonitoringStationData[0],
  };
  componentWillMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const res = await getSoLieuMoiNhat(this.state.LoaiDiem.value);
    if (res.success) {
      console.log(res.data);
      this.setState({
        data: res.data
      });
    } else if (res.error) {
      const error = errorType[res.typeError] || 'Error!!!';
      Message.error({ content: error });
    }
  };

  expandRow = {
    renderer: row => (
      <div>
        <BootstrapTable
          keyField="MaDiem"
          data={this.state.data}
          classes="tableNoSticky"
          striped
          hover
          condensed
          columns={[
            {
              dataField: 'ThoiGian',
              text: '#',
              headerStyle: {
                width: 10,
                display: 'none',
                minWidth: 10,
              },
              style: {
                width: 10,
                minWidth: 10
              },
              formatter: (cell, row, rowIndex) => {
                return <div>{rowIndex + 1}</div>;
              }
            },
            {
              dataField: 'MaDiem',
              text: 'Tên trạm',
              headerStyle: {
                display: 'none'
              }
            }
          ]}
        />
      </div>
    ),
    showExpandColumn: true,
    expanded: [0,1,2,3],
    expandHeaderColumnRenderer: ({ isAnyExpands }) => {
      return <div style={{ display: 'none' }} />;
    },
    expandColumnRenderer: ({ expanded }) => {
      if (expanded) {
        return <b>-</b>;
      }
      return <b>+</b>;
    }
  };

  render() {
    return (
      <div style={{ width: '100%' }}>
        <Clearfix height={8} />
        <Row>
          <Col xs={2}>
            <FilterContainer className="bp3-menu" style={{ width: '100%' }}>
              <li>
                {/* <Row style={{ display: 'flex', alignItems: 'center' }}>
                  <Col xs={6}>
                    <InputGroup placeholder="Tên Trạm" />
                  </Col>
                  <Col xs={6}>
                    <Select
                      placeholder="-- Select --"
                      style={{
                        flex: 1
                      }}
                      options={TypeMonitoringStationData}
                      defaultValue={TypeMonitoringStationData[0]}
                      onChange={item => {
                        this.setState(
                          {
                            LoaiDiem: item
                          },
                          () => this.fetchData()
                        );
                      }}
                    />
                  </Col>
                </Row> */}
                <Select
                  placeholder="-- Select --"
                  style={{
                    flex: 1
                  }}
                  classNamePrefix="react-select-custom"
                  options={TypeMonitoringStationData}
                  defaultValue={TypeMonitoringStationData[0]}
                  onChange={item => {
                    this.setState(
                      {
                        LoaiDiem: item
                      },
                      () => this.fetchData()
                    );
                  }}
                />
                <Clearfix height={8} />
              </li>
              <li
                className="table-wrapper-scroll-y"
                style={{
                  maxHeight: '83vh'
                }}
              >
                <BootstrapTable
                  keyField="id"
                  classes="tableNoSticky"
                  rowStyle={{
                    color: COLOR.SUCCESS
                  }}
                  data={[{ id: 1, name: this.state.LoaiDiem.label }]}
                  columns={columns}
                  expandRow={this.expandRow}
                />
              </li>
            </FilterContainer>
          </Col>
          <Col xs={10}>
            <Gmap
              markers={this.state.data.map(item => {
                return {
                  _id: item.MaDiem,
                  lat: item.ViDo,
                  lng: item.KinhDo,
                  name: item.MaDiem,
                  data: JSON.parse(item.Data)
                };
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
      </div>
    );
  }
}
