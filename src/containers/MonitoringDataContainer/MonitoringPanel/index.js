import React from 'react';
// import * as _ from 'lodash';
import moment from 'moment';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { Card, Elevation } from '@blueprintjs/core';
import { errorType } from 'constants/error';
import Message from 'rc-message';

import { Icon } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { TypeMonitoringStationData } from 'constants/custom-data';
import { getSoLieuMoiNhat } from 'api/monitoringApi';
import BootstrapTable from 'react-bootstrap-table-next';
import { COLOR, DATETIME_FORMAT } from 'config';
import Spinner from 'components/elements/spinner';

const NoDataContainer = styled.div`
  margin: 100px auto;
  //width: 50px;
  height: 40px;
  text-align: center;
  font-size: 10px;
`;

export default class MonitoringPanel extends React.Component {
  state = {
    isLoading: true,
    data: [],
    LoaiDiem: TypeMonitoringStationData[0],
    thongsoArr: [],
    columns: [
      {
        dataField: '_id',
        text: '#',
        style: {
          width: 10,
          minWidth: 10,
        },
        headerStyle: {
          width: 10,
          minWidth: 10,
        },
        formatter: (cell, row, rowIndex) => {
          return <div>{rowIndex + 1}</div>;
        }
      },
      {
        dataField: 'MaDiem',
        text: 'Tên trạm'
      },
      {
        dataField: 'ThoiGian',
        text: 'Thời gian',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATETIME_FORMAT)}</div>;
        }
      }
    ]
  };

  componentWillMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const res = await getSoLieuMoiNhat(this.state.LoaiDiem.value);
    if (res.success) {
      const data = res.data.map(item => {
        return {
          ...item,
          Data: JSON.parse(item.Data),
          DataObj: _.keyBy(JSON.parse(item.Data), 'MaThongSo')
        };
      });

      let thongsoArr = [];
      thongsoArr = _.flatten(_.map(data, 'Data'));
      thongsoArr = _.uniqBy(thongsoArr, 'MaThongSo');

      let columns = [
        ...this.state.columns,
        ...thongsoArr.map(item => {
          return {
            dataField: 'DataObj.' + item.MaThongSo + '.Value',
            text: item.MaThongSo + ' (' + item.DonVi + ')'
          };
        })
      ];

      this.setState({
        data: data,
        thongsoArr,
        columns,
        isLoading: false
      });
    }else if (res.error) {
      const error = errorType[res.typeError] || 'Error!!!';
      Message.error({ content: error });
      this.setState({
        isLoading: false
      })
    }
  };

  render() {
    return (
      <div style={{ width: '100%' }}>
        <Clearfix height={8} />
        <Card interactive={true} elevation={Elevation.TWO}>
          <h5>
            <a
               href=" "
              style={{
                display: 'flex',
                alignItems: 'center',
                color: COLOR.SUCCESS,
                
              }}
            >
              <Icon icon={IconNames.LIST} />
              <Clearfix width={8} />
              SỐ LIỆU - QUAN TRẮC NƯỚC THẢI TỰ ĐỘNG, LIÊN TỤC
            </a>
          </h5>

          {this.state.isLoading && <Spinner />}
          {!this.state.isLoading && (
            <BootstrapTable
              keyField="MaDiem"
              data={this.state.data}
              striped
              hover
              condensed
              columns={this.state.columns}
              noDataIndication={()=>{
                return <NoDataContainer>
                    <h2>No Data</h2>
                </NoDataContainer>
              }}
            />
          )}
        </Card>
        <Clearfix height={8} />
      </div>
    );
  }
}
