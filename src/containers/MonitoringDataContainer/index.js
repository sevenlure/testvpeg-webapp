import React from 'react';
import { Tab, Tabs } from '@blueprintjs/core';
import PageBuilding from 'components/pageBuilding';
import MapPanel from './MapPanel';
import MonitoringPanel from './MonitoringPanel';
import PageLockHOC from 'hoc/pageLock'

@PageLockHOC(['monitoringData.view'])
export default class MonitoringDataContainer extends React.Component {

  panelList = [
    {
      key: 'map',
      title: 'Bản đồ'
    },
    {
      key: 'monitoring',
      title: 'Số liệu online'
    },
  
  ];

  renderPanel = keyPanel => {
    switch (keyPanel) {
      case 'map': {
        return <MapPanel />;
      }
      case 'monitoring': {
        return <MonitoringPanel />;
      }
      
      default: {
        return <PageBuilding />;
      }
    }
  };


  render() {
    return (
      <div>
         <Tabs renderActiveTabPanelOnly={true} style={{ color: 'red' }}>
              {this.panelList.map(item => (
                <Tab
                  id={item.key}
                  key={item.key}
                  title={item.title}
                  panel={this.renderPanel(item.key)}
                />
              ))}
            </Tabs>
      </div>
    );
  }

}