import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { setMenuSelected } from '@redux/actions/menuAction';
import Confirm from 'components/elements/confirm-element';
import styled from 'styled-components';
import { NavbarDivider } from '@blueprintjs/core';
import QuanHuyenSelect from 'components/elements/select-quan-huyen';
import NganhNgheSelect from 'components/elements/select-nganh-nghe';
import BenTrongNgoaiSelect from 'components/elements/select-ben-trong-ngoai';
import KhuCumCongNghiepSelect from 'components/elements/select-khu-cum-cong-nghiep';
import ThamQuyenQuanLySelect from 'components/elements/select-tham-quyen-quan-ly';
import Clearfix from 'components/elements/clearfix';
import { getAll } from 'api/cosoApi';
import { API_KEY_GOOGLE_MAP } from 'config';
import { createForm } from 'rc-form';
import GoogleMapReact from 'google-map-react';
import supercluster from 'points-cluster';
import SimpleMarker from 'components/mapElements/SimpleMarker';
import ClusterMarker from 'components/mapElements/ClusterMarker';
import PageLockHOC from 'hoc/pageLock'


const MAP = {
  defaultZoom: 12,
  defaultCenter: { lat: 11.1073184, lng: 106.6822954 },
  options: {
    // styles: mapStyles,
    maxZoom: 19
  }
};

const FilterContainer = styled.ul`
  width: 200px;
  padding: 0;
  margin: 0;
  // border: 1px solid #dee2e6;
  // border-top: 1px;
  // border-left: 1px;
  // border-bottom: 1px;
`;

@PageLockHOC(['map.view'])
class MapContainer extends React.PureComponent {
  state = {
    cosoList: []
  };
  async componentDidMount() {
    const res = await getAll();
    if (res.success && res.data) {
      this.setState({
        dataSource: res.data,
        cosoList: res.data
      },()=>{
        if(this.mapRef) this.mapRef.createClusters()
      });
    }
  }

  handelFiter = () => {
    this.props.form.validateFields((error, value) => {
      if (error) return;
      value.district = _.get(value.district, 'value');
      value.sector = _.get(value.sector, 'value');
      value.industrialArea = _.get(value.industrialArea, 'value');
      value.authority = _.get(value.authority, 'value');

      const dataFiltered = _.filter(
        this.state.dataSource,
        _.pickBy(value, _.identity)
      );
      this.setState({
        cosoList: dataFiltered
      },()=>{
        if(this.mapRef) this.mapRef.createClusters()
       
      });
    });
  };

  render() {
    return (
      <div style={{ width: '100%', display: 'flex' }}>
        <Confirm ref={ConfirmElm => (this.ConfirmElm = ConfirmElm)} />
        <FilterContainer className="bp3-menu">
          <li className="bp3-menu-header">
            <h6
              className="bp3-heading"
              style={{
                textAlign: 'center'
              }}
            >
              Bộ lọc
            </h6>
          </li>
          <li>
            <span className="bp3-menu-item">Quận/huyện</span>
          </li>
          <li>
            <QuanHuyenSelect
              hideLabel
              className="bp3-menu-item"
              fieldForm={{
                form: this.props.form,
                name: 'district',
                option: {}
              }}
            />
          </li>
          <li>
            <span className="bp3-menu-item">Ngành/Nghề</span>
          </li>
          <li>
            <NganhNgheSelect
              hideLabel
              className="bp3-menu-item"
              fieldForm={{
                form: this.props.form,
                name: 'sector',
                option: {}
              }}
            />
          </li>
          <li>
            <span className="bp3-menu-item">Bên trong KCN/ngoài KCN</span>
          </li>
          <li>
            <BenTrongNgoaiSelect
              hideLabel
              className="bp3-menu-item"
              fieldForm={{
                form: this.props.form,
                name: 'inside',
                option: {}
              }}
            />
          </li>

          <li>
            <span className="bp3-menu-item">Khu/Cụm Công nghiệp</span>
          </li>
          <li>
            <KhuCumCongNghiepSelect
              hideLabel
              className="bp3-menu-item"
              fieldForm={{
                form: this.props.form,
                name: 'industrialArea',
                option: {}
              }}
            />
          </li>

          <li>
            <span className="bp3-menu-item">Thẩm quyền quản lý</span>
          </li>
          <li>
            <ThamQuyenQuanLySelect
              hideLabel
              className="bp3-menu-item"
              fieldForm={{
                form: this.props.form,
                name: 'authority',
                option: {}
              }}
            />
          </li>
          <Clearfix height={8} />
          <li>
            <button
              type="button"
              className="bp3-button bp3-icon-search bp3-fill"
              onClick={this.handelFiter}
            >
              Tìm kiếm
            </button>
          </li>
        </FilterContainer>
        <NavbarDivider
          style={{
            height: 'inherit'
          }}
        />
        <div style={{ flex: 1 }}>
          <Map dataList={this.state.cosoList} 
          ref={mapRef => this.mapRef = mapRef}
          />
        </div>
      </div>
    );
  }
}

class Map extends React.PureComponent {
  // constructor(props) {
  //   super(props);
  // }

  markers = [];
  state = {
    isLoaded: false,
    mapOptions: {
      center: { lat: 11.1073184, lng: 106.6822954 },
      zoom: 12
    },
    clusters: []
  };

  getClusters = () => {
    const clusters = supercluster(
      this.props.dataList.map((marker, index) => {
        const position =
          marker.Latitude && marker.Longitude
            ? { lat: marker.Latitude, lng: marker.Longitude }
            : { lat: 11.1073184, lng: 106.6822954 };
        return {
          ...position,
          id: index,
          name: marker.FacilityNameFull,
          facilityId: marker._id
        };
      }),
      {
        minZoom: 0,
        maxZoom: 16,
        radius: 120,
        extent: 1024
      }
    );

    return clusters(this.state.mapOptions);
  };

  createClusters = () => {
    this.setState({
      clusters: this.state.mapOptions.bounds
        ? this.getClusters(this.props).map(({ wx, wy, numPoints, points }) => {
            return {
              lat: wy,
              lng: wx,
              numPoints,
              id: `${numPoints}_${points[0].id}`,
              points
            };
          })
        : []
    });
  };

  handleMapChange = ({ center, zoom, bounds }) => {
    this.setState(
      {
        mapOptions: {
          center,
          zoom,
          bounds
        }
      },
      () => {
        this.createClusters(this.props);
      }
    );
  };

  async componentDidMount() {
    console.log('componentDidMount tat ca');
  }

  render() {
    return (
      <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          defaultZoom={MAP.defaultZoom}
          defaultCenter={MAP.defaultCenter}
          onChange={this.handleMapChange}
          options={{
            mapTypeId: 'hybrid'
          }}
          bootstrapURLKeys={{ key: API_KEY_GOOGLE_MAP }}
        >
          {this.state.clusters.map(item => {
            if (item.numPoints === 1) {
              return (
                <SimpleMarker
                  key={item.id}
                  lat={item.points[0].lat}
                  lng={item.points[0].lng}
                  name={item.points[0].name}
                  facilityId={item.points[0].facilityId}
                />
              );
            }

            return (
              <ClusterMarker
                key={item.id}
                lat={item.lat}
                lng={item.lng}
                text={item.numPoints}
              >
              
              </ClusterMarker>
            );
          })}
        </GoogleMapReact>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(createForm()(MapContainer))
);
