import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { setMenuSelected } from '@redux/actions/menuAction';
import Confirm from 'components/elements/confirm-element';
import styled from 'styled-components';
import { NavbarDivider } from '@blueprintjs/core';
import QuanHuyenSelect from 'components/elements/select-quan-huyen';
import NganhNgheSelect from 'components/elements/select-nganh-nghe';
import BenTrongNgoaiSelect from 'components/elements/select-ben-trong-ngoai';
import KhuCumCongNghiepSelect from 'components/elements/select-khu-cum-cong-nghiep';
import ThamQuyenQuanLySelect from 'components/elements/select-tham-quyen-quan-ly';
import Clearfix from 'components/elements/clearfix';
import { getAll } from 'api/cosoApi';

import { API_KEY_GOOGLE_MAP } from 'config';
import { compose, withProps } from 'recompose';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  InfoWindow,
} from 'react-google-maps';
import slug from 'constants/slug';

import { createForm } from 'rc-form';

const {
  MarkerClusterer
} = require('react-google-maps/lib/components/addons/MarkerClusterer');

const {
  MarkerWithLabel
} = require('react-google-maps/lib/components/addons/MarkerWithLabel');

const SpinnerContainer = styled.div`
  margin: 100px auto;
  //width: 50px;
  height: 40px;
  text-align: center;
  font-size: 10px;
`;

const FilterContainer = styled.ul`
  width: 200px;
  padding: 0;
  margin: 0;
  // border: 1px solid #dee2e6;
  // border-top: 1px;
  // border-left: 1px;
  // border-bottom: 1px;
`;

/* global google, _ */

class MapContainer extends React.PureComponent {
  state = {
    cosoList: []
  };
  async componentDidMount() {
    const res = await getAll();
    console.log('componentDidMount tat ca 222222222');
    if (res.success && res.data) {
      this.setState({
        dataSource: res.data,
        cosoList: res.data
      });
    }
  }

  handelFiter = () => {
    this.props.form.validateFields((error, value) => {
      console.log(value);
      if (error) return;
      value.district = _.get(value.district, 'value');
      value.sector = _.get(value.sector, 'value');
      value.industrialArea = _.get(value.industrialArea, 'value');
      value.authority = _.get(value.authority, 'value');

      const dataFiltered = _.filter(
        this.state.dataSource,
        _.pickBy(value, _.identity)
      );
      this.setState({
        cosoList: dataFiltered
      });
    });
  };

  render() {
    return (
      <div style={{ width: '100%', display: 'flex' }}>
        <Confirm ref={ConfirmElm => (this.ConfirmElm = ConfirmElm)} />
        <FilterContainer className="bp3-menu">
          <li className="bp3-menu-header">
            <h6
              className="bp3-heading"
              style={{
                textAlign: 'center'
              }}
            >
              Bộ lọc
            </h6>
          </li>
          <li>
            <span className="bp3-menu-item">Quận/huyện</span>
          </li>
          <li>
            <QuanHuyenSelect
              hideLabel
              className="bp3-menu-item"
              fieldForm={{
                form: this.props.form,
                name: 'district',
                option: {}
              }}
            />
          </li>
          <li>
            <span className="bp3-menu-item">Ngành/Nghề</span>
          </li>
          <li>
            <NganhNgheSelect
              hideLabel
              className="bp3-menu-item"
              fieldForm={{
                form: this.props.form,
                name: 'sector',
                option: {}
              }}
            />
          </li>
          <li>
            <span className="bp3-menu-item">Bên trong KCN/ngoài KCN</span>
          </li>
          <li>
            <BenTrongNgoaiSelect
              hideLabel
              className="bp3-menu-item"
              fieldForm={{
                form: this.props.form,
                name: 'inside',
                option: {}
              }}
            />
          </li>

          <li>
            <span className="bp3-menu-item">Khu/Cụm Công nghiệp</span>
          </li>
          <li>
            <KhuCumCongNghiepSelect
              hideLabel
              className="bp3-menu-item"
              fieldForm={{
                form: this.props.form,
                name: 'industrialArea',
                option: {}
              }}
            />
          </li>

          <li>
            <span className="bp3-menu-item">Thẩm quyền quản lý</span>
          </li>
          <li>
            <ThamQuyenQuanLySelect
              hideLabel
              className="bp3-menu-item"
              fieldForm={{
                form: this.props.form,
                name: 'authority',
                option: {}
              }}
            />
          </li>
          <Clearfix height={8} />
          <li>
            <button
              type="button"
              className="bp3-button bp3-icon-search bp3-fill"
              onClick={this.handelFiter}
            >
              Tìm kiếm
            </button>
          </li>
        </FilterContainer>
        <NavbarDivider
          style={{
            height: 'inherit'
          }}
        />
        <div style={{ flex: 1 }}>
          <MapComp dataList={this.state.cosoList} />
        </div>
      </div>
    );
  }
}

class MarkerCustom extends React.PureComponent {
  static propTypes = {
    position: PropTypes.object.isRequired,
    labelName: PropTypes.string.isRequired,
    facilityId: PropTypes.string.isRequired,
    trigger: PropTypes.bool
  };

  state = {
    isOpen: false,
    data: null
  };

  handelClick = () => {
    this.setState({
      isOpen: true
    });
  };

  closeInfo() {
    this.setState({
      isOpen: false
    });
  }

  getTextWidth() {
    // re-use canvas object for better performance
    var canvas =
      this.getTextWidth.canvas ||
      (this.getTextWidth.canvas = document.createElement('canvas'));
    var context = canvas.getContext('2d');
    context.font = '300 12pt arial';
    var metrics = context.measureText(this.props.labelName);
    return metrics.width;
  }

  render() {
    return (
      <div>
        <MarkerWithLabel
          noRedraw
          position={this.props.position}
          onClick={this.handelClick}
          labelAnchor={new google.maps.Point(this.getTextWidth() / 2, -5)}
          labelClass="bp3-tag bp3-intent-success"
          labelStyle={{
            // backgroundColor: 'white',
            fontSize: '12px',
            font: '300 12pt arial',
            //padding: '16px',
            // fontColor: `#08233B`
          }}
        >
          <div>
            {this.props.labelName ? this.props.labelName : ''}
            {this.state.isOpen && (
              <InfoWindow
                options={{
                  pixelOffset: new google.maps.Size(0, -45),
                  maxWidth: 300
                }}
                position={this.props.position}
                onCloseClick={() => {
                  this.setState({ isOpen: false });
                }}
              >
                <div className="mapInfowindow">
                  <div className="title">
                    <Link
                      style={{ color: '#37B44C' }}
                      to={slug.CO_SO.VIEW_WITH_ID + this.props.facilityId}
                    >
                      {this.props.labelName}
                    </Link>
                  </div>
                  <b>Info:</b> ...
                  <div className="smalldiv">
                    <b>Info:</b> ....
                  </div>
                  {/* <b>Mã trạm:</b> T5
                  <div className="smalldiv">
                    <b>Địa chỉ:</b> Xã Phạm Văn Hai, Huyện Bình ChánhTp Hồ Chí
                    Minh, Việt Nam
                  </div> */}
                </div>
              </InfoWindow>
            )}
          </div>
        </MarkerWithLabel>
      </div>
    );
  }
}

class Map extends React.PureComponent {
  // constructor(props) {
  //   super(props);
  // }

  markers = [];
  state = {
    isLoaded: false
  };

  async componentDidMount() {
    console.log('componentDidMount tat ca');
  }

  render() {
    return (
      <GoogleMap
        onTilesLoaded={() => {
          if (!this.state.isLoaded)
            this.setState({
              isLoaded: true
            });
        }}
        defaultMapTypeId="hybrid"
        defaultZoom={10}
        defaultCenter={
          this.props.mapLocation
            ? this.props.mapLocation
            : { lat: 11.1073184, lng: 106.6822954 }
        }
        onZoomChanged={() => {
          this.markers.map(marker => {
            if (marker.state.isOpen) marker.closeInfo();
            return null;
          });
        }}
      >
        <MarkerClusterer averageCenter enableRetinaIcons gridSize={80}>
          {this.state.isLoaded &&
            this.props.dataList.map(marker => {
              const position =
                marker.Latitude && marker.Longitude
                  ? { lat: marker.Latitude, lng: marker.Longitude }
                  : { lat: 11.1073184, lng: 106.6822954 };
              return (
                <MarkerCustom
                  ref={marker => {
                    if (marker) this.markers.push(marker);
                  }}
                  key={marker._id}
                  position={position}
                  labelName={marker.FacilityNameFull}
                  facilityId={marker._id}
                  trigger={this.state.trigger}
                />
              );
            })}
        </MarkerClusterer>
      </GoogleMap>
    );
  }
}

const MapComp = compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${API_KEY_GOOGLE_MAP}&v=3.exp`,
    loadingElement: (
      <SpinnerContainer style={{ height: '100%' }}>
        <svg
          className="bp3-spinner"
          height="100"
          width="100"
          viewBox="0 0 100 100"
          strokeWidth="4"
        >
          <path
            className="bp3-spinner-track"
            d="M 50,50 m 0,-44.5 a 44.5,44.5 0 1 1 0,89 a 44.5,44.5 0 1 1 0,-89"
          />
          <path
            className="bp3-spinner-head"
            d="M 50,50 m 0,-44.5 a 44.5,44.5 0 1 1 0,89 a 44.5,44.5 0 1 1 0,-89"
            pathLength="280"
            strokeDasharray="280 280"
            strokeDashoffset="210"
          />
        </svg>
      </SpinnerContainer>
    ),
    containerElement: <div style={{ height: `85vh` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  withScriptjs,
  withGoogleMap
)(Map);

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(createForm()(MapContainer))
);
