import React from 'react';
import PropTypes from 'prop-types';
import { getOne, deleteOne } from 'api/facilityEnviMonitoringReportApi';
import Message from 'rc-message';
// import * as _ from 'lodash';
import { Icon, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { Row, Col } from 'reactstrap';
import Confirm from 'components/elements/confirm-element';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import { withRouter } from 'react-router-dom';
import { COLOR, STYLE, DATETIME_FORMAT, DATE_FORMAT } from 'config';
import moment from 'moment';
import PageLockHOC from 'hoc/pageLock';
import Permissions from 'react-redux-permissions';
import RuleDiv from 'components/ruleDiv';
import { DOI_TUONG } from 'constants/custom-data';
// import { Cell, Column, Table, SelectionModes } from '@blueprintjs/table';

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

class RowValue extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    isRequiredField: PropTypes.bool
  };

  render() {
    return (
      <div>
        <Row>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              width: '100%'
            }}
          >
            <Col
              style={this.props.isRequiredField ? STYLE.REQUIREDFIELD : {}}
              xs={2}
            >
              {this.props.label}
            </Col>
            <Col xs={7}>{this.props.value}</Col>
          </div>
        </Row>
        <Clearfix height={8} />
      </div>
    );
  }
}

@PageLockHOC(['facilityEnviMonitoringReport.view'])
class FacilityEnviMonitoringReportView extends React.Component {
  state = {
    _idObject: '',
    isLoaded: false,
    isLoading: false,
    facility: {},
    facilityId: '',
    initialValue: null
  };

  getInitialValue = (keyPath, defaultValue = '') => {
    let val = _.result(this.state.initialValue, keyPath, defaultValue);
    if (!val) val = '';
    return val;
  };

  getInitialDateValue = (keyPath, isDate, defaultValue = '') => {
    let value = _.result(this.state.initialValue, keyPath, defaultValue);
    if (!value) return '';
    if (isDate) return moment(value).format(DATE_FORMAT);
    return moment(value).format(DATETIME_FORMAT);
  };

  async checkValidate() {
    const _id = _.result(this.props, 'match.params._id');
    if (_id) {
      const res = await getOne(_id);
      console.log(res);
      if (res.success && res.data) {
        this.setState({
          _idObject: res.data._id,
          facility: res.data.facility,
          facilityId: res.data.facility._id,
          isLoading: false,
          isLoaded: true,
          initialValue: res.data
        });
      }
    } else {
      this.props.history.push(slug.ENVI_MONITORING_REPORT.LIST);
    }
  }

  handelDelete = () => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      this.ConfirmElm.showConfirm({
        content: 'Bạn có muốn xoá ',
        cb: async value => {
          if (value) {
            const res = await deleteOne(this.getInitialValue('_id'));
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.props.history.push(slug.ENVI_MONITORING_REPORT.LIST);
            }
            if (res.error) Message.error({ content: res.message });
          }
        }
      });
    }
  };

  async componentWillMount() {
    await this.checkValidate();
  }

  render() {
    const DoiTuong = _.get(this.state.initialValue, 'DoiTuong');

    return (
      <div style={{ width: '100%' }}>
        <Confirm ref={ConfirmElm => (this.ConfirmElm = ConfirmElm)} />
        <h4
          className='bp3-heading'
          style={{
            color: COLOR.SUCCESS
          }}
        >
          Báo cáo quan trắc môi trường
        </h4>
        <HeaderContainer>
          <div style={{ display: 'inline-flex' }}>
            <Link to={slug.ENVI_MONITORING_REPORT.LIST}>
              <Icon
                style={{ cursor: 'pointer' }}
                icon={IconNames.CIRCLE_ARROW_LEFT}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.SUCCESS}
              />
            </Link>

            {this.state.initialValue && (
              <Permissions allowed={[`facilityEnviMonitoringReport.write`]}>
                <RuleDiv initialValue={this.state.initialValue}>
                  <React.Fragment>
                    <Clearfix width={8} />
                    <Link
                      to={
                        slug.ENVI_MONITORING_REPORT.EDIT_WITH_ID +
                        this.state._idObject
                      }
                    >
                      <Icon
                        style={{ cursor: 'pointer' }}
                        icon={IconNames.MANUALLY_ENTERED_DATA}
                        iconSize={Icon.SIZE_LARGE}
                        intent={Intent.PRIMARY}
                      />
                    </Link>
                  </React.Fragment>
                </RuleDiv>
              </Permissions>
            )}

            {this.state.initialValue && (
              <Permissions allowed={[`facilityEnviMonitoringReport.delete`]}>
                <RuleDiv initialValue={this.state.initialValue}>
                  <React.Fragment>
                    <Clearfix width={8} />
                    <Icon
                      style={{ cursor: 'pointer' }}
                      icon={IconNames.BAN_CIRCLE}
                      iconSize={Icon.SIZE_LARGE}
                      intent={Intent.DANGER}
                      onClick={this.handelDelete}
                    />
                  </React.Fragment>
                </RuleDiv>
              </Permissions>
            )}

            <Clearfix width={8} />
            <Icon
              style={{ cursor: 'pointer' }}
              icon={IconNames.PRINT}
              iconSize={Icon.SIZE_LARGE}
              intent={Intent.SUCCESS}
            />
          </div>
        </HeaderContainer>
        <Clearfix height={16} />
        <RowValue
          isRequiredField
          label='Tên Cơ Sở'
          value={this.getInitialValue('facility.FacilityNameFull')}
        />

        <RowValue
          isRequiredField
          label='Tên cán bộ thụ lý hồ sơ'
          value={this.getInitialValue('TenCanBoThuLy')}
        />

        <RowValue label='Đối tượng' value={this.getInitialValue('DoiTuong')} />

        <RowValue
          label='Tổng lưu lượng nước thải (m3/ngày)'
          value={this.getInitialValue('TongLLNuocThai')}
        />

        <RowValue
          label='Tổng lưu lượng khí thải (m3/giờ)'
          value={this.getInitialValue('TongLLKhiThai')}
        />

        {DoiTuong === DOI_TUONG.DANH_GIA_TAC_DONG_MOI_TRUONG && (
          <RowValue label='Bùn thải' value={this.getInitialValue('BunThai')} />
        )}

        <RowValue
          label='Ngày nộp báo cáo'
          value={this.getInitialDateValue('NgayNopBaoCao')}
        />

        <RowValue
          label='Thời gian đo đạc, quan trắc'
          value={this.getInitialDateValue('ThoiGianDoDacQuanTrac')}
        />

        <RowValue
          label='Ngày có văn bản thông báo'
          value={this.getInitialDateValue('NgayCoVanBanThongBao')}
        />

        <RowValue
          label='Số hiệu văn bản trả lời'
          value={this.getInitialValue('SoHieuVanBanTraLoi')}
        />

        <RowValue
          label='Kết quả quan trắc Nước thải so với quy chuẩn'
          value={this.getInitialValue('KQQT_NuocThai')}
        />

        <RowValue
          label='Kết quả quan trắc Khí thải so với quy chuẩn'
          value={this.getInitialValue('KQQT_KhiThai')}
        />

        <RowValue
          label='Kết quả quan trắc Bùn thải so với quy chuẩn'
          value={this.getInitialValue('KQQT_BunThai')}
        />

        {/* <RowValue
          isRequiredField
          label='Tần suất giám sát'
          value={this.getInitialValue('FrequencyOfMonitoring')}
        /> */}
        {/* <RowValue
          label="Nguyên liệu sử dụng"
          value={this.getInitialValue('RawUsedMaterials')}
        />
        <RowValue
          label="Công suất sản phẩm"
          value={this.getInitialValue('ProductCapacity')}
        /> */}
        {/* <RowValue
          label='Năm nộp báo cáo'
          value={this.getInitialValue('YearOfCharge')}
        />
        <RowValue
          isRequiredField
          label='Quý nộp báo cáo'
          value={this.getInitialValue('QuaterOfCharge')}
        />
        <RowValue
          isRequiredField
          label='Tình trạng nộp báo cáo'
          value={this.getInitialValue('PaymentStatus')}
        />

        <RowValue
          label='Ngày nộp'
          value={this.getInitialDateValue('SubmitDate', true)}
        /> */}
        {/* <RowValue
          isRequiredField
          label='Thời gian nộp'
          value={this.getInitialValue('SubmitTime')}
        /> */}

        {/* <Row>
          <Col xs={2}>Danh sách các thông số quan trắc</Col>
          <Col xs={6}>
            <Table
              numRows={this.getInitialValue('ParametersList').length}
              defaultRowHeight={40}
              defaultColumnWidth={150}
              enableGhostCells={true}
              selectionModes={SelectionModes.NONE}
              enableRowResizing={false}
            >
              <Column
                name="Thông số"
                cellRenderer={rowIndex => {
                  return (
                    <Cell>
                      {
                        this.getInitialValue('ParametersList')[rowIndex]
                          .Parameter
                      }
                    </Cell>
                  );
                }}
              />
              <Column
                name="Kết quả"
                cellRenderer={rowIndex => {
                  return (
                    <Cell>
                      {this.getInitialValue('ParametersList')[rowIndex].Result}
                    </Cell>
                  );
                }}
              />
              <Column
                name="Quy chuẩn"
                cellRenderer={rowIndex => {
                  return (
                    <Cell>
                      {
                        this.getInitialValue('ParametersList')[rowIndex]
                          .Standard
                      }
                    </Cell>
                  );
                }}
              />
              <Column
                name="Đánh giá"
                cellRenderer={rowIndex => {
                  return (
                    <Cell>
                      {this.getInitialValue('ParametersList')[rowIndex].Review}
                    </Cell>
                  );
                }}
              />
            </Table>
          </Col>
        </Row>
        <Clearfix height={8} /> */}
        <RowValue
          label='Thêm vào'
          value={
            this.getInitialDateValue('AddedOn') +
            ' bởi ' +
            this.getInitialValue('AddedBy')
          }
        />
        <RowValue
          label='Cập nhật lần cuối'
          value={
            this.getInitialDateValue('UpdatedOn') +
            ' bởi ' +
            this.getInitialValue('UpdatedBy')
          }
        />
      </div>
    );
  }
}

export default withRouter(FacilityEnviMonitoringReportView);
