import React from 'react';
import { Route } from 'react-router-dom';
import slug from 'constants/slug';
import FacilityEnviMonitoringReportList from './FacilityEnviMonitoringReportList';
import FacilityEnviMonitoringReportCreate from './FacilityEnviMonitoringReportCreate';
import FacilityEnviMonitoringReportEdit from './FacilityEnviMonitoringReportEdit';
import FacilityEnviMonitoringReportView from './FacilityEnviMonitoringReportView';

export default props => {
  return [
    <Route
      exact
      key={slug.ENVI_MONITORING_REPORT.LIST}
      path={slug.ENVI_MONITORING_REPORT.LIST}
      render={matchProps => (
        <FacilityEnviMonitoringReportList {...matchProps} {...props} />
      )}
    />,
    <Route
      key={slug.ENVI_MONITORING_REPORT.CREATE}
      path={slug.ENVI_MONITORING_REPORT.CREATE}
      render={matchProps => (
        <FacilityEnviMonitoringReportCreate {...matchProps} {...props} />
      )}
    />,
    <Route
      key={slug.ENVI_MONITORING_REPORT.VIEW}
      path={slug.ENVI_MONITORING_REPORT.VIEW}
      render={matchProps => (
        <FacilityEnviMonitoringReportView {...matchProps} {...props} />
      )}
    />,
    <Route
      key={slug.ENVI_MONITORING_REPORT.EDIT}
      path={slug.ENVI_MONITORING_REPORT.EDIT}
      render={matchProps => (
        <FacilityEnviMonitoringReportEdit {...matchProps} {...props} />
      )}
    />
  ];
};
