import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BaseListContainer } from 'containers/BaseContainer/BaseListContainer';
import { setMenuSelected } from '@redux/actions/menuAction';
import moment from 'moment';
import { DATE_FORMAT } from 'config';
import {
  getList,
  deleteOne,
  exportData
} from 'api/facilityEnviMonitoringReportApi';
import slug from 'constants/slug';
import styled from 'styled-components';
import { Colors } from '@blueprintjs/core';
import Message from 'rc-message';
import { createForm } from 'rc-form';
import PageLockHOC from 'hoc/pageLock';

const NameContainer = styled.div`
  color: ${Colors.GREEN2};
  text-decoration: underline;
`;

@PageLockHOC(['facilityEnviMonitoringReport.view'])
class FacilityEnviMonitoringReportList extends BaseListContainer {
  static propTypes = {
    facilityId: PropTypes.string,
    hideRightHeaderContent: PropTypes.bool
  };

  constructor(props) {
    const funcApi = {
      getList,
      exportData
    };
    const columns = [
      // {
      //   dataField: '_id',
      //   text: '',
      //   formatter: (cell, row) => {
      //     return (
      //       <Icon
      //         style={{ cursor: 'pointer' }}
      //         icon={IconNames.DISABLE}
      //         iconSize={Icon.SIZE_LARGE}
      //         intent={Intent.DANGER}
      //         onClick={() => {
      //           this.handelDelete(cell);
      //         }}
      //       />
      //     );
      //   }
      // },
      {
        dataField: 'facility.FacilityNameFull',
        text: 'Tên Cơ sở',
        formatter: (cell, row) => {
          return (
            <Link to={slug.ENVI_MONITORING_REPORT.VIEW_WITH_ID + row._id}>
              <NameContainer>{cell}</NameContainer>
            </Link>
          );
        }
      },
      {
        dataField: 'facility.industrialArea',
        text: '	Khu công nghiệp/Cụm Công nghiệp',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'facility.district',
        text: 'Quận/Huyện',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'facility.sector',
        text: 'Ngành/Nghề',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'TenCanBoThuLy',
        text: 'Tên cán bộ thụ lý hồ sơ',
        formatter: (cell, row) => {
          return <div>{cell}</div>;
        }
      },
      {
        dataField: 'DoiTuong',
        text: 'Đối tượng',
        formatter: (cell, row) => {
          return <div>{cell}</div>;
        }
      },
      {
        dataField: 'AddedOn',
        text: 'Thêm vào',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
      {
        dataField: 'AddedBy',
        text: 'Thêm bởi'
      },
      {
        dataField: 'UpdatedOn',
        text: 'Cập nhật ngày',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
      {
        dataField: 'UpdatedBy',
        text: 'Cập nhật bởi',
        formatter: (cell, row) => {
          return <div>{cell}</div>;
        }
      }
    ];
    super(
      props,
      funcApi,
      columns,
      `${slug.ENVI_MONITORING_REPORT.CREATE}?facilityId=${props.facilityId}`,
      'facilityEnviMonitoringReport'
    );

    this.state = {
      ...this.state,
      valueSearch: {
        facility: this.props.facilityId
      }
    };
  }

  renderSearchContainer() {
    return (
      <div>
        {super.renderSearchContainer()}
        {/* {this.renderSearchExpandContainer()} */}
      </div>
    );
  }

  renderSearchExpandContainer = () => {
    return (
      <div>
        {/* <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <EmissionTypeSelect
              fieldForm={{
                form: this.props.form,
                name: 'emissionType'
              }}
            />
          </Col>
        </Row> */}
      </div>
    );
  };

  handelDelete = _id => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      this.ConfirmElm.showConfirm({
        content: 'Bạn có muốn xoá ',
        cb: async value => {
          if (value) {
            const res = await deleteOne(_id);
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.fetData();
            }
            if (res.error) Message.error({ content: res.message });
          }
        }
      });
    }
  };
}

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(createForm()(FacilityEnviMonitoringReportList))
);
