import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { Icon, Intent, InputGroup } from '@blueprintjs/core';
import Clearfix from 'components/elements/clearfix';
import { IconNames } from '@blueprintjs/icons';
import { createForm } from 'rc-form';
// import * as _ from 'lodash';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import IconRequired from 'components/elements/required-icon';
import FacilitySelect from 'components/elements/select-facility';
import { STYLE } from 'config';
import CustomDataSelect from 'components/elements/select-custom-data';
import { COLOR, DATE_FORMAT, DATA_FROM_YEAR } from 'config';
import {
  getDataForCustomSelect,
  FrequencyOfMonitoringData,
  QuaterOfChargeData,
  PaymentStatusData,
  DoiTuongData,
  BunThaiData,
  KetQuaQuanTracData,
  DOI_TUONG
} from 'constants/custom-data';
import NumberInput from 'rc-input-number';
import { DateInput } from '@blueprintjs/datetime';
import moment from 'moment';
import { convertDateFromSever as convertDate } from 'utils/fnUtil';
import Message from 'rc-message';
import dynamicRoleHOC from 'hoc/dynamicRole';

const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

const HeaderContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-start;
  cursor: pointer;
`;

// const CellContainer = styled.div`
//   padding: 4px;
// `;

@dynamicRoleHOC()
class FacilityEnviProtectFeeForm extends React.Component {
  static propTypes = {
    isEdit: PropTypes.bool,
    initialValue: PropTypes.object,
    slugBack: PropTypes.string,
    handelSubmit: PropTypes.func
    // facility: PropTypes.shape({
    //   _id: PropTypes.string,
    //   FacilityNameFull: PropTypes.string
    // })
  };

  constructor(props) {
    super(props);
    this.state = {
      rowCount: this.setInitialValue('ParametersList.length', 2),
      DiaChiText: '',
      NganhNgheText: ''
    };
  }
  componentDidMount() {
    if (this.props.isEdit) {
      this.getSubmitTimeData();
      this.forceUpdate();
    }
    if(this.props.getRef) this.props.getRef(this)
  }

  CustomData_TanSuat6Thang = [
    { value: 'Lần 1', label: 'Lần 1' },
    { value: 'Lần 2', label: 'Lần 2' }
  ];

  setInitialValue(keyPath, defaultValue) {
    if (this.props.isEdit) {
      let value = _.result(this.props.initialValue, keyPath, defaultValue);
      if (!value) value = defaultValue;
      return value;
    }
    return defaultValue;
  }

  setSelInitialValue(keyPath, defaultValue, nameLabel) {
    if (this.props.isEdit) {
      let selItem = _.result(this.props.initialValue, keyPath);
      if (!selItem) return defaultValue;
      return {
        value: selItem._id,
        label: nameLabel ? selItem[nameLabel] : selItem.Name
      };
    }
    return defaultValue;
  }

  submit = () => {
    this.props.form.validateFields((error, value) => {
      console.log(value);
      value.facility = _.get(value.facility, 'value');
      value.FrequencyOfMonitoring = _.get(value.FrequencyOfMonitoring, 'value');
      value.QuaterOfCharge = _.get(value.QuaterOfCharge, 'value');
      value.PaymentStatus = _.get(value.PaymentStatus, 'value');
      value.SubmitTime = _.get(value.SubmitTime, 'value');



      // NOTE  update
      value.BunThai = _.get(value.BunThai, 'value');
      value.DoiTuong = _.get(value.DoiTuong, 'value');
      value.KQQT_NuocThai = _.get(value.KQQT_NuocThai, 'value');
      value.KQQT_KhiThai = _.get(value.KQQT_KhiThai, 'value');
      value.KQQT_BunThai = _.get(value.KQQT_BunThai, 'value');

      if (error) return;
      this.props.handelSubmit(value);
    });
  };

  addRowCount = () => {
    this.setState({
      rowCount: this.state.rowCount + 1
    });
  };

  deleteRow = async rowIndex => {
    const { form } = this.props;
    let ParametersList = form.getFieldValue('ParametersList');

    ParametersList = _.values(ParametersList);
    ParametersList = ParametersList.filter((item, index) => index !== rowIndex);
    ParametersList.map((item, index) => {
      return this.props.form.setFieldsValue({
        [`ParametersList.${index}`]: {
          Parameter: item.Parameter,
          Result: item.Result,
          Review: item.Review,
          Standard: item.Standard
        }
      });
    });

    this.setState({
      rowCount: --this.state.rowCount
    });
  };

  generateYear = () => {
    let loop = new Date().getFullYear() - DATA_FROM_YEAR;
    let arrYear = [];
    for (let i = loop; i >= 0; i--) {
      arrYear.push({
        value: DATA_FROM_YEAR + i,
        label: DATA_FROM_YEAR + i + ''
      });
    }
    return [...arrYear];
  };

  getSubmitTimeData() {
    const { getFieldValue } = this.props.form;
    let FrequencyOfMonitoring = _.result(
      getFieldValue('FrequencyOfMonitoring'),
      'value'
    );
    switch (FrequencyOfMonitoring) {
      case '3 tháng':
        return QuaterOfChargeData;
      case '6 tháng':
        return this.CustomData_TanSuat6Thang;
      case '1 năm':
        return this.generateYear();
      default:
        return [];
    }
  }

  onChangeFacility = obj => {
    this.setState({
      DiaChiText: _.get(obj, 'Address1'),
      NganhNgheText: _.get(obj, 'sector.Name')
    });
  };

  render() {
    const {
      getFieldProps,
      getFieldError,
      setFieldsValue,
      getFieldValue
    } = this.props.form;
    const DoiTuong = _.get(getFieldValue('DoiTuong'), 'value');
    return (
      <div>
        <h4
          className='bp3-heading'
          style={{
            color: COLOR.SUCCESS
          }}
        >
          Báo cáo quan trắc môi trường
        </h4>
        <Row>
          <Col xs={4}>
            <HeaderContainer>
              <Icon
                icon={IconNames.SAVED}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.PRIMARY}
                onClick={() => {
                  this.submit();
                }}
              />
              <Clearfix width={8} />
              <Link to={this.props.slugBack ? this.props.slugBack : slug.BASE}>
                <Icon
                  icon={IconNames.DISABLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                />
              </Link>
            </HeaderContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Tên Cơ sở
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <FacilitySelect
              isDisabled={this.props.facility ? true : false}
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'facility',
                option: {
                  initialValue: this.props.facility
                    ? {
                        value: this.props.facility._id,
                        label: this.props.facility.FacilityNameFull
                      }
                    : null,
                  rules: [
                    {
                      required: true,
                      message: 'Tên Cơ sở is required'
                    }
                  ],
                  // onChange: val => {
                  //   let FrequencyOfMonitoring = _.get(
                  //     val,
                  //     'obj.FrequencyOfMonitoring'
                  //   );
                  //   FrequencyOfMonitoring = FrequencyOfMonitoring
                  //     ? {
                  //         label: FrequencyOfMonitoring,
                  //         value: FrequencyOfMonitoring
                  //       }
                  //     : undefined;
                  //   setFieldsValue({
                  //     FrequencyOfMonitoring
                  //   });
                  //   if (!FrequencyOfMonitoring)
                  //     Message.warning({
                  //       content:
                  //         'Cơ Sở này chưa có thông tin Tần suất giám sát, vui lòng thêm truờng này tại mục Cơ Sở'
                  //     });
                  // }
                }
              }}
              cbChangeFacility={this.onChangeFacility}
            />
            <ErrorContainer>
              {(getFieldError('facility') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Địa chỉ</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup disabled value={this.state.DiaChiText} />
          </Col>
        </Row>

        <Row>
          <Col xs={4}>
            <LabelContainer>Ngành Nghề</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup disabled value={this.state.NganhNgheText} />
          </Col>
        </Row>

        <Row>
          <Col xs={4}>
            <LabelContainer>Tên cán bộ thụ lý hồ sơ</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Tên cán bộ thụ lý hồ sơ'
              {...getFieldProps('TenCanBoThuLy', {
                initialValue: this.setInitialValue('TenCanBoThuLy', '')
              })}
            />
          </Col>
        </Row>

        <Row>
          <Col xs={4}>
            <LabelContainer>Đối tuợng</LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={DoiTuongData}
              fieldForm={{
                form: this.props.form,
                name: 'DoiTuong',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('DoiTuong', null)
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>Tổng lưu lượng nước thải (m3/ngày)</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              {...getFieldProps('TongLLNuocThai', {
                initialValue: this.setInitialValue('TongLLNuocThai', null)
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>Tổng lưu lượng khí thải (m3/giờ)</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              {...getFieldProps('TongLLKhiThai', {
                initialValue: this.setInitialValue('TongLLKhiThai', null)
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        {DoiTuong === DOI_TUONG.DANH_GIA_TAC_DONG_MOI_TRUONG && (
          <React.Fragment>
            <Row>
              <Col xs={4}>
                <LabelContainer>Bùn thải</LabelContainer>
              </Col>
              <Col xs={4}>
                <CustomDataSelect
                  hideLabel
                  data={BunThaiData}
                  fieldForm={{
                    form: this.props.form,
                    name: 'BunThai',
                    option: {
                      initialValue: getDataForCustomSelect(
                        this.setInitialValue('BunThai', null)
                      )
                    }
                  }}
                />
              </Col>
            </Row>
            <Clearfix height={8} />
          </React.Fragment>
        )}

        <Row>
          <Col xs={4}>
            <LabelContainer>Ngày nộp báo cáo</LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder='Ngày nộp báo cáo'
              {...getFieldProps('NgayNopBaoCao', {
                initialValue: convertDate(
                  this.setInitialValue('NgayNopBaoCao', null)
                )
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>Thời gian đo đạc, quan trắc</LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder='Thời gian đo đạc, quan trắc'
              {...getFieldProps('ThoiGianDoDacQuanTrac', {
                initialValue: convertDate(
                  this.setInitialValue('ThoiGianDoDacQuanTrac', null)
                )
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>Ngày có văn bản thông báo</LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder='Ngày có văn bản thông báo'
              {...getFieldProps('NgayCoVanBanThongBao', {
                initialValue: convertDate(
                  this.setInitialValue('NgayCoVanBanThongBao', null)
                )
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>Số hiệu văn bản trả lời</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Số hiệu văn bản trả lời'
              {...getFieldProps('SoHieuVanBanTraLoi', {
                initialValue: this.setInitialValue('SoHieuVanBanTraLoi', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>
              Kết quả quan trắc Nước thải so với quy chuẩn
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={KetQuaQuanTracData}
              fieldForm={{
                form: this.props.form,
                name: 'KQQT_NuocThai',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('KQQT_NuocThai', null)
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>
              Kết quả quan trắc Khí thải so với quy chuẩn
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={KetQuaQuanTracData}
              fieldForm={{
                form: this.props.form,
                name: 'KQQT_KhiThai',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('KQQT_KhiThai', null)
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>
              Kết quả quan trắc Bùn thải so với quy chuẩn
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={KetQuaQuanTracData}
              fieldForm={{
                form: this.props.form,
                name: 'KQQT_BunThai',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('KQQT_BunThai', null)
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        {/* <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Tần suất giám sát <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              isDisabled
              hideLabel
              data={FrequencyOfMonitoringData}
              fieldForm={{
                form: this.props.form,
                name: 'FrequencyOfMonitoring',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('FrequencyOfMonitoring', null)
                  ),
                  rules: [
                    {
                      required: true,
                      message:
                        'Tần suất giám sát is required, vui lòng thêm truờng này tại mục Cơ Sở'
                    }
                  ],
                  onChange: val => {
                    setFieldsValue({
                      SubmitTime: null
                    });
                  }
                }
              }}
            />
            <ErrorContainer>
              {(getFieldError('FrequencyOfMonitoring') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} /> */}
        {/* <Row>
          <Col xs={4}>
            <LabelContainer>Nguyên liệu sử dụng</LabelContainer>
          </Col>
          <Col xs={4}>
            <textarea
              className='bp3-input bp3-fill'
              dir='auto'
              {...getFieldProps('RawUsedMaterials', {
                initialValue: this.setInitialValue('RawUsedMaterials', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Công suất sản phẩm</LabelContainer>
          </Col>
          <Col xs={4}>
            <textarea
              className='bp3-input bp3-fill'
              dir='auto'
              {...getFieldProps('ProductCapacity', {
                initialValue: this.setInitialValue('ProductCapacity', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} /> */}
        {/* <Row>
          <Col xs={4}>
            <LabelContainer>Năm nộp báo cáo</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              placeholder='Năm'
              {...getFieldProps('YearOfCharge', {
                initialValue: this.setInitialValue('YearOfCharge', '')
              })}
            />
            <ErrorContainer>
              {(getFieldError('YearOfCharge') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>

        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Quý nộp báo cáo <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={QuaterOfChargeData}
              fieldForm={{
                form: this.props.form,
                name: 'QuaterOfCharge',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('QuaterOfCharge', null)
                  ),
                  rules: [
                    {
                      required: true,
                      message: 'Quý tính phí is required'
                    }
                  ]
                }
              }}
            />
            <ErrorContainer>
              {(getFieldError('QuaterOfCharge') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>

        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Tình trạng nộp <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={PaymentStatusData}
              fieldForm={{
                form: this.props.form,
                name: 'PaymentStatus',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('PaymentStatus', null)
                  ),
                  rules: [
                    {
                      required: true,
                      message: 'Tình trạng nộp phí is required'
                    }
                  ]
                }
              }}
            />
            <ErrorContainer>
              {(getFieldError('PaymentStatus') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>

        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>Ngày nộp <IconRequired /></LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              popoverProps={{
                usePortal: true
              }}
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder='Ngày nộp'
              {...getFieldProps('SubmitDate', {
                initialValue: convertDate(
                  this.setInitialValue('SubmitDate', null)
                ),
                rules: [
                  {
                    required: true,
                    message: 'Vui lòng nhập ngày nộp'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('SubmitDate') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
 */}

        <Clearfix height={8} />
      </div>
    );
  }
}

export default createForm()(FacilityEnviProtectFeeForm);
