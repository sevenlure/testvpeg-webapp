import React from 'react';
import PropTypes from 'prop-types';
import UserSelect from 'components/elements/select-user';
import TypeActionSelect from 'components/elements/select-type-action';
import { Row, Col } from 'reactstrap';
import {
  Icon,
  Intent,
  Hotkey,
  Hotkeys,
  HotkeysTarget
} from '@blueprintjs/core';
import Clearfix from 'components/elements/clearfix';
import { IconNames } from '@blueprintjs/icons';
import { DATE_FORMAT } from 'config';
import { DateInput } from '@blueprintjs/datetime';
import moment from 'moment';
// import * as _ from 'lodash';

@HotkeysTarget
export default class SearchContainer extends React.Component {
  static propTypes = {
    handelSubmitSearch: PropTypes.func,
    form: PropTypes.object.isRequired
  };

  state = {};

  submit = () => {
    this.props.form.validateFields((error, value) => {
      if (error) return;
      value.user = _.get(value.user, 'value');
      value.TypeEvent = _.get(value.TypeEvent, 'value');
      if (!value.fromDate) value.fromDate = moment(1970).toDate();
      if (!value.toDate) value.toDate = moment().toDate();

      if (this.props.handelSubmitSearch) this.props.handelSubmitSearch(value);
    });
  };

  renderHotkeys() {
    return (
      <Hotkeys>
        <Hotkey
          //global={true}
          allowInInput={true}
          combo='enter'
          label='Be awesome all the time'
          onKeyDown={this.submit}
        />
      </Hotkeys>
    );
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        {/* Row 1 */}
        <Row>
          <Col xs={4}>
            <UserSelect
              fieldForm={{
                form: this.props.form,
                name: 'user'
              }}
            />
          </Col>
          <Col xs={4}>
            <TypeActionSelect
              fieldForm={{
                form: this.props.form,
                name: 'TypeEvent'
              }}
            />
          </Col>
          <Col style={{ display: 'flex' }}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <Icon
                icon={IconNames.SEARCH}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.SUCCESS}
                style={{ cursor: 'pointer' }}
                onClick={this.submit}
              />
            </div>
          </Col>
        </Row>
        <Clearfix height={8} />
        {/* Row 2 */}
        <Row>
          <Col xs={4}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <span>Từ ngày</span>
              <Clearfix width={8} />
              <div style={{ flex: 1 }}>
                {getFieldDecorator('fromDate', {})(
                  <DateInput
                    popoverProps={{
                      targetClassName: 'el-fill'
                    }}
                    formatDate={date => moment(date).format(DATE_FORMAT)}
                    parseDate={str => moment(str).toDate()}
                  />
                )}
              </div>
            </div>
          </Col>
          <Col xs={4}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <span>Đến ngày</span>
              <Clearfix width={8} />
              <div style={{ flex: 1 }}>
                {getFieldDecorator('toDate', {})(
                  <DateInput
                    popoverProps={{
                      targetClassName: 'el-fill'
                    }}
                    formatDate={date => moment(date).format(DATE_FORMAT)}
                    parseDate={str => moment(str).toDate()}
                  />
                )}
              </div>
            </div>
          </Col>
        </Row>
        {this.props.expand}
      </div>
    );
  }
}
