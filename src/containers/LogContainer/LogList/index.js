import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BaseListContainer } from 'containers/BaseContainer/BaseListContainer';
import { setMenuSelected } from '@redux/actions/menuAction';
import moment from 'moment';
import { DATETIME_FORMAT } from 'config';
import { getList } from 'api/logApi';
import styled from 'styled-components';
import { createForm } from 'rc-form';
import Clearfix from 'components/elements/clearfix';
import { get as _get } from 'lodash';
import  SearchContainer from './SearchContainer'


const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const TypeEvent = {
  CREATE: 'Thêm mới',
  UPDATE: 'Chỉnh sửa',
  DELETE: 'Xoá',
  LOGIN: 'Đăng nhập',
  LOGOUT: 'LOGOUT',
}

class LogList extends BaseListContainer {
  static propTypes = {
    hideRightHeaderContent: PropTypes.bool
  };

  constructor(props) {
    const funcApi = {
      getList
      // exportData
    };
    const columns = [
      {
        dataField: 'UserName',
        text: 'Tên nguời dùng'
      },
      {
        dataField: 'TypeEvent',
        text: 'Hành động',
        formatter: (cell, row) => {
          return TypeEvent[cell]
        }
      },
      {
        dataField: 'LogEvent',
        text: 'Nội dung'
      },
      {
        dataField: 'AddedOn',
        text: 'Thời gian',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATETIME_FORMAT)}</div>;
        }
      }
    ];
    super(props, funcApi, columns);

    this.state = {
      ...this.state,
      isOpenModal: false,
      isLoading: false,
      focusUserId: '',
      valueSearch: {
        // facility: this.props.facilityId
      }
    };
  }

  renderSearchContainer() {
    const form = this.props.form;
    return (
      <div>
        {!this.props.hideRightHeaderContent && form && (
          <div>
            <SearchContainer
              form={form}
              handelSubmitSearch={this.handelSubmitSearch.bind(this)}
            />
          </div>
        )}
      </div>
    );
  }

  // renderSearchContainer() {
  //   return (
  //     <div>
  //       <ConfirmElm ref={ConfirmElm => (this.ConfirmElm = ConfirmElm)} />
  //       {/* {super.renderSearchContainer()} */}
  //       <SearchContainer />
  //       {this.renderSearchExpandContainer()}
  //     </div>
  //   );
  // }

  handelSubmitSearch = values => {
    console.log('handelSubmitSearch', values)
    values.emissionType = _get(values.emissionType, 'value');
    super.handelSubmitSearch(values);
  };

  renderHeaderContainer() {
    this.options = {
      ...this.options,
      page: this.state.pagination.page,
      sizePerPage: this.state.pagination.itemPerPage
    };
    return (
      <HeaderContainer>
        <div style={{ display: 'inline-flex' }}>
          <Clearfix width={8} />
        </div>
      </HeaderContainer>
    );
  }
}

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(createForm()(LogList))
);
