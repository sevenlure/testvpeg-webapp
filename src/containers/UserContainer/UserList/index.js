import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BaseListContainer } from 'containers/BaseContainer/BaseListContainer';
import { setMenuSelected } from '@redux/actions/menuAction';
import moment from 'moment';
import { DATE_FORMAT } from 'config';
import { getList, deleteOne, changePassword } from 'api/userApi';
import slug from 'constants/slug';
import styled from 'styled-components';
import { Icon, Intent, Colors, Dialog, Classes, InputGroup, Button, AnchorButton } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { createForm } from 'rc-form';
import Message from 'rc-message';
import { Row, Col } from 'reactstrap';
import Clearfix from 'components/elements/clearfix';
import ConfirmElm from 'components/elements/confirm-element';
import { get as _get } from 'lodash';
import IconRequired from 'components/elements/required-icon';
import { STYLE } from 'config';

const NameContainer = styled.div`
  color: ${Colors.GREEN2};
  text-decoration: underline;
`;

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;

class UserList extends BaseListContainer {
  static propTypes = {
    hideRightHeaderContent: PropTypes.bool
  };

  constructor(props) {
    const funcApi = {
      getList,
      // exportData
    };
    const columns = [
      {
        dataField: '_id',
        text: '',
        headerStyle: {
          width: 10,
          minWidth: 10
        },
        formatter: (cell, row) => {
          return (
            <Icon
              style={{ cursor: 'pointer' }}
              icon={IconNames.DISABLE}
              iconSize={Icon.SIZE_LARGE}
              intent={Intent.DANGER}
              onClick={() => {
                this.handelDelete(cell);
              }}
            />
          );
        }
      },
      {
        dataField: 'tamp',
        text: '',
        headerStyle: {
          width: 10,
          minWidth: 10
        },
        formatter: (cell, row) => {
          return (
            <Icon
              style={{ cursor: 'pointer' }}
              icon={IconNames.LOCK}
              iconSize={Icon.SIZE_LARGE}
              intent={Intent.SUCCESS}
              onClick={() => {
                this.setState({
                  focusRow: row,
                  isOpenModal: true
                })
              }}
            />
          );
        }
      },
      {
        dataField: 'UserName',
        text: 'Tên nguời dùng',
        formatter: (cell, row) => {
          return (
            <Link to={slug.USER.EDIT_WITH_ID + row._id}>
              <NameContainer>{
                `${_.result(row,'FirstName','')} ${_.result(row,'LastName','')} (${cell})`
              }</NameContainer>
            </Link>
          );
        }
      },
      {
        dataField: 'Organization',
        text: 'Tổ chức',
      },
      {
        dataField: 'Email',
        text: 'Email',
      },
      {
        dataField: 'role.Name',
        text: 'Mức độ bảo mật',
      },
      {
        dataField: 'StatusCode',
        text: 'Tình trạng',
      },
      {
        dataField: 'LastAccessed',
        text: 'Đăng nhập lần cuối',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
    ];
    super(
      props,
      funcApi,
      columns,
      `${slug.USER.CREATE}`
    );

    this.state = {
      ...this.state,
      isOpenModal: false,
      isLoading: false,
      focusUserId: '',
      valueSearch: {
        // facility: this.props.facilityId
      }
    };
  }

  renderSearchContainer() {
    return (
      <div>
        <ConfirmElm ref={ConfirmElm => this.ConfirmElm = ConfirmElm} />
        {super.renderSearchContainer()}
        {this.renderSearchExpandContainer()}
      </div>
    );
  }

  handelSubmitSearch = values => {
    values.emissionType = _get(values.emissionType, 'value');
    super.handelSubmitSearch(values)
  };

  renderSearchExpandContainer = () => {
    return (
      <div>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            {/* <EmissionTypeSelect
              fieldForm={{
                form: this.props.form,
                name: 'emissionType'
              }}
            /> */}
          </Col>
        </Row>
      </div>
    );
  };

  handelDelete = _id => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      this.ConfirmElm.showConfirm({
        content: 'Bạn có muốn xoá ',
        cb: async value => {
          if (value) {
            const res = await deleteOne(_id);
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.fetData();
            }
            if (res.error) Message.error({ content: res.message });
          }
        }
      });
    }
  };

  changePassowd = () => {
    this.props.form.validateFields(async (error, value) => {
      // value.district = _.get(value.district, 'value');
      // // value.securityLevel = _.get(value.securityLevel, 'value');
      // value.role = _.get(value.role, 'value');
      console.log(error, value)
      if (error) return;
      let res = await changePassword(this.state.focusRow._id, value)
      if (res.success) {
        this.setState({
          isOpenModal: false
        })
        Message.success({ content: 'Success!!!' });
      }
      if (res.error) Message.error({ content: res.message });
    });
  };

  renderHeaderContainer() {
    this.options = {
      ...this.options,
      page: this.state.pagination.page,
      sizePerPage: this.state.pagination.itemPerPage
    };
    const { getFieldProps, getFieldError, getFieldValue } = this.props.form;
    return (
      <HeaderContainer>
        <div style={{ display: 'inline-flex' }}>
        <Link to={this.createPath}>
                <Icon
                  style={{ cursor: 'pointer' }}
                  icon={IconNames.ADD}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.SUCCESS}
                />
              </Link>

              <Clearfix width={8} />
              <Link to={this.createPath}>
                <h5
                  style={{
                    color: Colors.GREEN2,
                    cursor: 'pointer',
                    textDecoration: 'underline'
                  }}
                  className='bp3-heading'
                >
                  Thêm mục mới
                </h5>
              </Link>
                 <Dialog
          icon='lock'
          title='Thay đổi mật khẩu'
          isOpen={this.state.isOpenModal}
          onClose={() => {
            this.setState({
              isOpenModal: false
            });
          }}
          className={Classes.OVERLAY_SCROLL_CONTAINER}
          usePortal
          canOutsideClickClose
          canEscapeKeyClose
          hasBackdrop
        >
        <div>
        <Row>
          <Col xs={4}>
            <LabelContainer>
              Tên nguời dùng
            </LabelContainer>
          </Col>
          <Col xs={4}>
          <LabelContainer >
             {_.result(this.state.focusRow, 'UserName')}
              </LabelContainer>
          </Col>
        </Row>
        <Row key='passwordRow'>
            <Col xs={4}>
              <LabelContainer style={STYLE.REQUIREDFIELD}>
                Mật khẩu
                <IconRequired />
              </LabelContainer>
            </Col>
            <Col xs={4}>
              <InputGroup
                type='Password'
                placeholder='******'
                {...getFieldProps('Password', {
                  rules: [
                    {
                      required: true,
                      message: 'Mật khẩu is required'
                    },
                    {
                      min: 6,
                      message: 'Mật khẩu độ dài ít nhất 6 ký tự'
                    }
                  ]
                })}
              />
              <ErrorContainer>
                {(getFieldError('Password') || []).join(', ')}
              </ErrorContainer>
            </Col>
          </Row>,
          <Row key='passwordRepeatRow'>
            <Col xs={4}>
              <LabelContainer style={STYLE.REQUIREDFIELD}>
                Xác nhận mật khẩu
                <IconRequired />
              </LabelContainer>
            </Col>
            <Col xs={4}>
              <InputGroup
                type='Password'
                placeholder='******'
                {...getFieldProps('PasswordRepeat', {
                  rules: [
                    {
                      validator: (rule, val, callback) => {
                        let pass = getFieldValue('Password');
                        if (val !== pass)
                          callback(new Error('Xác nhận mật khẩu không đúng'));
                        callback();
                      }
                    }
                  ]
                })}
              />
              <ErrorContainer>
                {(getFieldError('PasswordRepeat') || []).join(', ')}
              </ErrorContainer>
            </Col>
          </Row>,
          <Clearfix key='clearfix_2' height={8} />
        </div>
        <div className={Classes.DIALOG_FOOTER}>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              <Button
                onClick={()=>{
                  this.setState({
                    isOpenModal: false
                  })
                }}
              >
                Huỷ bỏ
              </Button>
              <AnchorButton
                loading={this.state.isLoading}
                intent={Intent.SUCCESS}
                onClick={this.changePassowd}
              >
                Đổi mật khẩu
              </AnchorButton>
            </div>
          </div>
        </Dialog>
        </div>
      </HeaderContainer>
    );
  }

}

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(createForm()(UserList))
);


