import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import {
  Icon,
  Intent,
  InputGroup,
  NumericInput
} from '@blueprintjs/core';
import Clearfix from 'components/elements/clearfix';
import { IconNames } from '@blueprintjs/icons';
import { createForm } from 'rc-form';
// import * as _ from 'lodash';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import IconRequired from 'components/elements/required-icon';
import { STYLE } from 'config';
import QuanHuyenSelect from 'components/elements/select-quan-huyen';
import RoleSelect from 'components/elements/select-role';
import { SELECT_ITEM_DEFAULT } from 'config';

const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

const HeaderContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-start;
  cursor: pointer;
`;

class UserForm extends React.Component {
  static propTypes = {
    isEdit: PropTypes.bool,
    initialValue: PropTypes.object,
    slugBack: PropTypes.string,
    handelSubmit: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {};
  }

  setInitialValue(keyPath, defaultValue) {
    if (this.props.isEdit) {
      let value = _.result(this.props.initialValue, keyPath, defaultValue);
      if (!value) value = defaultValue;
      return value;
    }
    return defaultValue;
  }

  setSelInitialValue(keyPath, defaultValue, nameLabel) {
    if (this.props.isEdit) {
      let selItem = _.result(this.props.initialValue, keyPath);
      if (!selItem) return defaultValue;
      return {
        value: selItem._id,
        label: nameLabel ? selItem[nameLabel] : selItem.Name
      };
    }
    return defaultValue;
  }

  submit = () => {
    this.props.form.validateFields((error, value) => {
      value.district = _.get(value.district, 'value');
      // value.securityLevel = _.get(value.securityLevel, 'value');
      value.role = _.get(value.role, 'value');
      console.log(value)
      if (error) return;
      if (this.props.handelSubmit) this.props.handelSubmit(value);
    });
  };

  render() {
    const { getFieldProps, getFieldError, getFieldValue } = this.props.form;
    return (
      <div>
        <Row>
          <Col xs={4}>
            <HeaderContainer>
              <Icon
                icon={IconNames.SAVED}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.PRIMARY}
                onClick={this.submit}
              />
              <Clearfix width={8} />
              <Link to={this.props.slugBack ? this.props.slugBack : slug.BASE}>
                <Icon
                  icon={IconNames.DISABLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                />
              </Link>
            </HeaderContainer>
          </Col>
        </Row>
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Tên nguời dùng
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Tên nguời dùng'
              {...getFieldProps('UserName', {
                initialValue: this.setInitialValue('UserName', ''),
                rules: [
                  {
                    required: true,
                    message: 'Tên nguời dùng is required'
                  },
                  {
                    min: 6,
                    message: 'Tên nguời dùng độ dài ít nhất 6 ký tự'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('UserName') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Tên
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Tên'
              {...getFieldProps('FirstName', {
                initialValue: this.setInitialValue('FirstName', ''),
                rules: [
                  {
                    required: true,
                    message: 'Tên is required'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('FirstName') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Họ
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Họ'
              {...getFieldProps('LastName', {
                initialValue: this.setInitialValue('LastName', ''),
                rules: [
                  {
                    required: true,
                    message: 'Họ is required'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('LastName') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        {!this.props.isEdit && [
          <Row key='passwordRow'>
            <Col xs={4}>
              <LabelContainer style={STYLE.REQUIREDFIELD}>
                Mật khẩu
                <IconRequired />
              </LabelContainer>
            </Col>
            <Col xs={4}>
              <InputGroup
                type='Password'
                placeholder='******'
                {...getFieldProps('Password', {
                  initialValue: this.setInitialValue('Password', ''),
                  rules: [
                    {
                      required: true,
                      message: 'Mật khẩu is required'
                    },
                    {
                      min: 6,
                      message: 'Mật khẩu độ dài ít nhất 6 ký tự'
                    }
                  ]
                })}
              />
              <ErrorContainer>
                {(getFieldError('Password') || []).join(', ')}
              </ErrorContainer>
            </Col>
          </Row>,
          <Clearfix key='clearfix_1' height={8} />,
          <Row key='passwordRepeatRow'>
            <Col xs={4}>
              <LabelContainer style={STYLE.REQUIREDFIELD}>
                Xác nhận mật khẩu
                <IconRequired />
              </LabelContainer>
            </Col>
            <Col xs={4}>
              <InputGroup
                type='Password'
                placeholder='******'
                {...getFieldProps('PasswordRepeat', {
                  initialValue: this.setInitialValue('PasswordRepeat', ''),
                  rules: [
                    {
                      validator: (rule, val, callback) => {
                        let pass = getFieldValue('Password');
                        if (val !== pass)
                          callback(new Error('Xác nhận mật khẩu không đúng'));
                        callback();
                      }
                    }
                  ]
                })}
              />
              <ErrorContainer>
                {(getFieldError('PasswordRepeat') || []).join(', ')}
              </ErrorContainer>
            </Col>
          </Row>,
          <Clearfix key='clearfix_2' height={8} />
        ]}
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Email
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Email'
              {...getFieldProps('Email', {
                initialValue: this.setInitialValue('Email', ''),
                rules: [
                  {
                    required: true,
                    message: 'Email is required'
                  },
                  {
                    type: 'email',
                    message: 'Định dạng email không đúng'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('Email') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Quận/Huyện</LabelContainer>
          </Col>
          <Col xs={4}>
            <QuanHuyenSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'district',
                option: {
                  initialValue: this.setSelInitialValue(
                    'district',
                    SELECT_ITEM_DEFAULT
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Mức độ bảo mật</LabelContainer>
          </Col>
          <Col xs={4}>
            <RoleSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'role',
                option: {
                  initialValue: this.setSelInitialValue(
                    'role',
                    SELECT_ITEM_DEFAULT
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Fax</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Fax'
              {...getFieldProps('Fax', {
                initialValue: this.setInitialValue('Fax', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Điện thoại</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumericInput
              fill
              placeholder='Điện thoại'
              {...getFieldProps('Telephone', {
                initialValue: this.setInitialValue('Telephone', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Tổ chức</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Tổ chức'
              {...getFieldProps('Organization', {
                initialValue: this.setInitialValue('Organization', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Đơn vị tổ chức</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Đơn vị tổ chức'
              {...getFieldProps('OrganizationUnit', {
                initialValue: this.setInitialValue('OrganizationUnit', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
      </div>
    );
  }
}

export default createForm()(UserForm);
