import React from 'react';
import slug from 'constants/slug';
// import { getOne as getFacility } from 'api/userApi';
import { create } from 'api/userApi';
import Message from 'rc-message';
// import * as _ from 'lodash';
import UserForm from '../UserForm';
// import { getQueryString } from 'utils/fnUtil';

export default class UserCreate extends React.Component {
  static propTypes = {};

  state = {
    isLoaded: false,
    isLoading: false
  };


  handelSubmit = async values => {
    this.setState({
      isLoading: true
    });

    const res = await create(_.pickBy(values, _.identity));
    if (res.success) {
      Message.success({ content: 'Success!!!' });
      this.props.history.push(slug.ADMIN.BASE);
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div>
        <UserForm
            slugBack={slug.ADMIN.BASE}
            handelSubmit={this.handelSubmit}
          />
      </div>
    );
  }
}
