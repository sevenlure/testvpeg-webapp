import React from 'react';
import slug from 'constants/slug';
import { getOne, updateOne } from 'api/userApi';
import Message from 'rc-message';
import UserForm from '../UserForm';

export default class UserEdit extends React.Component {
  static propTypes = {};

  state = {
    _idObject: '',
    isLoaded: false,
    isLoading: false,
    initialValue: null
  };

  async checkValidate() {
    const _id = _.result(this.props, 'match.params._id');
    if (_id) {
      const res = await getOne(_id);
      if (res.success && res.data) {
        this.setState({
          _idObject: res.data._id,
          isLoading: false,
          isLoaded: true,
          initialValue: res.data
        });
      }
    } else {
      //this.props.history.push(slug.CO_SO.LIST);
    }
  }

  async componentWillMount() {
    await this.checkValidate();
  }

  handelSubmit = async values => {
    this.setState({
      isLoading: true
    });

    const res = await updateOne(this.state._idObject, values);
    if (res.success) {
      Message.success({ content: 'Success!!!' });
      this.props.history.push(slug.ADMIN.BASE);
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div>
        {this.state.isLoaded && (
          <UserForm
            isEdit
            slugBack={slug.ADMIN.BASE}
            initialValue={this.state.initialValue}
            handelSubmit={this.handelSubmit}
          />
        )}
      </div>
    );
  }
}
