import React from 'react';
import { Route } from 'react-router-dom';
import slug from 'constants/slug';
import UserCreate from './UserCreate';
import UserEdit from './UserEdit';
// import FacilityeiaView from './FacilityeiaView';

export default props => [
  <Route
    key={slug.USER.CREATE}
    path={slug.USER.CREATE}
    render={matchProps => <UserCreate {...matchProps} {...props} />}
  />,
  // <Route
  //   key={slug.FACILITY_EIA.VIEW}
  //   path={slug.FACILITY_EIA.VIEW}
  //   render={matchProps => <FacilityeiaView {...matchProps} {...props} />}
  // />,
  <Route
    key={slug.USER.EDIT}
    path={slug.USER.EDIT}
    render={matchProps => <UserEdit {...matchProps} {...props} />}
  />
];
