import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BaseListContainer } from 'containers/BaseContainer/BaseListContainer';
import { setMenuSelected } from '@redux/actions/menuAction';
import moment from 'moment';
import { DATE_FORMAT } from 'config';
import { getList, deleteOne, exportData } from 'api/facilitypermitApi';
import slug from 'constants/slug';
import styled from 'styled-components';
import { Colors } from '@blueprintjs/core';
import Message from 'rc-message';
import { Row, Col } from 'reactstrap';
import YearSelect from 'components/elements/select-year';
import PermitTypeSelect from 'components/elements/select-permit-type';
import Clearfix from 'components/elements/clearfix';
import { createForm } from 'rc-form';

const NameContainer = styled.div`
  color: ${Colors.GREEN2};
  text-decoration: underline;
`;

class FacilitypermitList extends BaseListContainer {
  static propTypes = {
    facilityId: PropTypes.string,
    hideRightHeaderContent: PropTypes.bool
  };

  constructor(props) {
    const funcApi = {
      getList,
      exportData
    };
    const columns = [
      // {
      //   dataField: '_id',
      //   text: '',
      //   headerStyle: {
      //     width: 10,
      //     minWidth: 10,
      //   },
      //   formatter: (cell, row) => {
      //     return (
      //       <Icon
      //         style={{ cursor: 'pointer' }}
      //         icon={IconNames.DISABLE}
      //         iconSize={Icon.SIZE_LARGE}
      //         intent={Intent.DANGER}
      //         onClick={() => {
      //           this.handelDelete(cell);
      //         }}
      //       />
      //     );
      //   }
      // },
      {
        dataField: 'facility.FacilityNameFull',
        text: 'Tên Cơ sở',
        formatter: (cell, row) => {
          return (
            <Link to={slug.FACILITY_PERMIT.VIEW_WITH_ID + row._id}>
              <NameContainer>{cell}</NameContainer>
            </Link>
          );
        }
      },
      {
        dataField: 'facility.industrialArea',
        text: '	Khu công nghiệp/Cụm Công nghiệp',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'facility.district',
        text: 'Quận/Huyện',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'facility.sector',
        text: 'Ngành/Nghề',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'IssueDate',
        text: 'Ngày cấp',
        formatter: (cell, row) => {
          return <div>{cell ? moment(cell).format(DATE_FORMAT) : ''}</div>;
        }
      },
      {
        dataField: 'permitType',
        text: 'Loại',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'AddedOn',
        text: 'Thêm vào',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
      {
        dataField: 'AddedBy',
        text: 'Thêm bởi'
      },
      {
        dataField: 'UpdatedOn',
        text: 'Cập nhật ngày',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
      {
        dataField: 'UpdatedBy',
        text: 'Cập nhật bởi',
        formatter: (cell, row) => {
          return <div>{cell}</div>;
        }
      }
    ];
    super(
      props,
      funcApi,
      columns,
      `${slug.FACILITY_PERMIT.CREATE}?facilityId=${props.facilityId}`,
      'facilityPermit'
    );

    this.state = {
      ...this.state,
      valueSearch: {
        facility: this.props.facilityId
      }
    };
  }

  renderSearchContainer() {
    return (
      <div>
        {super.renderSearchContainer()}
        {this.renderSearchExpandContainer()}
      </div>
    );
  }

  renderSearchExpandContainer = () => {
    const form = this.props.form;
    return (
      <div>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <YearSelect
              fieldForm={{
                form: form,
                name: 'year'
              }}
            />
          </Col>
          <Col xs={4}>
            <PermitTypeSelect
              fieldForm={{
                form: form,
                name: 'permitType'
              }}
            />
          </Col>
        </Row>
      </div>
    );
  };

  handelDelete = _id => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      this.ConfirmElm.showConfirm({
        content: 'Bạn có muốn xoá ',
        cb: async value => {
          if (value) {
            const res = await deleteOne(_id);
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.fetData();
            }
            if (res.error) Message.error({ content: res.message });
          }
        }
      });
    }
  };

  handelSubmitSearch(values) {
    values.year = _.get(values.year, 'value');
    values.permitType = _.get(values.permitType, 'value');
    super.handelSubmitSearch(values);
  }
}

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(createForm()(FacilitypermitList))
);
