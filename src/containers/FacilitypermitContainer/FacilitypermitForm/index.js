import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { Icon, Intent} from '@blueprintjs/core';
import Clearfix from 'components/elements/clearfix';
import { IconNames } from '@blueprintjs/icons';
import { createForm } from 'rc-form';
// import * as _ from 'lodash';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import IconRequired from 'components/elements/required-icon';
import FacilitySelect from 'components/elements/select-facility';
import { DateInput } from '@blueprintjs/datetime';
import { DATE_FORMAT, STYLE } from 'config';
import moment from 'moment';
import { convertDateFromSever as convertDate } from 'utils/fnUtil';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, ContentState } from 'draft-js';
import htmlToDraft from 'html-to-draftjs';
import { convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import PermitTypeSelect from 'components/elements/select-permit-type';
import {  COLOR } from 'config';
import dynamicRoleHOC from 'hoc/dynamicRole'

const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

const HeaderContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-start;
  cursor: pointer;
`;
@dynamicRoleHOC()
class FacilitypermitForm extends React.Component {
  static propTypes = {
    isEdit: PropTypes.bool,
    initialValue: PropTypes.object,
    slugBack: PropTypes.string,
    handelSubmit: PropTypes.func
    // facility: PropTypes.shape({
    //   _id: PropTypes.string,
    //   FacilityNameFull: PropTypes.string
    // })
  };

  constructor(props) {
    super(props);

    this.state = {
      editorState:
        this.props.isEdit && this.props.initialValue.Notes
          ? this.convertFromHTML2EditorState(this.props.initialValue.Notes)
          : EditorState.createEmpty()
    };
  }

  convertFromHTML2EditorState = rawHtml => {
    const contentBlock = htmlToDraft(rawHtml);
    const contentState = ContentState.createFromBlockArray(
      contentBlock.contentBlocks
    );
    return EditorState.createWithContent(contentState);
  };

  onEditorStateChange = editorState => {
    this.setState({
      editorState
    });
  };

  setInitialValue(keyPath, defaultValue) {
    if (this.props.isEdit) {
      let value = _.result(this.props.initialValue, keyPath, defaultValue);
      if (!value) value = defaultValue;
      return value;
    }
    return defaultValue;
  }

  setSelInitialValue(keyPath, defaultValue, nameLabel) {
    if (this.props.isEdit) {
      let selItem = _.result(this.props.initialValue, keyPath);
      if (!selItem) return defaultValue;
      return {
        value: selItem._id,
        label: nameLabel ? selItem[nameLabel] : selItem.Name
      };
    }
    return defaultValue;
  }

  submit = () => {
    this.props.form.validateFields((error, value) => {
      value.facility = _.get(value.facility, 'value');
      value.permitType = _.get(value.permitType, 'value');
      console.log(value);
      if (error) return;
      if (this.props.handelSubmit) this.props.handelSubmit(value);
    });
  };

  render() {
    const { getFieldProps, getFieldError, getFieldValue } = this.props.form;
    return (
      <div>
        <h4
          className="bp3-heading"
          style={{
            color: COLOR.SUCCESS
          }}
        >
          Chi tiết Giấy phép
        </h4>
        <Row>
          <Col xs={4}>
            <HeaderContainer>
              <Icon
                icon={IconNames.SAVED}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.PRIMARY}
                onClick={this.submit}
              />
              <Clearfix width={8} />
              <Link to={this.props.slugBack ? this.props.slugBack : slug.BASE}>
                <Icon
                  icon={IconNames.DISABLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                />
              </Link>
            </HeaderContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Tên Cơ sở
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <FacilitySelect
              isDisabled={this.props.facility ? true : false}
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'facility',
                option: {
                  initialValue: this.props.facility
                    ? {
                        value: this.props.facility._id,
                        label: this.props.facility.FacilityNameFull
                      }
                    : null,
                  rules: [
                    {
                      required: true,
                      message: 'Tên Cơ sở is required'
                    }
                  ]
                }
              }}
            />
            <ErrorContainer>
              {(getFieldError('facility') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Loại giấy phép <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <PermitTypeSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'permitType',
                option: {
                  initialValue: this.setSelInitialValue('permitType'),
                  rules: [
                    {
                      required: true,
                      message: 'Loại giấy phép is required'
                    }
                  ]
                }
              }}
            />
            <ErrorContainer>
              {(getFieldError('permitType') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Ngày cấp <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder="Ngày cấp"
              {...getFieldProps('IssueDate', {
                initialValue: convertDate(
                  this.setInitialValue('IssueDate', null)
                ),
                rules: [
                  {
                    required: true,
                    message: 'Ngày cấp is required'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('IssueDate') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Ngày hết hạn</LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder="Ngày hết hạn"
              {...getFieldProps('ExpiryDate', {
                initialValue: convertDate(
                  this.setInitialValue('ExpiryDate', null)
                ),
                rules: [
                  (rule, value, callback) => {
                    //Ngay het hạn đuợc phép để rỗng
                    if(!value) callback();
                    let IssueDate = getFieldValue('IssueDate');
                    if (value > IssueDate) callback();
                    else callback(new Error('Ngày hết hạn phải lớn hơn ngày cấp'));
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('ExpiryDate') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Ghi chú</LabelContainer>
          </Col>
          <Col xs={6}>
            <Editor
              editorState={this.state.editorState}
              wrapperClassName="demo-wrapper"
              editorClassName="demo-editor"
              onEditorStateChange={this.onEditorStateChange}
              {...getFieldProps('Notes', {
                initialValue: this.props.isEdit
                  ? draftToHtml(
                      convertToRaw(this.state.editorState.getCurrentContent())
                    )
                  : '',
                getValueFromEvent: value => {
                  return draftToHtml(
                    convertToRaw(this.state.editorState.getCurrentContent())
                  );
                }
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
      </div>
    );
  }
}

export default createForm()(FacilitypermitForm);
