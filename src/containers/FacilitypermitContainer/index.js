import React from 'react';
import { Route } from 'react-router-dom';
import slug from 'constants/slug';
import FacilitypermitList from './FacilitypermitList';
import FacilitypermitCreate from './FacilitypermitCreate';
import FacilitypermitEdit from './FacilitypermitEdit';
import FacilitypermitView from './FacilitypermitView';

export default props => [
  <Route
    exact
    key={slug.FACILITY_PERMIT.LIST}
    path={slug.FACILITY_PERMIT.LIST}
    render={matchProps => <FacilitypermitList {...matchProps} {...props} />}
  />,
  <Route
    key={slug.FACILITY_PERMIT.CREATE}
    path={slug.FACILITY_PERMIT.CREATE}
    render={matchProps => <FacilitypermitCreate {...matchProps} {...props} />}
  />,
  <Route
    key={slug.FACILITY_PERMIT.VIEW}
    path={slug.FACILITY_PERMIT.VIEW}
    render={matchProps => <FacilitypermitView {...matchProps} {...props} />}
  />,
  <Route
    key={slug.FACILITY_PERMIT.EDIT}
    path={slug.FACILITY_PERMIT.EDIT}
    render={matchProps => <FacilitypermitEdit {...matchProps} {...props} />}
  />
];