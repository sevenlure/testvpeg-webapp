import React from 'react';
import slug from 'constants/slug';
import { create } from 'api/facilitypermitApi';
import { getOne as getFacility } from 'api/cosoApi';
import Message from 'rc-message';
// import * as _ from 'lodash';
import FacilitypermitForm from '../FacilitypermitForm';
import { getQueryString } from 'utils/fnUtil';

export default class FacilitypermitCreate extends React.Component {
  static propTypes = {};

  state = {
    isLoaded: true,
    isLoading: false,
    facility: null,
    facilityId: ''
  };

  async checkValidate() {
    const queryString = getQueryString(this.props.location.search);
    const _id = queryString.facilityId;
    if (_id) {
      const res = await getFacility(_id);
      if (res.success) {
        this.setState({
          facility: res.data,
          facilityId: res.data._id,
          isLoading: false,
          isLoaded: true
        });
      }
    } else {
      //this.props.history.push(slug.CO_SO.LIST);
    }
  }

  async componentWillMount() {
    await this.checkValidate();
  }

  handelSubmit = async values => {
    this.setState({
      isLoading: true
    });
    console.log(values)
    const res = await create(_.pickBy(values, _.identity));
    if (res.success && res.data) {
      Message.success({ content: 'Success!!!' });
      this.props.history.push(slug.FACILITY_PERMIT.VIEW_WITH_ID + res.data._id);
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div>
        {this.state.isLoaded && (
          <FacilitypermitForm
            slugBack={slug.FACILITY_PERMIT.LIST}
            facility={this.state.facility? this.state.facility: null}
            handelSubmit={this.handelSubmit}
          />
        )}
      </div>
    );
  }
}
