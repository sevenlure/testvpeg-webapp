import React from 'react';
// import PropTypes from 'prop-types';
import Clearfix from 'components/elements/clearfix';
import UserList from 'containers/UserContainer/UserList';

export default  class UserPanel extends React.Component {
  render() {
    return (
      <div style={{ width: '100%' }}>
        <Clearfix height={8} />
        <UserList hideRightHeaderContent />
      </div>
    )
  }
}