import React from 'react';
import Clearfix from 'components/elements/clearfix';
import { Tab, Tabs } from '@blueprintjs/core';
import PageBuilding from 'components/pageBuilding'
import UserPanel from './UserPanel'
import RolePanel from './RolePanel'
import LogPanel from './LogPanel'
import PageLockHOC from 'hoc/pageLock'


const panelList = [
  {
    key: 'user',
    title: 'Quản lý người dùng'
  },
  {
    key: 'role',
    title: 'Quản lý phân quyền'
  },
  {
    key: 'log',
    title: 'Quản lý lịch sử'
  },
];


@PageLockHOC(['admin.full'])
export default class AdminContainer extends React.Component {
  static propTypes = {};

  state = {};


  renderPanel = keyPanel => {
    switch (keyPanel) {
      case 'user': {
        return <UserPanel />
      }
      case 'role': {
        return <RolePanel />
      }

      case 'log': {
        return <LogPanel />
      }
      
      default: {
        return <PageBuilding />;
      }
    }
  };

  componentDidMount() {}

  render() {
    return (
      <div>
        <Clearfix height={16} />
        <Tabs renderActiveTabPanelOnly={true} style={{ color: 'red' }}>
          {panelList.map(item => (
            <Tab
              id={item.key}
              key={item.key}
              title={item.title}
              panel={this.renderPanel(item.key)}
            />
          ))}
        </Tabs>
      </div>
    );
  }
}
