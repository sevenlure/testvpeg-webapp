import React from 'react';
import Clearfix from 'components/elements/clearfix';
import Collapse from 'rc-collapse';
import { Row, Col } from 'reactstrap';
import { IconNames } from '@blueprintjs/icons';
import { Checkbox, Spinner, Intent, Icon } from '@blueprintjs/core';
import { ListGroupItem } from 'reactstrap';
import { permissionRole, moduleName, permissionRoleName } from 'constants/role';
import RoleSelect from 'components/elements/select-role';
import { getOne, updateOne } from 'api/roleApi';
import 'rc-collapse/assets/index.css';
import styled from 'styled-components';
import Message from 'rc-message';

const HeaderContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-start;
  cursor: pointer;
`;

const Panel = Collapse.Panel;

const rolePaths = _.keys(flatObject(permissionRole));

function flatObject(input) {
  function flat(res, key, val, pre = '') {
    const prefix = [pre, key].filter(v => v).join('.');
    return typeof val === 'object'
      ? Object.keys(val).reduce(
          (prev, curr) => flat(prev, curr, val[curr], prefix),
          res
        )
      : Object.assign(res, { [prefix]: val });
  }

  return Object.keys(input).reduce(
    (prev, curr) => flat(prev, curr, input[curr]),
    {}
  );
}

// function getDeepKeys(obj) {
//   var keys = [];
//   for (var key in obj) {
//     keys.push(key);
//     if (typeof obj[key] === 'object') {
//       var subkeys = getDeepKeys(obj[key]);
//       keys = keys.concat(
//         subkeys.map(function(subkey) {
//           return key + '.' + subkey;
//         })
//       );
//     }
//   }
//   return keys;
// }

// function objectDeepKeys(obj) {
//   return Object.keys(obj)
//     .filter(key => obj[key] instanceof Object)
//     .map(key => objectDeepKeys(obj[key]).map(k => `${key}.${k}`))
//     .reduce((x, y) => x.concat(y), Object.keys(obj));
// }

export default class UserPanel extends React.Component {
  updateRole = (path, value) => {
    this.setState(
      {
        targetedRole: _.update(this.state.targetedRole, path, () => value)
      },
      () => {
        console.log(this.state);
      }
    );
  };

  state = {
    targetedRole: null,
    _idObject: '',

    // role: permissionRole
  };

   submit = async ()=>{
     this.setState({
       isLoading: true
     })
    const res = await updateOne(this.state._idObject, {
      permissionRole: this.state.targetedRole
    });
    if (res.success) {
      Message.success({ content: 'Success!!!' });
    } else {
      Message.error({ content: 'Đã có lỗi xảy ra!!!' })
    }
    this.setState({
      isLoading: false
    })
  }

  async loadRole(_id) {
    this.setState({ isLoading: true });
    const res = await getOne(_id);
    // console.log(res);
    let targetedRole = null;
    let _idObject = '';
    if (res.success) {
      targetedRole = res.data.permissionRole || {};
      _idObject = res.data._id
    }
    this.setState({ isLoading: false, targetedRole, _idObject });
  }

  render() {
    const modules = _.keys(permissionRole);
    // console.log(modules);
    // const facilityRoles = rolePaths.filter(path => path.includes('facility.'));
    // console.log(facilityRoles);
    // console.log(rolePaths);
    return (
      <div style={{ width: '100%' }}>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <HeaderContainer>
              <Icon
                icon={IconNames.SAVED}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.PRIMARY}
                onClick={this.submit}
              />
              <Clearfix width={8} />
            </HeaderContainer>
          </Col>
        </Row>

        <Row style={{ justifyContent: 'center' }}>
          <Col xs={8}>
            <RoleSelect
              onChange={selected => {
                console.log('assasa', selected);
                this.loadRole(selected.value);
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        {this.state.isLoading && <Spinner intent={Intent.PRIMARY} size={200} />}

        {this.state.targetedRole && (
          <Row style={{ justifyContent: 'center' }}>
            <Col xs={8}>
              <Collapse defaultActiveKey={modules}>
                {modules.map(mol => {
                  const molduleRoles = rolePaths.filter(path =>
                    path.includes(mol + '.')
                  );       
                  return (
                    <Panel key={mol} header={moduleName[mol]} 
                    headerClass="rc-collampse-header-custom"
                    >
                      {molduleRoles.map(molRule => {
                        return (
                          <ListGroupItem key={molRule}>
                            <Col xs={8}>
                              <Checkbox
                                defaultChecked={_.result(
                                  this.state.targetedRole,
                                  molRule,
                                  false
                                )}
                                className='bp3-align-right'
                                label={permissionRoleName[molRule]}
                                onChange={e => {
                                  // console.log(molRule, e.target.checked);
                                  this.updateRole(molRule, e.target.checked);
                                }}
                              />
                            </Col>
                          </ListGroupItem>
                        );
                      })}
                    </Panel>
                  );
                })}
              </Collapse>
            </Col>
          </Row>
        )}

        <Clearfix height={8} />
      </div>
    );
  }
}
