import React from 'react';
// import PropTypes from 'prop-types';
import Clearfix from 'components/elements/clearfix';
import LogList from 'containers/LogContainer/LogList';

export default  class LogPanel extends React.Component {
  render() {
    return (
      <div style={{ width: '100%' }}>
        <Clearfix height={8} />        
        <LogList />
      </div>
    )
  }
}