import React from 'react';
import PropTypes from 'prop-types';
import BootstrapTable from 'react-bootstrap-table-next';
import { updateChildren } from 'api/lookuplistitemApi';
import cellEditFactory from 'react-bootstrap-table2-editor';

export default class DanhMucItemPanel extends React.PureComponent {
  static propTypes = {
    childrenData: PropTypes.array.isRequired,
    lookuplistId: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      childrenData: this.props.childrenData,
      _id: props.lookuplistId
    };
  }

  handleEditData = async ({ rowId, dataField, newValue }) => {
    const res = await updateChildren(this.state._id, {
      key: rowId,
      name: newValue
    });

    if (res.success) {
      const data = this.state.childrenData.map(item => {
        if (item.key === rowId) item['name'] = newValue;
        return item;
      });
      this.setState({
        childrenData: data
      });
    }
  };

  render() {
    return (
      <div className="docs-panel-stack-contents-example">
        <DataList
          dataList={this.state.childrenData}
          handleEditData={this.handleEditData}
        />
      </div>
    );
  }
}

class DataList extends React.Component {
  static propTypes = {
    IsHasExtra: PropTypes.bool,
    dataList: PropTypes.array.isRequired,
    handleEditData: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.columns = [
      {
        dataField: 'name',
        text: 'Tên'
      }
    ];

    this.columns = props.IsHasExtra
      ? [
          ...this.columns,
          {
            dataField: 'ExtraInfo',
            text: 'ExtraInfo',
            formatter: (cell, row) => {
              return (
                <div
                  dangerouslySetInnerHTML={{
                    __html: cell
                  }}
                />
              );
            }
          }
        ]
      : this.columns;
  }

  cellEdit = {
    mode: 'dbclick'
  };

  handleTableChange = (type, { cellEdit, data }) => {
    switch (type) {
      case 'cellEdit': {
        if (this.props.handleEditData) this.props.handleEditData(cellEdit);
        break;
      }
      default:
        break;
    }
  };

  render() {
    return (
      <BootstrapTable
        remote={{ cellEdit: true }}
        keyField="key"
        data={this.props.dataList}
        columns={this.columns}
        cellEdit={cellEditFactory(this.cellEdit)}
        onTableChange={this.handleTableChange}
      />
    );
  }
}
