import React from 'react';
import PropTypes from 'prop-types';
import BootstrapTable from 'react-bootstrap-table-next';
import { COLOR } from 'config';
import Spinner from '../Spinner';
import {
  getLookupById,
  updateOne,
  create,
  deleteOneWithCheck
} from 'api/lookuplistitemApi';
import cellEditFactory from 'react-bootstrap-table2-editor';
import DanhMucChildrenPanel from '../3_ChildrenPanelStack';
import { Button,Callout } from '@blueprintjs/core';
import ReactDOM from 'react-dom';
import Message from 'rc-message';
import { Icon, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import Confirm from 'components/elements/confirm-element';

class DataList2 extends React.Component {
  static propTypes = {
    IsHasExtra: PropTypes.bool,
    dataList: PropTypes.array.isRequired,
    handleEditData: PropTypes.func,
    openNewPanel: PropTypes.func,
    handleDelete: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.columns = [
      {
        dataField: 'Name',
        text: 'Tên',
        formatter: (cell, row) => {
          if (!props.IsHasChildren) return cell;
          else
            return (
              <a
                href="_"
                onClick={e => {
                  e.preventDefault();
                  this.props.openNewPanel(row);
                }}
                style={{ color: COLOR.SUCCESS }}
              >
                {cell}
              </a>
            );
        }
      },
      {
        dataField: 'DisplaySequence',
        text: 'Thứ tự',
        style: {
          width: 200
        }
      }
    ];

    this.columns = props.IsHasExtra
      ? [
          ...this.columns,
          {
            dataField: 'ExtraInfo',
            text: 'ExtraInfo',
            formatter: (cell, row) => {
              return (
                <div
                  dangerouslySetInnerHTML={{
                    __html: cell
                  }}
                />
              );
            }
          }
        ]
      : this.columns;

    this.columns = [
      ...this.columns,
      {
        dataField: '_id',
        text: '',
        style: { width: 20 },
        formatter: (cell, row) => {
          return (
            <Icon
              style={{ cursor: 'pointer' }}
              icon={IconNames.DISABLE}
              iconSize={Icon.SIZE_LARGE}
              intent={Intent.DANGER}
              onClick={() => {
                if (this.props.handleDelete) this.props.handleDelete(cell);
              }}
            />
          );
        }
      }
    ];
  }

  cellEdit = {
    mode: 'dbclick'
  };

  handleTableChange = (type, { cellEdit, data }) => {
    switch (type) {
      case 'cellEdit': {
        if (this.props.handleEditData) this.props.handleEditData(cellEdit);
        break;
      }
      default:
        break;
    }
  };

  render() {
    return (
      <BootstrapTable
        remote={ true }
        // cellEdit={ {
        //   mode: 'click'
        // } }
        keyField="_id"
        data={this.props.dataList}
        columns={this.columns}
        cellEdit={cellEditFactory(this.cellEdit)}
        onTableChange={this.handleTableChange}
      />
    );
  }
}

export default class DanhMucItemPanel extends React.PureComponent {
  static propTypes = {
    lookuplistId: PropTypes.string.isRequired,
    IsHasExtra: PropTypes.bool,
    IsHasChildren: PropTypes.bool,
    referenced: PropTypes.object
  };

  state = {
    isLoading: true,
    lookuplistitemData: []
  };
  componentWillMount = async () => {
    const res = await getLookupById(this.props.lookuplistId);
    this.setState({
      isLoading: false
    });
    if (res.success)
      this.setState({
        lookuplistitemData: res.data
      });
  };

  handleEditData = async ({ rowId, dataField, newValue }) => {
    const res = await updateOne(rowId, {
      [dataField]: newValue
    });

    if (res.success) {
      const data = this.state.lookuplistitemData.map(item => {
        if (item._id === rowId) item[dataField] = newValue;
        return item;
      });
      this.setState({
        lookuplistitemData: data
      });
    }
  };

  handleAddItem = async () => {
    const res = await create({
      lookuplist: this.props.lookuplistId,
      LookupListID: this.props.LookupListID,
      Name: 'Tên danh mục',
      DisplaySequence: 0
    });
    if (res.success) {
      this.componentWillMount();
    } else Message.error({ content: res.message });
  };

  renderAdd() {
    if (document.getElementsByClassName('bp3-panel-stack-header')[0])
      return ReactDOM.createPortal(
        <Button
          onClick={this.handleAddItem}
          icon="add"
          className="bp3-intent-success"
        >
          Thêm mới
        </Button>,
        document.getElementsByClassName('bp3-panel-stack-header')[0]
      );
    else return <div />;
  }

  handleDelete = _id => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      this.ConfirmElm.showConfirm({
        content: 'Bạn có muốn xoá ',
        cb: async value => {
          if (value) {
            const res = await deleteOneWithCheck(_id, this.props.referenced);
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.componentWillMount();
            }
            if (res.error) {
              if (res.countReference) {
                this.ConfirmElm.setState({
                  notHide: true,
                  content: <Callout intent="danger">
                  Không thể xoá.
                  <br />
                  Danh mục này đang được {res.countReference} tham chiếu đến.
                  </Callout>
                })
              } else Message.error({ content: res.message });
            }
          }
        }
      });
    }
  };

  render() {
    return (
      <div className="docs-panel-stack-contents-example">
        <Confirm ref={ConfirmElm => (this.ConfirmElm = ConfirmElm)} />
        {this.state.isLoading && <Spinner />}
        {!this.state.isLoading && (
          <DataList2
            openNewPanel={this.openNewPanel}
            dataList={this.state.lookuplistitemData}
            handleEditData={this.handleEditData}
            IsHasExtra={this.props.IsHasExtra}
            IsHasChildren={this.props.IsHasChildren}
            handleDelete={this.handleDelete}
          />
        )}
        {this.renderAdd()}
      </div>
    );
  }

  openNewPanel = lookuplistitem => {
    const panelNumber = this.props.panelNumber + 1;
    this.props.openPanel({
      component: DanhMucChildrenPanel,
      props: {
        panelNumber,
        childrenData: lookuplistitem.children,
        lookuplistId: lookuplistitem._id
      },
      title: lookuplistitem.Name
    });
  };
}
