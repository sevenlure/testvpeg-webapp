import React from 'react';
import PropTypes from 'prop-types';
import { getAll } from 'api/lookuplistApi';
import BootstrapTable from 'react-bootstrap-table-next';
import { COLOR } from 'config';
import Spinner from '../Spinner';
import DanhMucItemPanel from '../2_LookuplistitemPanelStack';

export default class DanhMucPanel extends React.PureComponent {
  state = {
    isLoading: true,
    lookuplistData: []
  };
  componentWillMount = async () => {
    const res = await getAll();
    this.setState({
      isLoading: false
    });
    if (res.success)
      this.setState({
        lookuplistData: res.data
      });
  };
  
  render() {
    return (
      <div>
        {this.state.isLoading && <Spinner />}
        {!this.state.isLoading && (
          <DataList
            openNewPanel={this.openNewPanel}
            dataList={this.state.lookuplistData}
          />
        )}
      </div>
    );
  }

  openNewPanel = lookuplist => {
    const panelNumber = this.props.panelNumber + 1;
    this.props.openPanel({
      component: DanhMucItemPanel,
      props: {
        panelNumber,
        lookuplistId: lookuplist._id,
        LookupListID: lookuplist.LookupListID,
        IsHasExtra: lookuplist.IsHasExtra,
        IsHasChildren: lookuplist.IsHasChildren,
        referenced: lookuplist.referenced
      },
      title: lookuplist.Name
    });
  };
}

class DataList extends React.Component {
  static propTypes = {
    dataList: PropTypes.array.isRequired,
    handleEditData: PropTypes.func,
    openNewPanel: PropTypes.func.isRequired
  };

  cellEdit = {
    mode: 'dbclick'
  };

  render() {
    return (
      <BootstrapTable
        remote={{ cellEdit: true }}
        keyField="_id"
        data={this.props.dataList}
        columns={[
          {
            dataField: 'Name',
            text: 'Tên',
            formatter: (cell, row) => {
              return (
                <a
                  href="_"
                  onClick={e => {
                    e.preventDefault();
                    this.props.openNewPanel(row);
                  }}
                  style={{ color: COLOR.SUCCESS }}
                >
                  {cell}
                </a>
              );
            }
          }
        ]}
        onTableChange={this.handleTableChange}
      />
    );
  }
}
