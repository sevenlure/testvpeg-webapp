import React from 'react';
import Clearfix from 'components/elements/clearfix';
import { Row, Col } from 'reactstrap';
import { PanelStack } from '@blueprintjs/core';
import LookuplistPanelStack from './1_LookuplistPanelStack';

export default class LookuplistPanel extends React.Component {
  state = {
    lookuplistitemData: []
  };

  initialPanel = {
    component: LookuplistPanelStack,
    props: {
      panelNumber: 1
    },
    title: 'Danh mục'
  };

  state = {
    currentPanelStack: [this.initialPanel]
  };

  addToPanelStack = newPanel => {
    this.setState({
      currentPanelStack: [newPanel, ...this.state.currentPanelStack]
    });
  };

  removeFromPanelStack = _lastPanel => {
    this.setState({ currentPanelStack: this.state.currentPanelStack.slice(1) });
  };

  render() {
    return (
      <div style={{ width: '100%' }}>
        <Clearfix height={8} />
        <Clearfix height={8} />

        <Row>
          <Col xs={12}>
            <PanelStack
              className="bp3-panel-stack-custom"
              initialPanel={this.state.currentPanelStack[0]}
              onOpen={this.addToPanelStack}
              onClose={this.removeFromPanelStack}
            />
          </Col>
        </Row>
        <Clearfix height={16} />
      </div>
    );
  }
}
