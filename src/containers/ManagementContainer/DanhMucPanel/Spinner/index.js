import styled from 'styled-components';
import React from 'react';

const SpinnerContainer = styled.div`
  margin: 100px auto;
  //width: 50px;
  height: 40px;
  text-align: center;
  font-size: 10px;
`;

export default () => {
  return (
    <SpinnerContainer>
      <svg
        className="bp3-spinner"
        height="100"
        width="100"
        viewBox="0 0 100 100"
        strokeWidth="4"
      >
        <path
          className="bp3-spinner-track"
          d="M 50,50 m 0,-44.5 a 44.5,44.5 0 1 1 0,89 a 44.5,44.5 0 1 1 0,-89"
        />
        <path
          className="bp3-spinner-head"
          d="M 50,50 m 0,-44.5 a 44.5,44.5 0 1 1 0,89 a 44.5,44.5 0 1 1 0,-89"
          pathLength="280"
          strokeDasharray="280 280"
          strokeDashoffset="210"
        />
      </svg>
    </SpinnerContainer>
  );
};
