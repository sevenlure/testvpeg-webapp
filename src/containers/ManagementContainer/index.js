import React from 'react';
import Clearfix from 'components/elements/clearfix';
import { Tab, Tabs } from '@blueprintjs/core';
import PageBuilding from 'components/pageBuilding'
import DanhMucPanel from './DanhMucPanel'
import PageLockHOC from 'hoc/pageLock'

const panelList = [
  {
    key: 'danhmuc',
    title: 'Quản lý danh mục'
  },
  {
    key: 'role',
    title: 'Quản lý khác'
  },
];

@PageLockHOC(['manage.full'])
export default class ManagementContainer extends React.Component {
  static propTypes = {};

  state = {};


  renderPanel = keyPanel => {
    switch (keyPanel) {
      case 'danhmuc': {
        return <DanhMucPanel />
      }
      case 'chitietcoso': {
        return <div />
      }
      case 'facilityeia': {
        return <div />
      }
      
      default: {
        return <PageBuilding />;
      }
    }
  };

  componentDidMount() {}

  render() {
    return (
      <div>
        <Clearfix height={16} />
        <Tabs renderActiveTabPanelOnly={true} style={{ color: 'red' }}>
          {panelList.map(item => (
            <Tab
              id={item.key}
              key={item.key}
              title={item.title}
              panel={this.renderPanel(item.key)}
            />
          ))}
        </Tabs>
      </div>
    );
  }
}
