import React from 'react';
import slug from 'constants/slug';
import { withRouter } from 'react-router-dom';

class NoMatchRoute extends React.Component {
  static propTypes = {};

  
  componentWillMount() {
    console.log('NoMatchRoute')
    this.props.history.push(slug.CO_SO.LIST);
  }
  

  render(){
    return(
      <div>No Matching Route </div>
    )
  }
}

export default  withRouter(NoMatchRoute)