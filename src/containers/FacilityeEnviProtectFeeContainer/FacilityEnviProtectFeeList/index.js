import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BaseListContainer } from 'containers/BaseContainer/BaseListContainer';
import { setMenuSelected } from '@redux/actions/menuAction';
import moment from 'moment';
import { DATE_FORMAT } from 'config';
import { getList, deleteOne, exportData } from 'api/facilityEnviProtectFeeApi';
import slug from 'constants/slug';
import styled from 'styled-components';
import {  Colors } from '@blueprintjs/core';
import Message from 'rc-message';
import { Row, Col } from 'reactstrap';
import EmissionTypeSelect from 'components/elements/select-emission-type';
import Clearfix from 'components/elements/clearfix';
import { createForm } from 'rc-form';
import { get as _get } from 'lodash';
import PageLockHOC from 'hoc/pageLock'


const NameContainer = styled.div`
  color: ${Colors.GREEN2};
  text-decoration: underline;
`;

@PageLockHOC(['facilityEnviProtectFee.view'])
class FacilityeEnviProtectFeeList extends BaseListContainer {
  static propTypes = {
    facilityId: PropTypes.string,
    hideRightHeaderContent: PropTypes.bool
  };

  constructor(props,) {
    const funcApi = {
      getList,
      exportData
    };
    const columns = [
      {
        dataField: 'facility.FacilityNameFull',
        text: 'Tên Cơ sở',
        formatter: (cell, row) => {
          return (
            <Link to={slug.ENVI_PROTECT_FEE.VIEW_WITH_ID + row._id}>
              <NameContainer>{cell}</NameContainer>
            </Link>
          );
        }
      },
      {
        dataField: 'facility.industrialArea',
        text: '	Khu công nghiệp/Cụm Công nghiệp',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'facility.district',
        text: 'Quận/Huyện',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'facility.sector',
        text: 'Ngành/Nghề',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'QuyMo',
        text: 'Quy mô',
        formatter: (cell, row) => {
          return <div>{cell}</div>;
        }
      },
      {
        dataField: 'LuuLuongNuocThai',
        text: 'Lưu luợng nuớc thải',
        formatter: (cell, row) => {
          return <div>{cell}</div>;
        }
      },
      {
        dataField: 'AddedOn',
        text: 'Thêm vào',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
      {
        dataField: 'AddedBy',
        text: 'Thêm bởi'
      },
      {
        dataField: 'UpdatedOn',
        text: 'Cập nhật ngày',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
      {
        dataField: 'UpdatedBy',
        text: 'Cập nhật bởi',
        formatter: (cell, row) => {
          return <div>{cell}</div>;
        }
      }
    ];
    super(
      props,
      funcApi,
      columns,
      `${slug.ENVI_PROTECT_FEE.CREATE}?facilityId=${props.facilityId}`,
      'facilityEnviProtectFee'
    );

    this.state = {
      ...this.state,
      valueSearch: {
        facility: this.props.facilityId
      }
    };
  }

  renderSearchContainer() {
    return (
      <div>
        {super.renderSearchContainer()}
        {/* {this.renderSearchExpandContainer()} */}
      </div>
    );
  }

  handelSubmitSearch = values => {
    values.emissionType = _get(values.emissionType, 'value');
    super.handelSubmitSearch(values)
  };

  renderSearchExpandContainer = () => {
    return (
      <div>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <EmissionTypeSelect
              fieldForm={{
                form: this.props.form,
                name: 'emissionType'
              }}
            />
          </Col>
        </Row>
      </div>
    );
  };

  handelDelete = _id => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      this.ConfirmElm.showConfirm({
        content: 'Bạn có muốn xoá ',
        cb: async value => {
          if (value) {
            const res = await deleteOne(_id);
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.fetData();
            }
            if (res.error) Message.error({ content: res.message });
          }
        }
      });
    }
  };
}

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(createForm()(FacilityeEnviProtectFeeList))
);
