import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { Icon, Intent, InputGroup } from '@blueprintjs/core';
import Clearfix from 'components/elements/clearfix';
import { IconNames } from '@blueprintjs/icons';
import { createForm } from 'rc-form';
// import * as _ from 'lodash';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import IconRequired from 'components/elements/required-icon';
import FacilitySelect from 'components/elements/select-facility';
import { STYLE } from 'config';
import CustomDataSelect from 'components/elements/select-custom-data';
import { COLOR } from 'config';
import NumberInput from 'rc-input-number';
import {
  LUU_LUONG,
  QuaterOfChargeData,
  SubjectOfChargeData,
  getDataForCustomSelect,
  AuthorityBVMTData,
  PaymentStatusData as TinhTrangNopData,
  QuyMoData,
  LuuLuongNuocThaiData,
  ThongBaoLanData,
  LoaiThongBaoData
} from 'constants/custom-data';
import dynamicRoleHOC from 'hoc/dynamicRole';
import YearSelect from 'components/elements/select-year';
import { DATE_FORMAT } from 'config';
import moment from 'moment';
import { DateInput } from '@blueprintjs/datetime';
import { convertDateFromSever as convertDate } from 'utils/fnUtil';

const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

const HeaderContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-start;
  cursor: pointer;
`;

@dynamicRoleHOC()
class FacilityEnviProtectFeeForm extends React.Component {
  static propTypes = {
    isEdit: PropTypes.bool,
    initialValue: PropTypes.object,
    slugBack: PropTypes.string,
    handelSubmit: PropTypes.func
    // facility: PropTypes.shape({
    //   _id: PropTypes.string,
    //   FacilityNameFull: PropTypes.string
    // })
  };

  constructor(props) {
    super(props);
    this.state = {
      DiaChiText: '',
      NganhNgheText: ''
    };
    this.isPaymentStatus = false;
    if (props.isEdit) {
      this.isPaymentStatus =
        _.result(this.props.initialValue, 'PaymentStatus') === 'Đã nộp'
          ? true
          : false;
    }
  }

  componentDidMount() {
    if (this.props.getRef) this.props.getRef(this);
  }

  setInitialValue(keyPath, defaultValue) {
    if (this.props.isEdit) {
      let value = _.result(this.props.initialValue, keyPath, defaultValue);
      if (!value) value = defaultValue;
      return value;
    }
    return defaultValue;
  }

  setSelInitialValue(keyPath, defaultValue, nameLabel) {
    if (this.props.isEdit) {
      let selItem = _.result(this.props.initialValue, keyPath);
      if (!selItem) return defaultValue;
      return {
        value: selItem._id,
        label: nameLabel ? selItem[nameLabel] : selItem.Name
      };
    }
    return defaultValue;
  }

  submit = () => {
    this.props.form.validateFields((error, value) => {
      console.log(value);
      value.facility = _.get(value.facility, 'value');
      value.QuaterOfCharge = _.get(value.QuaterOfCharge, 'value');
      value.SubjectOfCharge = _.get(value.SubjectOfCharge, 'value');

      value.AuthorityBVMT = _.get(value.AuthorityBVMT, 'value');
      value.PaymentStatus = _.get(value.PaymentStatus, 'value');

      if (!value.AverageWaterDemand) value.AverageWaterDemand = null;
      if (!value.AverageWasteWater) value.AverageWasteWater = null;
      if (!value.AverageWasteWaterEnviProfile)
        value.AverageWasteWaterEnviProfile = null;
      if (!value.AverageWasteWaterActual) value.AverageWasteWaterActual = null;
      if (!value.TotalWasteWaterOfQuarter)
        value.TotalWasteWaterOfQuarter = null;

      // NOTE Cap Nhat
      value.QuyMo = _.get(value.QuyMo, 'value');
      value.LuuLuongNuocThai = _.get(value.LuuLuongNuocThai, 'value');
      value.Quy = _.get(value.Quy, 'value');
      value.Nam = _.get(value.Nam, 'value');
      value.ThongBaoLan = _.get(value.ThongBaoLan, 'value');
      value.LoaiThongBao = _.get(value.LoaiThongBao, 'value');
      value.TinhTrangNop = _.get(value.TinhTrangNop, 'value');
      value.NamThongBaoNopPhi = _.get(value.NamThongBaoNopPhi, 'value');

      if (error) return;
      this.props.handelSubmit(value);
    });
  };

  renderLogic() {
    const { getFieldProps } = this.props.form;
    return [
      <Clearfix key={'Clearfix1'} height={8} />,
      <Row key='Payment_Row'>
        <Col xs={4}>
          <LabelContainer>Số tiền</LabelContainer>
        </Col>
        <Col xs={4}>
          <NumberInput
            placeholder='Số tiền'
            {...getFieldProps('Payment', {
              initialValue: this.setInitialValue('Payment', null)
            })}
          />
        </Col>
      </Row>
    ];
  }

  onChangeFacility = obj => {
    this.setState({
      DiaChiText: _.get(obj, 'Address1'),
      NganhNgheText: _.get(obj, 'sector.Name')
    });
  };

  render() {
    const { getFieldProps, getFieldError, getFieldValue } = this.props.form;

    let LuuLuongNuocThai = _.get(getFieldValue('LuuLuongNuocThai'), 'value');
    return (
      <div>
        <h4
          className='bp3-heading'
          style={{
            color: COLOR.SUCCESS
          }}
        >
          Phí bảo vệ môi trường
        </h4>
        <Row>
          <Col xs={4}>
            <HeaderContainer>
              <Icon
                icon={IconNames.SAVED}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.PRIMARY}
                onClick={() => {
                  this.submit();
                }}
              />
              <Clearfix width={8} />
              <Link to={this.props.slugBack ? this.props.slugBack : slug.BASE}>
                <Icon
                  icon={IconNames.DISABLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                />
              </Link>
            </HeaderContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Tên Cơ sở
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <FacilitySelect
              isDisabled={this.props.facility ? true : false}
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'facility',
                option: {
                  initialValue: this.props.facility
                    ? {
                        value: this.props.facility._id,
                        label: this.props.facility.FacilityNameFull
                      }
                    : null,
                  rules: [
                    {
                      required: true,
                      message: 'Tên Cơ sở is required'
                    }
                  ]
                }
              }}
              cbChangeFacility={this.onChangeFacility}
            />
            <ErrorContainer>
              {(getFieldError('facility') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Địa chỉ</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup disabled value={this.state.DiaChiText} />
          </Col>
        </Row>

        <Row>
          <Col xs={4}>
            <LabelContainer>Ngành Nghề</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup disabled value={this.state.NganhNgheText} />
          </Col>
        </Row>

        <Row>
          <Col xs={4}>
            <LabelContainer>Tên cán bộ thụ lý hồ sơ</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Tên cán bộ thụ lý hồ sơ'
              {...getFieldProps('TenCanBoThuLy', {
                initialValue: this.setInitialValue('TenCanBoThuLy', '')
              })}
            />
          </Col>
        </Row>

        <Row>
          <Col xs={4}>
            <LabelContainer>Quy mô</LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={QuyMoData}
              fieldForm={{
                form: this.props.form,
                name: 'QuyMo',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('QuyMo', null)
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>Lưu luợng nuớc thải</LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={LuuLuongNuocThaiData}
              fieldForm={{
                form: this.props.form,
                name: 'LuuLuongNuocThai',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('LuuLuongNuocThai', null)
                  )
                }
              }}
            />
            <ErrorContainer />
          </Col>
        </Row>

        {LuuLuongNuocThai === LUU_LUONG.TREN_20M3 && (
          <Tren20m3Ngay {...this.props} />
        )}

        {LuuLuongNuocThai === LUU_LUONG.DUOI_20M3 && (
          <Duoi20m3Ngay {...this.props} />
        )}
      </div>
    );
  }
}

export default createForm()(FacilityEnviProtectFeeForm);

class Tren20m3Ngay extends React.Component {
  setInitialValue(keyPath, defaultValue) {
    if (this.props.isEdit) {
      let value = _.result(this.props.initialValue, keyPath, defaultValue);
      if (!value) value = defaultValue;
      return value;
    }
    return defaultValue;
  }

  render() {
    const { getFieldProps, getFieldError, getFieldValue } = this.props.form;

    return (
      <React.Fragment>
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Quý <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={QuaterOfChargeData}
              fieldForm={{
                form: this.props.form,
                name: 'Quy',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('Quy', null)
                  ),
                  rules: [
                    {
                      required: true,
                      message: 'Quý is required'
                    }
                  ]
                }
              }}
            />
            <ErrorContainer>
              {(getFieldError('Quy') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Năm</LabelContainer>
          </Col>
          <Col xs={4}>
            <YearSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'Nam',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('Nam', '')
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>Ngày nộp tờ khai</LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder='Ngày nộp tờ khai'
              {...getFieldProps('NgayNopToKhai', {
                initialValue: convertDate(
                  this.setInitialValue('NgayNopToKhai', null)
                )
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Ngày có văn bản thông báo</LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder='Ngày có văn bản thông báo'
              {...getFieldProps('NgayCoVanBanThongBao', {
                initialValue: convertDate(
                  this.setInitialValue('NgayCoVanBanThongBao', null)
                )
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Số hiệu văn bản trả lời</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Số hiệu văn bản trả lời'
              {...getFieldProps('SoHieuVanBanTraLoi', {
                initialValue: this.setInitialValue('SoHieuVanBanTraLoi', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Thông báo lần</LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={ThongBaoLanData}
              fieldForm={{
                form: this.props.form,
                name: 'ThongBaoLan',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('ThongBaoLan', null)
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Loại thông báo</LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={LoaiThongBaoData}
              fieldForm={{
                form: this.props.form,
                name: 'LoaiThongBao',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('LoaiThongBao', null)
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>
              Tổng số tiền theo thông báo nộp phí trong quý (VNĐ)
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              {...getFieldProps('TongTienTheoThongBaoNopPhi', {
                initialValue: this.setInitialValue(
                  'TongTienTheoThongBaoNopPhi',
                  null
                )
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Tình trạng nộp</LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={TinhTrangNopData}
              fieldForm={{
                form: this.props.form,
                name: 'TinhTrangNop',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('TinhTrangNop', null)
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
      </React.Fragment>
    );
  }
}

class Duoi20m3Ngay extends React.Component {
  setInitialValue(keyPath, defaultValue) {
    if (this.props.isEdit) {
      let value = _.result(this.props.initialValue, keyPath, defaultValue);
      if (!value) value = defaultValue;
      return value;
    }
    return defaultValue;
  }

  render() {
    const { getFieldProps, getFieldError, getFieldValue } = this.props.form;

    return (
      <React.Fragment>
        <Row>
          <Col xs={4}>
            <LabelContainer>Năm thông báo nộp phí</LabelContainer>
          </Col>
          <Col xs={4}>
            <YearSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'NamThongBaoNopPhi',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('NamThongBaoNopPhi', '')
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Ngày có văn bản thông báo</LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder='Ngày có văn bản thông báo'
              {...getFieldProps('NgayCoVanBanThongBao', {
                initialValue: convertDate(
                  this.setInitialValue('NgayCoVanBanThongBao', null)
                )
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Số hiệu văn bản trả lời</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Số hiệu văn bản trả lời'
              {...getFieldProps('SoHieuVanBanTraLoi', {
                initialValue: this.setInitialValue('SoHieuVanBanTraLoi', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Thông báo lần</LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={ThongBaoLanData}
              fieldForm={{
                form: this.props.form,
                name: 'Thông báo lần',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('ThongBaoLan', null)
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Tình trạng nộp</LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={TinhTrangNopData}
              fieldForm={{
                form: this.props.form,
                name: 'TinhTrangNop',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('TinhTrangNop', null)
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
      </React.Fragment>
    );
  }
}
