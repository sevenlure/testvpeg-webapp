import React from 'react';
import slug from 'constants/slug';
import { getOne, updateOne } from 'api/facilityEnviProtectFeeApi';
import Message from 'rc-message';
// import * as _ from 'lodash';
import FacilityEnviProtectFeeForm from '../FacilityEnviProtectFeeForm';
import PageLockHOC from 'hoc/pageLock';
import { get } from 'lodash';

@PageLockHOC(['facilityEnviProtectFee.write'])
export default class FacilityEnviProtectFeeEdit extends React.Component {
  static propTypes = {};

  state = {
    isLoaded: false,
    isLoading: false,
    objData: {},
    facility: null,
    facilityId: '',
    _idObject: ''
  };

  async checkValidate() {
    const _id = _.result(this.props, 'match.params._id');
    if (_id) {
      const res = await getOne(_id);
      if (res.success && res.data) {
        this.setState(
          {
            _idObject: res.data._id,
            objData: res.data,
            facility: res.data.facility,
            isLoading: false,
            isLoaded: true
          },
          () => {
            const valLuuLuongNuocThai = get(res.data, 'LuuLuongNuocThai');
            this.FacilityEnviProtectFeeForm.props.form.setFieldsValue({
              LuuLuongNuocThai: {
                label: valLuuLuongNuocThai,
                value: valLuuLuongNuocThai
              }
            });
            this.FacilityEnviProtectFeeForm.onChangeFacility(res.data.facility)
            
          }
        );
      }
    } else {
      //this.props.history.push(slug.CO_SO.LIST);
    }
  }

  async componentWillMount() {
    await this.checkValidate();
  }

  handelSubmit = async values => {
    this.setState({
      isLoading: true
    });
    console.log(values);
    const res = await updateOne(this.state._idObject, values);
    if (res.success && res.data) {
      Message.success({ content: 'Success!!!' });
      this.props.history.push(
        slug.ENVI_PROTECT_FEE.VIEW_WITH_ID + res.data._id
      );
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div>
        {this.state.isLoaded && (
          <FacilityEnviProtectFeeForm
            isEdit
            getRef={ref => (this.FacilityEnviProtectFeeForm = ref)}
            slugBack={slug.ENVI_PROTECT_FEE.VIEW_WITH_ID + this.state._idObject}
            initialValue={this.state.objData}
            facility={this.state.facility}
            handelSubmit={this.handelSubmit}
          />
        )}
      </div>
    );
  }
}
