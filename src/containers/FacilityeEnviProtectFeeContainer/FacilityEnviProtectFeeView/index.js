import React from 'react';
import PropTypes from 'prop-types';
import { getOne, deleteOne } from 'api/facilityEnviProtectFeeApi';
import Message from 'rc-message';
// import * as _ from 'lodash';
import { Icon, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { Row, Col } from 'reactstrap';
import Confirm from 'components/elements/confirm-element';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import { withRouter } from 'react-router-dom';
import { COLOR, STYLE, DATETIME_FORMAT } from 'config';
import moment from 'moment';
import PageLockHOC from 'hoc/pageLock';
import Permissions from 'react-redux-permissions';
import RuleDiv from 'components/ruleDiv';
import { LUU_LUONG } from 'constants/custom-data';
// import { Cell, Column, Table, SelectionModes } from '@blueprintjs/table';

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

class RowValue extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    isRequiredField: PropTypes.bool
  };

  render() {
    return (
      <div>
        <Row>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              width: '100%'
            }}
          >
            <Col
              style={this.props.isRequiredField ? STYLE.REQUIREDFIELD : {}}
              xs={2}
            >
              {this.props.label}
            </Col>
            <Col xs={7}>{this.props.value}</Col>
          </div>
        </Row>
        <Clearfix height={8} />
      </div>
    );
  }
}

@PageLockHOC(['facilityEnviProtectFee.view'])
class FacilityEnviProtectFeeView extends React.Component {
  state = {
    _idObject: '',
    isLoaded: false,
    isLoading: false,
    facility: {},
    facilityId: '',
    initialValue: null
  };

  getInitialValue = (keyPath, defaultValue = '') => {
    let val = _.result(this.state.initialValue, keyPath, defaultValue);
    if (!val) val = '';
    return val;
  };

  getInitialDateValue = (keyPath, defaultValue = '') => {
    let value = _.result(this.state.initialValue, keyPath, defaultValue);
    if (!value) return '';
    return moment(value).format(DATETIME_FORMAT);
  };

  async checkValidate() {
    const _id = _.result(this.props, 'match.params._id');
    if (_id) {
      const res = await getOne(_id);
      console.log(res);
      if (res.success && res.data) {
        this.setState({
          _idObject: res.data._id,
          facility: res.data.facility,
          facilityId: res.data.facility._id,
          isLoading: false,
          isLoaded: true,
          initialValue: res.data
        });
      }
    } else {
      this.props.history.push(slug.ENVI_PROTECT_FEE.LIST);
    }
  }

  handelDelete = () => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      this.ConfirmElm.showConfirm({
        content: 'Bạn có muốn xoá ',
        cb: async value => {
          if (value) {
            const res = await deleteOne(this.getInitialValue('_id'));
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.props.history.push(slug.ENVI_PROTECT_FEE.LIST);
            }
            if (res.error) Message.error({ content: res.message });
          }
        }
      });
    }
  };

  async componentWillMount() {
    await this.checkValidate();
  }

  render() {
    const LuuLuongNuocThai = _.get(this.state.initialValue, 'LuuLuongNuocThai');
    return (
      <div style={{ width: '100%' }}>
        <Confirm ref={ConfirmElm => (this.ConfirmElm = ConfirmElm)} />
        <h4
          className='bp3-heading'
          style={{
            color: COLOR.SUCCESS
          }}
        >
          Phí bảo vệ môi trường
        </h4>
        <HeaderContainer>
          <div style={{ display: 'inline-flex' }}>
            <Link to={slug.ENVI_PROTECT_FEE.LIST}>
              <Icon
                style={{ cursor: 'pointer' }}
                icon={IconNames.CIRCLE_ARROW_LEFT}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.SUCCESS}
              />
            </Link>

            <Permissions allowed={[`facilityEnviProtectFee.write`]}>
              <RuleDiv initialValue={this.state.initialValue}>
                <React.Fragment>
                  <Clearfix width={8} />
                  <Link
                    to={
                      slug.ENVI_PROTECT_FEE.EDIT_WITH_ID + this.state._idObject
                    }
                  >
                    <Icon
                      style={{ cursor: 'pointer' }}
                      icon={IconNames.MANUALLY_ENTERED_DATA}
                      iconSize={Icon.SIZE_LARGE}
                      intent={Intent.PRIMARY}
                    />
                  </Link>
                </React.Fragment>
              </RuleDiv>
            </Permissions>

            <Permissions allowed={[`facilityEnviProtectFee.delete`]}>
              <RuleDiv initialValue={this.state.initialValue}>
                <React.Fragment>
                  <Clearfix width={8} />
                  <Icon
                    style={{ cursor: 'pointer' }}
                    icon={IconNames.BAN_CIRCLE}
                    iconSize={Icon.SIZE_LARGE}
                    intent={Intent.DANGER}
                    onClick={this.handelDelete}
                  />
                </React.Fragment>
              </RuleDiv>
            </Permissions>

            <Clearfix width={8} />
            <Icon
              style={{ cursor: 'pointer' }}
              icon={IconNames.PRINT}
              iconSize={Icon.SIZE_LARGE}
              intent={Intent.SUCCESS}
            />
          </div>
        </HeaderContainer>
        <Clearfix height={16} />
        <RowValue
          isRequiredField
          label='Tên Cơ Sở'
          value={this.getInitialValue('facility.FacilityNameFull')}
        />

        <RowValue label='Quy mô' value={this.getInitialValue('QuyMo')} />

        <RowValue
          label='Lưu lượng nước thải'
          value={this.getInitialValue('LuuLuongNuocThai')}
        />

        {LuuLuongNuocThai === LUU_LUONG.TREN_20M3 && (
          <Tren20m3Ngay initialValue={this.state.initialValue} />
        )}
        {LuuLuongNuocThai === LUU_LUONG.DUOI_20M3 && (
          <Duoi20m3Ngay initialValue={this.state.initialValue} />
        )}

        <Clearfix height={8} />
        <RowValue
          label='Thêm vào'
          value={
            this.getInitialDateValue('AddedOn') +
            ' bởi ' +
            this.getInitialValue('AddedBy')
          }
        />
        <RowValue
          label='Cập nhật lần cuối'
          value={
            this.getInitialDateValue('UpdatedOn') +
            ' bởi ' +
            this.getInitialValue('UpdatedBy')
          }
        />
      </div>
    );
  }
}

export default withRouter(FacilityEnviProtectFeeView);

class Tren20m3Ngay extends React.Component {
  getInitialValue = (keyPath, defaultValue = '') => {
    let val = _.result(this.props.initialValue, keyPath, defaultValue);
    if (!val) val = '';
    return val;
  };

  getInitialDateValue = (keyPath, defaultValue = '') => {
    let value = _.result(this.props.initialValue, keyPath, defaultValue);
    if (!value) return '';
    return moment(value).format(DATETIME_FORMAT);
  };

  render() {
    return (
      <div>
        <RowValue label='Quý' value={this.getInitialValue('Quy')} />

        <RowValue label='Năm' value={this.getInitialValue('Nam')} />

        <RowValue
          label='Ngày nộp tờ khai'
          value={this.getInitialDateValue('NgayNopToKhai')}
        />

        <RowValue
          label='Ngày có văn bản thông báo'
          value={this.getInitialDateValue('NgayCoVanBanThongBao')}
        />

        <RowValue
          label='Số hiệu văn bản trả lời'
          value={this.getInitialValue('SoHieuVanBanTraLoi')}
        />

        <RowValue
          label='Thông báo lần'
          value={this.getInitialValue('ThongBaoLan')}
        />

        <RowValue
          label='Loại thông báo'
          value={this.getInitialValue('LoaiThongBao')}
        />

        <RowValue
          label='Tổng số tiền theo thông báo nộp phí trong quý (VNĐ)'
          value={this.getInitialValue('TongTienTheoThongBaoNopPhi')}
        />

        <RowValue
          label='Tình trạng nộp'
          value={this.getInitialValue('TinhTrangNop')}
        />
      </div>
    );
  }
}

class Duoi20m3Ngay extends React.Component {
  getInitialValue = (keyPath, defaultValue = '') => {
    let val = _.result(this.props.initialValue, keyPath, defaultValue);
    if (!val) val = '';
    return val;
  };

  getInitialDateValue = (keyPath, defaultValue = '') => {
    let value = _.result(this.props.initialValue, keyPath, defaultValue);
    if (!value) return '';
    return moment(value).format(DATETIME_FORMAT);
  };

  render() {
    return (
      <div>
        <RowValue
          label='Năm thông báo nộp phí'
          value={this.getInitialValue('NamThongBaoNopPhi')}
        />

        <RowValue
          label='Ngày có văn bản thông báo'
          value={this.getInitialDateValue('NgayCoVanBanThongBao')}
        />

        <RowValue
          label='Số hiệu văn bản trả lời'
          value={this.getInitialValue('SoHieuVanBanTraLoi')}
        />

        <RowValue
          label='Thông báo lần'
          value={this.getInitialValue('ThongBaoLan')}
        />

        <RowValue
          label='Tình trạng nộp'
          value={this.getInitialValue('TinhTrangNop')}
        />
      </div>
    );
  }
}
