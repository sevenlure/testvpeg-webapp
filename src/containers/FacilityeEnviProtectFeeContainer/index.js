import React from 'react';
import { Route } from 'react-router-dom';
import slug from 'constants/slug';
import FacilityEnviProtectFeeList from './FacilityEnviProtectFeeList';
import FacilityEnviProtectFeeCreate from './FacilityEnviProtectFeeCreate';
import FacilityEnviProtectFeeEdit from './FacilityEnviProtectFeeEdit';
import FacilityEnviProtectFeeView from './FacilityEnviProtectFeeView';

export default props => [
  <Route
    exact
    key={slug.ENVI_PROTECT_FEE.LIST}
    path={slug.ENVI_PROTECT_FEE.LIST}
    render={matchProps => (
      <FacilityEnviProtectFeeList {...matchProps} {...props} />
    )}
  />,
  <Route
    key={slug.ENVI_PROTECT_FEE.CREATE}
    path={slug.ENVI_PROTECT_FEE.CREATE}
    render={matchProps => (
      <FacilityEnviProtectFeeCreate {...matchProps} {...props} />
    )}
  />,
  <Route
    key={slug.ENVI_PROTECT_FEE.VIEW}
    path={slug.ENVI_PROTECT_FEE.VIEW}
    render={matchProps => (
      <FacilityEnviProtectFeeView {...matchProps} {...props} />
    )}
  />,
  <Route
    key={slug.ENVI_PROTECT_FEE.EDIT}
    path={slug.ENVI_PROTECT_FEE.EDIT}
    render={matchProps => (
      <FacilityEnviProtectFeeEdit {...matchProps} {...props} />
    )}
  />
];
