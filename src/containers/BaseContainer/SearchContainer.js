import React from 'react';
import PropTypes from 'prop-types';
import QuanHuyenSelect from 'components/elements/select-quan-huyen';
import NganhNgheSelect from 'components/elements/select-nganh-nghe';
import BenTrongNgoaiSelect from 'components/elements/select-ben-trong-ngoai';
import KhuCumCongNghiepSelect from 'components/elements/select-khu-cum-cong-nghiep';
import { Row, Col } from 'reactstrap';
import { Icon, Intent, InputGroup, Hotkey, Hotkeys, HotkeysTarget } from '@blueprintjs/core';
import Clearfix from 'components/elements/clearfix';
import ThamQuyenQuanLy from 'components/elements/select-tham-quyen-quan-ly';
import { IconNames } from '@blueprintjs/icons';
import { SELECT_ITEM_DEFAULT } from 'config';
// import * as _ from 'lodash';

@HotkeysTarget
export default class SearchContainer extends React.Component {
  static propTypes = {
    handelSubmitSearch: PropTypes.func,
    form: PropTypes.object.isRequired
  };
  

  state = {};

  submit = () => {
    this.props.form.validateFields((error, value) => {
      if (error) return;
      value.district = _.get(value.district, 'value');
      value.sector = _.get(value.sector, 'value');
      value.authority = _.get(value.authority, 'value');
      value.bentrongngoaiSelect =  _.get(value.bentrongngoaiSelect, 'value');
      value.industrialArea = _.get(value.industrialArea, 'value');
  
      if (this.props.handelSubmitSearch) this.props.handelSubmitSearch(value);
    });
  };

   renderHotkeys() {
    return <Hotkeys>
        <Hotkey
            //global={true}
            allowInInput={true}
            combo="enter"
            label="Be awesome all the time"
            onKeyDown={this.submit}
        />
    </Hotkeys>;
}

  render() {
    const { getFieldDecorator, setFieldsValue } = this.props.form;
    return (
      <div>
        {/* Row 1 */}
        <Row>
          <Col xs={4}>
            <QuanHuyenSelect
              fieldForm={{
                form: this.props.form,
                name: 'district'
              }}
            />
          </Col>
          <Col xs={4}>
            <NganhNgheSelect
              fieldForm={{
                form: this.props.form,
                name: 'sector'
              }}
            />
          </Col>
          <Col>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <span>Tìm kiếm</span>
              <Clearfix width={8} />
              <div style={{ flex: 1 }}>
                {getFieldDecorator('search', {
                  initialValue: ''
                })(<InputGroup />)}
              </div>
              <Clearfix width={8} />
              <Icon
                icon={IconNames.SEARCH}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.SUCCESS}
                style={{ cursor: 'pointer' }}
                onClick={this.submit}
              />
            </div>
          </Col>
        </Row>
        <Clearfix height={8} />
        {/* Row 2 */}
        <Row>
          <Col xs={4}>
            <BenTrongNgoaiSelect
              fieldForm={{
                form: this.props.form,
                name: 'bentrongngoaiSelect'
              }}
            />
          </Col>
          <Col xs={4}>
            <KhuCumCongNghiepSelect
            onChangeIsKhuCongNghiep={(val)=>{
              setFieldsValue({
                industrialArea: SELECT_ITEM_DEFAULT
              })
            }}
              fieldForm={{
                form: this.props.form,
                name: 'industrialArea'
              }}
            />
          </Col>
          <Col xs={4}>
            <ThamQuyenQuanLy
              fieldForm={{
                form: this.props.form,
                name: 'authority'
              }}
            />
          </Col>
        </Row>
        {this.props.expand}
      </div>
    );
  }
}

