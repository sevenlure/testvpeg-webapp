import { Link } from 'react-router-dom';
import React, {Fragment} from 'react';
import styled from 'styled-components';
import { Icon, Intent, Colors, Alert, Spinner } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import Clearfix from 'components/elements/clearfix';
import SearchContainer from './SearchContainer';
// // import * as _ from 'lodash';
import BootstrapTable from 'react-bootstrap-table-next';
import slug from 'constants/slug';
import ItemPerPageSelect from 'components/elements/select-itemPerPage';
import Pagination from 'rc-pagination';
import localeInfo from 'rc-pagination/lib/locale/en_US';
import PropTypes from 'prop-types';
import Permissions from 'react-redux-permissions';

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const SpinnerContainer = styled.div`
  margin: 100px auto;
  //width: 50px;
  height: 40px;
  text-align: center;
  font-size: 10px;
`;

const PaginationContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const TotalContainer = styled.div`
  .rc-pagination-item,
  .rc-pagination-prev,
  .rc-pagination-next {
    display: none;
  }
`;

export class BaseListContainer extends React.Component {
  static propTypes = {
    hideRightHeaderContent: PropTypes.bool,
    form: PropTypes.object.isRequired
  };

  constructor(
    props,
    funcApi = {},
    columns = [],
    createPath = slug.BASE,
    moduleName = ''
  ) {
    super(props);
    this.state = {
      isLoading: true,
      dataList: [],
      valueSearch: {},
      pagination: {
        page: 1,
        minIndex: 0,
        maxIndex: 5,
        itemPerPage: 10,
        totalItem: 0
      },
      isOpenExport: false
    };

    this.options = {
      page: this.state.pagination.page, // which page you want to show as default
      sizePerPage: this.state.pagination.itemPerPage, // which size per page you want to locate as default
      pageStartIndex: 1, // where to start counting the pages
      prePage: 'Prev', // Previous page button text
      nextPage: 'Next', // Next page button text
      firstPage: 'First', // First page button text
      lastPage: 'Last', // Last page button text
      paginationShowsTotal: true, // Accept bool or function
      paginationPosition: 'top', // default is bottom, top and both is all available
      alwaysShowAllBtns: true,
      hideSizePerPage: true,
      onPageChange: this.onPageChange
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false > Hide the going to First and Last page button
    };
    this.funcApi = funcApi;
    this.columns = columns;
    this.createPath = createPath;
    this.mounted = true;
    this.moduleName = moduleName;
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  handelSubmitSearch(values) {
    this.setState(
      {
        isLoading: true,
        valueSearch: _.pickBy(values, _.identity),
        pagination: {
          ...this.state.pagination,
          page: 1
        }
      },
      () => {
        this.fetData();
      }
    );
  }

  fetData = async () => {
    if (!this.funcApi.getList) return;
    const res = await this.funcApi.getList({
      ...this.state.pagination,
      ...this.state.valueSearch
    });

    if (res.success && this.mounted) {
      this.setState({
        dataList: res.data,
        pagination: res.pagination,
        isLoading: false
      });
    }
  };

  onPageChange = async (current, pageSize) => {
    this.setState(
      {
        isLoading: true,
        pagination: {
          ...this.state.pagination,
          page: current
        }
      },
      () => {
        this.fetData();
      }
    );
  };

  componentWillMount() {
    this.fetData();
  }

  renderHeaderContainer() {
    this.options = {
      ...this.options,
      page: this.state.pagination.page,
      sizePerPage: this.state.pagination.itemPerPage
    };
    return (
      <HeaderContainer>
        <div style={{ display: 'inline-flex' }}>
          <Permissions allowed={[`${this.moduleName}.write`]}>
            <Fragment>
              <Link to={this.createPath}>
                <Icon
                  style={{ cursor: 'pointer' }}
                  icon={IconNames.ADD}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.SUCCESS}
                />
              </Link>

              <Clearfix width={8} />
              <Link to={this.createPath}>
                <h5
                  style={{
                    color: Colors.GREEN2,
                    cursor: 'pointer',
                    textDecoration: 'underline'
                  }}
                  className='bp3-heading'
                >
                  Thêm mục mới
                </h5>
              </Link>
            </Fragment>
          </Permissions>
        </div>
        {!this.props.hideRightHeaderContent && (
          <div style={{ display: 'inline-flex', alignItems: 'center' }}>
            <div style={{ width: 300 }}>
              {' '}
              <ItemPerPageSelect
                onChange={selectedOption => {
                  if (selectedOption && selectedOption.value)
                    this.setState(
                      {
                        pagination: {
                          ...this.state.pagination,
                          itemPerPage: selectedOption.value,
                          page: 1
                        }
                      },
                      () => this.fetData()
                    );
                }}
              />
            </div>
            <Clearfix width={8} />
            <Icon
              icon={IconNames.EXPORT}
              iconSize={Icon.SIZE_LARGE}
              className='pointer'
              intent={Intent.SUCCESS}
              // onClick={this.funcApi.exportData}
              onClick={async () => {
                this.setState({ isOpenExport: true });
                let res = await this.funcApi.exportData();
                console.log(res);
                let dataBuff = toArrayBuffer(res.buffer.data);
                let blob = new Blob([dataBuff], {
                  type:
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                });
                getFile(blob, res.fileName);
              }}
            />
            <Clearfix width={8} />
            <Icon
              icon={IconNames.PRINT}
              iconSize={Icon.SIZE_LARGE}
              intent={Intent.SUCCESS}
            />
          </div>
        )}

        <Alert
          // {...alertProps}
          // className={this.props.data.themeName}
          confirmButtonText='Cancel'
          isOpen={this.state.isOpenExport}
          onClose={() => {
            this.setState({ isOpenExport: false });
          }}
        >
          <div id='downloadlink'>Export.....</div>
        </Alert>
      </HeaderContainer>
    );
  }

  renderSearchContainer() {
    const form = this.props.form;
    return (
      <div>
        {!this.props.hideRightHeaderContent && form && (
          <div>
            <SearchContainer
              form={form}
              handelSubmitSearch={this.handelSubmitSearch.bind(this)}
            />
          </div>
        )}
      </div>
    );
  }

  renderSpinnerNodata = () => {
    return (
      <SpinnerContainer>
        {this.state.isLoading ? (
          // <svg
          //   className="bp3-spinner"
          //   height="100"
          //   width="100"
          //   viewBox="0 0 100 100"
          //   strokeWidth="4"
          // >
          //   <path
          //     className="bp3-spinner-track"
          //     d="M 50,50 m 0,-44.5 a 44.5,44.5 0 1 1 0,89 a 44.5,44.5 0 1 1 0,-89"
          //   />
          //   <path
          //     className="bp3-spinner-head"
          //     d="M 50,50 m 0,-44.5 a 44.5,44.5 0 1 1 0,89 a 44.5,44.5 0 1 1 0,-89"
          //     pathLength="280"
          //     strokeDasharray="280 280"
          //     strokeDashoffset="210"
          //   />
          // </svg>
          <Spinner size={100} />
        ) : (
          <h2>No Data</h2>
        )}
      </SpinnerContainer>
    );
  };

  customTotal = (from, to, size) => (
    <span className='react-bootstrap-table-pagination-total'>
      Showing {from} to {to} of {size} Results
    </span>
  );

  rowClasses = (row, rowIndex) => {
    if (this.state.isLoading) return 'bp3-skeleton-row';
    return '';
  };
  renderDataTable() {
    const { pagination } = this.state;
    return (
      <div>
        <BootstrapTable
          keyField='_id'
          data={this.state.dataList}
          columns={this.columns}
          striped
          hover
          remote
          loading={this.state.isLoading}
          rowClasses={this.rowClasses}
          // pagination={paginationFactory({
          //   hidePageListOnlyOnePage: true,
          //   hideSizePerPage: true,
          //   page: page,
          //   sizePerPage: itemPerPage,
          //   totalSize: totalItem,
          //   showTotal: true,
          //   paginationTotalRenderer: this.customTotal
          // })}
          // onTableChange={this.onPageChange}
          noDataIndication={this.renderSpinnerNodata}
        />
        <PaginationContainer>
          <TotalContainer>
            {this.state.pagination.totalItem > 0 && (
              <Pagination
                pageSize={pagination.itemPerPage}
                showPrevNextJumpers={false}
                onChange={this.onPageChange}
                current={pagination.page}
                total={pagination.totalItem}
                showTotal={(total, range) =>
                  `Showing ${range[0]} to ${range[1]} of ${total} Results`
                }
              />
            )}
          </TotalContainer>

          <Pagination
            hideOnSinglePage
            pageSize={pagination.itemPerPage}
            showQuickJumper
            onChange={this.onPageChange}
            current={pagination.page}
            total={pagination.totalItem}
            locale={{
              ...localeInfo,
              jump_to: 'Go to'
            }}
          />
        </PaginationContainer>
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.renderHeaderContainer()}
        <Clearfix height={8} />
        {this.renderSearchContainer()}
        <Clearfix height={8} />
        {this.renderDataTable()}
      </div>
    );
  }
}
