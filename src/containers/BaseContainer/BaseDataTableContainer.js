import React from 'react';
import styled from 'styled-components';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

// const HeaderContainer = styled.div`
//   display: flex;
//   flex-direction: row;
//   align-items: center;
//   justify-content: space-between;
// `;

const SpinnerContainer = styled.div`
  margin: 100px auto;
  //width: 50px;
  height: 40px;
  text-align: center;
  font-size: 10px;
`;

// const PaginationContainer = styled.div`
//   display: flex;
//   flex-direction: row;
//   align-items: center;
//   justify-content: space-between;
// `;

// const TotalContainer = styled.div`
//   .rc-pagination-item,
//   .rc-pagination-prev,
//   .rc-pagination-next {
//     display: none;
//   }
// `;

export default class BaseDataTableContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dataList: []
    };
    this.mounted = true;
  }

  renderSpinnerNodata = () => {
    return (
      <SpinnerContainer>
        <h2>No Data</h2>
      </SpinnerContainer>
    );
  };

  render() {
    return (
      <BootstrapTable
        // keyField='_id'
        data={this.props.dataList}
        // columns={this.props.columns}
        // striped
        hover
        pagination
        // remote
        // pagination={paginationFactory()}
        // onTableChange={this.onPageChange}
        // noDataIndication={this.renderSpinnerNodata}
      >
        <TableHeaderColumn key="_id" dataField='_id' hidden isKey>
          _id
        </TableHeaderColumn>
        {this.props.columns.map((col, index) => {
          return (
            <TableHeaderColumn
              key={index}
              dataField={col.dataField}
              dataFormat={col.formatter}
            >
              {col.text}
            </TableHeaderColumn>
          );
        })}
      </BootstrapTable>
    );
  }
}
