import React from 'react';
import InspectionForm from '../InspectionForm';
import slug from 'constants/slug';
import { create } from 'api/inspectionApi';
import Message from 'rc-message';
import PageLockHOC from 'hoc/pageLock'

@PageLockHOC(['facilityInspection.write'])
export default class InspectionCreate extends React.Component {
  static propTypes = {};

  handelSubmit = async values => {
    //console.log(_.pickBy(values, _.identity), values, '_.identity');
    const res = await create(_.pickBy(values, _.identity));
    if (res.success) {
      Message.success({ content: 'Success!!!' });
      this.props.history.push(slug.INSPECTTION.VIEW_WITH_ID + res.data._id);
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div>
        <InspectionForm
          onSubmit={this.handelSubmit}
          slugBack={slug.INSPECTTION.LIST}
        />
      </div>
    );
  }
}

// export default PageLockHOC(['facilityInspection.write'])(InspectionCreate)