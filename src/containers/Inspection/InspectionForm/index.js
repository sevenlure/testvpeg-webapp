import React from 'react';
import PropTypes from 'prop-types';
import SelectMucDich from 'components/elements/select-muc-dich';
import SelectFacility from 'components/elements/select-facility';
import SelectTinhTrang from 'components/elements/select-tinh-trang';
import SelectSuKienTuanThu from 'components/elements/select-su-kien-tuan-thu';
import IconRequired from 'components/elements/required-icon';
import { Row, Col } from 'reactstrap';
import { Icon, Intent } from '@blueprintjs/core';
import { DateInput } from '@blueprintjs/datetime';
import { DATE_FORMAT } from 'config';
import Clearfix from 'components/elements/clearfix';
import moment from 'moment';
import { IconNames } from '@blueprintjs/icons';
import { createForm } from 'rc-form';
// import * as _ from 'lodash';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import { SELECT_ITEM_DEFAULT } from 'config';
import { convertDateFromSever as convertDate } from 'utils/fnUtil';
import EditorCustom from 'components/elements/editor';
import NumberInput from 'rc-input-number';
import Upload from 'components/elements/upload-file';
import dynamicRoleHOC from 'hoc/dynamicRole'



const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

const HeaderContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-start;
  cursor: pointer;
`;

@dynamicRoleHOC()
class InspectionForm extends React.Component {
  static propTypes = {
    isEdit: PropTypes.bool,
    initialValue: PropTypes.object,
    slugBack: PropTypes.string,
    onSubmit: PropTypes.func
  };

  setInitialValue(keyPath, defaultValue) {
    if (this.props.isEdit) {
      let value = _.result(this.props.initialValue, keyPath, defaultValue);
      if (!value) value = defaultValue;
      return value;
    }
    return defaultValue;
  }

  setSelInitialValue(keyPath, defaultValue, nameLabel) {
    if (this.props.isEdit) {
      let selItem = _.result(this.props.initialValue, keyPath);
      if (!selItem) return defaultValue;
      return {
        value: selItem._id,
        label: nameLabel ? selItem[nameLabel] : selItem.Name
      };
    }
    return defaultValue;
  }

  submit = () => {
    const { setFields, getFieldValue } = this.props.form;
    const facilityValue = _.get(getFieldValue('facility'), 'value');
    if (facilityValue == null) {
      setFields({
        facility: {
          value: facilityValue
        }
      });
    }

    this.props.form.validateFields((error, value) => {
      if (error) return;
      value.facility = _.get(value.facility, 'value');
      value.inspectionStatus = _.get(value.inspectionStatus, 'value');
      value.purposeObj = _.get(value.purposeObj, 'value');
      value.complianceType = _.get(value.complianceType, 'value');
      console.log(value)
      if (this.props.onSubmit) this.props.onSubmit(value);
    });
  };

  onSuccessUpload = res => {
    if (res) {
      this.props.form.setFieldsValue({
        complianceFile: res
      });
    }
  };

  renderLogic() {
    const { getFieldProps, getFieldError } = this.props.form;
    return [
      <Clearfix key={'Clearfix1'} height={8} />,
      <Row key={'ComplianceFineRow'}>
        <Col xs={4}>
          <LabelContainer>Tiền phạt</LabelContainer>
        </Col>
        <Col xs={4}>
          <NumberInput
            placeholder="Tiền phạt"
            {...getFieldProps('ComplianceFine', {
              initialValue: this.setInitialValue('ComplianceFine', '')
            })}
          />
        </Col>
      </Row>,
      <Clearfix key={'Clearfix2'} height={8} />,
      <Row key={'complianceFileRow'}>
        <Col xs={4}>
          <LabelContainer>
            File <IconRequired />
          </LabelContainer>
        </Col>
        <Col xs={4}>
          <Upload
            {...getFieldProps('complianceFile', {
              initialValue: this.setInitialValue('complianceFile', ''),
              rules: [
                {
                  required: true,
                  message: 'Cần đính kèm file'
                }
              ]
            })}
            name={this.setInitialValue('complianceFile.file.originalname', '')}
            onSuccess={this.onSuccessUpload}
          />
          <ErrorContainer>
            {(getFieldError('complianceFile') || []).join(', ')}
          </ErrorContainer>
        </Col>
      </Row>
    ];
  }

  constructor(props){
    super(props)
    this.isBiXuPhat = false
    if (props.isEdit) {
      this.isBiXuPhat = _.result(this.props.initialValue, 'complianceType.Name') === 'Bị xử phạt' ? true : false;
    }
  }

  render() {
    const { getFieldProps, getFieldError, getFieldValue } = this.props.form;
    const complianceType = getFieldValue('complianceType');
    const isBiXuPhat =
      (this.isBiXuPhat || _.result(complianceType, 'label') === 'Bị xử phạt') ? true : false;
    return (
      <div>
        <Row>
          <Col xs={4}>
            <HeaderContainer>
              <Icon
                icon={IconNames.SAVED}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.PRIMARY}
                onClick={this.submit}
              />
              <Clearfix width={8} />
              <Link to={this.props.slugBack ? this.props.slugBack : slug.BASE}>
                <Icon
                  icon={IconNames.DISABLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                />
              </Link>
            </HeaderContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>
              Tên cơ sở
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <SelectFacility
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'facility',
                option: {
                  initialValue: this.setSelInitialValue(
                    'facility',
                    SELECT_ITEM_DEFAULT,
                    'FacilityNameFull'
                  ),
                  rules: [
                    {
                      required: true,
                      message: 'Vui lòng chọn tên cơ sở'
                    }
                  ]
                }
              }}
            />
            <ErrorContainer>
              {(getFieldError('facility') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>
              Ngày thanh tra
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder="Ngày thanh tra"
              {...getFieldProps('InspectionDate', {
                initialValue: convertDate(
                  this.setInitialValue('InspectionDate', null)
                ),
                rules: [
                  {
                    required: true,
                    message: 'Vui lòng nhập ngày thanh tra'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('InspectionDate') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Mục đích</LabelContainer>
          </Col>
          <Col xs={4}>
            <SelectMucDich
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'purposeObj',
                option: {
                  initialValue: this.setSelInitialValue(
                    'purposeObj',
                    SELECT_ITEM_DEFAULT
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Tình trạng</LabelContainer>
          </Col>
          <Col xs={4}>
            <SelectTinhTrang
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'inspectionStatus',
                option: {
                  initialValue: this.setSelInitialValue(
                    'inspectionStatus',
                    SELECT_ITEM_DEFAULT
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Kết quả kiểm tra</LabelContainer>
          </Col>
          <Col xs={4}>
            <SelectSuKienTuanThu
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'complianceType',
                option: {
                  initialValue: this.setSelInitialValue(
                    'complianceType',
                    SELECT_ITEM_DEFAULT
                  ),
                  onChange: ()=>{
                    this.isBiXuPhat = false
                  }
                }
              }}
            />
          </Col>
        </Row>
        {isBiXuPhat && this.renderLogic()}
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Ghi chú</LabelContainer>
          </Col>
          <Col xs={6}>
            <EditorCustom
              {...getFieldProps('Notes', {
                initialValue: this.setInitialValue('Notes', null)
              })}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default createForm()(InspectionForm);
