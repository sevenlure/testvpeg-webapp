import React from 'react';
// import * as _ from 'lodash';
import slug from 'constants/slug';
import { getOne } from 'api/inspectionApi';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import DisplayField from 'components/elements/display-field';
import InnerHTML from 'components/elements/InnerHTML';
import DisplayFieldLink from 'components/elements/display-field-link';
import { Icon, Intent, Colors } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { Link } from 'react-router-dom';
import moment from 'moment';
import AttachmentContainer from 'containers/AttachmentContainer';
import {
  getList as getAttachList,
  deleteOne as deleteAttach,
  create as createAttach
} from 'api/inspectionAttachmentApi';
import { deleteOne } from 'api/inspectionApi';
import Message from 'rc-message';
import { getLookupTypeInspectionAttachment } from 'api/lookuplistitemApi';
import Confirm from 'components/elements/confirm-element';
import { Row, Col } from 'reactstrap';
import PageLockHOC from 'hoc/pageLock';
import Permissions from 'react-redux-permissions';
import RuleDiv from 'components/ruleDiv';

const ContainerWrapper = styled.div``;
const ContainerHeader = styled.div`
  display: flex;
`;
const ContainerAction = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const Content = styled.div``;
@PageLockHOC(['facilityInspection.view'])
export default class InspectionView extends React.Component {
  static propTypes = {};

  state = {
    _idObject: '',
    isLoading: true,
    initialValue: {}
  };

  async checkValidate() {
    const _id = _.result(this.props, 'match.params._id');
    if (_id) {
      const res = await getOne(_id);
      if (res.success) {
        this.setState({
          _idObject: _id,
          isLoading: false,
          initialValue: {
            ...res.data,
            InspectionDate:
              res.data && res.data.InspectionDate
                ? moment(res.data.InspectionDate).format('DD/MM/YYYY')
                : null
          }
        });
      }
    } else {
      this.props.history.push(slug.INSPECTTION.LIST);
    }
  }

  updateInitialValue = values => {
    this.setState({
      _idObject: values._id,
      isLoading: false,
      initialValue: values
    });
  };

  componentWillMount = async () => {
    await this.checkValidate();
  };

  getInitialValue = (keyPath, defaultValue = '') => {
    return _.result(this.state.initialValue, keyPath, defaultValue);
  };

  handelDelete = () => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      const datetimeTemp = this.getInitialValue('InspectionDate');
      this.ConfirmElm.showConfirm({
        content: `Bạn có muốn xoá lượt điều tra của cơ sở: ${this.getInitialValue(
          'facility.FacilityNameFull'
        )}\n Ngày: ${datetimeTemp}`,
        cb: async value => {
          if (value) {
            const res = await deleteOne(this.getInitialValue('_id'));
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.props.history.push(slug.INSPECTTION.LIST);
            }
            if (res.error) Message.error({ content: res.message });
          }
        }
      });
    }
  };

  render() {
    const isBiXuPhat =
      this.getInitialValue('complianceType.Name') === 'Bị xử phạt'
        ? true
        : false;

    return (
      <ContainerWrapper>
        <Confirm ref={ConfirmElm => (this.ConfirmElm = ConfirmElm)} />
        <Clearfix height={8} />
        {!this.state.isLoading && (
          <div>
            <ContainerHeader>
              <h3
                style={{
                  color: Colors.GREEN2
                }}
                className='bp3-heading'
              >
                Chi tiết thanh tra
              </h3>
            </ContainerHeader>
            <ContainerAction>
              <div style={{ display: 'inline-flex' }}>
                <Link to={slug.INSPECTTION.LIST}>
                  <Icon
                    style={{ cursor: 'pointer' }}
                    icon={IconNames.CIRCLE_ARROW_LEFT}
                    iconSize={Icon.SIZE_LARGE}
                    intent={Intent.SUCCESS}
                  />
                </Link>

                <Permissions allowed={[`facilityInspection.write`]}>
                  <RuleDiv initialValue={this.state.initialValue}>
                    <React.Fragment>
                      <Clearfix width={8} />
                      <Link
                        to={
                          slug.INSPECTTION.EDIT_WITH_ID +
                          this.getInitialValue('_id')
                        }
                      >
                        <Icon
                          style={{ cursor: 'pointer' }}
                          icon={IconNames.MANUALLY_ENTERED_DATA}
                          iconSize={Icon.SIZE_LARGE}
                          intent={Intent.PRIMARY}
                        />
                      </Link>
                    </React.Fragment>
                  </RuleDiv>
                </Permissions>

                <Permissions allowed={[`facilityInspection.delete`]}>
                  <RuleDiv initialValue={this.state.initialValue}>
                    <React.Fragment>
                      <Clearfix width={8} />
                      <Icon
                        style={{ cursor: 'pointer' }}
                        icon={IconNames.BAN_CIRCLE}
                        iconSize={Icon.SIZE_LARGE}
                        intent={Intent.DANGER}
                        onClick={this.handelDelete}
                      />
                    </React.Fragment>
                  </RuleDiv>
                </Permissions>

                <Clearfix width={8} />
                <Link to={''}>
                  <Icon
                    style={{ cursor: 'pointer' }}
                    icon={IconNames.PRINT}
                    iconSize={Icon.SIZE_LARGE}
                    intent={Intent.SUCCESS}
                  />
                </Link>
              </div>
            </ContainerAction>
            <Clearfix height={8} />
            <Content>
              <Clearfix height={8} />
              <DisplayFieldLink
                size={14}
                isRequiredField={true}
                label='Tên Cơ sở'
                linkURL={`${slug.CO_SO.VIEW_WITH_ID}${this.getInitialValue(
                  'facility._id'
                )}`}
                value={this.getInitialValue('facility.FacilityNameFull')}
              />
              <Clearfix height={8} />
              <DisplayField
                size={14}
                isRequiredField={true}
                label='Ngày thanh tra'
                value={`${
                  this.getInitialValue('InspectionDate')
                    ? this.getInitialValue('InspectionDate')
                    : ''
                }`}
              />
              <Clearfix height={8} />
              <DisplayField
                size={14}
                label='Mục đích'
                value={this.getInitialValue('purposeObj.Name')}
              />
              <Clearfix height={8} />
              <DisplayField
                size={14}
                label='Tình trạng'
                value={this.getInitialValue('inspectionStatus.Name')}
              />
              <Clearfix height={8} />
              <DisplayField
                size={14}
                label='Kết quả kiểm tra'
                value={this.getInitialValue('complianceType.Name')}
              />
              {isBiXuPhat && [
                <Clearfix key='Clearfix1' height={8} />,
                <DisplayField
                  key='ComplianceFineField'
                  size={14}
                  label='Tiền phạt'
                  value={this.getInitialValue('ComplianceFine').toString()}
                />,
                <Clearfix key='Clearfix2' height={8} />,
                <Row key='ComplianceFineRow'>
                  <Col xs={3}>File</Col>
                  <Col xs={8}>
                    <a
                      style={{
                        display: 'inline-flex',
                        color: Colors.GREEN2,
                        textDecoration: 'underline'
                      }}
                      href={this.getInitialValue('complianceFile.url')}
                    >
                      {this.getInitialValue('complianceFile.file.originalname')}
                    </a>
                  </Col>
                </Row>
              ]}

              <Clearfix height={8} />
              <DisplayField
                size={14}
                label='Thêm vào'
                value={`${
                  this.getInitialValue('AddedOn')
                    ? moment(this.getInitialValue('AddedOn')).format(
                        'DD/MM/YYYY HH:mm'
                      ) +
                      ' bởi ' +
                      this.getInitialValue('AddedBy')
                    : ''
                } `}
              />
              <Clearfix height={8} />
              <DisplayField
                size={14}
                label='Cập nhật lần cuối'
                value={`${
                  this.getInitialValue('UpdatedOn')
                    ? moment(this.getInitialValue('UpdatedOn')).format(
                        'DD/MM/YYYY HH:mm'
                      ) +
                      ' bởi ' +
                      this.getInitialValue('UpdatedBy')
                    : ''
                }`}
              />
              <Clearfix height={8} />
              <InnerHTML
                label='Ghi chú'
                value={this.getInitialValue('Notes')}
              />
              <Clearfix height={8} />

              <AttachmentContainer
                facility={this.state.initialValue}
                getListFunc={query => {
                  return getAttachList({
                    ...query,
                    inspection: this.getInitialValue('_id') //_id của inspection
                  });
                }}
                createOne={values => {
                  values.inspection = this.getInitialValue('_id');
                  return createAttach(_.pickBy(values, _.identity));
                }}
                deleteOne={deleteAttach}
                getAttachmentTypeList={getLookupTypeInspectionAttachment}
                moduleName="facilityInspection"
              />
            </Content>
          </div>
        )}
      </ContainerWrapper>
    );
  }
}
