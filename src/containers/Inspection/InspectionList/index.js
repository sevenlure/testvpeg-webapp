import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import React from 'react';
import { BaseListContainer } from 'containers/BaseContainer/BaseListContainer';
import { setMenuSelected } from '@redux/actions/menuAction';
import moment from 'moment';
import { DATE_FORMAT } from 'config';
import { getList, exportData } from 'api/inspectionApi';
import slug from 'constants/slug';
import { Colors } from '@blueprintjs/core';
import styled from 'styled-components';
import numeral from 'numeral';
import { createForm } from 'rc-form';
import YearSelect from 'components/elements/select-year';
import TinhTrangSelect from 'components/elements/select-tinh-trang';
import Clearfix from 'components/elements/clearfix';
import { Row, Col } from 'reactstrap';
import PageLockHOC from 'hoc/pageLock'

const NameContainer = styled.div`
  color: ${Colors.GREEN2};
  text-decoration: underline;
`;

@PageLockHOC(['facilityInspection.view'])
class InspectionList extends BaseListContainer {
  constructor(props) {
    const funcApi = {
      getList,
      exportData
    };
    const columns = [
      {
        dataField: 'facility',
        text: 'Tên Cơ sở',
        formatter: (cell, row) => {
          return (
            <Link
              to={`${slug.INSPECTTION.VIEW_WITH_ID}${row._id ? row._id : ''}`}
            >
              <NameContainer>{cell ? cell.FacilityNameFull : ''}</NameContainer>
            </Link>
          );
        }
      },
      {
        dataField: 'industrialArea',
        text: '	Khu công nghiệp/Cụm Công nghiệp',
        formatter: (cell, row) => {
          if (_.has(row, 'facility.industrialArea.Name')) {
            return <div>{row.facility.industrialArea.Name}</div>;
          } else return '';
        }
      },
      {
        dataField: 'district',
        text: 'Quận/Huyện',
        formatter: (cell, row) => {
          if (_.has(row, 'facility.district.Name')) {
            return <div>{row.facility.district.Name}</div>;
          } else return '';
        }
      },
      {
        dataField: 'sector',
        text: 'Ngành/Nghề',
        formatter: (cell, row) => {
          if (_.has(row, 'facility.sector.Name')) {
            return <div>{row.facility.sector.Name}</div>;
          } else return '';
        }
      },
      {
        dataField: 'InspectionDate',
        text: 'Ngày',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
      {
        // Todo
        dataField: 'inspectionStatus',
        text: 'Tình trạng',
        formatter: (cell, row) => {
          if (_.has(row, 'inspectionStatus.Name')) {
            return <div>{row.inspectionStatus.Name}</div>;
          } else return '';
        }
      },
      {
        // Todo
        dataField: 'purposeObj',
        text: 'Mục đích',
        formatter: (cell, row) => {
          if (_.has(row, 'purposeObj.Name')) {
            return <div>{row.purposeObj.Name}</div>;
          } else return '';
        }
      },
      {
        // Todo
        dataField: 'complianceType',
        text: 'Kết quả kiểm tra',
        formatter: (cell, row) => {
          if (_.has(row, 'complianceType.Name')) {
            return <div>{row.complianceType.Name}</div>;
          } else return '';
        }
      },
      {
        // Todo
        dataField: 'ComplianceFine',
        text: 'Số tiền (đ)',
        formatter: (cell, row) => {
          return <div>{cell ? numeral(cell).format('0,0') : ''}</div>;
        }
      },
      {
        dataField: 'AddedOn',
        text: 'Thêm vào',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
      {
        dataField: 'UpdatedOn',
        text: 'Cập nhật ngày',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      }
    ];
    super(props, funcApi, columns, slug.INSPECTTION.CREATE,'facilityInspection');
    this.state = {
      ...this.state
    };
  }

  renderSearchContainer() {
    return (
      <div>
        {super.renderSearchContainer()}
        {this.renderSearchExpandContainer()}
      </div>
    );
  }

  renderSearchExpandContainer = () => {
    const form = this.props.form;
    return (
      <div>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <YearSelect
              fieldForm={{
                form: form,
                name: 'year'
              }}
            />
          </Col>
          <Col xs={4}>
            <TinhTrangSelect
              fieldForm={{
                form: form,
                name: 'inspectionStatus'
              }}
            />
          </Col>
        </Row>
      </div>
    );
  };

  handelSubmitSearch(values) {
    values.year = _.get(values.year, 'value');
    values.inspectionStatus = _.get(values.inspectionStatus, 'value');
    super.handelSubmitSearch(values);
  }

}

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(createForm()(InspectionList))
);
