import React from 'react';
import { Route } from 'react-router-dom';
import slug from 'constants/slug';
import InspectionList from './InspectionList';
import InspectionCreate from './InspectionCreate';
import InspectionView from './InspectionView';
import InspectionEdit from './inspectionEdit';

export default props => [
  <Route
    exact
    key={slug.INSPECTTION.VIEW}
    path={slug.INSPECTTION.VIEW}
    render={matchProps => <InspectionView {...matchProps} {...props} />}
  />,
  <Route
    exact
    key={slug.INSPECTTION.LIST}
    path={slug.INSPECTTION.LIST}
    render={matchProps => <InspectionList {...matchProps} {...props} />}
  />,
  <Route
    key={slug.INSPECTTION.CREATE}
    path={slug.INSPECTTION.CREATE}
    render={matchProps => <InspectionCreate {...matchProps} {...props} />}
  />,
  <Route
    key={slug.INSPECTTION.EDIT}
    path={slug.INSPECTTION.EDIT}
    render={matchProps => <InspectionEdit {...matchProps} {...props} />}
  />
];
