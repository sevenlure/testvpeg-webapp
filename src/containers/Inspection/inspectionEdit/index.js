import React from 'react';
import InspectionForm from '../InspectionForm';
// import * as _ from 'lodash';
import slug from 'constants/slug';
import { getOne, updateOne } from 'api/inspectionApi';
import Message from 'rc-message';
import PageLockHOC from 'hoc/pageLock';

@PageLockHOC(['facilityInspection.write'])
export default class InspectionEdit extends React.Component {
  static propTypes = {};

  state = {
    _idObject: '',
    isLoading: true,
    initialValue: null
  };

  async checkValidate() {
    const _id = _.result(this.props, 'match.params._id');
    if (_id) {
      const res = await getOne(_id);
      console.log(res.data, 'data');
      if (res.success) {
        this.setState({
          _idObject: _id,
          isLoading: false,
          initialValue: res.data
        });
      }
    } else {
      this.props.history.push(slug.INSPECTTION.LIST);
    }
  }

  async componentWillMount() {
    await this.checkValidate();
  }

  handelSubmit = async values => {
    this.setState({
      isLoading: true
    });

    const res = await updateOne(this.state._idObject, values);
    if (res.success) {
      Message.success({ content: 'Update Success!!!' });
      this.props.history.push(
        slug.INSPECTTION.VIEW_WITH_ID + this.state._idObject
      );
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div>
        {!this.state.isLoading && (
          <div>
            <InspectionForm
              isEdit={true}
              initialValue={this.state.initialValue}
              onSubmit={this.handelSubmit}
              slugBack={slug.INSPECTTION.LIST}
            />
          </div>
        )}
      </div>
    );
  }
}
