import React from 'react';
// import { Route } from 'react-router-dom';
import slug from 'constants/slug';
import {
  Button,
  Card,
  Elevation,
  InputGroup,
  Intent,
  Hotkey,
  Hotkeys,
  HotkeysTarget
} from '@blueprintjs/core';
import styled from 'styled-components';
import { createForm } from 'rc-form';
import { login } from 'api/auth';
import Message from 'rc-message';
import { connect } from 'react-redux';
import { logged } from '@redux/actions/authAction';
// import styles from './login.module.scss';

// import '../../login.css';
// console.log(styles.app,'styles')

const CenterContainer = styled.div`
  position: absolute;
  left: 50%;
  top: 30%;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
`;

const HeaderContainer = styled.div`
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-align-items: center;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -webkit-box-pack: justify;
  -webkit-justify-content: space-between;
  -ms-flex-pack: justify;
  justify-content: space-between;
  font-size: 28px;
  margin-bottom: 0px;
  font-weight: 600;
`;

const Label = styled.label`
  color: rgba(0, 0, 0, 0.6);
  font-size: 14px;
  font-weight: 600;
`;

const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;


@connect(
  state => ({
    isLogged: state.auth.isLogged
  }),
  {
    logged
  }
)
@createForm()
@HotkeysTarget
export default class Login extends React.Component {

  state = {
    isSubmitting: false
  };

  renderHotkeys() {
    return <Hotkeys>
        <Hotkey
            //global={true}
            allowInInput={true}
            combo="enter"
            label="Be awesome all the time"
            onKeyDown={this.submit}
        />
    </Hotkeys>;
}

  submit = () => {
    if (this.state.isSubmitting) return;
    this.setState({ isSubmitting: true });
    this.props.form.validateFields((error, value) => {
      if (error) {
        this.setState({ isSubmitting: false });
        return;
      }
      console.log(value);
      this.login(value);
    });
  };

  login = async data => {
    const res = await login(data);
    if (res.error) Message.error({ content: res.message });
    if (res.success) {
      this.props.logged(res.token, res.data);
      this.props.history.push(slug.CO_SO.LIST);
    }
    this.setState({ isSubmitting: false });
  };

  render() {
    const { getFieldProps, getFieldError } = this.props.form;
    return (
      <div
        style={{
          width: '100vw',
          height: '100vh',
          background: 'linear-gradient(135deg,#6ABE4E, rgb(13, 128, 80) 100%)'
        }}
      >
        <CenterContainer>
          <Card
            interactive={true}
            elevation={Elevation.TWO}
            style={{
              width: 650,
              padding: '24px 32px'
            }}
          >
            <HeaderContainer>
           
              <div style={{ padding: '16px 0px' }}>
              <div style={{ paddingBottom: 8, fontSize: 24, color: "#0f9960" }}>Cổng Thông Tin Quản Lý Môi Truờng Bình Duơng</div>
              Login
              </div>
              {/* <div>
                <img
                  alt="logo"
                  height={36}
                  width="auto"
                  src="/vietan-logo.png"
                />
              </div> */}
            </HeaderContainer>
            <div>
              <Label className="bp3-label">
                Username
                <InputGroup
                  fill
                  type="text"
                  placeholder="UserName"
                  dir="auto"
                  {...getFieldProps('UserName', {
                    initialValue: '',
                    rules: [
                      {
                        required: true
                      }
                    ]
                  })}
                />
                <ErrorContainer>
                  {(getFieldError('UserName') || []).join(', ')}
                </ErrorContainer>
              </Label>
            </div>
            <div>
              <Label className="bp3-label">
                Password
                <InputGroup
                  fill
                  type="Password"
                  placeholder="******"
                  dir="auto"
                  {...getFieldProps('Password', {
                    initialValue: '',
                    rules: [
                      {
                        required: true
                      }
                    ]
                  })}
                />
                <ErrorContainer>
                  {(getFieldError('Password') || []).join(', ')}
                </ErrorContainer>
              </Label>
            </div>
            <Button
              loading={this.state.isSubmitting}
              fill
              intent={Intent.SUCCESS}
              onClick={this.submit}
            >
              Login
            </Button>
          </Card>
        </CenterContainer>
        <div
        style={{
          bottom: 0,
          right: 0,
          position: 'absolute'
        }}
        >
                <img
                  alt="logo"
                  height={36}
                  width="auto"
                  src="/vietan-logo.png"
                />
              </div>
      </div>
    );
  }
}
