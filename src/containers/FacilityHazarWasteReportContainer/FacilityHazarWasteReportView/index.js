import React from 'react';
import PropTypes from 'prop-types';
import { getOne, deleteOne } from 'api/facilityHazarWasteReportApi';
import Message from 'rc-message';
// import * as _ from 'lodash';
import { Icon, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { Row, Col } from 'reactstrap';
import Confirm from 'components/elements/confirm-element';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import { withRouter } from 'react-router-dom';
import { COLOR, STYLE, DATETIME_FORMAT, DATE_FORMAT } from 'config';
import moment from 'moment';
import PageLockHOC from 'hoc/pageLock';
import Permissions from 'react-redux-permissions';
import RuleDiv from 'components/ruleDiv';

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

class RowValue extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    isRequiredField: PropTypes.bool
  };

  render() {
    return (
      <div>
        <Row>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              width: '100%'
            }}
          >
            <Col
              style={this.props.isRequiredField ? STYLE.REQUIREDFIELD : {}}
              xs={2}
            >
              {this.props.label}
            </Col>
            <Col xs={7}>{this.props.value}</Col>
          </div>
        </Row>
        <Clearfix height={8} />
      </div>
    );
  }
}

@PageLockHOC(['facilityHazarWasteReport.view'])
class FacilityHazarWasteReportView extends React.Component {
  state = {
    _idObject: '',
    isLoaded: false,
    isLoading: false,
    facility: {},
    facilityId: '',
    initialValue: null
  };

  getInitialValue = (keyPath, defaultValue = '') => {
    let val = _.result(this.state.initialValue, keyPath, defaultValue);
    if (!val) val = '';
    return val;
  };

  getInitialDateValue = (keyPath, isDate, defaultValue = '') => {
    let value = _.result(this.state.initialValue, keyPath, defaultValue);
    if (!value) return '';
    if (isDate) return moment(value).format(DATE_FORMAT);
    return moment(value).format(DATETIME_FORMAT);
  };

  async checkValidate() {
    const _id = _.result(this.props, 'match.params._id');
    if (_id) {
      const res = await getOne(_id);
      console.log(res);
      if (res.success && res.data) {
        this.setState({
          _idObject: res.data._id,
          facility: res.data.facility,
          facilityId: res.data.facility._id,
          isLoading: false,
          isLoaded: true,
          initialValue: res.data
        });
      }
    } else {
      this.props.history.push(slug.HAZARDOUS_WASTE_REPORT.LIST);
    }
  }

  handelDelete = () => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      this.ConfirmElm.showConfirm({
        content: 'Bạn có muốn xoá ',
        cb: async value => {
          if (value) {
            const res = await deleteOne(this.getInitialValue('_id'));
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.props.history.push(slug.HAZARDOUS_WASTE_REPORT.LIST);
            }
            if (res.error) Message.error({ content: res.message });
          }
        }
      });
    }
  };

  async componentWillMount() {
    await this.checkValidate();
  }

  render() {
    return (
      <div style={{ width: '100%' }}>
        <Confirm ref={ConfirmElm => (this.ConfirmElm = ConfirmElm)} />
        <h4
          className='bp3-heading'
          style={{
            color: COLOR.SUCCESS
          }}
        >
          Phí bảo vệ môi trường
        </h4>
        <HeaderContainer>
          <div style={{ display: 'inline-flex' }}>
            <Link to={slug.HAZARDOUS_WASTE_REPORT.LIST}>
              <Icon
                style={{ cursor: 'pointer' }}
                icon={IconNames.CIRCLE_ARROW_LEFT}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.SUCCESS}
              />
            </Link>

            <Permissions allowed={[`facilityHazarWasteReport.write`]}>
            <RuleDiv initialValue={this.state.initialValue}>
            <React.Fragment>
                <Clearfix width={8} />
                <Link
                  to={
                    slug.HAZARDOUS_WASTE_REPORT.EDIT_WITH_ID +
                    this.state._idObject
                  }
                >
                  <Icon
                    style={{ cursor: 'pointer' }}
                    icon={IconNames.MANUALLY_ENTERED_DATA}
                    iconSize={Icon.SIZE_LARGE}
                    intent={Intent.PRIMARY}
                  />
                </Link>
              </React.Fragment>
            </RuleDiv>
             
            </Permissions>

            <Permissions allowed={[`facilityHazarWasteReport.delete`]}>
            <RuleDiv initialValue={this.state.initialValue}>
            <React.Fragment>
                <Clearfix width={8} />
                <Icon
                  style={{ cursor: 'pointer' }}
                  icon={IconNames.BAN_CIRCLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                  onClick={this.handelDelete}
                />
              </React.Fragment>
            </RuleDiv>
             
            </Permissions>

            <Clearfix width={8} />
            <Icon
              style={{ cursor: 'pointer' }}
              icon={IconNames.PRINT}
              iconSize={Icon.SIZE_LARGE}
              intent={Intent.SUCCESS}
            />
          </div>
        </HeaderContainer>
        <Clearfix height={16} />
        <RowValue
          isRequiredField
          label='Tên Cơ Sở'
          value={this.getInitialValue('facility.FacilityNameFull')}
        />

<RowValue label='Mã số QLCTNH' value={this.getInitialValue('MaSoQLCTNH')} />
<RowValue label='Cấp lần' value={this.getInitialValue('CapLan')} />
<RowValue label='Năm thực hiện báo cáo' value={this.getInitialValue('NamThucHienBaoCao')} />
<RowValue label='Hình thức nộp' value={this.getInitialValue('HinhThucNop')} />
<RowValue label='Ngày nộp báo cáo' value={this.getInitialDateValue('NgayNopBaoCao')} />
<RowValue label='Tên cán bộ thụ lý hồ sơ' value={this.getInitialValue('TenCanBoThuLy')} />
<RowValue label='Ngày có ý kiến trả lời' value={this.getInitialDateValue('NgayCoYKienTraLoi')} />
<RowValue label='Số hiệu văn bản trả lời' value={this.getInitialValue('SoHieuVanBanTraLoi')} />
<RowValue label='Tổng khối lượng CTCNTT (kg)' value={this.getInitialValue('TongKhoiLuongCTCNTT')} />
<RowValue label='Tổng khối lượng CTCNNH (kg)' value={this.getInitialValue('TongKhoiLuongCTCNNH')} />
<RowValue label='Tổng khối lượng bùn nguy hại phát sinh (kg)' value={this.getInitialValue('TongKhoiLuongBunNguyHai')} />

<RowValue label='Đơn vị thu gom, xử lý CTCNTT' value={this.getInitialValue('DonViThuGom_CTCNTT.Name')} />

<RowValue label='Đơn vị thu gom, xử lý CTCNNH' value={this.getInitialValue('DonViThuGom_CTCNNH.Name')} />



        {/* <RowValue label='Năm' value={this.getInitialValue('Year')} />
        <RowValue
          label='Ngày nộp'
          value={this.getInitialDateValue('SubmitDate', true)}
        />
        <RowValue
          isRequiredField
          label='Tình trạng nộp'
          value={this.getInitialValue('PaymentStatus')}
        />
        <RowValue
          label='Đơn vị thu gom'
          value={this.getInitialValue('collector.Name')}
        />
        {this.getInitialValue('collector.Name')==='Khác' && <RowValue
          label=' '
          value={this.getInitialValue('ExtraInfo')}
        />}
        <RowValue label='Khối luợng' value={this.getInitialValue('Volume')} /> */}
        <RowValue
          label='Thêm vào'
          value={
            this.getInitialDateValue('AddedOn') +
            ' bởi ' +
            this.getInitialValue('AddedBy')
          }
        />
        <RowValue
          label='Cập nhật lần cuối'
          value={
            this.getInitialDateValue('UpdatedOn') +
            ' bởi ' +
            this.getInitialValue('UpdatedBy')
          }
        />
      </div>
    );
  }
}

export default withRouter(FacilityHazarWasteReportView);
