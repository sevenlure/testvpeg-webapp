import React from 'react';
import slug from 'constants/slug';
import { getOne, updateOne } from 'api/facilityHazarWasteReportApi';
import Message from 'rc-message';
// import * as _ from 'lodash';
import FacilityHazarWasteReportForm from '../FacilityHazarWasteReportForm';
import PageLockHOC from 'hoc/pageLock';

@PageLockHOC(['facilityHazarWasteReport.write'])
export default class FacilityHazarWasteReportEdit extends React.Component {
  static propTypes = {};

  state = {
    isLoaded: false,
    isLoading: false,
    objData: {},
    facility: null,
    facilityId: '',
    _idObject: ''
  };

  async checkValidate() {
    const _id = _.result(this.props, 'match.params._id');
    if (_id) {
      const res = await getOne(_id);
      if (res.success && res.data) {
        this.setState(
          {
            _idObject: res.data._id,
            objData: res.data,
            facility: res.data.facility,
            isLoading: false,
            isLoaded: true
          },
          () => {
            this.FacilityHazarWasteReportForm.onChangeFacility(
              res.data.facility
            );
          }
        );
      }
    } else {
      //this.props.history.push(slug.CO_SO.LIST);
    }
  }

  async componentWillMount() {
    await this.checkValidate();
  }

  handelSubmit = async values => {
    this.setState({
      isLoading: true
    });
    console.log(values);
    const res = await updateOne(this.state._idObject, values);
    if (res.success && res.data) {
      Message.success({ content: 'Success!!!' });
      this.props.history.push(
        slug.HAZARDOUS_WASTE_REPORT.VIEW_WITH_ID + res.data._id
      );
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div>
        {this.state.isLoaded && (
          <FacilityHazarWasteReportForm
            getRef={ref => (this.FacilityHazarWasteReportForm = ref)}
            isEdit
            slugBack={
              slug.HAZARDOUS_WASTE_REPORT.VIEW_WITH_ID + this.state._idObject
            }
            initialValue={this.state.objData}
            facility={this.state.facility}
            handelSubmit={this.handelSubmit}
          />
        )}
      </div>
    );
  }
}
