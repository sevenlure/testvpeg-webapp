import React from 'react';
import { Route } from 'react-router-dom';
import slug from 'constants/slug';
import FacilityHazarWasteReportList from './FacilityHazarWasteReportList';
import FacilityHazarWasteReportCreate from './FacilityHazarWasteReportCreate';
import FacilityHazarWasteReportEdit from './FacilityHazarWasteReportEdit';
import FacilityHazarWasteReportView from './FacilityHazarWasteReportView';

export default props => [
  <Route
    exact
    key={slug.HAZARDOUS_WASTE_REPORT.LIST}
    path={slug.HAZARDOUS_WASTE_REPORT.LIST}
    render={matchProps => <FacilityHazarWasteReportList {...matchProps} {...props} />}
  />,
  <Route
    key={slug.HAZARDOUS_WASTE_REPORT.CREATE}
    path={slug.HAZARDOUS_WASTE_REPORT.CREATE}
    render={matchProps => <FacilityHazarWasteReportCreate {...matchProps} {...props} />}
  />,
  <Route
    key={slug.HAZARDOUS_WASTE_REPORT.VIEW}
    path={slug.HAZARDOUS_WASTE_REPORT.VIEW}
    render={matchProps => <FacilityHazarWasteReportView {...matchProps} {...props} />}
  />,
  <Route
    key={slug.HAZARDOUS_WASTE_REPORT.EDIT}
    path={slug.HAZARDOUS_WASTE_REPORT.EDIT}
    render={matchProps => <FacilityHazarWasteReportEdit {...matchProps} {...props} />}
  />
];