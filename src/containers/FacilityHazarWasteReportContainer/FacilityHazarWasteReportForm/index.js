import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { Icon, Intent, InputGroup } from '@blueprintjs/core';
import Clearfix from 'components/elements/clearfix';
import { IconNames } from '@blueprintjs/icons';
import { createForm } from 'rc-form';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import IconRequired from 'components/elements/required-icon';
import FacilitySelect from 'components/elements/select-facility';
import NumberInput from 'rc-input-number';
import { DateInput } from '@blueprintjs/datetime';
import moment from 'moment';
import { convertDateFromSever as convertDate } from 'utils/fnUtil';
import { COLOR, DATE_FORMAT, STYLE } from 'config';
import {
  getDataForCustomSelect,
  PaymentStatusData,
  ThongBaoLanData as CapLanData,
  HinhThucNopData
} from 'constants/custom-data';
import CustomDataSelect from 'components/elements/select-custom-data';
import dynamicRoleHOC from 'hoc/dynamicRole';
import SelectBoPhanThuGomCTNH from 'components/elements/select-bo-phan-thu-gom-ctnh';
import YearSelect from 'components/elements/select-year';

const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

const HeaderContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-start;
  cursor: pointer;
`;
@dynamicRoleHOC()
class FacilityHazarWasteReportForm extends React.Component {
  static propTypes = {
    isEdit: PropTypes.bool,
    initialValue: PropTypes.object,
    slugBack: PropTypes.string,
    handelSubmit: PropTypes.func
    // facility: PropTypes.shape({
    //   _id: PropTypes.string,
    //   FacilityNameFull: PropTypes.string
    // })
  };

  constructor(props) {
    super(props);
    let isExtraInfo = _.result(
      this.props.initialValue,
      'collector.isHasCustomInfo',
      false
    );
    this.state = {
      isExtraInfo,
      DiaChiText: '',
      NganhNgheText: ''
    };
  }

  componentDidMount(){
    if(this.props.getRef) this.props.getRef(this)
  }

  setInitialValue(keyPath, defaultValue) {
    if (this.props.isEdit) {
      let value = _.result(this.props.initialValue, keyPath, defaultValue);
      if (!value) value = defaultValue;
      return value;
    }
    return defaultValue;
  }

  setSelInitialValue(keyPath, defaultValue, nameLabel) {
    if (this.props.isEdit) {
      let selItem = _.result(this.props.initialValue, keyPath);
      if (!selItem) return defaultValue;
      return {
        value: selItem._id,
        label: nameLabel ? selItem[nameLabel] : selItem.Name
      };
    }
    return defaultValue;
  }

  submit = () => {
    this.props.form.validateFields((error, value) => {
      console.log(value);
      value.facility = _.get(value.facility, 'value');
      value.PaymentStatus = _.get(value.PaymentStatus, 'value');
      value.collector = _.get(value.collector, 'value');

      // NOTE update
      value.CapLan = _.get(value.CapLan, 'value');
      value.NamThucHienBaoCao = _.get(value.NamThucHienBaoCao, 'value');
      value.HinhThucNop = _.get(value.HinhThucNop, 'value');
      value.DonViThuGom_CTCNTT = _.get(value.DonViThuGom_CTCNTT, 'value');
      value.DonViThuGom_CTCNNH = _.get(value.DonViThuGom_CTCNNH, 'value');
      
      if (error) return;
      this.props.handelSubmit(value);
    });
  };

  onChangeFacility = obj => {
    this.setState({
      DiaChiText: _.get(obj, 'Address1'),
      NganhNgheText: _.get(obj, 'sector.Name')
    });
  };

  render() {
    const { getFieldProps, getFieldError } = this.props.form;

    return (
      <div>
        <h4
          className='bp3-heading'
          style={{
            color: COLOR.SUCCESS
          }}
        >
          Báo cáo chất thải nguy hại
        </h4>
        <Row>
          <Col xs={4}>
            <HeaderContainer>
              <Icon
                icon={IconNames.SAVED}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.PRIMARY}
                onClick={() => {
                  this.submit();
                }}
              />
              <Clearfix width={8} />
              <Link to={this.props.slugBack ? this.props.slugBack : slug.BASE}>
                <Icon
                  icon={IconNames.DISABLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                />
              </Link>
            </HeaderContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Tên Cơ sở
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <FacilitySelect
              isDisabled={this.props.facility ? true : false}
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'facility',
                option: {
                  initialValue: this.props.facility
                    ? {
                        value: this.props.facility._id,
                        label: this.props.facility.FacilityNameFull
                      }
                    : null,
                  rules: [
                    {
                      required: true,
                      message: 'Tên Cơ sở is required'
                    }
                  ]
                }
              }}
              cbChangeFacility={this.onChangeFacility}
            />
            <ErrorContainer>
              {(getFieldError('facility') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>

        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Địa chỉ</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup disabled value={this.state.DiaChiText} />
          </Col>
        </Row>

        <Row>
          <Col xs={4}>
            <LabelContainer>Ngành Nghề</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup disabled value={this.state.NganhNgheText} />
          </Col>
        </Row>

        <Row>
          <Col xs={4}>
            <LabelContainer>Mã số QLCTNH</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Mã số QLCTNH'
              {...getFieldProps('MaSoQLCTNH', {
                initialValue: this.setInitialValue('MaSoQLCTNH', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Cấp lần</LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={CapLanData}
              fieldForm={{
                form: this.props.form,
                name: 'CapLan',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('CapLan', null)
                  )
                }
              }}
            />
          </Col>
        </Row>

        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Năm thực hiện báo cáo</LabelContainer>
          </Col>
          <Col xs={4}>
            <YearSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'NamThucHienBaoCao',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('NamThucHienBaoCao', '')
                  )
                }
              }}
            />
          </Col>
        </Row>

        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Hình thức nộp</LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={HinhThucNopData}
              fieldForm={{
                form: this.props.form,
                name: 'HinhThucNop',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('HinhThucNop', null)
                  )
                }
              }}
            />
          </Col>
        </Row>

        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>Ngày nộp báo cáo</LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder='Ngày nộp báo cáo'
              {...getFieldProps('NgayNopBaoCao', {
                initialValue: convertDate(
                  this.setInitialValue('NgayNopBaoCao', null)
                )
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Tên cán bộ thụ lý hồ sơ</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Tên cán bộ thụ lý hồ sơ'
              {...getFieldProps('TenCanBoThuLy', {
                initialValue: this.setInitialValue('TenCanBoThuLy', '')
              })}
            />
          </Col>
        </Row>

        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>Ngày có ý kiến trả lời</LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder='Ngày có ý kiến trả lời'
              {...getFieldProps('NgayCoYKienTraLoi', {
                initialValue: convertDate(
                  this.setInitialValue('NgayCoYKienTraLoi', null)
                )
              })}
            />
          </Col>
        </Row>

        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Số hiệu văn bản trả lời</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Số hiệu văn bản trả lời'
              {...getFieldProps('SoHieuVanBanTraLoi', {
                initialValue: this.setInitialValue('SoHieuVanBanTraLoi', '')
              })}
            />
          </Col>
        </Row>

        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>Tổng khối lượng CTCNTT (kg)</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              {...getFieldProps('TongKhoiLuongCTCNTT', {
                initialValue: this.setInitialValue('TongKhoiLuongCTCNTT', null)
              })}
            />
          </Col>
        </Row>

        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>Tổng khối lượng CTCNNH (kg)</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              {...getFieldProps('TongKhoiLuongCTCNNH', {
                initialValue: this.setInitialValue('TongKhoiLuongCTCNNH', null)
              })}
            />
          </Col>
        </Row>

        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>
              Tổng khối lượng bùn nguy hại phát sinh (kg)
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              {...getFieldProps('TongKhoiLuongBunNguyHai', {
                initialValue: this.setInitialValue(
                  'TongKhoiLuongBunNguyHai',
                  null
                )
              })}
            />
          </Col>
        </Row>

        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Đơn vị thu gom, xử lý CTCNTT</LabelContainer>
          </Col>
          <Col xs={4}>
            <SelectBoPhanThuGomCTNH
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'DonViThuGom_CTCNTT',
                option: {
                  initialValue: this.setSelInitialValue(
                    'DonViThuGom_CTCNTT',
                    null
                  )
                  // onChange: val => {
                  //   this.setState({
                  //     isExtraInfo: _.result(val, 'isHasCustomInfo', false)
                  //   });
                  // }
                }
              }}
            />
          </Col>
        </Row>

        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Đơn vị thu gom, xử lý CTCNNH</LabelContainer>
          </Col>
          <Col xs={4}>
            <SelectBoPhanThuGomCTNH
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'DonViThuGom_CTCNNH',
                option: {
                  initialValue: this.setSelInitialValue(
                    'DonViThuGom_CTCNNH',
                    null
                  )
                  // onChange: val => {
                  //   this.setState({
                  //     isExtraInfo: _.result(val, 'isHasCustomInfo', false)
                  //   });
                  // }
                }
              }}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default createForm()(FacilityHazarWasteReportForm);
