import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { Icon, Intent } from '@blueprintjs/core';
import Clearfix from 'components/elements/clearfix';
import { IconNames } from '@blueprintjs/icons';
import { createForm } from 'rc-form';
// import * as _ from 'lodash';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import IconRequired from 'components/elements/required-icon';
import FacilitySelect from 'components/elements/select-facility';
import { STYLE } from 'config';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, ContentState } from 'draft-js';
import htmlToDraft from 'html-to-draftjs';
import { convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import EmissionTypeSelect from 'components/elements/select-emission-type';
import { COLOR } from 'config';
import NumberInput from 'rc-input-number';
import dynamicRoleHOC from 'hoc/dynamicRole'

const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

const HeaderContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-start;
  cursor: pointer;
`;

@dynamicRoleHOC()
class FacilityemissionForm extends React.Component {
  static propTypes = {
    isEdit: PropTypes.bool,
    initialValue: PropTypes.object,
    slugBack: PropTypes.string,
    handelSubmit: PropTypes.func
    // facility: PropTypes.shape({
    //   _id: PropTypes.string,
    //   FacilityNameFull: PropTypes.string
    // })
  };

  constructor(props) {
    super(props);

    this.state = {
      editorState:
        this.props.isEdit && this.props.initialValue.Description
          ? this.convertFromHTML2EditorState(this.props.initialValue.Description)
          : EditorState.createEmpty()
    };
  }

  convertFromHTML2EditorState = rawHtml => {
    const contentBlock = htmlToDraft(rawHtml);
    const contentState = ContentState.createFromBlockArray(
      contentBlock.contentBlocks
    );
    return EditorState.createWithContent(contentState);
  };

  onEditorStateChange = editorState => {
    this.setState({
      editorState
    });
  };

  setInitialValue(keyPath, defaultValue) {
    if (this.props.isEdit) {
      let value = _.result(this.props.initialValue, keyPath, defaultValue);
      if (!value) value = defaultValue;
      return value;
    }
    return defaultValue;
  }

  setSelInitialValue(keyPath, defaultValue, nameLabel) {
    if (this.props.isEdit) {
      let selItem = _.result(this.props.initialValue, keyPath);
      if (!selItem) return defaultValue;
      return {
        value: selItem._id,
        label: nameLabel ? selItem[nameLabel] : selItem.Name
      };
    }
    return defaultValue;
  }

  submit = isAddNew => {
    this.props.form.validateFields((error, value) => {
      value.facility = _.get(value.facility, 'value');
      value.emissionType = _.get(value.emissionType, 'value');
      console.log(value);
      if (error) return;
      if (this.props.handelSubmit) this.props.handelSubmit(value, isAddNew);
    });
  };

  resetForm() {
    this.props.form.resetFields(['Name','DataValue','Description','emissionType']);
    this.setState({
      editorState: EditorState.createEmpty()
    });
  }

  render() {
    const { getFieldProps, getFieldError, getFieldValue } = this.props.form;
    return (
      <div>
        <h4
          className="bp3-heading"
          style={{
            color: COLOR.SUCCESS
          }}
        >
          Quản lý Ô nhiễm
        </h4>
        <Row>
          <Col xs={4}>
            <HeaderContainer>
              <Icon
                icon={IconNames.SAVED}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.PRIMARY}
                onClick={() => {
                  this.submit();
                }}
              />
              <Clearfix width={8} />
              <Link to={this.props.slugBack ? this.props.slugBack : slug.BASE}>
                <Icon
                  icon={IconNames.DISABLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                />
              </Link>
              <Clearfix width={8} />
              {!this.props.isEdit && (
                <Icon
                  icon={IconNames.DOCUMENT_OPEN}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.SUCCESS}
                  onClick={() => {
                     this.submit(true);
                    //this.resetForm();
                  }}
                />
              )}
            </HeaderContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Tên Cơ sở
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <FacilitySelect
              isDisabled={this.props.facility ? true : false}
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'facility',
                option: {
                  initialValue: this.props.facility
                    ? {
                        value: this.props.facility._id,
                        label: this.props.facility.FacilityNameFull
                      }
                    : null,
                  rules: [
                    {
                      required: true,
                      message: 'Tên Cơ sở is required'
                    }
                  ]
                }
              }}
            />
            <ErrorContainer>
              {(getFieldError('facility') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        {/* <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Tên chất thải <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder="Tên chất thải"
              {...getFieldProps('Name', {
                initialValue: this.setInitialValue('Name', ''),
                rules: [
                  {
                    required: true,
                    message: 'Tên chất thải is required'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('Name') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row> */}
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Loại chất thải <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <EmissionTypeSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'emissionType',
                option: {
                  initialValue: this.setSelInitialValue('emissionType', null),
                  rules: [
                    {
                      required: true,
                      message: 'Loại chất thải is required'
                    }
                  ]
                }
              }}
            />
            <ErrorContainer>
              {(getFieldError('emissionType') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer style={STYLE.REQUIREDFIELD}>
              Giá trị <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={3}>
            <NumberInput
              placeholder="Giá trị"
              {...getFieldProps('DataValue', {
                initialValue: this.setInitialValue('DataValue', ''),
                rules: [
                  {
                    required: true,
                    message: 'Giá trị is required'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('DataValue') || []).join(', ')}
            </ErrorContainer>
          </Col>
          <Col xs={1} style={{
            marginTop: 'auto',
            marginBottom: 'auto',
            paddingBottom: 4,
          }}>
           {_.result(getFieldValue('emissionType'),'ExtraInfo','')}
          </Col>
        </Row>

        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Chú thích</LabelContainer>
          </Col>
          <Col xs={6}>
            <Editor
              editorState={this.state.editorState}
              wrapperClassName="demo-wrapper"
              editorClassName="demo-editor"
              onEditorStateChange={this.onEditorStateChange}
              {...getFieldProps('Description', {
                initialValue: this.props.isEdit
                  ? draftToHtml(
                      convertToRaw(this.state.editorState.getCurrentContent())
                    )
                  : '',
                getValueFromEvent: value => {
                  return draftToHtml(
                    convertToRaw(this.state.editorState.getCurrentContent())
                  );
                }
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
      </div>
    );
  }
}

export default createForm()(FacilityemissionForm);
