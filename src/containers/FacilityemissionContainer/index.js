import React from 'react';
import { Route } from 'react-router-dom';
import slug from 'constants/slug';
import FacilityemissionList from './FacilityemissionList';
import FacilityemissionCreate from './FacilityemissionCreate';
import FacilityemissionEdit from './FacilityemissionEdit';
import FacilityemissionView from './FacilityemissionView';

export default props => [
  <Route
    exact
    key={slug.FACILITY_EMISSTION.LIST}
    path={slug.FACILITY_EMISSTION.LIST}
    render={matchProps => <FacilityemissionList {...matchProps} {...props} />}
  />,
  <Route
    key={slug.FACILITY_EMISSTION.CREATE}
    path={slug.FACILITY_EMISSTION.CREATE}
    render={matchProps => <FacilityemissionCreate {...matchProps} {...props} />}
  />,
  <Route
    key={slug.FACILITY_EMISSTION.VIEW}
    path={slug.FACILITY_EMISSTION.VIEW}
    render={matchProps => <FacilityemissionView {...matchProps} {...props} />}
  />,
  <Route
    key={slug.FACILITY_EMISSTION.EDIT}
    path={slug.FACILITY_EMISSTION.EDIT}
    render={matchProps => <FacilityemissionEdit {...matchProps} {...props} />}
  />
];