import React from 'react';
import slug from 'constants/slug';
import { create } from 'api/facilityemissionApi';
import { getOne as getFacility } from 'api/cosoApi';
import Message from 'rc-message';
// import * as _ from 'lodash';
import FacilityemissionForm from '../FacilityemissionForm';
import { getQueryString } from 'utils/fnUtil';
import PageLockHOC from 'hoc/pageLock'
@PageLockHOC(['facilityEmission.write'])
export default class FacilityemissionCreate extends React.Component {
  static propTypes = {};

  state = {
    isLoaded: true,
    isLoading: false,
    facility: null,
    facilityId: ''
  };

  async checkValidate() {
    const queryString = getQueryString(this.props.location.search);
    const _id = queryString.facilityId;
    if (_id) {
      const res = await getFacility(_id);
      if (res.success) {
        this.setState({
          facility: res.data,
          facilityId: res.data._id,
          isLoading: false,
          isLoaded: true
        });
      }
    } else {
      //this.props.history.push(slug.CO_SO.LIST);
    }
  }

  async componentWillMount() {
    await this.checkValidate();
  }

  handelSubmit = async (values, isAddNew) => {
    this.setState({
      isLoading: true
    });
    console.log(values);
    const res = await create(_.pickBy(values, _.identity));
    if (res.success && res.data) {
      Message.success({ content: 'Success!!!' });
      if (isAddNew) {
        this.props.history.replace(
          slug.FACILITY_EMISSTION.CREATE + '?facilityId=' + res.data.facility
        );
        if (this.form && this.form.resetForm) this.form.resetForm();
      } else
        this.props.history.push(
          slug.FACILITY_EMISSTION.VIEW_WITH_ID + res.data._id
        );
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div>
        {this.state.isLoaded && (
          <FacilityemissionForm
          wrappedComponentRef={form => {this.form = form}}
            slugBack={slug.FACILITY_EMISSTION.LIST}
            facility={this.state.facility ? this.state.facility : null}
            handelSubmit={this.handelSubmit}
          />
        )}
      </div>
    );
  }
}
