import React from 'react';
import CosoForm from '../CosoForm';
// import * as _ from 'lodash';
import slug from 'constants/slug';
import { getOne, updateOne } from 'api/cosoApi';
import Message from 'rc-message';
import PageLockHOC from 'hoc/pageLock'


@PageLockHOC(['facility.write'])
export default class CosoEdit extends React.Component {
  static propTypes = {};

  state = {
    _idObject: '',
    isLoading: true,
    initialValue: null
  };

  async checkValidate() {
    const _id = _.result(this.props, 'match.params._id');
    if (_id) {
      const res = await getOne(_id);
      if (res.success) {
        this.setState({
          _idObject: _id,
          isLoading: false,
          initialValue: res.data
        });
      }
    } else {
      this.props.history.push(slug.CO_SO.LIST);
    }
  }

  async componentWillMount() {
    await this.checkValidate();
  }

  handelSubmitSearch = async values => {
    this.setState({
      isLoading: true
    });

    const res = await updateOne(this.state._idObject, values);
    if (res.success) {
      Message.success({ content: 'Update Success!!!' });
      this.props.history.push(slug.CO_SO.VIEW_WITH_ID + this.state._idObject);
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div>
        {!this.state.isLoading && (
          <CosoForm
            isEdit
            initialValue={this.state.initialValue}
            handelSubmitSearch={this.handelSubmitSearch}
            slugBack={slug.CO_SO.LIST}
          />
        )}
      </div>
    );
  }
}
