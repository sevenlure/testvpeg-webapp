import React from 'react';
import PropTypes from 'prop-types';
import QuanHuyenSelect from 'components/elements/select-quan-huyen';
import PhuongXaSelect from 'components/elements/select-phuong-xa';
import NganhNgheSelect from 'components/elements/select-nganh-nghe';
import KhuCumCongNghiepSelect from 'components/elements/select-khu-cum-cong-nghiep';
import { Row, Col } from 'reactstrap';
import { Icon, Intent, InputGroup } from '@blueprintjs/core';
import Clearfix from 'components/elements/clearfix';
import ThamQuyenQuanLy from 'components/elements/select-tham-quyen-quan-ly';
import { IconNames } from '@blueprintjs/icons';
import { createForm } from 'rc-form';
// import * as _ from 'lodash';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import NumberInput from 'rc-input-number';
import { SELECT_ITEM_DEFAULT } from 'config';
import { checkFacilityCodeExist } from 'api/cosoApi';
import IconRequired from 'components/elements/required-icon';
import CustomDataSelect from 'components/elements/select-custom-data';
import {
  getDataForCustomSelect,
  FrequencyOfMonitoringData
} from 'constants/custom-data';
import { VungCapNuocData } from 'constants/custom-data';
import Select from 'react-select';
import dynamicRoleHOC from 'hoc/dynamicRole'

const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

const HeaderContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-start;
  cursor: pointer;
`;
@dynamicRoleHOC()
class CosoForm extends React.Component {
  static propTypes = {
    isEdit: PropTypes.bool,
    initialValue: PropTypes.object,
    slugBack: PropTypes.string,
    handelSubmitSearch: PropTypes.func
  };

  constructor(props) {
    super(props);
    if (props.isEdit)
      this.state = {
        VungCapNuoc_4: [...VungCapNuocData],
        VungCapNuoc_3: _.result(
          props.initialValue.ReceviedSource_4,
          'children'
        ),
        VungCapNuoc_2: _.result(
          props.initialValue.ReceviedSource_3,
          'children'
        ),
        VungCapNuoc_1: _.result(
          props.initialValue.ReceviedSource_2,
          'children'
        ),
        VungCapNuoc_4_Val: props.initialValue.ReceviedSource_4,
        VungCapNuoc_3_Val: props.initialValue.ReceviedSource_3,
        VungCapNuoc_2_Val: props.initialValue.ReceviedSource_2,
        VungCapNuoc_1_Val: props.initialValue.ReceviedSource_1
      };
    else
      this.state = {
        VungCapNuoc_4: [...VungCapNuocData],
        VungCapNuoc_3: [],
        VungCapNuoc_2: [],
        VungCapNuoc_1: [],
        VungCapNuoc_4_Val: null,
        VungCapNuoc_3_Val: null,
        VungCapNuoc_2_Val: null,
        VungCapNuoc_1_Val: null
      };

    let isKhuCongNghiep = true;
    let industrialAreaName = _.result(
      this.props,
      'initialValue.industrialArea.Name'
    );
  
    if (industrialAreaName && industrialAreaName.startsWith('CỤM'))
      isKhuCongNghiep = false;

    this.state = {
      ...this.state,
      isKhuCongNghiep
    };
  }

  setInitialValue(keyPath, defaultValue) {
    if (this.props.isEdit) {
      let value = _.result(this.props.initialValue, keyPath, defaultValue);
      if (!value) value = defaultValue;
      return value;
    }
    return defaultValue;
  }

  setSelInitialValue(keyPath, defaultValue, nameLabel) {
    if (this.props.isEdit) {
      let selItem = _.result(this.props.initialValue, keyPath);
      if (!selItem) return defaultValue;
      return {
        value: selItem._id,
        label: nameLabel ? selItem[nameLabel] : selItem.Name
      };
    }
    return defaultValue;
  }

  submit = () => {
    this.props.form.validateFields((error, value) => {
      console.log(value);
      console.log(error);
      if (error) return;
      value.district = _.get(value.district, 'value');
      value.sector = _.get(value.sector, 'value');
      value.industrialArea = _.get(value.industrialArea, 'value');
      value.authority = _.get(value.authority, 'value');
      value.Commune = _.get(value.Commune, 'value');
      value.FrequencyOfMonitoring = _.get(value.FrequencyOfMonitoring, 'value');

      value.ReceviedSource_4 = this.state.VungCapNuoc_4_Val;
      value.ReceviedSource_3 = this.state.VungCapNuoc_3_Val;
      value.ReceviedSource_2 = this.state.VungCapNuoc_2_Val;
      value.ReceviedSource_1 = this.state.VungCapNuoc_1_Val;

      if (!value.PContactTel) value.PContactTel = null;
      if (!value.SContactTel) value.SContactTel = null;
      if (this.props.handelSubmitSearch) this.props.handelSubmitSearch(value);
    });
  };

  getFacilityCode = () => {
    const { getFieldValue } = this.props.form;
    let BusinessLicenseNumber = getFieldValue('BusinessLicenseNumber');
    let Branchcode = getFieldValue('Branchcode')
      ? '.' + getFieldValue('Branchcode')
      : '';

    let FacilityCode = BusinessLicenseNumber + Branchcode;

    return FacilityCode;
  };

  checkFacilityCode = async (rule, value, callback) => {
    const { setFields } = this.props.form;
    let FacilityCode = this.getFacilityCode();

    let oldCode = this.props.isEdit ? this.props.initialValue.FacilityCode : '';

    const res = await checkFacilityCodeExist({ FacilityCode, oldCode });
    if (res.success) {
      if (res.data.exist) {
        // callback(new Error('need to be upper!'));
        setFields({
          FacilityCode: {
            value: FacilityCode
          }
        });
        if (callback) callback(new Error('Mã Cơ sở đã tồn tại'));
      } else {
        setFields({
          FacilityCode: {
            value: FacilityCode
          }
        });
        if (callback) callback();
      }
    }
    // callback();
  };

  render() {
    const {
      getFieldProps,
      getFieldValue,
      getFieldError,
      setFieldsValue
    } = this.props.form;
    return (
      <div>
        <Row>
          <Col xs={4}>
            <HeaderContainer>
              <Icon
                icon={IconNames.SAVED}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.PRIMARY}
                onClick={this.submit}
              />
              <Clearfix width={8} />
              <Link to={this.props.slugBack ? this.props.slugBack : slug.BASE}>
                <Icon
                  icon={IconNames.DISABLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                />
              </Link>
            </HeaderContainer>
          </Col>
        </Row>
        <Row>
          <Col xs={4}>
            <LabelContainer>
              Tên Cơ sở
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Tên Cơ sở'
              {...getFieldProps('FacilityNameFull', {
                initialValue: this.setInitialValue('FacilityNameFull', ''),
                rules: [
                  {
                    required: true,
                    message: 'Tên sở sở is required'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('FacilityNameFull') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>
              Số giấy phép chứng nhận đăng ký doanh nghiệp
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Số giấy phép chứng nhận đăng ký doanh nghiệp'
              {...getFieldProps('BusinessLicenseNumber', {
                initialValue: this.setInitialValue('BusinessLicenseNumber', ''),
                validateTrigger: ['onBlur'],
                onBlur: this.checkFacilityCode,
                rules: [
                  {
                    required: true,
                    message:
                      'Số giấy phép chứng nhận đăng ký doanh nghiệp is required'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('BusinessLicenseNumber') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Mã chi nhánh</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Mã chi nhánh'
              {...getFieldProps('Branchcode', {
                initialValue: this.setInitialValue('Branchcode', ''),
                validateTrigger: ['onBlur'],
                onBlur: this.checkFacilityCode,
                rules: [
                  {
                    max: 10,
                    message: 'Mã chi nhánh không được quá 10 ký tự'
                  },
                  {
                    pattern: /^[a-zA-Z0-9_]+$/,
                    message:
                      'Mã chi nhánh không bao gồm ký tự có dấu và ký tự đặc biệt'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('Branchcode') || []).map(mess => (
                <div key={mess}>
                  {mess} <br />
                </div>
              ))}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Mã Cơ sở</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              readOnly
              className='disabled'
              placeholder='Mã Cơ sở'
              {...getFieldProps('FacilityCode', {
                initialValue: this.setInitialValue('FacilityCode', ''),
                validateTrigger: ['onBlur'],
                rules: [this.checkFacilityCode]
              })}
            />
            <ErrorContainer>
              {(getFieldError('FacilityCode') || []).map(mess => (
                <div key={mess}>
                  {mess} <br />
                </div>
              ))}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />

        <Row>
          <Col xs={4}>
            <LabelContainer>
              Tần suất giám sát
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <CustomDataSelect
              hideLabel
              data={FrequencyOfMonitoringData}
              fieldForm={{
                form: this.props.form,
                name: 'FrequencyOfMonitoring',
                option: {
                  initialValue: getDataForCustomSelect(
                    this.setInitialValue('FrequencyOfMonitoring', null)
                  ),
                  rules: [
                    {
                      required: true,
                      message: 'Tần suất giám sát is required'
                    }
                  ]
                  // onChange: val => {
                  //   setFieldsValue({
                  //     SubmitTime: null
                  //   });
                  // }
                }
              }}
            />
            <ErrorContainer>
              {(getFieldError('FrequencyOfMonitoring') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>
              {this.state.isKhuCongNghiep
                ? 'Khu công nghiệp'
                : 'Cụm Công nghiệp'}
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <KhuCumCongNghiepSelect
              onChangeIsKhuCongNghiep={val => {
                this.setState({
                  isKhuCongNghiep: val
                });
                setFieldsValue({
                  industrialArea: SELECT_ITEM_DEFAULT
                });
              }}
              isKhuCongNghiep={this.state.isKhuCongNghiep}
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'industrialArea',
                option: {
                  initialValue: this.setSelInitialValue(
                    'industrialArea',
                    SELECT_ITEM_DEFAULT
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Thẩm quyền quản lý môi trường</LabelContainer>
          </Col>
          <Col xs={4}>
            <ThamQuyenQuanLy
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'authority',
                option: {
                  initialValue: this.setSelInitialValue(
                    'authority',
                    SELECT_ITEM_DEFAULT
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Ngành/Nghề</LabelContainer>
          </Col>
          <Col xs={4}>
            <NganhNgheSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'sector',
                option: {
                  initialValue: this.setSelInitialValue(
                    'sector',
                    SELECT_ITEM_DEFAULT
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Điện thoại</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              placeholder='Điện thoại'
              {...getFieldProps('Telephone', {
                initialValue: this.setInitialValue('Telephone', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Fax</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              placeholder='Fax'
              {...getFieldProps('Fax', {
                initialValue: this.setInitialValue('Fax', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Diện tích (m²)</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              min={0}
              placeholder='Diện tích (m²)'
              {...getFieldProps('Aream2', {
                initialValue: this.setInitialValue('Aream2', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Vĩ độ (°)</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              placeholder='Vĩ độ (°)'
              min={-90}
              max={90}
              {...getFieldProps('Latitude', {
                initialValue: this.setInitialValue('Latitude', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Kinh độ (°)</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              placeholder='Kinh độ (°)'
              min={0}
              max={360}
              {...getFieldProps('Longitude', {
                initialValue: this.setInitialValue('Longitude', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Quận/Huyện</LabelContainer>
          </Col>
          <Col xs={4}>
            <QuanHuyenSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'district',
                option: {
                  initialValue: this.setSelInitialValue(
                    'district',
                    SELECT_ITEM_DEFAULT
                  ),
                  onChange: value => {
                    setFieldsValue({
                      Commune: SELECT_ITEM_DEFAULT
                    });
                  }
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Phường/Xã</LabelContainer>
          </Col>
          <Col xs={4}>
            <PhuongXaSelect
              quanHuyenTarget={
                getFieldValue('district')
                  ? getFieldValue('district').value
                  : this.setInitialValue('district._id', null)
              }
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'Commune',
                option: {
                  initialValue:
                    this.props.isEdit &&
                    _.get(this.props.initialValue, 'Commune')
                      ? {
                          value: this.props.initialValue.Commune,
                          label: this.props.initialValue.Commune
                        }
                      : SELECT_ITEM_DEFAULT
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Địa chỉ</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Địa chỉ'
              {...getFieldProps('Address1', {
                initialValue: this.setInitialValue('Address1', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Nguồn tiếp nhận 4</LabelContainer>
          </Col>
          <Col xs={4}>
            <Select
              options={this.state.VungCapNuoc_4}
              isClearable
              getOptionValue={option => option['name']}
              getOptionLabel={option => option['name']}
              placeholder='-- Select --'
              value={this.state.VungCapNuoc_4_Val}
              onChange={val => {
                let res = _.result(val, 'children', []);
                this.setState({
                  VungCapNuoc_4_Val: val,
                  VungCapNuoc_3: [...res],
                  VungCapNuoc_3_Val: null,
                  VungCapNuoc_2_Val: null,
                  VungCapNuoc_1_Val: null
                });
              }}
            />
            {/* <KenhRachSuoiSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'ReceviedSource_1',
                option: {
                  initialValue:
                    this.props.isEdit &&
                    _.get(this.props.initialValue, 'ReceviedSource_1')
                      ? {
                          value: this.props.initialValue.ReceviedSource_1,
                          label: this.props.initialValue.ReceviedSource_1
                        }
                      : SELECT_ITEM_DEFAULT
                }
              }}
            /> */}
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Nguồn tiếp nhận 3</LabelContainer>
          </Col>
          <Col xs={4}>
            {/* <KenhRachSuoiSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'ReceviedSource_2',
                option: {
                  initialValue:
                    this.props.isEdit &&
                    _.get(this.props.initialValue, 'ReceviedSource_2')
                      ? {
                          value: this.props.initialValue.ReceviedSource_2,
                          label: this.props.initialValue.ReceviedSource_2
                        }
                      : SELECT_ITEM_DEFAULT
                }
              }}
            /> */}
            <Select
              options={this.state.VungCapNuoc_3}
              isClearable
              getOptionValue={option => option['name']}
              getOptionLabel={option => option['name']}
              placeholder='-- Select --'
              value={this.state.VungCapNuoc_3_Val}
              onChange={val => {
                let res = _.result(val, 'children', []);
                this.setState({
                  VungCapNuoc_3_Val: val,
                  VungCapNuoc_2: [...res],
                  VungCapNuoc_2_Val: null,
                  VungCapNuoc_1_Val: null
                });
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Nguồn tiếp nhận 2</LabelContainer>
          </Col>
          <Col xs={4}>
            {/* <SongSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'ReceviedSource_3',
                option: {
                  initialValue:
                    this.props.isEdit &&
                    _.get(this.props.initialValue, 'ReceviedSource_3')
                      ? {
                          value: this.props.initialValue.ReceviedSource_3,
                          label: this.props.initialValue.ReceviedSource_3
                        }
                      : SELECT_ITEM_DEFAULT
                }
              }}
            /> */}
            <Select
              options={this.state.VungCapNuoc_2}
              isClearable
              getOptionValue={option => option['name']}
              getOptionLabel={option => option['name']}
              placeholder='-- Select --'
              value={this.state.VungCapNuoc_2_Val}
              onChange={val => {
                let res = _.result(val, 'children', []);
                this.setState({
                  VungCapNuoc_2_Val: val,
                  VungCapNuoc_1: [...res],
                  VungCapNuoc_1_Val: null
                });
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Nguồn tiếp nhận 1</LabelContainer>
          </Col>
          <Col xs={4}>
            {/* <SongSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'ReceviedSource_3',
                option: {
                  initialValue:
                    this.props.isEdit &&
                    _.get(this.props.initialValue, 'ReceviedSource_3')
                      ? {
                          value: this.props.initialValue.ReceviedSource_3,
                          label: this.props.initialValue.ReceviedSource_3
                        }
                      : SELECT_ITEM_DEFAULT
                }
              }}
            /> */}
            <Select
              options={this.state.VungCapNuoc_1}
              isClearable
              getOptionValue={option => option['name']}
              getOptionLabel={option => option['name']}
              placeholder='-- Select --'
              value={this.state.VungCapNuoc_1_Val}
              onChange={val => {
                this.setState({
                  VungCapNuoc_1_Val: val
                });
              }}
            />
          </Col>
        </Row>

        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>
              <b>Cán bộ phụ trách môi trường</b>
            </LabelContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Họ và tên</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Họ và tên'
              {...getFieldProps('PContactFirst', {
                initialValue: this.setInitialValue('PContactFirst', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Điện thoại</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              placeholder='Điện thoại'
              {...getFieldProps('PContactTel', {
                initialValue: this.setInitialValue('PContactTel', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Email</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Email'
              {...getFieldProps('PContactEmail', {
                initialValue: this.setInitialValue('PContactEmail', ''),
                rules: [
                  {
                    type: 'email',
                    message: 'định dạng email không đúng'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('PContactEmail') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>
              <b>Người đại diện pháp luật </b>
            </LabelContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Họ và tên</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Họ và tên'
              {...getFieldProps('SContactFirst', {
                initialValue: this.setInitialValue('SContactFirst', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Điện thoại</LabelContainer>
          </Col>
          <Col xs={4}>
            <NumberInput
              placeholder='Điện thoại'
              {...getFieldProps('SContactTel', {
                initialValue: this.setInitialValue('SContactTel', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Email</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Email'
              {...getFieldProps('SContactEmail', {
                initialValue: this.setInitialValue('SContactEmail', ''),
                rules: [
                  {
                    type: 'email',
                    message: 'định dạng email không đúng'
                  }
                ]
              })}
            />
            <ErrorContainer>
              {(getFieldError('SContactEmail') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
      </div>
    );
  }
}

export default createForm()(CosoForm);
