import React from 'react';
import CosoForm from '../CosoForm';
import slug from 'constants/slug';
import { create } from 'api/cosoApi';
import Message from 'rc-message';
// import * as _ from 'lodash';
import PageLockHOC from 'hoc/pageLock'


@PageLockHOC(['facility.write'])
export default class CosoCreate extends React.Component {
  static propTypes = {};

  handelSubmitSearch = async values => {
    this.setState({
      isLoading: true
    });

    const res = await create(_.pickBy(values, _.identity));
    if (res.success) {
      Message.success({ content: 'Success!!!' });
      this.props.history.push(slug.CO_SO.VIEW_WITH_ID + res.data._id);
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div>
        <CosoForm
          handelSubmitSearch={this.handelSubmitSearch}
          slugBack={slug.CO_SO.LIST}
        />
      </div>
    );
  }
}
