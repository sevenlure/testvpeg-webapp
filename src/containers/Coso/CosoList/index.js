import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import React from 'react';
import { BaseListContainer } from 'containers/BaseContainer/BaseListContainer';
import { setMenuSelected } from '@redux/actions/menuAction';
import moment from 'moment';
import { DATE_FORMAT } from 'config';
import { getList, exportData } from 'api/cosoApi';
import slug from 'constants/slug';
import { Colors } from '@blueprintjs/core';
import styled from 'styled-components';
import { createForm } from 'rc-form';
import PageLockHOC from 'hoc/pageLock'

const NameContainer = styled.div`
  color: ${Colors.GREEN2};
  text-decoration: underline;
`;
@PageLockHOC(['facility.view'])
class CosoList extends BaseListContainer {
  constructor(props) {
    const funcApi = {
      getList,
      exportData
    };
    const columns = [
      {
        dataField: 'FacilityNameFull',
        text: 'Tên Cơ sở',
        formatter: (cell, row) => {
          return (
            <Link to={slug.CO_SO.VIEW_WITH_ID + row._id}>
              <NameContainer>{cell}</NameContainer>
            </Link>
          );
        }
      },
      {
        dataField: 'industrialArea',
        text: '	Khu công nghiệp/Cụm Công nghiệp',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'district',
        text: 'Quận/Huyện',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'Commune',
        text: 'Phường/Xã',
      },
      {
        dataField: 'sector',
        text: 'Ngành/Nghề',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'authority',
        text: 'Thẩm quyền quản lý môi trường',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        // Todo
        dataField: 'facilityinspections',
        text: 'Thanh tra',
        formatter: (cell, row) => {
          return <div>
          {cell && cell.length >0? cell.length : ""}
        </div>;
        }
      },
      {
        // Todo
        dataField: 'facilitypermits',
        text: 'Giấy phép',
        formatter: (cell, row) => {
          return <div>
            {cell && cell.length >0? cell.length : ""}
          </div>;
        }
      },
      {
        dataField: 'AddedOn',
        text: 'Thêm vào',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
      {
        dataField: 'UpdatedOn',
        text: 'Cập nhật ngày',
        // headerFormatter: (column, colIndex) => (
        //   <div style={{ width: 90 }} className="bp3-text-overflow-ellipsis">
        //     Cập nhật ngày
        //   </div>
        // ),
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      }
    ];
    super(props, funcApi, columns, slug.CO_SO.CREATE, 'facility');
    this.state = {
      ...this.state
    };
  }
}

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(createForm()(CosoList))
);
