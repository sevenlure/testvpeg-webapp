import React from 'react';
import PropTypes from 'prop-types';
import Clearfix from 'components/elements/clearfix';
import FacilityeiaList from 'containers/FacilityeiaContainer/FacilityeiaList';

class FacilityeiaPanel extends React.Component {
  static propTypes = {
    facilityId: PropTypes.string.isRequired,
    dataList: PropTypes.array,
  };

  render() {
    return (
      <div style={{ width: '100%' }}>
        <Clearfix height={8} />
        <FacilityeiaList hideRightHeaderContent facilityId={this.props.facilityId} dataList={this.props.dataList} />
      </div>
    );
  }
}

export default FacilityeiaPanel;
