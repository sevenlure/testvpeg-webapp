import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// import * as _ from 'lodash';
import { Row, Col } from 'reactstrap';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { API_KEY_GOOGLE_MAP } from 'config';
import { compose, withProps } from 'recompose';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from 'react-google-maps';
import { Icon, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import Confirm from 'components/elements/confirm-element';
import { deleteOne } from 'api/cosoApi';
import Message from 'rc-message';
import { withRouter } from 'react-router-dom';
import PageLockHOC from 'hoc/pageLock';
import Permissions from 'react-redux-permissions';
import RuleDiv from 'components/ruleDiv';

const MyMapComponent = compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${API_KEY_GOOGLE_MAP}&v=3.exp&libraries=geometry,drawing,places`,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap
    defaultMapTypeId='hybrid'
    defaultZoom={10}
    defaultCenter={
      props.mapLocation
        ? props.mapLocation
        : { lat: 11.1073184, lng: 106.6822954 }
    }
  >
    {props.mapLocation && (
      <Marker
        position={props.mapLocation}
        //position={{ lat: 11.1073184, lng: 106.6822954 }}
      />
    )}
  </GoogleMap>
));

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

class RowValue extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  };

  state = {};

  async componentWillMount() {}

  render() {
    return (
      <div>
        <Row>
          <Col xs={5}>{this.props.label}</Col>
          <Col xs={7}>{this.props.value}</Col>
        </Row>
        <Clearfix height={8} />
      </div>
    );
  }
}

@PageLockHOC(['facility.view'])
class CosoPanel extends React.Component {
  static propTypes = {
    initialValue: PropTypes.object
  };

  state = {};

  async componentWillMount() {}

  getInitialValue = (keyPath, defaultValue = '') => {
    return _.result(this.props.initialValue, keyPath, defaultValue);
  };

  handelDelete = () => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      this.ConfirmElm.showConfirm({
        content: 'Bạn có muốn xoá ' + this.getInitialValue('FacilityNameFull'),
        cb: async value => {
          if (value) {
            const res = await deleteOne(this.getInitialValue('_id'));
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.props.history.push(slug.CO_SO.LIST);
            }
            if (res.error) Message.error({ content: res.message });
          }
        }
      });
    }
  };

  render() {
    const mapLocation =
      this.getInitialValue('Latitude') && this.getInitialValue('Longitude')
        ? {
            lat: this.getInitialValue('Latitude'),
            lng: this.getInitialValue('Longitude')
          }
        : null;

    return (
      <div style={{ width: '100%' }}>
        <Confirm ref={ConfirmElm => (this.ConfirmElm = ConfirmElm)} />
        <Clearfix height={8} />
        <HeaderContainer>
          <div style={{ display: 'inline-flex' }}>
            <Link to={slug.CO_SO.LIST}>
              <Icon
                style={{ cursor: 'pointer' }}
                icon={IconNames.CIRCLE_ARROW_LEFT}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.SUCCESS}
              />
            </Link>

            <Permissions allowed={[`facility.write`]}>
            <RuleDiv initialValue={this.props.initialValue}>
            <Fragment>
                <Clearfix width={8} />
                <Link
                  to={slug.CO_SO.EDIT_WITH_ID + this.getInitialValue('_id')}
                >
                  <Icon
                    style={{ cursor: 'pointer' }}
                    icon={IconNames.MANUALLY_ENTERED_DATA}
                    iconSize={Icon.SIZE_LARGE}
                    intent={Intent.PRIMARY}
                  />
                </Link>
              </Fragment>
            </RuleDiv>
             
            </Permissions>

            <Permissions allowed={[`facility.delete`]}>
            <RuleDiv initialValue={this.props.initialValue}>
            <Fragment>
                <Clearfix width={8} />
                <Icon
                  style={{ cursor: 'pointer' }}
                  icon={IconNames.BAN_CIRCLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                  onClick={this.handelDelete}
                />
              </Fragment>
            </RuleDiv>
           
            </Permissions>

            <Clearfix width={8} />
            <Link to={''}>
              <Icon
                style={{ cursor: 'pointer' }}
                icon={IconNames.PRINT}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.SUCCESS}
              />
            </Link>
          </div>
        </HeaderContainer>
        <Clearfix height={8} />
        <Row>
          <Col xs={6}>
            <RowValue
              label='Tên Cơ sở'
              value={this.getInitialValue('FacilityNameFull')}
            />
            <RowValue
              label='Tần suất giám sát'
              value={this.getInitialValue('FrequencyOfMonitoring')}
            />
            <RowValue
              label='Khu công nghiệp/Cụm Công nghiệp'
              value={this.getInitialValue('industrialArea.Name')}
            />
            <RowValue
              label='Thẩm quyền quản lý môi trường'
              value={this.getInitialValue('authority.Name')}
            />
            <RowValue
              label='Ngành/Nghề'
              value={this.getInitialValue('sector.Name')}
            />
            <RowValue
              label='Điện thoại'
              value={this.getInitialValue('Telephone')}
            />
            <RowValue label='Fax' value={this.getInitialValue('Fax')} />
            <RowValue
              label='Diện tích (m²)'
              value={this.getInitialValue('Aream2')}
            />
            <RowValue
              label='Vĩ độ (°)'
              value={this.getInitialValue('Latitude')}
            />
            <RowValue
              label='Kinh độ (°)'
              value={this.getInitialValue('Longitude')}
            />
            <RowValue
              label='Quận/Huyện'
              value={this.getInitialValue('district.Name')}
            />
            <RowValue
              label='Phường/Xã'
              value={this.getInitialValue('Commune')}
            />
            <RowValue
              label='Địa chỉ'
              value={this.getInitialValue('Address1')}
            />
            <RowValue
              label='Vùng tiếp nhận 4'
              value={this.getInitialValue('ReceviedSource_4.name')}
            />
            <RowValue
              label='Vùng tiếp nhận 3'
              value={this.getInitialValue('ReceviedSource_3.name')}
            />
            <RowValue
              label='Vùng tiếp nhận 2'
              value={this.getInitialValue('ReceviedSource_2.name')}
            />
            <RowValue
              label='Vùng tiếp nhận 1'
              value={this.getInitialValue('ReceviedSource_1.name')}
            />
            <Row>
              <Col xs={5}>
                <b>Cán bộ phụ trách môi trường</b>
              </Col>
            </Row>
            <Clearfix height={8} />
            <RowValue
              label='Họ và tên'
              value={this.getInitialValue('PContactFirst')}
            />
            <RowValue
              label='Điện thoại'
              value={this.getInitialValue('PContactTel')}
            />
            <RowValue
              label='Email'
              value={this.getInitialValue('PContactEmail')}
            />
            <Row>
              <Col xs={5}>
                <b>Người đại diện pháp luật </b>
              </Col>
            </Row>
            <Clearfix height={8} />
            <RowValue
              label='Họ và tên'
              value={this.getInitialValue('SContactFirst')}
            />
            <RowValue
              label='Điện thoại'
              value={this.getInitialValue('SContactTel')}
            />
            <RowValue
              label='Email'
              value={this.getInitialValue('SContactEmail')}
            />
          </Col>
          <Col xs={6}>
            <MyMapComponent key='map' mapLocation={mapLocation} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default withRouter(CosoPanel);
