import React from 'react';
import PropTypes from 'prop-types';
import Clearfix from 'components/elements/clearfix';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { BaseListContainer } from 'containers/BaseContainer/BaseListContainer';
import { setMenuSelected } from '@redux/actions/menuAction';
import moment from 'moment';
import { DATE_FORMAT } from 'config';
import slug from 'constants/slug';
import styled from 'styled-components';
import { Colors } from '@blueprintjs/core';

const NameContainer = styled.div`
  color: ${Colors.GREEN2};
  text-decoration: underline;
`;

class FacilitypermitPanel extends React.Component {
  static propTypes = {
    facilityId: PropTypes.string.isRequired,
    hideRightHeaderContent: PropTypes.bool,
    dataList: PropTypes.array
  };

  render() {
    return (
      <div style={{ width: '100%' }}>
        <Clearfix height={8} />
        <FacilitypermitList hideRightHeaderContent facilityId={this.props.facilityId}
        dataList={this.props.dataList}
        />
      </div>
    );
  }
}








class FacilitypermitList extends BaseListContainer {
  static propTypes = {
    facilityId: PropTypes.string,
    hideRightHeaderContent: PropTypes.bool
  };

  constructor(props) {
    const funcApi = {
  
    };
    const columns = [
      {
        dataField: 'facility.FacilityNameFull',
        text: 'Tên Cơ sở',
        formatter: (cell, row) => {
          return (
            <Link to={slug.FACILITY_PERMIT.VIEW_WITH_ID + row._id}>
              <NameContainer>{cell}</NameContainer>
            </Link>
          );
        }
      },
      {
        dataField: 'facility.industrialArea',
        text: '	Khu công nghiệp/Cụm Công nghiệp',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'facility.district',
        text: 'Quận/Huyện',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'facility.sector',
        text: 'Ngành/Nghề',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'IssueDate',
        text: 'Ngày cấp',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
      {
        dataField: 'permitType',
        text: 'Loại',
        formatter: (cell, row) => {
          return <div>{cell ? cell.Name : ''}</div>;
        }
      },
      {
        dataField: 'AddedOn',
        text: 'Thêm vào',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
      {
        dataField: 'AddedBy',
        text: 'Thêm bởi'
      },
      {
        dataField: 'UpdatedOn',
        text: 'Cập nhật ngày',
        formatter: (cell, row) => {
          return <div>{moment(cell).format(DATE_FORMAT)}</div>;
        }
      },
      {
        dataField: 'UpdatedBy',
        text: 'Cập nhật bởi',
        formatter: (cell, row) => {
          return <div>{cell}</div>;
        }
      }
    ];
    super(
      props,
      funcApi,
      columns,
      `${slug.FACILITY_PERMIT.CREATE}?facilityId=${props.facilityId}`
    );

    this.state = {
      ...this.state,
      valueSearch: {
        facility: this.props.facilityId
      },
      dataList: this.props.dataList,
      isLoading: false
    };
  }

}

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(FacilitypermitPanel)
);
