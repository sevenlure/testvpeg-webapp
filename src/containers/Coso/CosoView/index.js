import React from 'react';
// import * as _ from 'lodash';
import slug from 'constants/slug';
import { Row, Col } from 'reactstrap';
import { getOne } from 'api/cosoApi';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { Tab, Tabs } from '@blueprintjs/core';
import CosoPanel from './CosoPanel';
import ChiTietCosoPanel from './ChiTietCosoPanel';
import FacilityeiaPanel from './FacilityeiaPanel';
import FacilitypermitPanel from './FacilitypermitPanel';
import FacilityemissionPanel from './FacilityemissionPanel';
import PageLockHOC from 'hoc/compNull';
import { connect } from 'react-redux';


const HeaderContainer = styled.div``;
@PageLockHOC(['facility.view'])
@connect(state => ({
  permissions: state.permissions
}))
export default class CosoView extends React.Component {
  static propTypes = {};

  state = {
    _idObject: '',
    isLoading: true,
    initialValue: {}
  };

  panelList = [
    {
      key: 'coso',
      title: 'Chi tiết Cơ sở',
      permissionRole: 'facility.view'
    },
    {
      key: 'chitietcoso',
      title: 'Thông tin chung',
      permissionRole: 'facility.view'
    },
    {
      key: 'facilityeia',
      title: 'Hồ sơ môi trường',
      permissionRole: 'facilityeia.view'
    },
    {
      key: 'facilitypermit',
      title: 'Giấy phép',
      permissionRole: 'facilityPermit.view'
    },
    {
      key: 'facilityEmission',
      title: 'Quản lý Ô nhiễm',
      permissionRole: 'facilityEmission.view'
    }
  ];

  async checkValidate() {
    const _id = _.result(this.props, 'match.params._id');
    if (_id) {
      const res = await getOne(_id);
      if (res.success) {
        this.setState(
          {
            _idObject: _id,
            isLoading: false,
            initialValue: res.data
          },
          () => {
            this.forceUpdate();
          }
        );
      }
    } else {
      this.props.history.push(slug.CO_SO.LIST);
    }
  }

  updateInitialValue = values => {
    this.setState({
      _idObject: values._id,
      isLoading: false,
      initialValue: values
    });
  };

  componentWillMount = async () => {
    await this.checkValidate();
  };

  getInitialValue = (keyPath, defaultValue = '') => {
    return _.result(this.state.initialValue, keyPath, defaultValue);
  };

  renderPanel = keyPanel => {
    switch (keyPanel) {
      case 'coso': {
        return <CosoPanel initialValue={this.state.initialValue} />;
      }
      case 'chitietcoso': {
        return (
          <ChiTietCosoPanel
            initialValue={this.state.initialValue}
            callback={this.updateInitialValue}
          />
        );
      }
      case 'facilityeia': {
        return (
          <FacilityeiaPanel
            facilityId={this.state._idObject}
            dataList={this.getInitialValue('facilityeias')}
            // initialValue={this.state.initialValue}
            // callback={this.updateInitialValue}
          />
        );
      }
      case 'facilitypermit': {
        return (
          <FacilitypermitPanel
            facilityId={this.state._idObject}
            dataList={this.getInitialValue('facilitypermits')}
            // initialValue={this.state.initialValue}
            // callback={this.updateInitialValue}
          />
        );
      }
      case 'facilityEmission': {
        return (
          <FacilityemissionPanel
            facilityId={this.state._idObject}
            dataList={this.getInitialValue('facilityemissions')}
            // initialValue={this.state.initialValue}
            // callback={this.updateInitialValue}
          />
        );
      }

      default: {
        return <div />;
      }
    }
  };

  render() {
    return (
      <div>
        {!this.state.isLoading && (
          <div>
            <HeaderContainer>
              <Row>
                <Col xs={1}>Tên Cơ sở</Col>
                <Col xs={2}>{this.getInitialValue('FacilityNameFull')}</Col>
                <Col xs={1}>Mã Cơ sở </Col>
                <Col xs={2}>{this.getInitialValue('FacilityCode')}</Col>
                <Col xs={1}>Quận/Huyện</Col>
                <Col xs={2}>{this.getInitialValue('district.Name')}</Col>
              </Row>
              <Clearfix height={8} />
              <Row>
                <Col xs={1}>Điện thoại</Col>
                <Col xs={2}>{this.getInitialValue('Telephone')}</Col>
                <Col xs={1}>Fax</Col>
                <Col xs={2}>{this.getInitialValue('Fax')}</Col>
                <Col xs={1}>Ngành/Nghề</Col>
                <Col xs={2}>{this.getInitialValue('sector.Name')}</Col>
              </Row>
            </HeaderContainer>
            <Clearfix height={16} />
            <Tabs renderActiveTabPanelOnly={true} style={{ color: 'red' }}>
              {this.panelList.map(item => {
                const isHavePermission = this.props.permissions.includes(item.permissionRole)
                if(isHavePermission)
                return (
                  <Tab
                    id={item.key}
                    key={item.key}
                    title={item.title}
                    panel={this.renderPanel(item.key)}
                  />
                );
                else return undefined
              })}
            </Tabs>
          </div>
        )}
      </div>
    );
  }
}
