import React from 'react';
import PropTypes from 'prop-types';
// import * as _ from 'lodash';
import { Row, Col } from 'reactstrap';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { convertDateFromSeverToString as convertDate } from 'utils/fnUtil';
import PageLockHOC from 'hoc/pageLock'

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  // justify-content: flex-end;
`;

class RowValue extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  };

  state = {};

  async componentWillMount() {}

  render() {
    return (
      <div>
        <Row>
          <Col xs={3}>{this.props.label}</Col>
          <Col xs={8}>{this.props.value}</Col>
        </Row>
        <Clearfix height={8} />
      </div>
    );
  }
}

@PageLockHOC(['facility.view'])
export default class ChiTietCosoView extends React.Component {
  static propTypes = {
    initialValue: PropTypes.object
  };
  state = {};

  async componentWillMount() {}

  getInitialValue = (keyPath, defaultValue = '') => {
    return _.result(this.props.initialValue, keyPath, defaultValue);
  };

  render() {
    return (
      <div style={{ width: '100%' }}>
        <Row>
          <Col xs={3}>
            <LabelContainer>
              <b>Giấy chứng nhận đầu tư</b>
            </LabelContainer>
          </Col>
        </Row>
        <RowValue
          label="Số giấy phép"
          value={this.getInitialValue('InvestmentLicenseNumber')}
        />
        <RowValue
          label="Ngày cấp lần đầu"
          value={convertDate(
            this.getInitialValue('InvestmentLicenseIssueDateFirst')
          )}
        />
        <RowValue
          label="Ngày cấp lần gần nhất"
          value={convertDate(
            this.getInitialValue('InvestmentLicenseIssueDateLast')
          )}
        />
        <Row>
          <Col xs={3}>
            <LabelContainer>
              <b>Giấy chứng nhận đăng ký doanh nghiệp</b>
            </LabelContainer>
          </Col>
        </Row>
        <RowValue
          label="Số giấy phép"
          value={this.getInitialValue('InvestmentLicenseNumber')}
        />
        <RowValue
          label="Ngày cấp lần đầu"
          value={convertDate(
            this.getInitialValue('InvestmentLicenseIssueDateFirst')
          )}
        />
        <RowValue
          label="Ngày cấp lần gần nhất"
          value={convertDate(
            this.getInitialValue('InvestmentLicenseIssueDateLast')
          )}
        />
        <Row>
          <Col xs={3}>
            <LabelContainer>
              <b>Thông tin khác</b>
            </LabelContainer>
          </Col>
        </Row>
        <RowValue
          label="Hình thức sở hữu"
          value={this.getInitialValue('ownership.Name')}
        />
        <RowValue
          label="Năm bắt đầu hoạt động"
          value={this.getInitialValue('YearsOfOperation')}
        />
        <RowValue
          label="Số lượng công nhân viên"
          value={this.getInitialValue('Employees')}
        />
        <RowValue label="Công suất" value={this.getInitialValue('Capacity')} />
        <RowValue
          label="Quy trình sản xuất"
          value={this.getInitialValue('ProductDesc')}
        />
        <RowValue label="Nhiên liệu" value={this.getInitialValue('Fuel')} />
        {/* <RowValue
          label="Vật liệu chính"
          value={this.getInitialValue('MainMaterials')}
        />
          <RowValue
          label="Nguyên liệu tiêu thụ"
          value={this.getInitialValue('MaterialConsumption')}
        /> */}
        <RowValue
          label="Số ngày làm việc trong tháng"
          value={this.getInitialValue('WorkingDaysMonth')}
        />
        <RowValue
          label="Số ngày làm việc/năm"
          value={this.getInitialValue('WorkingDaysYear')}
        />
        <RowValue
          label="Lượng nước sử dụng (m3/ngày)"
          value={this.getInitialValue('WaterUseDay')}
        />
        {/* <RowValue
          label="Lượng nước sử dụng (m3/tháng)"
          value={this.getInitialValue('WaterUseMonth')}
        /> */}
        <RowValue
          label="Nguồn nước"
          value={this.getInitialValue('waterSource.Name')}
        />
      </div>
    );
  }
}
