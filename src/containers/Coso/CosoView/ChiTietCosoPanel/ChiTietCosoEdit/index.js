import React from 'react';
import PropTypes from 'prop-types';
// import * as _ from 'lodash';
import { Row, Col } from 'reactstrap';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { TextArea } from '@blueprintjs/core';
import { createForm } from 'rc-form';
import NumberInput from 'rc-input-number';
import { DateInput } from '@blueprintjs/datetime';
import { DATE_FORMAT } from 'config';
import moment from 'moment';
import NguonNuocSelect from 'components/elements/select-nguon-nuoc';
import HinhThucSelect from 'components/elements/select-hinh-thuc-so-huu';
import { SELECT_ITEM_DEFAULT } from 'config';
import { convertDateFromSever as convertDate } from 'utils/fnUtil';
import PageLockHOC from 'hoc/pageLock'



const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

@PageLockHOC(['facility.write'])
class ChiTietCosoEdit extends React.Component {
  static propTypes = {
    initialValue: PropTypes.object
  };
  state = {};

  async componentWillMount() {
    console.log(this.props.initialValue);
  }

  setInitialValue(keyPath, defaultValue) {
    let value = _.result(this.props.initialValue, keyPath, defaultValue);
    if (!value) value = defaultValue;
    return value;
  }

  setSelInitialValue(keyPath, defaultValue, nameLabel) {
    let selItem = _.result(this.props.initialValue, keyPath);
    if (!selItem) return defaultValue;
    return {
      value: selItem._id,
      label: nameLabel ? selItem[nameLabel] : selItem.Name
    };
  }

  render() {
    const { getFieldProps } = this.props.form;

    return (
      <div style={{ width: '100%' }}>
        <Row>
          <Col xs={3}>
            <LabelContainer>
              <b>Giấy chứng nhận đầu tư</b>
            </LabelContainer>
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <LabelContainer>Số giấy phép</LabelContainer>
          </Col>
          <Col xs={5}>
            <NumberInput
              placeholder="Số giấy phép"
              {...getFieldProps('InvestmentLicenseNumber', {
                initialValue: this.setInitialValue(
                  'InvestmentLicenseNumber',
                  ''
                )
              })}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <LabelContainer>Ngày cấp lần đầu</LabelContainer>
          </Col>
          <Col xs={5}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder="Ngày cấp lần đầu"
              {...getFieldProps('InvestmentLicenseIssueDateFirst', {
                initialValue: convertDate(
                  this.setInitialValue('InvestmentLicenseIssueDateFirst', null)
                )
              })}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <LabelContainer>Ngày cấp lần gần nhất</LabelContainer>
          </Col>
          <Col xs={5}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder="Ngày cấp lần gần nhất"
              {...getFieldProps('InvestmentLicenseIssueDateLast', {
                initialValue: convertDate(
                  this.setInitialValue('InvestmentLicenseIssueDateLast', null)
                )
              })}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <LabelContainer>
              <b>Giấy chứng nhận đăng ký doanh nghiệp</b>
            </LabelContainer>
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <LabelContainer>Số giấy phép</LabelContainer>
          </Col>
          <Col xs={5}>
            <NumberInput
              placeholder="Số giấy phép"
              {...getFieldProps('BusinessLicenseNumber', {
                initialValue: this.setInitialValue(
                  'BusinessLicenseNumber',
                  ''
                )
              })}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <LabelContainer>Ngày cấp lần đầu</LabelContainer>
          </Col>
          <Col xs={5}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder="Ngày cấp lần đầu"
              {...getFieldProps('BusinessLicenseIssueDateFirst', {
                initialValue: convertDate(
                  this.setInitialValue('BusinessLicenseIssueDateFirst', null)
                )
              })}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <LabelContainer>Ngày cấp lần gần nhất</LabelContainer>
          </Col>
          <Col xs={5}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder="Ngày cấp lần gần nhất"
              {...getFieldProps('BusinessLicenseIssueDateLast', {
                initialValue: convertDate(
                  this.setInitialValue('BusinessLicenseIssueDateLast', null)
                )
              })}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <LabelContainer>
              <b>Thông tin khác</b>
            </LabelContainer>
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <LabelContainer>Hình thức sở hữu</LabelContainer>
          </Col>
          <Col xs={5}>
            <HinhThucSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'ownership',
                option: {
                  initialValue: this.setSelInitialValue(
                    'ownership',
                    SELECT_ITEM_DEFAULT
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={3}>
            <LabelContainer>Năm bắt đầu hoạt động</LabelContainer>
          </Col>
          <Col xs={5}>
            <NumberInput
              placeholder="Năm bắt đầu hoạt động"
              {...getFieldProps('YearsOfOperation', {
                initialValue: this.setInitialValue('YearsOfOperation', '')
              })}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <LabelContainer>Số lượng công nhân viên</LabelContainer>
          </Col>
          <Col xs={5}>
            <NumberInput
              placeholder="Số lượng công nhân viên"
              {...getFieldProps('Employees', {
                initialValue: this.setInitialValue('Employees', '')
              })}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <LabelContainer>Công suất</LabelContainer>
          </Col>
          <Col xs={5}>
            <TextArea
              fill
              placeholder="Công suất"
              {...getFieldProps('Capacity', {
                initialValue: this.setInitialValue('Capacity', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={3}>
            <LabelContainer>Quy trình sản xuất</LabelContainer>
          </Col>
          <Col xs={5}>
            <TextArea
              fill
              placeholder="Quy trình sản xuất"
              {...getFieldProps('ProductDesc', {
                initialValue: this.setInitialValue('ProductDesc', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={3}>
            <LabelContainer>Nhiên liệu</LabelContainer>
          </Col>
          <Col xs={5}>
            <TextArea
              fill
              placeholder="Nhiên liệu"
              {...getFieldProps('Fuel', {
                initialValue: this.setInitialValue('Fuel', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        {/* <Row>
          <Col xs={3}>
            <LabelContainer>Vật liệu chính</LabelContainer>
          </Col>
          <Col xs={5}>
            <TextArea
              fill
              placeholder="Vật liệu chính"
              {...getFieldProps('MainMaterials', {
                initialValue: this.setInitialValue('MainMaterials', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={3}>
            <LabelContainer>Nguyên liệu tiêu thụ</LabelContainer>
          </Col>
          <Col xs={5}>
            <TextArea
              fill
              placeholder="Nguyên liệu tiêu thụ"
              {...getFieldProps('MaterialConsumption', {
                initialValue: this.setInitialValue('MaterialConsumption', '')
              })}
            />
          </Col>
        </Row> */}
        <Clearfix height={8} />
        <Row>
          <Col xs={3}>
            <LabelContainer>Số ngày làm việc trong tháng</LabelContainer>
          </Col>
          <Col xs={5}>
            <NumberInput
              placeholder="Số ngày làm việc trong tháng"
              {...getFieldProps('WorkingDaysMonth', {
                initialValue: this.setInitialValue('WorkingDaysMonth', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={3}>
            <LabelContainer>Số ngày làm việc/năm</LabelContainer>
          </Col>
          <Col xs={5}>
            <NumberInput
              placeholder="Số ngày làm việc/năm"
              {...getFieldProps('WorkingDaysYear', {
                initialValue: this.setInitialValue('WorkingDaysYear', '')
              })}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <LabelContainer>Lượng nước sử dụng (m3/ngày)</LabelContainer>
          </Col>
          <Col xs={5}>
            <NumberInput
              placeholder="Lượng nước sử dụng (m3/ngày)"
              {...getFieldProps('WaterUseDay', {
                initialValue: this.setInitialValue('WaterUseDay', '')
              })}
            />
          </Col>
        </Row>
        {/* <Row>
          <Col xs={3}>
            <LabelContainer>Lượng nước sử dụng (m3/tháng)</LabelContainer>
          </Col>
          <Col xs={5}>
            <NumberInput
              placeholder="Lượng nước sử dụng (m3/tháng)"
              {...getFieldProps('WaterUseMonth', {
                initialValue: this.setInitialValue('WaterUseMonth', '')
              })}
            />
          </Col>
        </Row> */}
        <Row>
          <Col xs={3}>
            <LabelContainer>Nguồn nước</LabelContainer>
          </Col>
          <Col xs={5}>
            <NguonNuocSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'waterSource',
                option: {
                  initialValue: this.setSelInitialValue(
                    'waterSource',
                    SELECT_ITEM_DEFAULT
                  )
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
      </div>
    );
  }
}

export default createForm()(ChiTietCosoEdit);
