import React from 'react';
import PropTypes from 'prop-types';
// import * as _ from 'lodash';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { Icon, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import ChiTietCosoView from './ChiTietCosoView';
import ChiTietCosoEdit from './ChiTietCosoEdit';
import { updateOne } from 'api/cosoApi';
import Message from 'rc-message';
import AttachmentContainer from 'containers/AttachmentContainer';
import {
  getList as getFacAttachList,
  deleteOne as deleteAttach,
  create as createFacAttach
} from 'api/facilityattachmentApi';
import { getLookupTypeFacilityAttachment } from 'api/lookuplistitemApi';
import RuleDiv from 'components/ruleDiv';

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export default class ChiTietCosoPanel extends React.Component {
  static propTypes = {
    initialValue: PropTypes.object,
    callback: PropTypes.func
  };
  state = {
    isEdit: false
  };

  async componentWillMount() {}

  getInitialValue = (keyPath, defaultValue = '') => {
    return _.result(this.props.initialValue, keyPath, defaultValue);
  };

  showEditForm = () => {
    this.setState({
      isEdit: true
    });
  };

  hideEditForm = () => {
    this.setState({
      isEdit: false
    });
  };

  getFormValue = () => {
    let formValues;
    if (this.CosoEditComp && this.CosoEditComp.getForm()) {
      const form = this.CosoEditComp.getForm();
      form.validateFields((error, value) => {
        if (error) return;
        value.ownership = _.get(value.ownership, 'value');
        value.waterSource = _.get(value.waterSource, 'value');
        formValues = value;
      });
    }
    return formValues;
  };

  handelSubmit = async () => {
    const values = await this.getFormValue();
    if (!values) return;

    const res = await updateOne(
      this.props.initialValue._id,
      _.pickBy(values, _.identity)
    );
    if (res.success) {
      Message.success({ content: 'Update Success!!!' });
      this.hideEditForm();
      if (this.props.callback) this.props.callback(res.data);
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div style={{ width: '100%' }}>
        <Clearfix height={8} />
        <HeaderContainer>
          <div style={{ display: 'inline-flex' }}>
            {!this.state.isEdit && (
              <div>
                 <RuleDiv initialValue={this.props.initialValue}>
                 <Icon
                  style={{ cursor: 'pointer' }}
                  icon={IconNames.MANUALLY_ENTERED_DATA}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.PRIMARY}
                  onClick={this.showEditForm}
                />
                 </RuleDiv>
              
              </div>
            )}

            {this.state.isEdit && (
              <div style={{ display: 'inline-flex' }}>
                <Icon
                  style={{ cursor: 'pointer' }}
                  icon={IconNames.SAVED}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.PRIMARY}
                  onClick={this.handelSubmit}
                />
                <Clearfix width={8} />
                <Icon
                  style={{ cursor: 'pointer' }}
                  icon={IconNames.DISABLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                  onClick={this.hideEditForm}
                />
              </div>
            )}
          </div>
        </HeaderContainer>
        <Clearfix height={8} />
        {!this.state.isEdit && (
          <ChiTietCosoView initialValue={this.props.initialValue} />
        )}
        {this.state.isEdit && (
          <ChiTietCosoEdit
            initialValue={this.props.initialValue}
            ref={CosoEditComp => {
              this.CosoEditComp = CosoEditComp;
            }}
          />
        )}
        <AttachmentContainer
           facility={this.props.initialValue}
          getListFunc={query => {
            return getFacAttachList({
              ...query,
              facility: this.props.initialValue._id
            });
          }}
          createOne={values => {
            values.facility = this.props.initialValue._id;
            return createFacAttach(_.pickBy(values, _.identity));
          }}
          deleteOne={deleteAttach}
          getAttachmentTypeList={getLookupTypeFacilityAttachment}
          moduleName="facility"
        />
      </div>
    );
  }
}
