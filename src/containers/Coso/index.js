import React from 'react';
import { Route } from 'react-router-dom';
import slug from 'constants/slug';
import CosoList from './CosoList';
import CosoCreate from './CosoCreate';
import CosoEdit from './CosoEdit';
import CosoView from './CosoView';

export default props => [
  <Route
    exact
    key={slug.CO_SO.LIST}
    path={slug.CO_SO.LIST}
    render={matchProps => <CosoList {...matchProps} {...props} />}
  />,
  <Route
    key={slug.CO_SO.CREATE}
    path={slug.CO_SO.CREATE}
    render={matchProps => <CosoCreate {...matchProps} {...props} />}
  />,
  <Route
    key={slug.CO_SO.VIEW}
    path={slug.CO_SO.VIEW}
    render={matchProps => <CosoView {...matchProps} {...props} />}
  />,
  <Route
    key={slug.CO_SO.EDIT}
    path={slug.CO_SO.EDIT}
    render={matchProps => <CosoEdit {...matchProps} {...props} />}
  />
];
