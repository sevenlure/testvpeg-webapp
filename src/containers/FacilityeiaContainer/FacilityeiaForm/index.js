import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { Icon, Intent, InputGroup, Radio, RadioGroup } from '@blueprintjs/core';
import Clearfix from 'components/elements/clearfix';
import { IconNames } from '@blueprintjs/icons';
import { createForm } from 'rc-form';
// import * as _ from 'lodash';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import IconRequired from 'components/elements/required-icon';
import FacilitySelect from 'components/elements/select-facility';
import LoaiHoSoSelect from 'components/elements/select-loai-ho-so';
import { DateInput } from '@blueprintjs/datetime';
import { DATE_FORMAT } from 'config';
import moment from 'moment';
import { convertDateFromSever as convertDate } from 'utils/fnUtil';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, ContentState } from 'draft-js';
import htmlToDraft from 'html-to-draftjs';
import { convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import {getDataForCustomSelect} from 'constants/custom-data'
import dynamicRoleHOC from 'hoc/dynamicRole'

const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

const RadioContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
`;

const HeaderContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-start;
  cursor: pointer;
`;
@dynamicRoleHOC()
class FacilityeiaForm extends React.Component {
  static propTypes = {
    isEdit: PropTypes.bool,
    initialValue: PropTypes.object,
    slugBack: PropTypes.string,
    handelSubmit: PropTypes.func,
    facility: PropTypes.shape({
      _id: PropTypes.string,
      FacilityNameFull: PropTypes.string
    })
  };

  constructor(props) {
    super(props);

    this.state = {
      editorState:
        this.props.isEdit && this.props.initialValue.Remarks
          ? this.convertFromHTML2EditorState(this.props.initialValue.Remarks)
          : EditorState.createEmpty()
    };
  }

  convertFromHTML2EditorState = rawHtml => {
    const contentBlock = htmlToDraft(rawHtml);
    const contentState = ContentState.createFromBlockArray(
      contentBlock.contentBlocks
    );
    return EditorState.createWithContent(contentState);
  };

  onEditorStateChange = editorState => {
    this.setState({
      editorState
    });
  };

  setInitialValue(keyPath, defaultValue) {
    if (this.props.isEdit) {
      let value = _.result(this.props.initialValue, keyPath, defaultValue);
      if (!value) value = defaultValue;
      return value;
    }
    return defaultValue;
  }

  setSelInitialValue(keyPath, defaultValue, nameLabel) {
    if (this.props.isEdit) {
      let selItem = _.result(this.props.initialValue, keyPath);
      if (!selItem) return defaultValue;
      return {
        value: selItem._id,
        label: nameLabel ? selItem[nameLabel] : selItem.Name
      };
    }
    return defaultValue;
  }

  setSelInitialBoolValue(keyPath, defaultValue) {
    if (this.props.isEdit) {
      let value = _.result(this.props.initialValue, keyPath, defaultValue);
      return value + '';
    }
    return defaultValue;
  }

  submit = () => {
    this.props.form.validateFields((error, value) => {
      value.IsCompleteDTM = value.IsCompleteDTM === 'true' ? true : false;
      value.IsEnvironmentalIncident =
        value.IsEnvironmentalIncident === 'true' ? true : false;
      value.facility = _.get(value.facility, 'value');
      value.Title = _.get(value.Title, 'label');
      console.log(value);
      if (error) return;
      if (this.props.handelSubmit) this.props.handelSubmit(value);
    });
  };

  render() {
    const { getFieldProps, getFieldError, getFieldValue } = this.props.form;
    return (
      <div>
        <Row>
          <Col xs={4}>
            <HeaderContainer>
              <Icon
                icon={IconNames.SAVED}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.PRIMARY}
                onClick={this.submit}
              />
              <Clearfix width={8} />
              <Link to={this.props.slugBack ? this.props.slugBack : slug.BASE}>
                <Icon
                  icon={IconNames.DISABLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                />
              </Link>
            </HeaderContainer>
          </Col>
        </Row>
        <Row>
          <Col xs={4}>
            <LabelContainer>
              Tên Cơ sở
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <FacilitySelect
              isDisabled={this.props.facility ? true : false}
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'facility',
                option: {
                  initialValue: this.props.facility
                    ? {
                        value: this.props.facility._id,
                        label: this.props.facility.FacilityNameFull
                      }
                    : null,
                  rules: [
                    {
                      required: true,
                      message: 'Tên Cơ sở is required'
                    }
                  ]
                }
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>
              Loại hồ sơ
              <IconRequired />
            </LabelContainer>
          </Col>
          <Col xs={4}>
            {/* <InputGroup
              placeholder="Loại hồ sơ"
              {...getFieldProps('Title', {
                initialValue: this.setInitialValue('Title', ''),
                rules: [
                  {
                    required: true,
                    message: 'Loại hồ sơ is required'
                  }
                ]
              })}
            /> */}
            <LoaiHoSoSelect
              hideLabel
              fieldForm={{
                form: this.props.form,
                name: 'Title',
                option: {
                  initialValue: getDataForCustomSelect(this.setInitialValue('Title')),
                  rules: [
                    {
                      required: true,
                      message: 'Loại hồ sơ'
                    }
                  ]
                }
              }}
            />
            <ErrorContainer>
              {(getFieldError('Title') || []).join(', ')}
            </ErrorContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Số quyết định</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Số quyết định'
              {...getFieldProps('DecisionNumber', {
                initialValue: this.setInitialValue('DecisionNumber', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Ngày phát hành</LabelContainer>
          </Col>
          <Col xs={4}>
            <DateInput
              formatDate={date => moment(date).format(DATE_FORMAT)}
              parseDate={str => moment(str).toDate()}
              placeholder='Ngày phát hành'
              {...getFieldProps('DateIssued', {
                initialValue: convertDate(
                  this.setInitialValue('DateIssued', null)
                )
              })}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={4}>
            <LabelContainer>Cơ quan cấp</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Cơ quan cấp'
              {...getFieldProps('IssuingAgency', {
                initialValue: this.setInitialValue('IssuingAgency', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>
              Thuộc đối tượng lập hồ sơ xác nhận hoàn thành ĐTM
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <RadioContainer>
              <RadioGroup
                inline
                name='IsCompleteDTM'
                {...getFieldProps('IsCompleteDTM', {
                  initialValue: this.setSelInitialBoolValue(
                    'IsCompleteDTM',
                    'true'
                  )
                })}
                selectedValue={
                  getFieldValue('IsCompleteDTM')
                    ? getFieldValue('IsCompleteDTM')
                    : 'true'
                }
              >
                <Radio label='Có' value='true' />
                <Radio label='Không' value='false' />
              </RadioGroup>
            </RadioContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>
              Thuộc đối tượng lập phương án ứng phó sự cố môi trường
            </LabelContainer>
          </Col>
          <Col xs={4}>
            <RadioContainer>
              <RadioGroup
                inline
                name='IsEnvironmentalIncident'
                {...getFieldProps('IsEnvironmentalIncident', {
                  initialValue: this.setSelInitialBoolValue(
                    'IsEnvironmentalIncident',
                    'true'
                  )
                })}
                selectedValue={
                  getFieldValue('IsEnvironmentalIncident')
                    ? getFieldValue('IsEnvironmentalIncident')
                    : 'true'
                }
              >
                <Radio label='Có' value='true' />
                <Radio label='Không' value='false' />
              </RadioGroup>
            </RadioContainer>
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Đường dẫn (URL)</LabelContainer>
          </Col>
          <Col xs={4}>
            <InputGroup
              placeholder='Đường dẫn (URL)'
              {...getFieldProps('EIAURL', {
                initialValue: this.setInitialValue('EIAURL', '')
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        <Row>
          <Col xs={4}>
            <LabelContainer>Ghi chú</LabelContainer>
          </Col>
          <Col xs={6}>
            <Editor
              editorState={this.state.editorState}
              wrapperClassName='demo-wrapper'
              editorClassName='demo-editor'
              onEditorStateChange={this.onEditorStateChange}
              {...getFieldProps('Remarks', {
                initialValue: this.props.isEdit
                  ? draftToHtml(
                      convertToRaw(this.state.editorState.getCurrentContent())
                    )
                  : '',
                getValueFromEvent: value => {
                  return draftToHtml(
                    convertToRaw(this.state.editorState.getCurrentContent())
                  );
                }
              })}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
      </div>
    );
  }
}

export default createForm()(FacilityeiaForm);
