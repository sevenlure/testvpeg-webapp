import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BaseListContainer } from 'containers/BaseContainer/BaseListContainer';
import { setMenuSelected } from '@redux/actions/menuAction';
import moment from 'moment';
import { DATE_FORMAT } from 'config';
import {  deleteOne } from 'api/facilityeiaApi';
import slug from 'constants/slug';
import styled from 'styled-components';
import {  Colors } from '@blueprintjs/core';
import Confirm from 'components/elements/confirm-element';
import Message from 'rc-message';
import { createForm } from 'rc-form';
import PageLockHOC from 'hoc/pageLock'

const NameContainer = styled.div`
  color: ${Colors.GREEN2};
  text-decoration: underline;
`;

@PageLockHOC(['facilityeia.view'])
class FacilityeiaList extends BaseListContainer {
  static propTypes = {
    facilityId: PropTypes.string.isRequired,
    hideRightHeaderContent: PropTypes.bool
  };

  constructor(props) {
    const funcApi = {
      //getList
    };
    const columns = [
      // {
      //   dataField: '_id',
      //   text: '',
      //   formatter: (cell, row) => {
      //     return (
      //       <Icon
      //         style={{ cursor: 'pointer' }}
      //         icon={IconNames.DISABLE}
      //         iconSize={Icon.SIZE_LARGE}
      //         intent={Intent.DANGER}
      //         onClick={() => {
      //           this.handelDelete(cell);
      //         }}
      //       />
      //     );
      //   }
      // },
      {
        dataField: 'Title',
        text: 'Loại hồ sơ',
        formatter: (cell, row) => {
          console.log(this.state)
          return (
            <Link to={slug.FACILITY_EIA.VIEW_WITH_ID + row._id}>
              <NameContainer>{cell}</NameContainer>
            </Link>
          );
        }
      },
      {
        dataField: 'DateIssued',
        text: 'Ngày phát hành',
        formatter: (cell, row) => {
          return moment(cell).format(DATE_FORMAT);
        }
      },
      {
        dataField: 'EIAURL',
        text: 'Đường dẫn (URL)'
      },
      {
        dataField: 'DecisionNumber',
        text: 'Số quyết định'
      },
      {
        dataField: 'IssuingAgency',
        text: 'Cơ quan cấp'
      },
      {
        dataField: 'AddedOn',
        text: 'Thêm vào',
        formatter: (cell, row) => {
          return moment(cell).format(DATE_FORMAT);
        }
      },
      {
        dataField: 'AddedBy',
        text: 'Thêm bởi'
      },
      {
        dataField: 'UpdatedOn',
        text: 'Cập nhật ngày',
        formatter: (cell, row) => {
          return moment(cell).format(DATE_FORMAT);
        }
      },
      {
        dataField: 'UpdatedBy',
        text: 'Cập nhật bởi'
      }
    ];
    super(
      props,
      funcApi,
      columns,
      `${slug.FACILITY_EIA.CREATE}?facilityId=${props.facilityId}`,
      'facilityeia'
    );

    this.state = {
      ...this.state,
      valueSearch: {
        facility: this.props.facilityId
      },
      dataList: this.props.dataList,
      isLoading: false
    };
  }

  renderSearchContainer() {
    return <Confirm ref={ConfirmElm => (this.ConfirmElm = ConfirmElm)} />;
  }

  handelDelete = _id => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      this.ConfirmElm.showConfirm({
        content: 'Bạn có muốn xoá ',
        cb: async value => {
          if (value) {
            const res = await deleteOne(_id);
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.fetData();
            }
            if (res.error) Message.error({ content: res.message });
          }
        }
      });
    }
  };
}

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(createForm()(FacilityeiaList))
);
