import React from 'react';
import { Route } from 'react-router-dom';
import slug from 'constants/slug';
import FacilityeiaCreate from './FacilityeiaCreate';
import FacilityeiaEdit from './FacilityeiaEdit';
import FacilityeiaView from './FacilityeiaView';

export default props => [
  <Route
    key={slug.FACILITY_EIA.CREATE}
    path={slug.FACILITY_EIA.CREATE}
    render={matchProps => <FacilityeiaCreate {...matchProps} {...props} />}
  />,
  <Route
    key={slug.FACILITY_EIA.VIEW}
    path={slug.FACILITY_EIA.VIEW}
    render={matchProps => <FacilityeiaView {...matchProps} {...props} />}
  />,
  <Route
    key={slug.FACILITY_EIA.EDIT}
    path={slug.FACILITY_EIA.EDIT}
    render={matchProps => <FacilityeiaEdit {...matchProps} {...props} />}
  />
];
