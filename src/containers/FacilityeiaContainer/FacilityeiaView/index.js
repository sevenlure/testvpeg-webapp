import React from 'react';
import PropTypes from 'prop-types';
import { getOne, deleteOne } from 'api/facilityeiaApi';
import Message from 'rc-message';
// import * as _ from 'lodash';
import { Icon, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { Row, Col } from 'reactstrap';
import Confirm from 'components/elements/confirm-element';
import { Link } from 'react-router-dom';
import slug from 'constants/slug';
import { withRouter } from 'react-router-dom';
import { COLOR, STYLE, DATETIME_FORMAT, DATE_FORMAT } from 'config';
import moment from 'moment';
import AttachmentContainer from 'containers/AttachmentContainer';
import {
  getList as getFacAttachList,
  deleteOne as deleteAttach,
  create as createFacAttach
} from 'api/eiaatachmentApi';
import { getLookupTypeEiaAttachment } from 'api/lookuplistitemApi';
import PageLockHOC from 'hoc/pageLock';
import Permissions from 'react-redux-permissions';
import RuleDiv from 'components/ruleDiv';

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

class RowValue extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    isRequiredField: PropTypes.bool
  };

  render() {
    return (
      <div>
        <Row>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              width: '100%'
            }}
          >
            <Col
              style={this.props.isRequiredField ? STYLE.REQUIREDFIELD : {}}
              xs={2}
            >
              {this.props.label}
            </Col>
            <Col xs={7}>{this.props.value}</Col>
          </div>
        </Row>
        <Clearfix height={8} />
      </div>
    );
  }
}

@PageLockHOC(['facilityeia.view'])
class FacilityeiaView extends React.Component {
  static propTypes = {};

  state = {
    _idObject: '',
    isLoaded: false,
    isLoading: false,
    facility: {},
    facilityId: '',
    initialValue: null
  };

  async checkValidate() {
    const _id = _.result(this.props, 'match.params._id');
    if (_id) {
      const res = await getOne(_id);
      console.log(res);
      if (res.success) {
        this.setState({
          _idObject: res.data._id,
          facility: res.data.facility,
          facilityId: res.data.facility._id,
          isLoading: false,
          isLoaded: true,
          initialValue: res.data
        });
      }
    } else {
      //this.props.history.push(slug.CO_SO.LIST);
    }
  }

  getInitialValue = (keyPath, defaultValue = '') => {
    return _.result(this.state.initialValue, keyPath, defaultValue);
  };

  getInitialDateValue = (keyPath, isDate, defaultValue = '') => {
    let value = _.result(this.state.initialValue, keyPath, defaultValue);
    if (!value) return '';
    if (isDate) return moment(value).format(DATE_FORMAT);
    return moment(value).format(DATETIME_FORMAT);
  };

  handelDelete = () => {
    if (this.ConfirmElm && this.ConfirmElm.showConfirm) {
      this.ConfirmElm.showConfirm({
        content: 'Bạn có muốn xoá ' + this.getInitialValue('Title'),
        cb: async value => {
          if (value) {
            const res = await deleteOne(this.getInitialValue('_id'));
            if (res.success) {
              Message.success({ content: 'Success!!!' });
              this.props.history.push(
                slug.CO_SO.VIEW_WITH_ID + this.state.facilityId
              );
            }
            if (res.error) Message.error({ content: res.message });
          }
        }
      });
    }
  };

  async componentWillMount() {
    await this.checkValidate();
  }

  render() {
    return (
      <div style={{ width: '100%' }}>
        <Confirm ref={ConfirmElm => (this.ConfirmElm = ConfirmElm)} />
        <h4
          className='bp3-heading'
          style={{
            color: COLOR.SUCCESS
          }}
        >
          Đánh giá tác động môi trường
        </h4>
        <HeaderContainer>
          <div style={{ display: 'inline-flex' }}>
            <Link to={slug.CO_SO.VIEW_WITH_ID + this.state.facilityId}>
              <Icon
                style={{ cursor: 'pointer' }}
                icon={IconNames.CIRCLE_ARROW_LEFT}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.SUCCESS}
              />
            </Link>

            <Permissions allowed={[`facilityeia.write`]}>
            <RuleDiv initialValue={this.state.initialValue}>
            <React.Fragment>
                <Clearfix width={8} />
                <Link
                  to={slug.FACILITY_EIA.EDIT_WITH_ID + this.state._idObject}
                >
                  <Icon
                    style={{ cursor: 'pointer' }}
                    icon={IconNames.MANUALLY_ENTERED_DATA}
                    iconSize={Icon.SIZE_LARGE}
                    intent={Intent.PRIMARY}
                  />
                </Link>
              </React.Fragment>
            </RuleDiv>
             
            </Permissions>

            <Permissions allowed={[`facilityeia.delete`]}>
            <RuleDiv initialValue={this.state.initialValue}>
            <React.Fragment>
                <Clearfix width={8} />
                <Icon
                  style={{ cursor: 'pointer' }}
                  icon={IconNames.BAN_CIRCLE}
                  iconSize={Icon.SIZE_LARGE}
                  intent={Intent.DANGER}
                  onClick={this.handelDelete}
                />
              </React.Fragment>
            </RuleDiv>
            
            </Permissions>

            <Clearfix width={8} />
            <Icon
              style={{ cursor: 'pointer' }}
              icon={IconNames.PRINT}
              iconSize={Icon.SIZE_LARGE}
              intent={Intent.SUCCESS}
            />
          </div>
        </HeaderContainer>
        <Clearfix height={16} />
        <RowValue
          isRequiredField
          label='Tên Cơ sở'
          value={this.getInitialValue('facility.FacilityNameFull')}
        />
        <RowValue
          isRequiredField
          label='Loại hồ sơ'
          value={this.getInitialValue('Title')}
        />
        <RowValue
          label='Số quyết định'
          value={this.getInitialValue('DecisionNumber')}
        />
        <RowValue
          label='Ngày phát hành'
          value={this.getInitialDateValue('DateIssued', true)}
        />
        <RowValue
          label='Cơ quan cấp'
          value={this.getInitialValue('IssuingAgency')}
        />
        <RowValue
          label='Thuộc đối tượng lập hồ sơ xác nhận hoàn thành ĐTM'
          value={this.getInitialValue('IsCompleteDTM') ? 'Có' : 'Không'}
        />
        <RowValue
          label='Thuộc đối tượng lập phương án ứng phó sự cố môi trường'
          value={
            this.getInitialValue('IsEnvironmentalIncident') ? 'Có' : 'Không'
          }
        />
        <RowValue
          label='Đường dẫn (URL)'
          value={this.getInitialValue('EIAURL')}
        />
        <RowValue
          label='Thêm vào'
          value={
            this.getInitialDateValue('AddedOn') +
            ' bởi ' +
            this.getInitialValue('AddedBy')
          }
        />
        <RowValue
          label='Cập nhật lần cuối'
          value={
            this.getInitialDateValue('UpdatedOn') +
            ' bởi ' +
            this.getInitialValue('UpdatedBy')
          }
        />
        <Row>
          <Col xs={2}>Ghi chú</Col>
          <Col xs={7}>
            <div
              style={{
                padding: 5
              }}
              dangerouslySetInnerHTML={{
                __html: this.getInitialValue('Remarks')
              }}
            />
          </Col>
        </Row>
        <Clearfix height={8} />
        {this.state.isLoaded && (
          <AttachmentContainer
          facility={this.state.initialValue}
            getListFunc={query => {
              return getFacAttachList({
                ...query,
                facilityeia: this.state._idObject
              });
            }}
            createOne={values => {
              values.facilityeia = this.state._idObject;
              return createFacAttach(_.pickBy(values, _.identity));
            }}
            deleteOne={deleteAttach}
            getAttachmentTypeList={getLookupTypeEiaAttachment}
            moduleName="facilityeia"
          />
        )}
      </div>
    );
  }
}

export default withRouter(FacilityeiaView);
