import React from 'react';
import slug from 'constants/slug';
import { getOne as getFacility } from 'api/cosoApi';
import { create } from 'api/facilityeiaApi';
import Message from 'rc-message';
// import * as _ from 'lodash';
import FacilityeiaForm from '../FacilityeiaForm';
import { getQueryString } from 'utils/fnUtil';
import PageLockHOC from 'hoc/pageLock'
@PageLockHOC(['facilityeia.write'])
export default class FacilityeiaCreate extends React.Component {
  static propTypes = {};

  state = {
    isLoaded: false,
    isLoading: false,
    facility: {},
    facilityId: ''
  };

  async checkValidate() {
    const queryString = getQueryString(this.props.location.search);
    const _id = queryString.facilityId;
    if (_id) {
      const res = await getFacility(_id);
      if (res.success) {
        this.setState({
          facility: res.data,
          facilityId: res.data._id,
          isLoading: false,
          isLoaded: true
        });
      }
    } else {
      //this.props.history.push(slug.CO_SO.LIST);
    }
  }

  async componentWillMount() {
    await this.checkValidate();
  }

  handelSubmit = async values => {
    this.setState({
      isLoading: true
    });

    const res = await create(_.pickBy(values, _.identity));
    if (res.success) {
      Message.success({ content: 'Success!!!' });
      this.props.history.push(slug.CO_SO.VIEW_WITH_ID + this.state.facilityId);
    }
    if (res.error) Message.error({ content: res.message });
  };

  render() {
    return (
      <div>
        {this.state.isLoaded && (
          <FacilityeiaForm
            slugBack={slug.CO_SO.VIEW_WITH_ID + this.state.facilityId}
            facility={this.state.facility}
            handelSubmit={this.handelSubmit}
          />
        )}
      </div>
    );
  }
}
