import React from 'react';
import { Tab, Tabs } from '@blueprintjs/core';
import PageBuilding from 'components/pageBuilding';
// import MapPanel from './MapPanel';
import CosoPanel from './CosoPanel';
import FacilityInspectionPanel from './FacilityInspectionPanel';
import FacilityeiaPanel from './FacilityeiaPanel';
import FacilitypermitPanel from './FacilitypermitPanel';
import FacilityemissionPanel from './FacilityemissionPanel';
import FacilityEnviMonitoringReportPanel from './FacilityEnviMonitoringReportPanel';
import FacilityEnviProtectFeePanel from './FacilityEnviProtectFeePanel';
import FacilityHazarWasteReportPanel from './FacilityHazarWasteReportPanel';
import PageLockHOC from 'hoc/pageLock';
import { connect } from 'react-redux';

@PageLockHOC(['report.view'])
@connect(state => ({
  permissions: state.permissions
}))
export default class ReportContainer extends React.Component {
  panelList = [
    {
      key: 'coso',
      title: 'Cơ sở',
      permissionRole: 'facility.view'
    },
    {
      key: 'eia',
      title: 'Hồ sơ môi truờng',
      permissionRole: 'facilityeia.view'
    },
    {
      key: 'thanhtra',
      title: 'Thanh tra',
      permissionRole: 'facilityInspection.view'
    },
    {
      key: 'permit',
      title: 'Giấy phép',
      permissionRole: 'facilityPermit.view'
    },
    {
      key: 'emission',
      title: 'Quản lý Ô nhiễm',
      permissionRole: 'facilityEmission.view'
    },
    {
      key: 'enviProtectFee',
      title: 'Phí bảo vệ môi truờng',
      permissionRole: 'facilityEnviProtectFee.view'
    },
    {
      key: 'enviMonitoringReport',
      title: 'Quan trắc môi truờng',
      permissionRole: 'facilityEnviMonitoringReport.view'
    },
    {
      key: 'hazarWasteReport',
      title: 'Chất thải nguy hại',
      permissionRole: 'facilityHazarWasteReport.view'
    }
  ];

  renderPanel = keyPanel => {
    switch (keyPanel) {
      case 'coso': {
        return <CosoPanel />;
      }
      case 'eia': {
        return <FacilityeiaPanel />;
      }
      case 'thanhtra': {
        return <FacilityInspectionPanel />;
      }
      case 'permit': {
        return <FacilitypermitPanel />;
      }
      case 'emission': {
        return <FacilityemissionPanel />;
      }
      case 'enviProtectFee': {
        return <FacilityEnviProtectFeePanel />;
      }
      case 'enviMonitoringReport': {
        return <FacilityEnviMonitoringReportPanel />;
      }
      case 'hazarWasteReport': {
        return <FacilityHazarWasteReportPanel />;
      }
      default: {
        return <PageBuilding />;
      }
    }
  };

  render() {
    return (
      <div>
        <Tabs renderActiveTabPanelOnly={true} style={{ color: 'red' }}>
          {this.panelList.map(item => {
            const isHavePermission = this.props.permissions.includes(
              item.permissionRole
            );
            if (isHavePermission)
              return (
                <Tab
                  id={item.key}
                  key={item.key}
                  title={item.title}
                  panel={this.renderPanel(item.key)}
                />
              );
            else return undefined;
          })}

          {/* {this.panelList.map(item => (
                <Tab
                  id={item.key}
                  key={item.key}
                  title={item.title}
                  panel={this.renderPanel(item.key)}
                />
              ))} */}
        </Tabs>
      </div>
    );
  }
}
