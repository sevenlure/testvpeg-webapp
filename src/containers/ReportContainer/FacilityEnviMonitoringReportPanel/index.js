import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import {
  Card,
  Elevation,
  Spinner,
  Intent,
  Classes,
  Tooltip,
  Colors,
  Dialog,
  Button,
  Alert
} from '@blueprintjs/core';
import { Icon } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { COLOR, DATE_FORMAT, DEFAULT_NULL_VALUE } from 'config';
import { getAll } from 'api/facilityEnviMonitoringReportApi';
import { getAll as getAllLookup } from 'api/lookuplistitemApi';

import PivotTableUI from 'react-pivottable/PivotTableUI';
import 'react-pivottable/pivottable.css';
// import TableRenderers from 'react-pivottable/TableRenderers';
// import Plot from 'react-plotly.js';
// import createPlotlyRenderers from 'react-pivottable/PlotlyRenderers';
import { DateInput } from '@blueprintjs/datetime';
import { Row } from 'reactstrap';
import { createForm } from 'rc-form';
import Jquery from 'jquery';
// create Plotly renderers via dependency injection
import BaseDataTableContainer from 'containers/BaseContainer/BaseDataTableContainer';
import slug from 'constants/slug';
import { exportDataWithQuery } from 'api/facilityEnviMonitoringReportApi';
// const PlotlyRenderers = createPlotlyRenderers(Plot);

const SpinnerContainer = styled.div`
  margin: 100px auto;
  text-align: center;
`;

const NameContainer = styled.div`
  color: ${Colors.GREEN2};
  text-decoration: underline;
`;

const copyTable = () => {
  let props = [
    'background-color',
    'border',
    'font-size',
    'padding',
    'text-align',
    'color',
    'vertical-align',
    'font-weight',
  ];
  appyStyle(Jquery('.pvtAxisLabel'), props);
  appyStyle(Jquery('.pvtRowLabel'), props);
  appyStyle(Jquery('.pvtColLabel'), props);
  appyStyle(Jquery('.pvtVal'), props);
  appyStyle(Jquery('.pvtTotal'), props);
  appyStyle(Jquery('.pvtGrandTotal'), props);
  appyStyle(Jquery('.pvtTotalLabel'), props);
  appyStyle(Jquery('.pvtTable'), props);
  // if (Jquery('.pvtTable th').length > 0)
  //   appyStyle(Jquery('.pvtTable th'), props);
  // copy( document.getElementsByClassName('pvtOutput')[0].outerHTML.toString())

  // this.copyStringToClipboard(
  //   document.getElementsByClassName('pvtOutput')[0].outerHTML
  // );

  let htmlString = document.getElementsByClassName('pvtOutput')[0].outerHTML;
  exportToExcel(htmlString, 'BaoCaoQuanTracMoiTruongData');
};

const appyStyle = (target, props = []) => {
  for (var i = 0; i < props.length; i++) {
    let tamp = target.css(props[i]);
    target.css(props[i], tamp);
  }
};

export default class BCTKHoSoMoiTruongPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      isLoaded: false,
      dataSource: [],
      data: [],
      filter: {},
      lookupItemObj: {},
      copyData: '',
      isOpenExport: false
    };
  }
  async componentDidMount() {
    const lookupItemRes = await getAllLookup();

    const res = await getAll();
    if (res.success) {
      const dataSource = _.clone(res.data);
      const lookupItemObj = _.keyBy(lookupItemRes.data, '_id');
      for (var i = 0; i < res.data.length; i++) {
        res.data[i].district = _.result(res.data[i], 'facility.district');
        res.data[i].sector = _.result(res.data[i], 'facility.sector');
        res.data[i].authority = _.result(res.data[i], 'facility.authority');
        res.data[i].industrialArea = _.result(
          res.data[i],
          'facility.industrialArea'
        );

        if (res.data[i].district && lookupItemObj[res.data[i].district])
        res.data[i]['Quận/Huyện'] = lookupItemObj[res.data[i].district].Name;
        else res.data[i]['Quận/Huyện'] = DEFAULT_NULL_VALUE;

      if (res.data[i].sector && lookupItemObj[res.data[i].sector])
        res.data[i]['Ngành/Nghề'] = lookupItemObj[res.data[i].sector].Name;
        else res.data[i]['Ngành/Nghề'] = DEFAULT_NULL_VALUE;

      if (res.data[i].authority && lookupItemObj[res.data[i].authority])
        res.data[i]['Thẩm quyền quản lý'] =
          lookupItemObj[res.data[i].authority].Name;
          else res.data[i]['Thẩm quyền quản lý'] = DEFAULT_NULL_VALUE;

      if (
        res.data[i].industrialArea &&
        lookupItemObj[res.data[i].industrialArea]
      )
        res.data[i]['Khu/Cụm Công nghiệp'] =
          lookupItemObj[res.data[i].industrialArea].Name;
          else res.data[i]['Khu/Cụm Công nghiệp'] = DEFAULT_NULL_VALUE;

       
            res.data[i]['Tần suất giám sát'] = res.data[i].FrequencyOfMonitoring
            res.data[i]['Năm nộp báo cáo'] = res.data[i].YearOfCharge
            res.data[i]['Quý nộp báo cáo'] = res.data[i].QuaterOfCharge
            res.data[i]['Tình trạng nộp'] = res.data[i].PaymentStatus
            res.data[i]['Thời gian nộp'] = res.data[i].SubmitTime
            
        // res.data[i] = _.pick(res.data[i], ['Quận/Huyện', 'Ngành/Nghề','Thẩm quyền quản lý'])
        res.data[i] = _.omit(res.data[i], [
          'district',
          'sector',
          // '_id',
          'authority',
          'Latitude',
          'Longitude',
          'FacilityCode',
          'FacilityNameFull',
          'province',
          'industrialArea',
          'AddedOn',
          // 'facility',
          'emissionType',
          'IssueDate',
          'FrequencyOfMonitoring',
          'YearOfCharge',
          'QuaterOfCharge',
          'PaymentStatus',
          'SubmitDate',
          'SubmitTime'
        ]);
      }

      this.setState({
        data: res.data,
        isLoaded: true,
        dataSource,
        lookupItemObj
      });
    }
  }

  convertToDataTable() {
    let dataSource = _.clone(this.state.dataSource);
    dataSource = dataSource.filter(item => {
      if (!item.AddedOn) return true;
      const time = moment(item.AddedOn);
      return (
        time >= this.state.filter.fromDate && time <= this.state.filter.toDate
      );
    });

    for (var i = 0; i < dataSource.length; i++) {
      dataSource[i].district = _.result(dataSource[i], 'facility.district');
      dataSource[i].sector = _.result(dataSource[i], 'facility.sector');
      dataSource[i].authority = _.result(dataSource[i], 'facility.authority');
      dataSource[i].industrialArea = _.result(
        dataSource[i],
        'facility.industrialArea'
      );

      if (
        dataSource[i].district &&
        this.state.lookupItemObj[dataSource[i].district]
      )
        dataSource[i]['Quận/Huyện'] = this.state.lookupItemObj[
          dataSource[i].district
        ].Name;
        else dataSource[i]['Quận/Huyện'] = DEFAULT_NULL_VALUE;

      if (
        dataSource[i].sector &&
        this.state.lookupItemObj[dataSource[i].sector]
      )
        dataSource[i]['Ngành/Nghề'] = this.state.lookupItemObj[
          dataSource[i].sector
        ].Name;
        else dataSource[i]['Ngành/Nghề'] = DEFAULT_NULL_VALUE;

      if (
        dataSource[i].authority &&
        this.state.lookupItemObj[dataSource[i].authority]
      )
        dataSource[i]['Thẩm quyền quản lý'] = this.state.lookupItemObj[
          dataSource[i].authority
        ].Name;
        else dataSource[i]['Thẩm quyền quản lý'] = DEFAULT_NULL_VALUE;

      if (
        dataSource[i].industrialArea &&
        this.state.lookupItemObj[dataSource[i].industrialArea]
      )
        dataSource[i]['Khu/Cụm Công nghiệp'] = this.state.lookupItemObj[
          dataSource[i].industrialArea
        ].Name;
        else dataSource[i]['Khu/Cụm Công nghiệp'] = DEFAULT_NULL_VALUE;


        dataSource[i]['Tần suất giám sát'] = dataSource[i].FrequencyOfMonitoring
        dataSource[i]['Năm nộp báo cáo'] = dataSource[i].YearOfCharge
        dataSource[i]['Quý nộp báo cáo'] = dataSource[i].QuaterOfCharge
        dataSource[i]['Tình trạng nộp'] = dataSource[i].PaymentStatus
        dataSource[i]['Thời gian nộp'] = dataSource[i].SubmitTime

      dataSource[i] = _.omit(dataSource[i], [
        'district',
          'sector',
          // '_id',
          'authority',
          'Latitude',
          'Longitude',
          'FacilityCode',
          'FacilityNameFull',
          'province',
          'industrialArea',
          'AddedOn',
          // 'facility',
          'emissionType',
          'IssueDate',
          'FrequencyOfMonitoring',
          'YearOfCharge',
          'QuaterOfCharge',
          'PaymentStatus',
          'SubmitDate',
          'SubmitTime'
      ]);
    }
    this.setState({
      data: dataSource,
      isLoaded: true
    });
  }

  columns = [
    {
      dataField: 'FacilityNameFull',
      text: 'Tên Cơ sở',
      formatter: (cell, row) => {
        return (
          <Link target="_blank" to={slug.ENVI_MONITORING_REPORT.VIEW_WITH_ID + row._id}>
            <NameContainer>{cell}</NameContainer>
          </Link>
        );
      }
    },
    {
      dataField: 'Tần suất giám sát',
      text: 'Tần suất giám sát',
    },
    {
      dataField: 'Tình trạng nộp',
      text: 'Tình trạng nộp',
    },
  ];

  render() {
    return (
      <div style={{ width: '100%' }}>
        <Clearfix height={8} />
        <Card interactive={true} elevation={Elevation.TWO}>
          <h5>
            <a
            href=" "
              style={{
                display: 'flex',
                alignItems: 'center',
                color: COLOR.SUCCESS
              }}
            >
              <Icon icon={IconNames.LIST} />
              <Clearfix width={8} />
              Báo Cáo Thống Kê Quan trắc môi truờng
            </a>
          </h5>
          <SearchContainer
            handlClick={val => {
              this.setState(
                {
                  filter: val
                },
                () => {
                  this.convertToDataTable();
                }
              );
            }}
          />
          <Clearfix height={8} />

          {/* <PivotTableUI 
         unusedOrientationCutoff={900}
                data={this.state.data}
                onChange={s => this.setState(s)}
                renderers={Object.assign({}, TableRenderers, PlotlyRenderers)}
                {...this.state}
            /> */}
          {this.state.isLoaded && (
            <PivotTableUI
            tableOptions={{
              clickCallback: (e, value, filters, pivotData) => {
                var names = [];
                pivotData.forEachMatchingRecord(filters, function(record) {
                  names.push({
                    _id: record._id,
                    FacilityNameFull: _.result(record, 'facility.FacilityNameFull',DEFAULT_NULL_VALUE),
                    "Tần suất giám sát": record['Tần suất giám sát'],
                    "Tình trạng nộp": record['Tình trạng nộp'],
                  });
                });
                this.setState({
                  dataList: names,
                  isOpenModal: true
                });
              }
            }}
              unusedOrientationCutoff={900}
              data={this.state.data}
              hiddenFromDragDrop={['_id','facility']}
              onChange={s => this.setState(s)}
              {...this.state}
              aggregators={this.state.aggregators}
              ref={pivotComp => {
                if (
                  _.result(pivotComp, 'props.aggregators') &&
                  !this.state.aggregators
                ) {
                  let aggregators = _.result(pivotComp, 'props.aggregators');

                  this.setState({
                    aggregators: _.pick(aggregators, ['Count'])
                  });
                }
              }}
            />
          )}

          {!this.state.isLoaded && (
            <SpinnerContainer>
              <Spinner intent={Intent.PRIMARY} size={200} />
            </SpinnerContainer>
          )}
        </Card>
        <Clearfix height={8} />
          <Dialog
          style={{
            width: '70vw'
          }}
          icon='info-sign'
          title='Thông tin'
          isOpen={this.state.isOpenModal}
          onClose={() => {
            this.setState({
              isOpenModal: false
            });
          }}
          className={Classes.OVERLAY_SCROLL_CONTAINER}
          usePortal
          canOutsideClickClose
          canEscapeKeyClose
          hasBackdrop
        >
          <div
            style={{ whiteSpace: 'pre-line' }}
            className={`${Classes.DIALOG_BODY}`}
          >
            <BaseDataTableContainer
              dataList={this.state.dataList}
              columns={this.columns}
            />
          </div>
          <div className='bp3-dialog-footer'>
            <div className='bp3-dialog-footer-actions'>
              <Button
                intent={Intent.SUCCESS}
                onClick={async () => {
                  this.setState({ isOpenExport: true });
                  let res = await exportDataWithQuery({
                    _id: this.state.dataList.map(item => item._id)
                  }); 
                  let dataBuff = toArrayBuffer(res.buffer.data);
                  let blob = new Blob([dataBuff], {
                    type:
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                  });
                  getFile(blob, res.fileName);
                }}
              >
                Xuất Excel
              </Button>
            </div>
          </div>
        </Dialog>
        <Alert
          confirmButtonText='Cancel'
          isOpen={this.state.isOpenExport}
          onClose={() => {
            this.setState({ isOpenExport: false });
          }}
        >
          <div id='downloadlink'>Export.....</div>
        </Alert>
        <Clearfix height={8} />
      </div>
    );
  }
}

@createForm()
class SearchContainer extends React.Component {
  getSearch() {
    const { getFieldsValue } = this.props.form;
    let val = getFieldsValue(['fromDate', 'toDate']);
    if (!val.fromDate) val.fromDate = moment(1970);
    if (!val.toDate) val.toDate = moment();
    return val;
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Row>
          <Clearfix width={16} />
          <div style={{ display: 'flex', alignItems: 'center', width: 300 }}>
            <span>Từ ngày</span>
            <Clearfix width={8} />
            <div style={{ flex: 1 }}>
              {getFieldDecorator('fromDate', {})(
                <DateInput
                  popoverProps={{
                    targetClassName: 'el-fill'
                  }}
                  formatDate={date => moment(date).format(DATE_FORMAT)}
                  parseDate={str => moment(str).toDate()}
                />
              )}
            </div>
          </div>
          <Clearfix width={16} />
          <div style={{ display: 'flex', alignItems: 'center', width: 330 }}>
            <span>Đến ngày</span>
            <Clearfix width={8} />
            <div style={{ flex: 1 }}>
              {getFieldDecorator('toDate', {})(
                <DateInput
                  popoverProps={{
                    targetClassName: 'el-fill'
                  }}
                  formatDate={date => moment(date).format(DATE_FORMAT)}
                  parseDate={str => moment(str).toDate()}
                />
              )}
            </div>
            <Clearfix width={8} />
            <Tooltip content='Lọc theo Ngày tạo'>
              <Icon
                icon={IconNames.SEARCH}
                iconSize={Icon.SIZE_LARGE}
                intent={Intent.SUCCESS}
                style={{ cursor: 'pointer' }}
                onClick={() => {
                  let val = this.getSearch();
                  if (this.props.handlClick) this.props.handlClick(val);
                }}
              />
            </Tooltip>
            <Clearfix width={8} />
              <Tooltip content='Xuất file Excel'>
              <Icon
                icon={IconNames.EXPORT}
                iconSize={Icon.SIZE_LARGE}
                className='pointer'
                intent={Intent.SUCCESS}
                onClick={copyTable}
              />
            </Tooltip>
          </div>
        </Row>
      </div>
    );
  }
}
