import { getFetch, postFetch, putFetch, deleteFetch } from 'utils/fetch';
import { INSPECTION_ATTACHMENT_API } from 'api/config';

export function getList(query) {
  return getFetch(`${INSPECTION_ATTACHMENT_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${INSPECTION_ATTACHMENT_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${INSPECTION_ATTACHMENT_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${INSPECTION_ATTACHMENT_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${INSPECTION_ATTACHMENT_API}`, obj);
}
