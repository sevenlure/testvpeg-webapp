import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import {
  FACILITYEIA_API,
  FACILITYEIA_GETALL_API,
  FACILITYEIA_EXPORT_WITH_QUERY_API
} from './config';

export function getList(query) {
  return getFetch(`${FACILITYEIA_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${FACILITYEIA_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${FACILITYEIA_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${FACILITYEIA_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${FACILITYEIA_API}`, obj);
}

export function getAll() {
  return getFetch(`${FACILITYEIA_GETALL_API}`);
}

export function exportDataWithQuery(query) {
  return postFetch(`${FACILITYEIA_EXPORT_WITH_QUERY_API}`,query);
}