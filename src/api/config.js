import { HOST } from '../config';

export const AUTH_API = HOST + 'auth'
export const AUTH_LOGIN_API = HOST + 'auth/login'

export const LOOKUP_ITEM_API = HOST + 'lookuplistitem'
export const LOOKUP_ITEM_DELETEWITHCHECK_API = HOST + 'lookuplistitem/deleteWithCheck'
export const LOOKUP_ITEM_KHUCUMCN_API = HOST + 'lookuplistitem/lookuplist/1'
export const LOOKUP_ITEM_THAMQUYENQLMT_API = HOST + 'lookuplistitem/lookuplist/2'
export const LOOKUP_ITEM_TYPE_INSPECTION_ATTACHMENT_API = 
  HOST + 'lookuplistitem/lookuplist/3';
export const LOOKUP_ITEM_QUANHUYEN_API = HOST + 'lookuplistitem/lookuplist/4'
export const LOOKUP_ITEM_EMISSIONTYPE_API = HOST + 'lookuplistitem/lookuplist/6'
export const LOOKUP_ITEM_MUCDICH_API = HOST + 'lookuplistitem/lookuplist/8';
export const LOOKUP_ITEM_TINHTRANG_API = HOST + 'lookuplistitem/lookuplist/9';
export const LOOKUP_ITEM_SUKIENTUANTHU_API =
  HOST + 'lookuplistitem/lookuplist/7';
export const LOOKUP_ITEM_PERMITTYPE_API = HOST + 'lookuplistitem/lookuplist/10'
export const LOOKUP_ITEM_NGANHNGHE_API = HOST + 'lookuplistitem/lookuplist/12'

export const LOOKUP_ITEM_HINHTHUCSOHUU_API = HOST + 'lookuplistitem/lookuplist/20'
export const LOOKUP_ITEM_NGUONNUOC_API = HOST + 'lookuplistitem/lookuplist/25'
export const LOOKUP_ITEM_LOAIHOSO_API = HOST + 'lookuplistitem/lookuplist/27'
export const LOOKUP_ITEM_GETALL_API = HOST + 'lookuplistitem/getAll'
export const LOOKUP_ITEM_UPDATE_CHILDREN_API = HOST + 'lookuplistitem/updateChildren'

export const LOOKUP_ITEM_TYPE_PERMIT_ATTACHMENT_API = HOST + 'lookuplistitem/lookuplist/15'
export const LOOKUP_ITEM_TYPE_EIA_ATTACHMENT_API = HOST + 'lookuplistitem/lookuplist/17'
export const LOOKUP_ITEM_TYPE_FACILITY_ATTACHMENT_API = HOST + 'lookuplistitem/lookuplist/19'

export const LOOKUP_ITEM_BPTGCTNH_API = HOST + 'lookuplistitem/lookuplist/28' //bộ phận thu gom chất thải nguy hại

export const LOOKUPLIST_API = HOST + 'lookuplist'
export const LOOKUPLIST_GETALL_API = HOST + 'lookuplist/getAll'

export const COSO_API = HOST + 'facility'
export const COSO_CHECK_FACILITYCODE_API = HOST + 'facility/checkFacilityCodeExist'
export const COSO_GETALL_API = HOST + 'facility/getAll'
export const COSO_EXPORT_API = HOST + 'facility/export'
export const COSO_EXPORT_WITH_QUERY_API = HOST + 'facility/exportWithQuery'

export const INSPECTION_API = HOST + 'facilityInspection';
export const INSPECTION_ATTACHMENT_API = HOST + 'inspectionAttachment'
export const INSPECTION_GETALL_API = HOST + 'facilityInspection/getAll'
export const INSPECTION_EXPORT_API = HOST + 'facilityInspection/export'
export const INSPECTION_EXPORT_WITH_QUERY_API = HOST + 'facilityInspection/exportWithQuery'

export const FACILITY_ATTACHMENT_API = HOST + 'facilityattachment'
export const EIA_ATTACHMENT_API = HOST + 'eiaattachment'


export const FACILITYEIA_API = HOST + 'facilityeia';
export const FACILITYEIA_GETALL_API = HOST + 'facilityeia/getAll';
export const FACILITYEIA_EXPORT_WITH_QUERY_API = HOST + 'facilityeia/exportWithQuery'


export const UPLOAD_FILE_API = HOST + 'file/upload';

export const FACILITYPERMIT_API = HOST + 'facilitypermit'
export const FACILITYPERMIT_EXPORT_API = HOST + 'facilitypermit/export'
export const FACILITYPERMIT_EXPORT_WITH_QUERY_API = HOST + 'facilitypermit/exportWithQuery'
export const FACILITYPERMIT_GETALL_API = HOST + 'facilitypermit/getAll'
export const PERMIT_ATTACHMENT_API = HOST + 'permitattachment'

export const FACILITYEMISSION_API = HOST + 'facilityemission'
export const FACILITYEMISSION_EXPORT_API = HOST + 'facilityemission/export'
export const FACILITYEMISSION_EXPORT_WITH_QUERY_API = HOST + 'facilityemission/exportWithQuery'
export const FACILITYEMISSION_GETALL_API = HOST + 'facilityemission/getAll';


export const FACILITYENVIPROTECTFEE_API = HOST + 'facilityEnviProtectFee'
export const FACILITYENVIPROTECTFEE_EXPORT_API = HOST + 'facilityEnviProtectFee/export'
export const FACILITYENVIPROTECTFEE_EXPORT_WITH_QUERY_API = HOST + 'facilityEnviProtectFee/exportWithQuery'
export const FACILITYENVIPROTECTFEE_GETALL_API = HOST + 'facilityEnviProtectFee/getAll'

export const FACILITYENVIMONITORINGREPORT_API = HOST + 'facilityEnviMonitoringReport'
export const FACILITYENVIMONITORINGREPORT_EXPORT_API = HOST + 'facilityEnviMonitoringReport/export'
export const FACILITYENVIMONITORINGREPORT_EXPORT_WITH_QUERY_API = HOST + 'facilityEnviMonitoringReport/exportWithQuery'
export const FACILITYENVIMONITORINGREPORT_GETALL_API = HOST + 'facilityEnviMonitoringReport/getAll'


export const MONITORING_API = HOST + 'monitoring'
export const MONITORING_SOLIEUMOINHAT_API = HOST + 'monitoring/solieumoinhat'

export const FACILITY_HAZAR_WASTE_REPORT_API = HOST + 'facilityHazarWasteReport'
export const FACILITY_HAZAR_WASTE_REPORT_API_EXPORT_API = HOST + 'facilityHazarWasteReport/export'
export const FACILITY_HAZAR_WASTE_REPORT_API_EXPORT_WITH_QUERY_API = HOST + 'facilityHazarWasteReport/exportWithQuery'
export const FACILITY_HAZAR_WASTE_REPORT_GETALL_API = HOST + 'facilityHazarWasteReport/getAll'

export const USER_API = HOST + 'user'

export const ROLE_API = HOST + 'role'


export const LOG_API = HOST + 'log'
// export const USER_API = HOST + 'monitoring/solieumoinhat'

export const TEST_ROUTE_API = HOST + 'testRoute'



