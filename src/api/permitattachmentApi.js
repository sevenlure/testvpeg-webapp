import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import { PERMIT_ATTACHMENT_API } from './config';

export function getList(query) {
  return getFetch(`${PERMIT_ATTACHMENT_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${PERMIT_ATTACHMENT_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${PERMIT_ATTACHMENT_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${PERMIT_ATTACHMENT_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${PERMIT_ATTACHMENT_API}`, obj);
}
