import { postFetch } from '../utils/fetch';
import { AUTH_LOGIN_API } from './config';

export function login(data) {
  return postFetch(`${AUTH_LOGIN_API}`, data);
}
