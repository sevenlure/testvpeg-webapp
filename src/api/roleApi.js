
import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import {
  ROLE_API
} from './config';

export function getList(query) {
  return getFetch(`${ROLE_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${ROLE_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  console.log(data,'dadada')
  return putFetch(`${ROLE_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${ROLE_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${ROLE_API}`, obj);
}
