import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import {
  FACILITYENVIMONITORINGREPORT_API,
  FACILITYENVIMONITORINGREPORT_EXPORT_API,
  FACILITYENVIMONITORINGREPORT_GETALL_API,
  FACILITYENVIMONITORINGREPORT_EXPORT_WITH_QUERY_API
} from './config';

export function getList(query) {
  return getFetch(`${FACILITYENVIMONITORINGREPORT_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${FACILITYENVIMONITORINGREPORT_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${FACILITYENVIMONITORINGREPORT_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${FACILITYENVIMONITORINGREPORT_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${FACILITYENVIMONITORINGREPORT_API}`, obj);
}

export function exportData() {
  // return window.location.href  = FACILITYEMISSION_EXPORT_API
  return getFetch(`${FACILITYENVIMONITORINGREPORT_EXPORT_API}`);
}

export function getAll() {
  return getFetch(`${FACILITYENVIMONITORINGREPORT_GETALL_API}`);
}

export function exportDataWithQuery(query) {
  return postFetch(`${FACILITYENVIMONITORINGREPORT_EXPORT_WITH_QUERY_API}`,query);
}