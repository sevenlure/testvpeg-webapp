import { getFetch } from '../utils/fetch';
import { LOOKUPLIST_GETALL_API } from './config'

export function getAll() {
  return getFetch(LOOKUPLIST_GETALL_API);
}