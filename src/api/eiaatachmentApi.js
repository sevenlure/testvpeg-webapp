import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import { EIA_ATTACHMENT_API } from './config';

export function getList(query) {
  return getFetch(`${EIA_ATTACHMENT_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${EIA_ATTACHMENT_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${EIA_ATTACHMENT_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${EIA_ATTACHMENT_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${EIA_ATTACHMENT_API}`, obj);
}
