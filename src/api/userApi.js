
import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import {
  USER_API
} from './config';

export function getList(query) {
  return getFetch(`${USER_API}`, query);
}

export function getAll() {
  return getFetch(`${USER_API}/getAll`);
}

export function getOne(_id) {
  return getFetch(`${USER_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${USER_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${USER_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${USER_API}`, obj);
}

export function changePassword(_id, data = {}) {
  return putFetch(`${USER_API}/changePassword/${_id}`, data);
}

export function userChangePassword(_id, data = {}) {
  return putFetch(`${USER_API}/userChangePassword/${_id}`, data);
}
