import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import {
  COSO_API,
  COSO_CHECK_FACILITYCODE_API,
  COSO_GETALL_API,
  COSO_EXPORT_API,
  COSO_EXPORT_WITH_QUERY_API
} from './config';

export function getList(query) {
  return getFetch(`${COSO_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${COSO_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${COSO_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${COSO_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${COSO_API}`, obj);
}

export function checkFacilityCodeExist(obj) {
  return postFetch(`${COSO_CHECK_FACILITYCODE_API}`, obj);
}

export function getSearchList(query) {
  return getFetch(`${COSO_API}`, {
    search: query,
    field: ['FacilityNameFull','FrequencyOfMonitoring','Address1', 'Address2'],
    page: 1,
    itemPerPage: 30
  });
}

export function getAll() {
  return getFetch(`${COSO_GETALL_API}`);
}

export function exportData() {
  return getFetch(`${COSO_EXPORT_API}`);
}

export function exportDataWithQuery(query) {
  return postFetch(`${COSO_EXPORT_WITH_QUERY_API}`,query);
}
