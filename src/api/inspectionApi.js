import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import { INSPECTION_API,
  INSPECTION_EXPORT_API,
  INSPECTION_GETALL_API,
  INSPECTION_EXPORT_WITH_QUERY_API
 } from './config';

export function getList(query) {
  return getFetch(`${INSPECTION_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${INSPECTION_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${INSPECTION_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${INSPECTION_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${INSPECTION_API}`, obj);
}

export function getSearchList(query) {
  return getFetch(`${INSPECTION_API}`, {
    search: query,
    field: ['FacilityNameFull'],
    page: 1,
    itemPerPage: 10
  });
}

export function getAll() {
  return getFetch(`${INSPECTION_GETALL_API}`);
}

export function exportData() {
  // return window.location.href  = INSPECTION_EXPORT_API
  return getFetch(`${INSPECTION_EXPORT_API}`);
}

export function exportDataWithQuery(query) {
  return postFetch(`${INSPECTION_EXPORT_WITH_QUERY_API}`,query);
}