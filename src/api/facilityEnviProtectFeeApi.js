import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import {
  FACILITYENVIPROTECTFEE_API,
  FACILITYENVIPROTECTFEE_EXPORT_API,
  FACILITYENVIPROTECTFEE_GETALL_API,
  FACILITYENVIPROTECTFEE_EXPORT_WITH_QUERY_API
} from './config';

export function getList(query) {
  return getFetch(`${FACILITYENVIPROTECTFEE_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${FACILITYENVIPROTECTFEE_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${FACILITYENVIPROTECTFEE_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${FACILITYENVIPROTECTFEE_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${FACILITYENVIPROTECTFEE_API}`, obj);
}

export function exportData() {
  return getFetch(`${FACILITYENVIPROTECTFEE_EXPORT_API}`);

}

export function getAll() {
  return getFetch(`${FACILITYENVIPROTECTFEE_GETALL_API}`);
}

export function exportDataWithQuery(query) {
  return postFetch(`${FACILITYENVIPROTECTFEE_EXPORT_WITH_QUERY_API}`,query);
}