
import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import {
  LOG_API
} from './config';

export function getList(query) {
  return getFetch(`${LOG_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${LOG_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${LOG_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${LOG_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${LOG_API}`, obj);
}