import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import {
  FACILITYPERMIT_API,
  FACILITYPERMIT_EXPORT_API,
  FACILITYPERMIT_GETALL_API,
  FACILITYPERMIT_EXPORT_WITH_QUERY_API
} from './config';

export function getList(query) {
  return getFetch(`${FACILITYPERMIT_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${FACILITYPERMIT_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${FACILITYPERMIT_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${FACILITYPERMIT_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${FACILITYPERMIT_API}`, obj);
}

export function exportData() {
  // return window.location.href  = FACILITYPERMIT_EXPORT_API
  return getFetch(`${FACILITYPERMIT_EXPORT_API}`);
}

export function getAll() {
  return getFetch(`${FACILITYPERMIT_GETALL_API}`);
}

export function exportDataWithQuery(query) {
  return postFetch(`${FACILITYPERMIT_EXPORT_WITH_QUERY_API}`,query);
}
