import { getFetch, putFetch, postFetch, deleteFetch } from '../utils/fetch';
import {
  LOOKUP_ITEM_API,
  LOOKUP_ITEM_DELETEWITHCHECK_API,
  LOOKUP_ITEM_QUANHUYEN_API,
  LOOKUP_ITEM_NGANHNGHE_API,
  LOOKUP_ITEM_KHUCUMCN_API,
  LOOKUP_ITEM_THAMQUYENQLMT_API,
  LOOKUP_ITEM_NGUONNUOC_API,
  LOOKUP_ITEM_HINHTHUCSOHUU_API,
  LOOKUP_ITEM_TYPE_FACILITY_ATTACHMENT_API,
  LOOKUP_ITEM_TYPE_EIA_ATTACHMENT_API,
  LOOKUP_ITEM_MUCDICH_API,
  LOOKUP_ITEM_TINHTRANG_API,
  LOOKUP_ITEM_SUKIENTUANTHU_API,
  LOOKUP_ITEM_TYPE_INSPECTION_ATTACHMENT_API,
  LOOKUP_ITEM_PERMITTYPE_API,
  LOOKUP_ITEM_TYPE_PERMIT_ATTACHMENT_API,
  LOOKUP_ITEM_EMISSIONTYPE_API,
  LOOKUP_ITEM_GETALL_API,
  LOOKUP_ITEM_UPDATE_CHILDREN_API,
  LOOKUP_ITEM_LOAIHOSO_API,
  LOOKUP_ITEM_BPTGCTNH_API
} from './config';

export function getLookupSuKienTuanThu() {
  return getFetch(`${LOOKUP_ITEM_SUKIENTUANTHU_API}`, {
    ProvinceID: 2
  });
}

export function getLookupTinhTrang() {
  return getFetch(`${LOOKUP_ITEM_TINHTRANG_API}`, {
    ProvinceID: 2
  });
}

export function getLookupMucDich() {
  return getFetch(`${LOOKUP_ITEM_MUCDICH_API}`, {
    ProvinceID: 2
  });
}

export function getLookupKhuCumCN() {
  return getFetch(`${LOOKUP_ITEM_KHUCUMCN_API}`, {
    ProvinceID: 2
  });
}

export function getLookupThamQuyenQLMT() {
  return getFetch(`${LOOKUP_ITEM_THAMQUYENQLMT_API}`, {
    ProvinceID: 2
  });
}

export function getLookupQuanHuyen() {
  return getFetch(`${LOOKUP_ITEM_QUANHUYEN_API}`, {
    ProvinceID: 2
  });
}

export function getAllLookupQuanHuyen() {
  return getFetch(`${LOOKUP_ITEM_QUANHUYEN_API}`, {
    // ProvinceID: 2
  });
}

export function getLookupNganhNghe() {
  return getFetch(`${LOOKUP_ITEM_NGANHNGHE_API}`, {
    ProvinceID: 2
  });
}

export function getAllLookupNganhNghe() {
  return getFetch(`${LOOKUP_ITEM_NGANHNGHE_API}`, {
    // ProvinceID: 2
  });
}

export function getLookupHinhThucSoHuu() {
  return getFetch(`${LOOKUP_ITEM_HINHTHUCSOHUU_API}`, {
    ProvinceID: 2
  });
}

export function getLookupNguonNuoc() {
  return getFetch(`${LOOKUP_ITEM_NGUONNUOC_API}`, {
    ProvinceID: 2
  });
}

export function getLookupTypeFacilityAttachment() {
  return getFetch(`${LOOKUP_ITEM_TYPE_FACILITY_ATTACHMENT_API}`, {
    ProvinceID: 2
  });
}
export function getLookupTypeInspectionAttachment() {
  return getFetch(`${LOOKUP_ITEM_TYPE_INSPECTION_ATTACHMENT_API}`, {
    ProvinceID: 2
  });
}

export function getLookupTypeEiaAttachment() {
  return getFetch(`${LOOKUP_ITEM_TYPE_EIA_ATTACHMENT_API}`, {
    ProvinceID: 2
  });
}

export function getLookupPermitType() {
  return getFetch(`${LOOKUP_ITEM_PERMITTYPE_API}`, {
    ProvinceID: 2,
    itemPerPage: 20
  });
}

export function getLookupTypePermitAttachment() {
  return getFetch(`${LOOKUP_ITEM_TYPE_PERMIT_ATTACHMENT_API}`, {
    ProvinceID: 2
  });
}

export function getLookupEmissionType() {
  return getFetch(`${LOOKUP_ITEM_EMISSIONTYPE_API}`, {
    ProvinceID: 2
  });
}

export function getLookupById(_id) {
  return getFetch(`${LOOKUP_ITEM_GETALL_API}`, {
    lookuplist: _id,
    ProvinceID: 2
  });
}

export function updateOne(_id, data = {}) {
  return putFetch(`${LOOKUP_ITEM_API}/${_id}`, data);
}

export function updateChildren(_id, { key, name }) {
  return putFetch(`${LOOKUP_ITEM_UPDATE_CHILDREN_API}/${_id}`, { key, name });
}

export function create(obj) {
  return postFetch(`${LOOKUP_ITEM_API}`, {
    ...obj,
    ProvinceID: 2
  });
}

export function deleteOne(_id) {
  return deleteFetch(`${LOOKUP_ITEM_API}/${_id}`);
}

export function deleteOneWithCheck(_id, data = {}) {
  return deleteFetch(`${LOOKUP_ITEM_DELETEWITHCHECK_API}/${_id}`, data);
}

export function getAll() {
  return getFetch(`${LOOKUP_ITEM_GETALL_API}`, {
    ProvinceID: 2
  });
}


export function getAllLookupLoaiHoSo() {
  return getFetch(`${LOOKUP_ITEM_LOAIHOSO_API}`, {
    ProvinceID: 2
  });
}

export function getLookupBPCTNH() {
  return getFetch(`${LOOKUP_ITEM_BPTGCTNH_API}`, {
    ProvinceID: 2
  });
}