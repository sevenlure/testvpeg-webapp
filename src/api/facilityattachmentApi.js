import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import { FACILITY_ATTACHMENT_API } from './config';

export function getList(query) {
  return getFetch(`${FACILITY_ATTACHMENT_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${FACILITY_ATTACHMENT_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${FACILITY_ATTACHMENT_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${FACILITY_ATTACHMENT_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${FACILITY_ATTACHMENT_API}`, obj);
}
