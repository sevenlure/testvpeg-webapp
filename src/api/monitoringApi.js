import { getFetch } from '../utils/fetch';
import { MONITORING_SOLIEUMOINHAT_API } from './config';

export function getSoLieuMoiNhat(LoaiDiem) {
  return getFetch(`${MONITORING_SOLIEUMOINHAT_API}/${LoaiDiem}`, {});
}