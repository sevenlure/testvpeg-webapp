import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import {
  FACILITY_HAZAR_WASTE_REPORT_API,
  FACILITY_HAZAR_WASTE_REPORT_API_EXPORT_API,
  FACILITY_HAZAR_WASTE_REPORT_GETALL_API,
  FACILITY_HAZAR_WASTE_REPORT_API_EXPORT_WITH_QUERY_API
} from './config';

export function getList(query) {
  return getFetch(`${FACILITY_HAZAR_WASTE_REPORT_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${FACILITY_HAZAR_WASTE_REPORT_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${FACILITY_HAZAR_WASTE_REPORT_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${FACILITY_HAZAR_WASTE_REPORT_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${FACILITY_HAZAR_WASTE_REPORT_API}`, obj);
}

export function exportData() {
  return getFetch(`${FACILITY_HAZAR_WASTE_REPORT_API_EXPORT_API}`);

}

export function getAll() {
  return getFetch(`${FACILITY_HAZAR_WASTE_REPORT_GETALL_API}`);
}

export function exportDataWithQuery(query) {
  return postFetch(`${FACILITY_HAZAR_WASTE_REPORT_API_EXPORT_WITH_QUERY_API}`,query);
}