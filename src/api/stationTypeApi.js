import { getFetch } from '../utils/fetch';
import { HOST } from '../config'

export function getAll() {
  return getFetch(`${HOST}station-type/total-count/station-auto`, {});
}