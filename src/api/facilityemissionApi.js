import { getFetch, postFetch, putFetch, deleteFetch } from '../utils/fetch';
import {
  FACILITYEMISSION_API,
  FACILITYEMISSION_EXPORT_API,
  FACILITYEMISSION_GETALL_API,
  FACILITYEMISSION_EXPORT_WITH_QUERY_API
} from './config';

export function getList(query) {
  return getFetch(`${FACILITYEMISSION_API}`, query);
}

export function getOne(_id) {
  return getFetch(`${FACILITYEMISSION_API}/${_id}`);
}

export function updateOne(_id, data = {}) {
  return putFetch(`${FACILITYEMISSION_API}/${_id}`, data);
}

export function deleteOne(_id) {
  return deleteFetch(`${FACILITYEMISSION_API}/${_id}`);
}

export function create(obj) {
  return postFetch(`${FACILITYEMISSION_API}`, obj);
}

export function exportData() {
  // return window.location.href  = FACILITYEMISSION_EXPORT_API
  return getFetch(`${FACILITYEMISSION_EXPORT_API}`);
}

export function getAll() {
  return getFetch(`${FACILITYEMISSION_GETALL_API}`);
}

export function exportDataWithQuery(query) {
  return postFetch(`${FACILITYEMISSION_EXPORT_WITH_QUERY_API}`,query);
}