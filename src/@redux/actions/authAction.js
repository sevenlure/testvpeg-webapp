import { add, clear } from "react-redux-permissions"

export const LOGGED = 'AUTH/logged';
export const UN_LOGGED = 'AUTH/un_logged';

export const logged = (token, info) => {
  return dispatch => {
    dispatch(clear())
    dispatch(add(info.permissionRole))
    dispatch({ type: LOGGED, payload: { token, info } });
  };
};

export const unLogged = () => {
  return dispatch => {
    dispatch(clear())
    dispatch({ type: UN_LOGGED });
  };
};
