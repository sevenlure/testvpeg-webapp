export const SET_MENU_SELECTED = 'MENU/selected';

export const setMenuSelected = (menu) => {
  return dispatch => {
    console.log('menu: ', menu)
    dispatch({type: SET_MENU_SELECTED, payload: menu}); 
  }
}
