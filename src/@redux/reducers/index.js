import { combineReducers } from 'redux';
import menu from './menuReducer';
import auth from './authReducer';
import { reducer as permissions } from "react-redux-permissions"

export default combineReducers({ menu, auth, permissions });
