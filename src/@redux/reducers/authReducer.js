import update from 'react-addons-update';
import { LOGGED, UN_LOGGED } from '../actions/authAction';

let initialState = {
  token: null,
  isLogged: false,
  info: null,
  permissionRole: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGGED: {
      return update(state, {
        isLogged: { $set: true },
        token: { $set: action.payload.token },
        info: { $set: action.payload.info },
        permissionRole: { $set: action.payload.info.permissionRole },
      });
    }
    case UN_LOGGED: {
      return update(state, {
        isLogged: { $set: false },
        token: { $set: null },
        info: { $set: null },
        permissionRole: { $set: [] },
      });
    }
    default:
      return { ...state };
  }
};
