import update from 'react-addons-update'
import { SET_MENU_SELECTED } from '../actions/menuAction'

let initialState = {
  menuSelected: '',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_MENU_SELECTED:
      return setMenuSelected(state, action)
    default:
      return { ...state }
  }
}

function setMenuSelected(state, action) {
  console.log('menu reducer: ', action.payload)
  return update(state, { menuSelected: {$set: action.payload}})
}