import React from 'react';
import { InputGroup } from '@blueprintjs/core'

export default () => (
  <InputGroup leftIcon='search' placeholder='Tìm kiếm' round='false'/>
)