import { connect } from 'react-redux';
import { Icon } from '@blueprintjs/core'
import { Link, withRouter } from "react-router-dom";
import React from 'react';
import styled from 'styled-components'

import { setMenuSelected } from '../@redux/actions/menuAction'
import slug from '../constants/slug';

const NavContainer = styled.div`
  -webkit-transition: width 0.3s;
  transition: width 0.3s;

`
const IIcon = styled.i`
  ${props => (props.size ? `font-size: ${props.size}` : '')}, ${props =>
      props.color ? `color: ${props.color}` : ''};
`


class NavComponent extends React.Component {
  render () {
    return (
      <NavContainer style={{width: this.props.menuStore.menuSelected ? 220 : 0}}> 
        <ul className="bp3-menu bp3-elevation-1" style={{height: '100%'}}>
          <li className="bp3-menu-header" style={{display: 'flex', flexDirection: 'row'}}>
            <h4 className="bp3-heading" style={{flex: 1}}>Nước mặt</h4>
            <Link to={slug.MAP}>
              <Icon className="bp3-intent-danger" icon="cross" />
            </Link>
          </li>
          <li><Link className='bp3-menu-item bp3-icon-database' to={slug.DATA_WITH_PARAMS}>Số liệu</Link></li>
          <li className="bp3-menu-divider"></li>
          <li><Link className='bp3-menu-item bp3-icon-comment' to={slug.QA}>Q&A</Link></li>
          <li className="bp3-menu-divider"></li>
        </ul>
      </NavContainer>
    );
  }
}

const mapStateToProps = state => ({
  menuStore: state.menu
});

const mapDispatchToProps = {
  setMenuSelected
};

export default connect(mapStateToProps, mapDispatchToProps)(NavComponent)
