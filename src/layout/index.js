import React from 'react';

import Header from './header';
import slug from '../constants/slug';
import { withRouter, Switch, Route, Redirect } from 'react-router-dom';
import CosoRoute from 'containers/Coso';
import FacilitypermitRoute from 'containers/FacilitypermitContainer';
import FacilityemissionRoute from 'containers/FacilityemissionContainer';
import FacilityeiaRoute from 'containers/FacilityeiaContainer';
import UserRoute from 'containers/UserContainer';
import FacilityHazarWasteReportRoute from 'containers/FacilityHazarWasteReportContainer';

import InspectionRoute from 'containers/Inspection';

import FacilityeEnviProtectFeeRoute from 'containers/FacilityeEnviProtectFeeContainer';

import FacilityEnviMonitoringReportRoute from 'containers/FacilityEnviMonitoringReportContainer';
// import { arrRoutes as FacilityHazarWasteReportRoutes } from 'containers/FacilityHazarWasteReportContainer';

import MapContainer from 'containers/MapContainer';
import MonitoringDataContainer from 'containers/MonitoringDataContainer';
import ReportContainer from 'containers/ReportContainer';
import ManagementContainer from 'containers/ManagementContainer';
import AdminContainer from 'containers/AdminContainer';
import PageBuilding from 'components/pageBuilding';

import NoMatchRoute from 'containers/NoMatchRoute';
import Login from 'containers/LoginContainer';
import { connect } from 'react-redux';

@connect(
  state => ({
    isLogged: state.auth.isLogged
  }),
  {}
)
class MainLayout extends React.Component {
  componentDidMount() {}

  render() {
    return (
      <Switch>
        <Route
          exact
          key={slug.LOGIN}
          path={slug.LOGIN}
          render={matchProps => <Login {...matchProps} />}
        />
        {this.props.isLogged ? <AppRoute /> : <Redirect to={slug.LOGIN} />}
        {/* <AppRoute /> */}
      </Switch>
    );
  }
}

class AppRoute extends React.Component {
  render() {
    return (
      <div id="app">
        <Header />
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          {/* <Nav /> */}
          <div style={{ flex: 1, paddingLeft: 1 }}>
            <div
              style={{
                background: '#fff',
                padding: 24,
                margin: 0,
                minHeight: '95vh'
              }}
            >
              <Switch>
                {CosoRoute()}
                {FacilityeiaRoute()}
                {FacilityEnviMonitoringReportRoute()}
                {FacilityeEnviProtectFeeRoute()}
                {InspectionRoute()}
                {FacilitypermitRoute()}
                {FacilityemissionRoute()}
                {FacilityHazarWasteReportRoute()}
                {UserRoute()}
                
                <Route
                  exact
                  path={slug.INSPECTTION.LIST}
                  render={matchProps => <PageBuilding {...matchProps} />}
                />

                <Route
                  exact
                  path={slug.MAP.BASE}
                  render={matchProps => <MapContainer {...matchProps} />}
                />

                <Route
                  exact
                  path={slug.MONITORING_DATA.BASE}
                  render={matchProps => (
                    <MonitoringDataContainer {...matchProps} />
                  )}
                />

                <Route
                  exact
                  path={slug.MANAGEMENT.LIST}
                  render={matchProps => <ManagementContainer {...matchProps} />}
                />

                <Route
                  exact
                  path={slug.ADMIN.LIST}
                  render={matchProps => <AdminContainer {...matchProps} />}
                />

                <Route
                  exact
                  path={slug.REPORT.BASE}
                  render={matchProps => <ReportContainer {...matchProps} />}
                />

                {/* <Redirect from="/" to={slug.CO_SO.LIST} /> */}
                <Route
                  render={matchProps => <NoMatchRoute {...matchProps} />}
                />
                {/* <Route path="/dashboard" component={DashboardView} /> */}
              </Switch>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(MainLayout);
