import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  Navbar,
  Button,
  Alignment,
  FocusStyleManager,
  Colors,
  OverflowList,
  Popover,
  Menu,
  MenuItem,
  Dialog,
  Classes,
  Intent,
  InputGroup
} from '@blueprintjs/core';
import React from 'react';

import { setMenuSelected } from '../@redux/actions/menuAction';
import { unLogged } from '../@redux/actions/authAction';
import { menus } from '../constants/slug';
import styled from 'styled-components';
import ClearFix from 'components/elements/clearfix';
import { startsWith } from 'lodash';
import Permissions from 'react-redux-permissions';
import { createForm } from 'rc-form';
import { Row, Col } from 'reactstrap';
import { STYLE } from 'config';
import IconRequired from 'components/elements/required-icon';
import {  userChangePassword } from 'api/userApi';
import Message from 'rc-message';

const ErrorContainer = styled.div`
  margin-top: 5px;
  color: #5c7080;
  font-size: 12px;
  color: red;
`;

const LabelContainer = styled.div`
  align-items: center;
  height: 38px;
  display: flex;
  justify-content: flex-end;
`;

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 8px 16px;
  h2 {
    font-size: 16px;
    margin-top: 0px;
    margin-bottom: 0px;
  }
`;

const MENU_COLOR = '#7B8E52';
const MENU_ACTIVE_COLOR = '#C0F455';

//boxShadow: 'inset 1px -1px 0px 9px grey;'

class HeaderComponent extends React.Component {
  state = {
    stationTypeList: [],
    selected: '',
    isOpenModal: false
  };

  // componentWillMount () {
  //   if (this.props.match && this.props.match.isExact) {
  //   } else {
  //     console.log('detect')
  //     this.props.history.push('/');
  //   }
  // }

  async componentDidMount() {
    FocusStyleManager.onlyShowFocusOnTabs();
    // let rs = await getAll();
    // if (rs && rs.data) {
    //   this.setState({ stationTypeList: rs.data });
    // } else {
    // }
  }

  handleMenuClick = (key, href) => {
    this.setState({ selected: key });
    this.props.setMenuSelected(key);
    this.props.history.push(href);
    this.forceUpdate();
  };

  handleLogout = () => {
    if (this.props.unLogged) this.props.unLogged();
  };

  submitChangePassword = async () => {
    this.props.form.validateFields( async (error, value) => {
      // value.district = _.get(value.district, 'value');
      // // value.securityLevel = _.get(value.securityLevel, 'value');
      // value.role = _.get(value.role, 'value');
      console.log(error, value, this.props.info)
      if (error) return;
      let res = await userChangePassword(_.result(this.props.info,'_id'), value)
      if (res.success) {
        this.setState({
          isOpenModal: false
        })
        Message.success({ content: 'Success!!!' });
      }
      if (res.error) Message.error({ content: res.message });
    });
  };

  render() {
    const pathname = this.props.location.pathname;
    const { getFieldProps, getFieldError, getFieldValue } = this.props.form;
    return (
      <div>
        <HeaderContainer>
          <img src='/vietan-logo.png' alt='logo' style={{ height: 25 }} />
          <ClearFix width={24} />
          <div>
            <h2>Cổng Thông Tin Quản Lý Môi Trường Bình Dương </h2>
          </div>
          <div style={{ flex: 1 }} />
          <div style={{ display: 'inherit' }}>
            <a
              href='blank'
              style={{
                display: 'inline-flex',
                color: Colors.GREEN2,
                textDecoration: 'underline'
              }}
              onClick={e => {
                e.preventDefault();
              }}
            >
              {_.result(this.props.info, 'UserName')}
            </a>
            <ClearFix width={8} />
            <a
              href='blank'
              style={{
                display: 'inline-flex',
                color: Colors.GREEN2,
                textDecoration: 'underline'
              }}
              onClick={e => {
                e.preventDefault();
                // this.handleLogout();
                this.setState({
                  isOpenModal: true
                });
              }}
            >
              Đổi mật khẩu
            </a>
            <ClearFix width={8} />
            <a
              href='blank'
              style={{
                display: 'inline-flex',
                color: Colors.GREEN2,
                textDecoration: 'underline'
              }}
              onClick={e => {
                e.preventDefault();
                this.handleLogout();
              }}
            >
              Thoát
            </a>

            <Dialog
              icon='info-sign'
              title='Đổi mật khẩu'
              isOpen={this.state.isOpenModal}
              onClose={() => {
                this.setState({
                  isOpenModal: false
                });
              }}
              style={{
                width: 700
              }}
              className={Classes.OVERLAY_SCROLL_CONTAINER}
              usePortal
              canOutsideClickClose
              canEscapeKeyClose
              hasBackdrop
            >
              <div
                style={{ whiteSpace: 'pre-line' }}
                className={`${Classes.DIALOG_BODY}`}
              >
                <div>
                  <Row key='passwordRow'>
                    <Col xs={4}>
                      <LabelContainer style={STYLE.REQUIREDFIELD}>
                        Mật khẩu hiện tại
                        <IconRequired />
                      </LabelContainer>
                    </Col>
                    <Col xs={4}>
                      <InputGroup
                        type='Password'
                        placeholder='******'
                        {...getFieldProps('Password', {
                          initialValue: '',
                          rules: [
                            {
                              required: true,
                              message: 'Mật khẩu is required'
                            },
                            {
                              min: 6,
                              message: 'Mật khẩu độ dài ít nhất 6 ký tự'
                            }
                          ]
                        })}
                      />
                      <ErrorContainer>
                        {(getFieldError('Password') || []).join(', ')}
                      </ErrorContainer>
                    </Col>
                  </Row>
                  <Row >
                    <Col xs={4}>
                      <LabelContainer style={STYLE.REQUIREDFIELD}>
                        Mật khẩu mới
                        <IconRequired />
                      </LabelContainer>
                    </Col>
                    <Col xs={4}>
                      <InputGroup
                        type='Password'
                        placeholder='******'
                        {...getFieldProps('NewPassword', {
                          initialValue: '',
                          rules: [
                            {
                              required: true,
                              message: 'Mật khẩu is required'
                            },
                            {
                              min: 6,
                              message: 'Mật khẩu độ dài ít nhất 6 ký tự'
                            }
                          ]
                        })}
                      />
                      <ErrorContainer>
                        {(getFieldError('NewPassword') || []).join(', ')}
                      </ErrorContainer>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={4}>
                      <LabelContainer style={STYLE.REQUIREDFIELD}>
                      Xác nhận mật khẩu mới
                        <IconRequired />
                      </LabelContainer>
                    </Col>
                    <Col xs={4}>
                      <InputGroup
                        type='Password'
                        placeholder='******'
                        {...getFieldProps('PasswordRepeat', {
                          initialValue: '',
                          rules: [
                            {
                              validator: (rule, val, callback) => {
                                let pass = getFieldValue('NewPassword');
                                if (val !== pass)
                                  callback(new Error('Xác nhận mật khẩu không đúng'));
                                callback();
                              }
                            }
                          ]
                        })}
                      />
                      <ErrorContainer>
                        {(getFieldError('PasswordRepeat') || []).join(', ')}
                      </ErrorContainer>
                    </Col>
                  </Row>
                </div>
              </div>
              <div className='bp3-dialog-footer'>
                <div className='bp3-dialog-footer-actions'>
                  <Button
                    intent={Intent.SUCCESS}
                    onClick={this.submitChangePassword}
                  >
                    Thay đổi
                  </Button>
                </div>
              </div>
            </Dialog>
          </div>
        </HeaderContainer>
        <Navbar
          className='bp3-dark'
          style={{
            backgroundColor: MENU_COLOR,
            boxShadow: 'inset 0px 0px 20px 5px grey',
            display: 'flex',
            alignItems: 'center'
          }}
        >
          <Navbar.Group
            className='bp3-dark'
            align={Alignment.LEFT}
            style={{ backgroundColor: MENU_COLOR, height: 35 }}
          >
            <OverflowList
              items={menus}
              observeParents={true}
              minVisibleItems={5}
              overflowRenderer={items => {
                console.log(items);

                return (
                  <Popover position='bottom' inline usePortal={true}>
                    <Button
                      large='true'
                      // className="bp3-fill"
                      icon='more'
                      // key={`${index}`}
                      style={{ fontSize: 13, flex: 'none' }}
                      minimal='true'
                      // text={`${title}`}
                      // onClick={() => this.handleMenuClick(index, href)}
                    />
                    <Menu>
                      {items.map((item, index) => {
                        return (
                          <MenuItem
                            text={item.title}
                            key={item.href}
                            onClick={() =>
                              this.handleMenuClick(index, item.href)
                            }
                          />
                        );
                      })}
                    </Menu>
                  </Popover>
                );
              }}
              collapseFrom='end'
              visibleItemRenderer={(
                {
                  key,
                  name,
                  totalStation,
                  icon,
                  color,
                  title,
                  href,
                  permissionRole
                },
                index
              ) => {
                let styleActive =
                  //   (this.props.menuStore.menuSelected === index  ||
                  //   startsWith(pathname, href)
                  // )
                  startsWith(pathname, href)
                    ? { background: MENU_ACTIVE_COLOR, color: '#006600' }
                    : { background: MENU_COLOR, color: '#CCCCCC' };
                return (
                  <Permissions key={`per_${index}`} allowed={[permissionRole]}>
                    <Button
                      // large='true'
                      key={`${index}`}
                      style={{
                        ...styleActive,
                        fontSize: 13,
                        flex: 'none',
                        fontWeight: 700
                      }}
                      minimal='true'
                      text={`${title}`}
                      onClick={() => this.handleMenuClick(index, href)}
                    />
                  </Permissions>
                );
              }}
            />

            {/* {menus.map(
              (
                { key, name, totalStation, icon, color, title, href },
                index
              ) => {
                let styleActive =
                  //   (this.props.menuStore.menuSelected === index  ||
                  //   startsWith(pathname, href)
                  // )
                  startsWith(pathname, href)
                    ? { background: '#78e08f' }
                    : { background: '#394b59' };
                return (
                  <Button
                    large="true"
                    key={`${index}`}
                    style={{ ...styleActive, fontSize: 13 }}
                    minimal="true"
                    text={`${title}`}
                    onClick={() => this.handleMenuClick(index, href)}
                  />
                );
              }
            )} */}
          </Navbar.Group>
          {/* <Navbar.Group align={Alignment.RIGHT}>
            <div>Right Content</div>
          </Navbar.Group> */}
        </Navbar>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  menuStore: state.menu,
  info: state.auth.info
});

const mapDispatchToProps = {
  setMenuSelected,
  unLogged
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(createForm()(HeaderComponent))
);
