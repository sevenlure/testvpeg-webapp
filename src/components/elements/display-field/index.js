import React from 'react';
import Proptypes from 'prop-types';
import styled from 'styled-components';
import { Row, Col } from 'reactstrap';
import { FOTNT_SIZE } from 'config';

const ElementWrapper = styled.div`
  .RequiredField {
    color: #660000;
    font-weight: 700;
  }
`;
export default class DisplayFieldLink extends React.PureComponent {
  static propTypes = {
    label: Proptypes.string,
    value: Proptypes.string,
    size: Proptypes.number,
    isRequiredField: Proptypes.bool
  };

  render() {
    return (
      <ElementWrapper>
        <Row>
          <Col
            className={this.props.isRequiredField ? 'RequiredField' : ''}
            xs={3}
          >
            <span
              style={{
                fontSize: this.props.size ? this.props.size : FOTNT_SIZE
              }}
            >
              {this.props.label}
            </span>
          </Col>
          <Col xs={8}>
            <span
              style={{
                fontSize: this.props.size ? this.props.size : FOTNT_SIZE
              }}
            >
              {this.props.value}
            </span>
          </Col>
        </Row>
      </ElementWrapper>
    );
  }
}
