import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { SELECT_ITEM_DEFAULT } from 'config';

const Container = styled.div`
  display: flex;
  align-items: center;
`;

export default class BenTrongNgoaiSelect extends React.Component {
  static propTypes = {
    width: PropTypes.any,
    hideLabel: PropTypes.bool,
    fieldForm: PropTypes.shape({
      form: PropTypes.any,
      name: PropTypes.string,
      option: PropTypes.object
    })
  };

  state = {
    itemList: [
      SELECT_ITEM_DEFAULT,
      {
        value: 'Inside',
        label: 'Bên trong KCN'
      },
      {
        value: 'Outside',
        label: 'Bên ngoài KCN'
      }
    ]
  };

  componentWillMount = () => {};

  render() {
    const { fieldForm } = this.props;
    let getFieldProps = {};
    if (fieldForm) {
      getFieldProps = fieldForm.form.getFieldProps(fieldForm.name, {
        ...fieldForm.option
      });
    }

    const isHidden = {
      style: {
        display: this.props.hideLabel ? 'none' : ''
      }
    };

    return (
      <Container>
        <span {...isHidden}>Bên trong KCN/ngoài KCN</span>
        <Clearfix {...isHidden} width={8} />
        <div style={{ flex: 1 }}>
          <Select
            placeholder="-- Select --"
            style={{
              flex: 1,
            }}
            classNamePrefix="react-select-custom"
            // styles={{
            //   container:(base, state) => ({
            //     ...base,
            //     zIndex: 20,
            //   }),
            // }}
            // menuIsOpen
            options={this.state.itemList}
            {...getFieldProps}
          />
        </div>
      </Container>
    );
  }
}
