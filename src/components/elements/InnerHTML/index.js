import React from 'react';
import Proptypes from 'prop-types';
import styled from 'styled-components';
import { Row, Col } from 'reactstrap';
import { FOTNT_SIZE } from 'config';

const ElementWrapper = styled.div``;
export default class DisplayFieldLink extends React.PureComponent {
  static propTypes = {
    label: Proptypes.string,
    value: Proptypes.string
  };

  render() {
    return (
      <ElementWrapper>
        <Row>
          <Col xs={3}>
            <span
              style={{
                fontSize: this.props.size ? this.props.size : FOTNT_SIZE
              }}
            >
              {this.props.label}
            </span>
          </Col>
          <Col xs={8}>
            <div
              dangerouslySetInnerHTML={{
                __html: this.props.value
              }}
            />
          </Col>
        </Row>
      </ElementWrapper>
    );
  }
}
