import React from 'react';
import styled from 'styled-components';
import { Icon, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

const Container = styled.div`
  color: red;
  font-weight: 500;
  margin: 0px 0px 8px 4px;
  font-size: 70%;
  vertical-align: baseline;
`;

const IconRequired = () => {
  return (
    <Container>
      (<Icon icon={IconNames.ASTERISK} iconSize={8} intent={Intent.DANGER} />)
    </Container>
  );
};

export default IconRequired;
