import React from 'react';
import PropTypes from 'prop-types';
import {
  Intent,
  Dialog,
  Classes,
  Button,
  AnchorButton
} from '@blueprintjs/core';
import styled from 'styled-components';

const ElementWrapper = styled.div`
  .display-linebreak {
    white-space: pre-line;
  }
`;

export default class ConfirmElement extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    content: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      title: this.props.title ? this.props.title : 'Xác nhận',
      content: this.props.content ? this.props.content : 'Content',
      cb: undefined,
      notHide: false
    };
  }

  showConfirm = ({ title, content, cb, notHide } = {}) => {
    console.log(content);
    this.setState({
      title: title ? title : this.state.title,
      content: content ? content : this.state.content,
      isOpen: true,
      cb: cb,
      notHide: notHide ? notHide : false
    });
  };

  closeConfirm = () => {
    this.setState({
      isOpen: false
    });
  };

  handelConfirm = async confirm => {
    if (this.state.cb) await this.state.cb(confirm);
    if (!this.state.notHide) this.closeConfirm();
  };

  render() {
    return (
      <ElementWrapper>
        <Dialog
          //  className={this.props.data.themeName}
          icon="info-sign"
          onClose={this.closeConfirm}
          title={this.state.title ? this.state.title : 'Title'}
          isOpen={this.state.isOpen}
          usePortal={true}
        >
          <div
            style={{ whiteSpace: 'pre-line' }}
            className={`${Classes.DIALOG_BODY}`}
          >
            {this.state.content ? this.state.content : 'Content'}
          </div>
          <div className={Classes.DIALOG_FOOTER}>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              <Button
                onClick={this.closeConfirm}
              >
                Huỷ bỏ
              </Button>
              <AnchorButton
                intent={Intent.SUCCESS}
                onClick={() => {
                  this.handelConfirm(true);
                }}
              >
                Đồng ý
              </AnchorButton>
            </div>
          </div>
        </Dialog>
      </ElementWrapper>
    );
  }
}
