import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import { convertToRaw, ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

const Wrapper = styled.div``;
export default class EditorCustom extends React.PureComponent {
  static propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      editorState: this.props.value
        ? this.convertFromHTML2EditorState(this.props.value)
        : EditorState.createEmpty()
    };
  }

  state = {
    editorState: null
  };

  convertFromHTML2EditorState = rawHtml => {
    const contentBlock = htmlToDraft(rawHtml);
    const contentState = ContentState.createFromBlockArray(
      contentBlock.contentBlocks
    );
    return EditorState.createWithContent(contentState);
  };

  componentDidMount() {

  }

  onChange = editorState => {
    this.setState({
      editorState
    });
    //ban gia tri ra ngoai
    const obj = convertToRaw(editorState.getCurrentContent());
    this.props.onChange(draftToHtml(obj));
  };

  render() {
    return (
      <Wrapper>
        <Editor
          editorState={this.state.editorState}
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor"
          onEditorStateChange={this.onChange}
        />
      </Wrapper>
    );
  }
}
