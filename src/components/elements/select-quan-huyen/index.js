import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { getLookupQuanHuyen } from 'api/lookuplistitemApi';
import { SELECT_ITEM_DEFAULT } from 'config';

const Container = styled.div`
  display: flex;
  align-items: center;
`;

export default class QuanHuyenSelect extends React.Component {
  static propTypes = {
    width: PropTypes.any,
    hideLabel: PropTypes.bool,
    fieldForm: PropTypes.shape({
      form: PropTypes.any,
      name: PropTypes.string,
      option: PropTypes.object
    })
  };

  state = {
    itemList: [SELECT_ITEM_DEFAULT]
  };

  componentWillMount = async () => {
    const res = await getLookupQuanHuyen();
    if (res.success)
      this.setState({
        itemList: [
          SELECT_ITEM_DEFAULT,
          ...res.data.map(item => {
            return { value: item._id, label: item.Name };
          })
        ]
      });
  };

  render() {
    const { fieldForm } = this.props;
    let getFieldProps = {};
    if (fieldForm) {
      getFieldProps = fieldForm.form.getFieldProps(fieldForm.name, {
        ...fieldForm.option
      });
    }

    const isHidden = {
      style: {
        display: this.props.hideLabel ? 'none' : ''
      }
    };

    return (
      <Container>
        <span
        {...isHidden}
        >
          Quận/Huyện
        </span>
        <Clearfix    {...isHidden} width={8} />
        <div style={{ flex: 1 }}>
          <Select
            placeholder="-- Select --"
            style={{
              flex: 1
            }}
            classNamePrefix="react-select-custom"
            options={this.state.itemList}
            {...getFieldProps}
          />
        </div>
      </Container>
    );
  }
}
