import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { SELECT_ITEM_DEFAULT } from 'config';

const Container = styled.div`
  display: flex;
  align-items: center;
`;

export default class CustomDataSelect extends React.Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
    width: PropTypes.any,
    hideLabel: PropTypes.bool,
    isDisabled: PropTypes.bool,
    fieldForm: PropTypes.shape({
      form: PropTypes.any,
      name: PropTypes.string,
      option: PropTypes.object
    })
  };

  // constructor(props){
  //   super(props)
  //   this.state={
  //     itemList: [
  //       SELECT_ITEM_DEFAULT,
  //       ...this.props.data
  //     ]
  //   }
  // }


  render() {
    const { fieldForm } = this.props;
    let getFieldProps = {};
    if (fieldForm) {
      getFieldProps = fieldForm.form.getFieldProps(fieldForm.name, {
        ...fieldForm.option
      });
    }

    const isHidden = {
      style: {
        display: this.props.hideLabel ? 'none' : ''
      }
    };

    return (
      <Container>
        <span
          style={{
            display: this.props.hideLabel ? 'none' : ''
          }}
        >
          custom data
        </span>
        <Clearfix {...isHidden} width={8} />
        <div style={{ flex: 1 }}>
          <Select
            isDisabled={this.props.isDisabled}
            placeholder="-- Select --"
            style={{
              flex: 1
            }}
            // styles={{
            //   container:(base, state) => ({
            //     ...base,
            //     zIndex: 20,
            //   }),
            // }}
            classNamePrefix="react-select-custom"
            options={[
              SELECT_ITEM_DEFAULT,
              ...this.props.data
            ]}
            {...getFieldProps}
          />
        </div>
      </Container>
    );
  }
}
