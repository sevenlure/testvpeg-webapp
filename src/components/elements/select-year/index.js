import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { SELECT_ITEM_DEFAULT, DATA_FROM_YEAR } from 'config';

const Container = styled.div`
  display: flex;
  align-items: center;
`;

export default class YearSelect extends React.Component {
  static propTypes = {
    width: PropTypes.any,
    hideLabel: PropTypes.bool,
    fieldForm: PropTypes.shape({
      form: PropTypes.any,
      name: PropTypes.string,
      option: PropTypes.object
    })
  };

  state = {
    itemList: [SELECT_ITEM_DEFAULT]
  };

  generateYear = () => {
    let loop = new Date().getFullYear() - DATA_FROM_YEAR;
    let arrYear = [];
    for (let i = loop; i >= 0; i--) {
      arrYear.push({
        value: DATA_FROM_YEAR + i,
        label: DATA_FROM_YEAR + i + ''
      });
    }
    return [SELECT_ITEM_DEFAULT, ...arrYear];
  };

  componentWillMount = async () => {
    const data = this.generateYear();
    this.setState({
      itemList: data
    });
  };

  render() {
    const { fieldForm } = this.props;
    let getFieldProps = {};
    if (fieldForm) {
      getFieldProps = fieldForm.form.getFieldProps(fieldForm.name, {
        ...fieldForm.option
      });
    }

    const isHidden = {
      style: {
        display: this.props.hideLabel ? 'none' : ''
      }
    };

    return (
      <Container>
        <span {...isHidden}>Năm</span>
        <Clearfix {...isHidden} width={8} />
        <div style={{ flex: 1 }}>
          <Select
            placeholder="-- Select --"
            style={{
              flex: 1
            }}
            classNamePrefix="react-select-custom"
            options={this.state.itemList}
            {...getFieldProps}
          />
        </div>
      </Container>
    );
  }
}
