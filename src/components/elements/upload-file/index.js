import React from 'react';
import PropTypes from 'prop-types';
import Upload from 'rc-upload';
import { UPLOAD_FILE_API } from 'api/config';
import {connect} from 'react-redux'

@connect(state => ({
  token: state.auth.token
}))
export default class UploadComponent extends React.Component {
  static propTypes = {
    onSuccess: PropTypes.func,
    name: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.uploaderProps = {
      action: UPLOAD_FILE_API,
      // data: { a: 1, b: 2 },
      headers: {
        Authorization: this.props.token
      },
      beforeUpload: file => {},
      onStart: file => {
        this.setState({
          text: 'Uploading.....'
        });
        // this.refs.inner.abort(file);
      },
      onSuccess: res => {
        this.setState({
          text: res.file.filename,
          res: res
        });
        if (this.props.onSuccess) this.props.onSuccess(res);
      },
      onProgress: (step, file) => {
        //console.log('onProgress', Math.round(step.percent), file.name);
      },
      onError: err => {
        // console.log('onError', err);
      }
    };
    this.state = {
      destroyed: false,
      text: this.props.name? this.props.name : 'Choose file...'
    };
  }

  render() {
    return (
      <Upload {...this.uploaderProps} Component="div">
        <label className="bp3-file-input bp3-fill" style={{ marginBottom: 0 }}>
          <span className="bp3-file-upload-input">{this.state.text}</span>
        </label>
      </Upload>
    );
  }
}
