import React from 'react';
import Proptypes from 'prop-types';
import styled from 'styled-components';
import { Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Colors } from '@blueprintjs/core';
import { FOTNT_SIZE } from 'config';

const ElementWrapper = styled.div`
  .RequiredField {
    color: #660000;
    font-weight: 700;
  }
`;
export default class DisplayField extends React.PureComponent {
  static propTypes = {
    label: Proptypes.string,
    value: Proptypes.string,
    size: Proptypes.number,
    isRequiredField: Proptypes.bool,
    linkURL: Proptypes.string.isRequired
  };

  render() {
    return (
      <ElementWrapper>
        <Row>
          <Col
            className={this.props.isRequiredField ? 'RequiredField' : ''}
            xs={3}
          >
            <span
              style={{
                fontSize: this.props.size ? this.props.size : FOTNT_SIZE
              }}
            >
              {this.props.label}
            </span>
          </Col>
          <Col xs={8}>
            <Link to={this.props.linkURL}>
              <span
                style={{
                  fontSize: this.props.size ? this.props.size : FOTNT_SIZE,
                  color: Colors.GREEN2
                }}
              >
                {this.props.value}
              </span>
            </Link>
          </Col>
        </Row>
      </ElementWrapper>
    );
  }
}
