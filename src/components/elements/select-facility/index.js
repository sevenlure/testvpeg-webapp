import React from 'react';
import PropTypes from 'prop-types';
import AsyncSelect from 'react-select/lib/Async';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { getSearchList } from 'api/cosoApi';
import { SELECT_ITEM_DEFAULT } from 'config';

const Container = styled.div`
  display: flex;
  align-items: center;
`;
//Combobox default se lay 10 item, khi search se goi api lay theo fullName
export default class FacilitySelect extends React.Component {
  static propTypes = {
    width: PropTypes.any,
    hideLabel: PropTypes.bool,
    fieldForm: PropTypes.shape({
      form: PropTypes.any,
      name: PropTypes.string,
      option: PropTypes.object
    }),
    cbChangeFacility: PropTypes.func
  };

  state = {
    itemList: [SELECT_ITEM_DEFAULT]
  };

  componentWillMount = async () => {};

  promiseOptions = inputValue =>
    new Promise(resolve => {
      setTimeout(() => {
        resolve(
          getSearchList(inputValue).then(res => {
            return res.data.map(item => {
              return { value: item._id, label: item.FacilityNameFull, obj: item };
            });
          })
        );
      }, 1000);
    });
  render() {
    const { fieldForm } = this.props;
    let getFieldProps = {};
    if (fieldForm) {
      getFieldProps = fieldForm.form.getFieldProps(fieldForm.name, {
        ...fieldForm.option
      });
      let onChange = getFieldProps.onChange
      getFieldProps.onChange = (item)=>{
        if(this.props.cbChangeFacility) this.props.cbChangeFacility(item.obj)
        onChange(item)
      }
    }

    // let onChange2 = (val)=>{
    //   console.log('onChange2',val)
    //   getFieldProps.onChange(val)
    // }

    const isHidden = {
      style: {
        display: this.props.hideLabel ? 'none' : ''
      }
    };

    return (
      <Container>
        <span {...isHidden}>Cơ sở</span>
        <Clearfix {...isHidden} width={8} />
        <div style={{ flex: 1 }}>
          <AsyncSelect
            isDisabled={this.props.isDisabled}
            cacheOptions
            defaultOptions
            loadOptions={this.promiseOptions}
            placeholder="-- Select --"
            style={{
              flex: 1
            }}
            // onChange={(val)=>{
            //   console.log('onChange',val)
            // }}
            classNamePrefix="react-select-custom"
            options={this.state.itemList}
            {...getFieldProps}
            // onChange={onChange2}
          />
        </div>
      </Container>
    );
  }
}
