import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';

const Container = styled.div`
  display: flex;
  align-items: center;
`;

export default class ItemPerPageSelect extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    width: PropTypes.any,
    hideLabel: PropTypes.bool,
    fieldForm: PropTypes.shape({
      form: PropTypes.any,
      name: PropTypes.string,
      option: PropTypes.object
    })
  };

  state = {
    itemList: [
      { value: 10, label: '10' },
      { value: 15, label: '15' },
      { value: 20, label: '20' },
      { value: 50, label: '50' },
      { value: 100, label: '100' }
    ],
    selectedOption: { value: 10, label: '10' }
  };

  render() {
    const { fieldForm } = this.props;
    let getFieldProps = {};
    if (fieldForm) {
      getFieldProps = fieldForm.form.getFieldProps(fieldForm.name, {
        ...fieldForm.option
      });
    }

    const isHidden = {
      style: {
        display: this.props.hideLabel ? 'none' : ''
      }
    };

    return (
      <Container>
        <span {...isHidden}>Số mục trên trang</span>
        <Clearfix {...isHidden} width={8} />
        <div style={{ flex: 1 }}>
          <Select
            placeholder="-- Select --"
            style={{
              flex: 1
            }}
            options={this.state.itemList}
            value={this.state.selectedOption}
            onChange={selectedOption => {
              this.setState({ selectedOption });
              if (this.props.onChange) this.props.onChange(selectedOption);
            }}
            classNamePrefix="react-select-custom"
            {...getFieldProps}
          />
        </div>
      </Container>
    );
  }
}
