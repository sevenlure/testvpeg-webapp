import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { getLookupQuanHuyen } from 'api/lookuplistitemApi';
import { SELECT_ITEM_DEFAULT } from 'config';
// import * as _ from 'lodash';

const Container = styled.div`
  display: flex;
  align-items: center;
`;

export default class PhuongXaSelect extends React.Component {
  static propTypes = {
    width: PropTypes.any,
    hideLabel: PropTypes.bool,
    quanHuyenTarget: PropTypes.string,
    fieldForm: PropTypes.shape({
      form: PropTypes.any,
      name: PropTypes.string,
      option: PropTypes.object
    })
  };

  constructor(props){
    super(props)
    this.state = {
      itemList: [SELECT_ITEM_DEFAULT],
      quanHuyenList: []
    };

    // this.phuongXaList()
  }

  

  phuongXaList = () => {
    const data = [].concat.apply(
      [],
      _.map(
        this.state.quanHuyenList.filter(item => {
          if (this.props.quanHuyenTarget)
            return item._id === this.props.quanHuyenTarget;
          else return true;
        }),
        'children'
      )
    );
    return [
      SELECT_ITEM_DEFAULT,
      ...data.map(item => {
        return { value: item.name, label: item.name };
      })
    ];
  };

  componentWillMount = async () => {
    const res = await getLookupQuanHuyen();
    if (res.success) {
      const phuongXaData = [].concat.apply([], _.map(res.data, 'children'));
      this.setState({
        quanHuyenList: res.data,
        itemList: [
          SELECT_ITEM_DEFAULT,
          ...phuongXaData.map(item => {
            return { value: item.name, label: item.name };
          })
        ]
      });
    }
  };

  render() {
    const { fieldForm } = this.props;
    let getFieldProps = {};
    if (fieldForm) {
      getFieldProps = fieldForm.form.getFieldProps(fieldForm.name, {
        ...fieldForm.option
      });
    }

    const isHidden = {
      style: {
        display: this.props.hideLabel ? 'none' : ''
      }
    };

    return (
      <Container>
        <span {...isHidden}>Quận/Huyện</span>
        <Clearfix {...isHidden} width={8} />
        <div style={{ flex: 1 }}>
          <Select
            placeholder="-- Select --"
            style={{
              flex: 1
            }}
            classNamePrefix="react-select-custom"
            options={this.phuongXaList()}
            {...getFieldProps}
          />
        </div>
      </Container>
    );
  }
}
