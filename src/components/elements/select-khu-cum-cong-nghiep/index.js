import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import styled from 'styled-components';
import Clearfix from 'components/elements/clearfix';
import { SELECT_ITEM_DEFAULT } from 'config';
import { getLookupKhuCumCN } from 'api/lookuplistitemApi';
import { Switch } from '@blueprintjs/core';

const Container = styled.div`
  display: flex;
  align-items: center;
`;

export default class KhuCumCongNghiepSelect extends React.Component {
  static propTypes = {
    width: PropTypes.any,
    hideLabel: PropTypes.bool,
    isKhuCongNghiep: PropTypes.bool,
    fieldForm: PropTypes.shape({
      form: PropTypes.any,
      name: PropTypes.string,
      option: PropTypes.object
    })
  };


  constructor(props){
    super(props)
    this.state = {
      itemList: [SELECT_ITEM_DEFAULT],
      isKhuCongNghiep: (this.props.isKhuCongNghiep === false)? false: true,
      dataSource: []
    };
  }

  componentWillMount = async () => {
    const res = await getLookupKhuCumCN();
    if (res.success)
      this.setState({
        dataSource: [
          SELECT_ITEM_DEFAULT,
          ...res.data.map(item => {
            return {
              value: item._id,
              label: item.Name,
              isKhuCongNghiep: item.isKhuCongNghiep
                ? item.isKhuCongNghiep
                : false
            };
          })
        ]
      });
  };

  render() {
    const { fieldForm } = this.props;
    let getFieldProps = {};
    if (fieldForm) {
      getFieldProps = fieldForm.form.getFieldProps(fieldForm.name, {
        ...fieldForm.option
      });
    }

    const isHidden = {
      style: {
        display: this.props.hideLabel ? 'none' : ''
      }
    };

    return (
      <Container>
        <span {...isHidden}>
          {this.state.isKhuCongNghiep ? 'Khu Công nghiệp' : 'Cụm Công nghiệp'}
        </span>
        <Clearfix {...isHidden} width={8} />
        <Switch
          style={{
            marginTop: 'auto'
          }}
          checked={this.state.isKhuCongNghiep}
          onChange={e => {
            this.setState(
              {
                isKhuCongNghiep: e.target.checked
              },
              () => {
                if (this.props.onChangeIsKhuCongNghiep)
                  this.props.onChangeIsKhuCongNghiep(
                    this.state.isKhuCongNghiep
                  );
              }
            );
          }}
        />
        <div style={{ flex: 1 }}>
          <Select
            placeholder='-- Select --'
            style={{
              flex: 1
            }}
            classNamePrefix='react-select-custom'
            options={[
              SELECT_ITEM_DEFAULT,
              ...this.state.dataSource.filter(item => {
                return item.isKhuCongNghiep === this.state.isKhuCongNghiep;
              })
            ]}
            {...getFieldProps}
          />
        </div>
      </Container>
    );
  }
}
