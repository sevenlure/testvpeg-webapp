import React from 'react';

export default class PageLock extends React.Component {
  //  constructor(props){
  //    super(props)
  //    this.Title = 'Bạn không đuợc phép truy cập trang này'
  //    if(props.Title)
  //    this.Title = props.Title
  //  }
  

  render() {
    return (
      <div className="bp3-non-ideal-state" style={{height: '50vh'}}>
      <div className="bp3-non-ideal-state-visual" style={{fontSize: 150}}>
        <span className="bp3-icon bp3-icon-lock"></span>
      </div>
      <h1 className="bp3-heading" style={{maxWidth: 700}}>
      {this.props.TitleMessage? this.props.TitleMessage: 'Bạn không đuợc phép truy cập trang này'}
      </h1>
    </div>
    );
  }
}
