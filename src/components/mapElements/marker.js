import React from 'react';
import PropTypes from 'prop-types';
import {
  Marker,
  InfoWindow,
  withScriptjs,
  withGoogleMap
} from 'react-google-maps';
import { compose, withProps } from 'recompose';

const {
  MarkerWithLabel
} = require('react-google-maps/lib/components/addons/MarkerWithLabel');

class MarkerCustom extends React.Component {
  static propTypes = {
    position: PropTypes.object.isRequired,
    labelName: PropTypes.string.isRequired
  };

  state = {
    isOpen: false
  };

  handelClick = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  render() {
    return (
      <MarkerWithLabel
        position={this.props.position}
        onClick={this.handelClick}
      >
        {this.state.isOpen && (
          <InfoWindow
            onCloseClick={() => {
              this.setState({ isOpen: false });
            }}
          >
            name
          </InfoWindow>
        )}
      </MarkerWithLabel>
    );
  }
}

export default compose(
  withScriptjs,
  withGoogleMap
)(MarkerCustom);
