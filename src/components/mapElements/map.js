import React from 'react';
import {  Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { API_KEY_GOOGLE_MAP } from 'config';
import { compose, withProps } from 'recompose';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  InfoWindow
} from 'react-google-maps';
import styled from 'styled-components';
import slug from 'constants/slug';
import BootstrapTable from 'react-bootstrap-table-next';

const {
  MarkerWithLabel
} = require('react-google-maps/lib/components/addons/MarkerWithLabel');

const SpinnerContainer = styled.div`
  margin: 100px auto;
  //width: 50px;
  height: 40px;
  text-align: center;
  font-size: 10px;
`;

/* global google */

class Map extends React.PureComponent {
  static proptypes = {
    markers: PropTypes.array
  };

  markers = [];
  state = {
    isLoaded: false
  };

  async componentDidMount() {}

  render() {
    return (
      <GoogleMap
        onTilesLoaded={() => {
          if (!this.state.isLoaded)
            this.setState({
              isLoaded: true
            });
        }}
        defaultMapTypeId="hybrid"
        defaultZoom={10}
        defaultCenter={
          this.props.mapLocation
            ? this.props.mapLocation
            : { lat: 11.1073184, lng: 106.6822954 }
        }
        onZoomChanged={() => {
          this.markers.map(marker => {
            if (marker.state.isOpen) marker.closeInfo();
            return null;
          });
        }}
      >
        {this.state.isLoaded &&
          this.props.markers &&
          this.props.markers.map(marker => {
            const position =
              marker.lat && marker.lng
                ? { lat: marker.lat, lng: marker.lng }
                : { lat: 11.1073184, lng: 106.6822954 };
            return (
              <MarkerCustom
                ref={marker => {
                  if (marker) this.markers.push(marker);
                }}
                key={marker._id}
                position={position}
                labelName={marker.name}
                facilityId={marker._id}
                trigger={this.state.trigger}
                data={marker.data}
              />
            );
          })}
      </GoogleMap>
    );
  }
}

export default compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${API_KEY_GOOGLE_MAP}&v=3.exp`,
    loadingElement: (
      <SpinnerContainer style={{ height: '100%' }}>
        <svg
          className="bp3-spinner"
          height="100"
          width="100"
          viewBox="0 0 100 100"
          strokeWidth="4"
        >
          <path
            className="bp3-spinner-track"
            d="M 50,50 m 0,-44.5 a 44.5,44.5 0 1 1 0,89 a 44.5,44.5 0 1 1 0,-89"
          />
          <path
            className="bp3-spinner-head"
            d="M 50,50 m 0,-44.5 a 44.5,44.5 0 1 1 0,89 a 44.5,44.5 0 1 1 0,-89"
            pathLength="280"
            strokeDasharray="280 280"
            strokeDashoffset="210"
          />
        </svg>
      </SpinnerContainer>
    ),
    containerElement: <div style={{ height: `85vh` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  withScriptjs,
  withGoogleMap
)(Map);

class MarkerCustom extends React.PureComponent {
  static propTypes = {
    position: PropTypes.object.isRequired,
    labelName: PropTypes.string.isRequired,
    facilityId: PropTypes.string.isRequired,
    trigger: PropTypes.bool,
    data: PropTypes.array
  };

  state = {
    isOpen: false,
    data: null
  };

  componentWillMount() {
    console.log(this.props.data);
  }

  handelClick = () => {
    this.setState({
      isOpen: true
    });
  };

  closeInfo() {
    this.setState({
      isOpen: false
    });
  }

  getTextWidth() {
    // re-use canvas object for better performance
    var canvas =
      this.getTextWidth.canvas ||
      (this.getTextWidth.canvas = document.createElement('canvas'));
    var context = canvas.getContext('2d');
    context.font = '300 12pt arial';
    var metrics = context.measureText(this.props.labelName);
    return metrics.width;
  }

  render() {
    return (
      <div>
        <MarkerWithLabel
          noRedraw
          position={this.props.position}
          onClick={this.handelClick}
          labelAnchor={new google.maps.Point(this.getTextWidth() / 2, -5)}
          labelClass="bp3-tag bp3-intent-success"
          labelStyle={{
            // backgroundColor: 'white',
            fontSize: '12px',
            font: '300 12pt arial',
            //padding: '16px',
            // fontColor: `#08233B`,
          }}
        >
          <div>
            {this.props.labelName ? this.props.labelName : ''}
            {this.state.isOpen && (
              <InfoWindow
                options={{
                  pixelOffset: new google.maps.Size(0, -45),
                  maxWidth: 300,
                }}
                position={this.props.position}
                onCloseClick={() => {
                  this.setState({ isOpen: false });
                }}
              >
                <div className="mapInfowindow" style={{width: 300}}>
                  <div className="title">
                    <Link
                      style={{ color: '#37B44C' }}
                      to={slug.CO_SO.VIEW_WITH_ID + this.props.facilityId}
                    >
                      {this.props.labelName}
                    </Link>
                  </div>
                  <b>Info:</b> ...
                  <div className="smalldiv">
                    <b>Info:</b> ....
                  </div>
                  <BootstrapTable
                    keyField="MaThongSo"
                    data={this.props.data}
                    striped
                    hover
                    condensed
                    columns={[
                      {
                        dataField: 'MaThongSo',
                        text: 'Thông số',
                      },
                      {
                        dataField: 'Value',
                        text: 'Giá trị',
                      },
                      {
                        dataField: 'DonVi',
                        text: 'Đơn vị',
                      },
                    ]}
                  />
                </div>
              </InfoWindow>
            )}
          </div>
        </MarkerWithLabel>
      </div>
    );
  }
}
