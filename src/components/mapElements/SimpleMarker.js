import React from 'react';
import { Motion, spring } from 'react-motion';
import { delay } from 'core-js';
import { Link } from 'react-router-dom';
import slug from 'constants/slug.js';


const markerWidth = 49;
const markerHeight = 64;

const originX = markerWidth * 0.5;
const originY = markerHeight * 0.5;

const styleMarker = {
  backgroundImage: `url('/mapIcon.svg')`,
  position: 'absolute',
  cursor: 'pointer',
  width: markerWidth,
  height: markerHeight,
  top: -originY,
  left: -originX,
  transformOrigin: `${originX}px ${originY}px`,
  margin: 0,
  padding: 0,
  zIndex: 10
};

const textMarker = {
  // backgroundImage: `url('/mapIcon.svg')`,
  // position: 'absolute',
  cursor: 'pointer',
  width: markerWidth,
  // height: markerHeight,
  top: markerHeight/8 *3,
  left: originX,
  transformOrigin: `${originX} ${originY}`,
  // margin: 0,
  // padding: 0,
  maxWidth: 'inherit',
  zIndex: 10

  // position: 'fixed'
};

const defaultScale = 0.6;
const initialScale = 0.3;
const hoveredScale = 0.8;
const springConfig = {
  stiffness: 320,
  damping: 7,
  precision: 0.001,
  val: 0.6
};

export class Marker extends React.Component {
  state = {
    isHover: false,
    isClicked: false,
    motionStyle: {
      scale: spring(initialScale, springConfig)
    }
  };

  componentWillMount() {
    this.setState({
      isClicked: false
    });
  }

  getTextWidth = () => {
    // re-use canvas object for better performance
    var canvas =
      this.getTextWidth.canvas ||
      (this.getTextWidth.canvas = document.createElement('canvas'));
    var context = canvas.getContext('2d');
    context.font = '300 12pt arial';
    var metrics = context.measureText(this.props.name);
    return metrics.width;
  };

  async componentDidMount() {
    await delay(100);
    this.setState(
      {
        motionStyle: {
          scale: spring(defaultScale, springConfig)
        }
      }
      // async () => {
      //   await delay(500);
      //   this.setState({
      //     motionStyle: {
      //       scale: spring(defaultScale, springConfig)
      //     }
      //   });
      // }
    );
  }

  render() {
    let calWidth = this.getTextWidth();
    calWidth = calWidth <= 300 ? calWidth : 300;
    return (
      <React.Fragment>
        <Motion
          defaultStyle={{ scale: defaultScale }}
          style={this.state.motionStyle}
        >
          {/* sử dụng Motion, bên trong Motion luon luon la function tra ve element */}
          {({ scale }) => {
            return (
              <div
                onClick={() => {
                  this.setState({
                    isClicked: !this.state.isClicked
                  });
                }}
                onMouseEnter={() => {
                  this.setState({
                    isHover: true,
                    motionStyle: {
                      scale: spring(hoveredScale, springConfig)
                    }
                  });
                }}
                onMouseLeave={() => {
                  this.setState({
                    isHover: false,
                    motionStyle: {
                      scale: spring(defaultScale, springConfig)
                    }
                  });
                }}
                style={{
                  ...styleMarker,
                  transform: `translate3D(0,0,0) scale(${scale}, ${scale})`
                }}
              />
            );
          }}
        </Motion>
        <div
          className="bp3-tag bp3-intent-success bp3-text-overflow-ellipsis"
          style={{
            ...textMarker,
            left: -calWidth / 2,
            width: calWidth + 20,
            fontSize: '12px',
            font: '300 12pt arial'
          }}
        >
         <div
         className="bp3-text-overflow-ellipsis"
         style={{
           width: calWidth
         }}
         >
         {this.props.name}
         </div>
        </div>
        {this.state.isClicked && (
          <div
            className="bp3-popover bp3-popover-content-sizing"
            style={{
              position: 'absolute',
              // top: -175,
              left: -140,
              minWidth: 300,
              bottom: 30,
              zIndex: 30
            }}
          >
            <div
              className="bp3-popover-arrow"
              style={{
                left: 126,
                bottom: -10
                // top: 102

              }}
            >
              <svg
                viewBox="0 0 30 30"
                style={{
                  transform: `rotate(-90deg)`
                }}
              >
                <path
                  className="bp3-popover-arrow-border"
                  d="M8.11 6.302c1.015-.936 1.887-2.922 1.887-4.297v26c0-1.378-.868-3.357-1.888-4.297L.925 17.09c-1.237-1.14-1.233-3.034 0-4.17L8.11 6.302z"
                />
                <path
                  className="bp3-popover-arrow-fill"
                  d="M8.787 7.036c1.22-1.125 2.21-3.376 2.21-5.03V0v30-2.005c0-1.654-.983-3.9-2.21-5.03l-7.183-6.616c-.81-.746-.802-1.96 0-2.7l7.183-6.614z"
                />
              </svg>
            </div>
            <div className="bp3-popover-content">
              <div className="mapInfowindow">
                <div className="title" style={{ height: 45 }}>
                  <Link
                    style={{ color: '#37B44C' }}
                    to={slug.CO_SO.VIEW_WITH_ID + this.props.facilityId}
                  >
                    {this.props.name}
                  </Link>
                </div>
                {/* <b>Info:</b> ...
                <div className="smalldiv">
                  <b>Info:</b> ....
                </div> */}
              </div>
            </div>
          </div>
        )}
            {/* <div>Markerfrrrr</div> */}
      </React.Fragment>
    );
  }
}
export default Marker;
