import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';
import {API_KEY_GOOGLE_MAP} from 'config'
import url from 'url'


export default class StaticMapImg extends React.Component {
  constructor(props){
    super(props)
    // url
    //  const myURL = new URL('https://maps.googleapis.com/maps/api/staticmap');
    // // myURL.searchParams.
    // myURL.search.q

    var parts = url.parse("https://maps.googleapis.com/maps/api/staticmap", true);
    parts.query = {
      center:  '11.1073184,106.6822954',
      maptype: 'hybrid',
      size: '600x400',
      // markers: 
      markers: [
        `icon:https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2_hdpi.png|11.1073184,106.6822954`
      ],
      key: API_KEY_GOOGLE_MAP,
      zoom: 13,
    }
    this.href = url.format(parts)
    console.log('parts',)
  }
  render() {
    return (
      <img src={this.href} alt="mapLocation" />
    )
  }
}