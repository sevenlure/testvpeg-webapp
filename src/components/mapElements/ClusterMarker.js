import React from 'react';
import { Motion, spring } from 'react-motion';
import { delay } from 'core-js';

const markerWidth = 60;
const markerHeight = 60;
const markerBorderWidth = 5;
const markerFontSize = 14;


const styleMarker = {
  // backgroundImage: `url('/mapIcon.svg')`,
  position: 'absolute',
  cursor: 'pointer',
  width: markerWidth,
  height: markerHeight,
  top: -markerWidth / 2,
  left: -markerHeight / 2,
  border: `${markerBorderWidth}px solid #004336`,
  borderRadius: '50%',
  backgroundColor: 'white',
  textAlign: 'center',
  color: '#333',
  fontSize: markerFontSize,
  fontWeight: 'bold',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
};

const defaultScale = 1;
const initialScale = 0.6;
const hoveredScale = 1.15;
const springConfig = {
  stiffness: 320,
  damping: 7,
  precision: 0.001,
  val: 0.6
};

export default class ClusterMarker extends React.Component {
  state = {
    isHover: false,
    isClicked: false,
    motionStyle: {
      scale: spring(initialScale, springConfig)
    }
  };

  async componentDidMount() {
    await delay(100);
    this.setState({
      motionStyle: {
        scale: spring(defaultScale, springConfig)
      }
    });
  }

  render() {
    return (
      <React.Fragment>
        <Motion
        defaultStyle={{ scale: defaultScale }}
        style={this.state.motionStyle}
      >
        {({ scale }) => (
          <div
            style={{
              ...styleMarker,
              zIndex: 20,
              transform: `translate3D(0,0,0) scale(${scale}, ${scale})`
            }}
            onMouseEnter={() => {
              this.setState({
                isHover: true,
                motionStyle: {
                  scale: spring(hoveredScale, springConfig)
                }
              });
            }}
            onMouseLeave={() => {
              this.setState({
                isHover: false,
                motionStyle: {
                  scale: spring(defaultScale, springConfig)
                }
              });
            }}
          >
            <div
            // className={styles.text}
            >
              {this.props.text}
            </div>
          </div>
        )}
      </Motion>
      {/* <div>
        Clusterrrrr
      </div> */}
      </React.Fragment>
    );
  }
}
