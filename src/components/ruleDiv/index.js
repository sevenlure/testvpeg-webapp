import React from 'react';
import { connect } from 'react-redux';

// @DynamicRoleCompNull()
@connect(state => ({
  info: state.auth.info
}))
export default class IconCustom extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: this.check()
    };
  }

  componentWillReceiveProps(nextProps){
    if(this.state.isVisible === false){
      let isVisible = this.checkNextProps(nextProps)
      if(isVisible) this.setState({
        isVisible
      })
    }
  }

  checkNextProps(nextProps) {
    let roleKey = _.result(nextProps.info, 'role.Key');
    let districtId = _.result(nextProps.info, 'district._id');
    let facilityDistrictId = _.result(
      nextProps.initialValue,
      'facility.district._id'
    );
    if (!facilityDistrictId)
    facilityDistrictId = _.result(
      nextProps.initialValue,
      'district._id'
    );

    let isVisible = true
    if (roleKey === 'DISTRICT_USER') {
      if (facilityDistrictId !== districtId) {
        isVisible = false
      }
    }

    return isVisible
  }

  check() {
    let roleKey = _.result(this.props.info, 'role.Key');
    let districtId = _.result(this.props.info, 'district._id');
    let facilityDistrictId = _.result(
      this.props.initialValue,
      'facility.district._id'
    );
    if (!facilityDistrictId)
    facilityDistrictId = _.result(
      this.props.initialValue,
      'district._id'
    );

    let isVisible = true
    if (roleKey === 'DISTRICT_USER') {
      if (facilityDistrictId !== districtId) {
        isVisible = false
      }
    }

    console.log('check:', isVisible,facilityDistrictId, districtId)

    return isVisible
  }

  render() {
    if(this.state.isVisible) return this.props.children
    else return null
  }
}
