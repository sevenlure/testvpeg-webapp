export const HOST = 'http://localhost:3005/';
export const HOST_ATTACHMENT_PUBLIC = HOST + 'FileDatabase/';
export const SELECT_ITEM_DEFAULT = { value: null, label: '-- Select --' };
export const DATE_FORMAT = 'DD/MM/YYYY';
export const FOTNT_SIZE = 14;
export const DATETIME_FORMAT = 'DD/MM/YYYY hh:mm';
export const API_KEY_GOOGLE_MAP = 'AIzaSyB8Lw-LWcdPxtz01j99UE44V9QUFw9vEO4';
export const DATA_FROM_YEAR = 2000;
export const DEFAULT_NULL_VALUE = 'Chưa xác định';
export const AGGREGATORS = ['Count', 'Sum', 'Average', 'Minimum', 'Maximum'];

export const COLOR = {
  SUCCESS: '#0D8050',
  REQUIREDFIELD: '#660000'
};

export const STYLE = {
  REQUIREDFIELD: {
    color: COLOR.REQUIREDFIELD,
    fontWeight: 700
  }
};
