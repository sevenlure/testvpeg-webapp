import React from 'react';
import PageLock from 'components/pageLock';
import { connect } from 'react-redux';
import styled from 'styled-components';

const CompNull = styled.div``;

const DynamicRole = () => WrappedComponent => {
  return connect(state => ({
    info: state.auth.info
  }))(
    class extends React.Component {
      constructor(props) {
        super(props);

        let roleKey = _.result(this.props.info, 'role.Key');
        let districtId = _.result(this.props.info, 'district._id');
        let isEdit = _.result(this.props, 'isEdit', false);
        let facilityDistrictId = _.result(
          this.props.initialValue,
          'facility.district._id'
        );
        if (!facilityDistrictId)
          facilityDistrictId = _.result(
            this.props.initialValue,
            'district._id'
          );

        this.TargetComponent = WrappedComponent;
        if (!isEdit) {
          return;
        }
        if (roleKey === 'DISTRICT_USER') {
          if (facilityDistrictId !== districtId)
            this.TargetComponent = PageLock;
          return;
        }
      }

      componentDidMount(){
      }
    
      shouldComponentUpdate(nextProps, nextState){
        return true
      } 

      render() {
        return (
          <this.TargetComponent
            {...this.props}
            TitleMessage='Bạn không đuợc phép chỉnh sửa nội dung này'
          />
        );
      }
    }
  );
};

export default DynamicRole;

export const DynamicRoleCompNull = () => WrappedComponent => {
  return connect(state => ({
    info: state.auth.info
  }))(
    class extends React.Component {
      constructor(props) {
        super(props);
        let roleKey = _.result(this.props.info, 'role.Key');
        let districtId = _.result(this.props.info, 'district._id');
        // let isEdit = _.result(this.props, 'isEdit', false);
        let facilityDistrictId = _.result(
          this.props.initialValue,
          'facility.district._id'
        );

        this.TargetComponent = WrappedComponent;
        if (roleKey === 'DISTRICT_USER') {
          if (facilityDistrictId !== districtId)
            this.TargetComponent = CompNull;
          return;
        }
      }

      render() {
        return <this.TargetComponent {...this.props} />;
      }
    }
  );
};
