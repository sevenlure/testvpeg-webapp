import React from 'react';
import { HOC as Permissions } from 'react-redux-permissions';


const pageNullkHOC = (allowed, disallowed) => Component =>
  Permissions(allowed, disallowed)(Component, <div/>);

export default pageNullkHOC;
