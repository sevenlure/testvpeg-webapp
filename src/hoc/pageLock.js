import React from 'react';
import { HOC as Permissions } from 'react-redux-permissions';
import PageLock from 'components/pageLock';

const pageLockHOC = (allowed, disallowed) => Component =>
  Permissions(allowed, disallowed)(Component, <PageLock />);

export default pageLockHOC;
