module.exports = {
  apps : [{
    "name": "environment-BD-WEB",
    "script": "serve -s -p 9002  build",
    "env": {
      "NODE_ENV": "development",
      "NODE_PATH": "./src"
    },
    "env_production": {
      "NODE_ENV": "production",
      "NODE_PATH": "./dist"
    }
  }],

  // deploy : {
  //   production : {
  //     user : 'node',
  //     host : 'localhost',
  //     ref  : 'origin/master',
  //     repo : 'git@github.com:repo.git',cr
  //     path : '/var/www/production',
  //     'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
  //   }
  // }
};
